import java.util.Arrays;

public class Func {


    /**
     * This method to check and transform from all numerals system to decimal
     * */
    public static int error = 0;
    public static int Value_1 =0;
    public static int Value_2 =0;
    public static boolean  check = true;
    public static int First_Num[] = new int[2000];
    public static int Second_Num[] = new int[2000];
    public static int  result[]=new int [2000];

    public static void InToBin(String NUM_1 ,String NUM_2 , int NS_1 ) {

        String digits = "0123456789ABCDEF";
        String num_1 = NUM_1.toUpperCase();
        String num_2 = NUM_2.toUpperCase();

            for (int i = 0; i < num_1.length(); i++){
                    char c = num_1.charAt(i); int d = digits.indexOf(c);
                    if(d > NS_1-1 || c > 'F' ){ check = false;error = 1;break; }
                    else{  Value_1 = NS_1*Value_1 + d; }
            }
            for(int i = 0 ; i < num_2.length();i++){
                    char c_2 = num_2.charAt(i); int d_2 = digits.indexOf(c_2);
                    if(d_2 > NS_1-1 || c_2 > 'F' ){  check = false;error = 1;break; }
                    else{  Value_2 = NS_1*Value_2 + d_2;  }
            }

        if (check==false){
            System.out.println("!!Error!! The number you entered is incorrect  ");
        }

    }

    /**
     * This method to transform from decimal system to all numerals systems.
     * */


    public static int  counter_1 = 0; public static int counter_2 = 0;
    public static int max =0; public static int min = 0;

    public static void  OutFromDec() {
        if (error == 1) {
            System.out.println("!!Error!! The number you entered is incorrect  ");
        }
        if(check == true){
                while (Value_1 > 0) { First_Num[counter_1++] = Value_1 % 2; Value_1 = Value_1 / 2; }
                while (Value_2 > 0) { Second_Num[counter_2++] = Value_2 % 2; Value_2 = Value_2 / 2; }
        }
        if( counter_1 < counter_2){  max = counter_2;}
        else if (counter_2 < counter_1){ max = counter_1; }
        else{ max = counter_1;}
    }

    /**
     * This method to find the sum of inputted numbers
     * */


    public static void  sum(){

        int sum = 0;  int remainder = 0;

        for (int i = 0; i <= max; i++) {

            sum = First_Num[i] + Second_Num[i] + remainder;
            if (sum == 1) { result[i] = 1; remainder = 0; }
            else if (sum == 2) { result[i] = 0; remainder = 1; }
            else if (sum == 3) { result[i] = 1; remainder = 1; }
            else if (sum == 0) { result[i] = 0; remainder = 0; }
            else if (remainder == 1) {   result[i] = 1;  }

        }
        System.out.print("[ result ]->> [");
        for(int i = max ; i >=0 ; i--){ System.out.print(result[i]+""); }
        System.out.print("]");
    }

    /**
     * This method to find the subtract of inputted numbers
     * */


    public static void subtract(){

        int minus = 0;
        int remainder = 0;int remainder_1 = 0;int cast =0;
        int complement []=new int [2000]; int complement_1 []=new int [2000];
        int comp []=new int [2000];


        for(int i = 0; i <= max-1; i++){

            if(Second_Num[i]==1){ Second_Num[i]=0; }
            else{ Second_Num[i]=1; }
            if(i==0){ comp[i]=1; }else{comp[i]=0;}
        }
        for (int i = 0; i <= max; i++) {

            minus = Second_Num[i] + comp[i] + remainder;
            if (minus == 1) { complement[i] = 1; remainder = 0;  }
            else if (minus == 2) { complement[i] = 0; remainder = 1;  }
            else if (minus == 3) {  complement[i] = 1; remainder = 1;  }
            else if (minus == 0) {  complement[i] = 0; remainder = 0;  }
            else if (remainder == 1) { complement[i] = 1; }

        }
        for (int i = 0; i <= max; i++) {

            minus = First_Num[i] + complement[i] + remainder_1;
            if (minus == 1) { result[i] = 1; remainder_1 = 0;  }
            else if (minus == 2) { result[i] = 0; remainder_1 = 1;  }
            else if (minus == 3) {  result[i] = 1; remainder_1 = 1;  }
            else if (minus == 0) {  result[i] = 0; remainder_1 = 0;  }
        }


        if(result[max] == 1){ result[max]=0;
            System.out.print("[ result ]->> [");
            for(int i = max ; i >=0 ; i--){ System.out.print(result[i]+""); }
            System.out.print("]");
        }

        /**
         * This condition to fix negative result
         * */
        else {

            for(int i = 0; i <= max; i++){
                if(result[i]==1){ result[i]=0; }
                else{  result[i]=1;  }
                if(i==0){ comp[i]=1; }else{comp[i]=0;}
            }

            for (int i = 0; i <= max; i++) {

                minus = result[i] + comp[i] + remainder;
                if (minus == 1) { complement_1[i] = 1; remainder = 0; }
                else if (minus == 2) { complement_1[i] = 0; remainder = 1;  }
                else if (minus == 3) { complement_1[i] = 1; remainder = 1;  }
                else if (minus == 0) { complement_1[i] = 0; remainder = 0;  }
                else if (remainder == 1) {  complement_1[i] = 1; }

            }
            complement_1[max]=0;
            System.out.print("[ result ]->> [-");
            for(int i = max ; i >=0 ; i--){ System.out.print(complement_1[i]+""); }
            System.out.print("]");
        }
    }

    /**
     * This method to find the multiples of inputted numbers
     * */



    public static void multiply(){
        String num_1 ="";
        String num_2="";

        for(int i = 0; i < max; i++){
            if(First_Num[i]==0){
                num_1="0"+num_1;
            }else {
                num_1="1"+num_1;
            }
            if(Second_Num[i]==0){
                num_2="0"+num_2;
            }else {
                num_2="1"+num_2;
            }
        }

        int decimal=(Integer.parseInt(num_1,2))*(Integer.parseInt(num_2,2));
        String result=Integer.toBinaryString(decimal);
        result=result.replaceAll("....", "$0 ");
        System.out.println("[ result ]->> ["+result+"]");

    }

    public static void divide(){

        String num_1 ="";
        String num_2="";

        for(int i = 0; i < max; i++){
            if(First_Num[i]==0){
                num_1="0"+num_1;
            }else {
                num_1="1"+num_1;
            }
            if(Second_Num[i]==0){
                num_2="0"+num_2;
            }else {
                num_2="1"+num_2;
            }


        }

        int decimal;
        if(Integer.parseInt(num_2,2) == 0) {							// check if user enter 0
            System.out.println("You can not divide by 0!");
        }
        else {
            decimal=(Integer.parseInt(num_1,2))/(Integer.parseInt(num_2,2));
            String result=Integer.toBinaryString(decimal);
            result=result.replaceAll("....", "$0 ");
            System.out.println("[ result ]->> ["+result+"]");
        }

    }
    public static void clear (){

        for(int i = 0 ; i <=max ; i++){
            First_Num[i] *=0; Second_Num[i] *=0; result[i] *=0;
        }
        counter_1 *=0; counter_2 *=0; max *=0;

    }

}


public class Func {


    /**
     * This method to check and transform from all numerals system to decimal
     * */
    public static int error = 0;
    public static int InToDec(String NUM , int NS_1 ) {

        String digits = "0123456789ABCDEF";
        String num = NUM.toUpperCase();
        int Value = 0;
        boolean check = true;

        for (int i = 0; i < num.length(); i++){
            char c = num.charAt(i); int d = digits.indexOf(c);
            if(d > NS_1-1 || c > 'F' ){
                check = false;error = 1;

                break;
            }
        }

        if (check==true){
            for (int i = 0; i < num.length(); i++){
                char c = num.charAt(i);
                int d = digits.indexOf(c);
                Value = NS_1*Value + d;
            }
        }
        return Value;
    }

    /**
     * This method to transform from decimal system to all numerals systems.
     * */
    public static void OutFromDec(int digit1, int NS_2 ) {
        if(error == 1){
            System.out.println("!!Error!! The number you entered is incorrect  ");
        }

        char chars[]= new char [2000];
        int store [] = new int [2000];
        int counter = 0;
        String str =" ";

        int val = 0;
        while (digit1 > 0){
            store[counter++] = digit1 % NS_2;
            digit1=digit1/NS_2;
        }

        for(int i = counter - 1 ; i >=0 ; i--){
            if(store [i] >= 10 && store [i] <= 15 ){
                chars[i]= (char) ((char) store[i]+55);
            }else if(store [i] < 10 ){
                chars[i]= (char) ((char) store[i]+48);
            }
        }

        for(int i = counter - 1 ; i >= 0 ; i--){
            System.out.print(chars[i]+"");
        }

    }

}

package com.company;

public class Convert {

    private final char[] dictionary = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E'};
    private final int from;
    private final int to;

    /**
     * конструктор класса
     *
     * @param from  исходная сс
     * @param to итоговая сс
     */
    Convert(int from, int to) {
        this.from = from;
        this.to = to;
    }

    /**
     * Метод перевода числа из исходной сс в десятичную
     *
     * @param in исходное число
     * @return числов  десятичной сс
     */
    public int convertToTen(String in) {

        char[] step = in.toCharArray();
        int temp10 = 0;
        for (int i = 0; i < in.length(); i++) {     // Собственно сам алгоритм перевода из исходной СС в 10-чную
            for (int j = 0; j < dictionary.length; j++) {
                if (step[i] == dictionary[j]) {
                    temp10 += j * Math.pow(from, in.length() - i - 1);
                }
            }
        }
        return temp10;
    }

    /**
     * Метод перевода из десятичной сс в итоговую
     *
     * @param temp10 число в десятичной сс
     * @return число в итоговой сс
     */
    public StringBuffer convertTenTo(int temp10) {
        StringBuffer out = new StringBuffer();  // Создаём стрингбуффер для хранения строки
        while (temp10 > 0) {
            out.append(dictionary[temp10 % to]);    // Алгоритм перевода из десятичной в итоговую
            temp10 /= to;
        }
        return out.reverse();   // Возвращаем ответ
    }
    // Возвращаем ошибку если ввели неверную систему счисления

}
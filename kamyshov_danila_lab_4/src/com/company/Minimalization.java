package com.company;

public class Minimalization {

    private StringBuffer stringBuffer;
    private TruthTable truthTable;

    public void convertToDisjunctiveNormalForm(int[][] array) {
        truthTable = new TruthTable();
        stringBuffer = new StringBuffer();
        stringBuffer.append("(");
        for (int i = 0; i < truthTable.getCountOfStrings(); i++) {
            if (array[i][truthTable.getCountOfColumns() - 1] == 1) {
                for (int j = 0; j < truthTable.getCountOfColumns() - 1; j++) {
                    if (array[i][j] == 0) {
                        if (j == truthTable.getCountOfColumns() - 2) {
                            stringBuffer.append("-X").append(j + 1);
                        } else {
                            stringBuffer.append("-X").append(j + 1).append(" * ");
                        }
                    }
                    if (array[i][j] == 1) {
                        if (j == truthTable.getCountOfColumns() - 2) {
                            stringBuffer.append("X").append(j + 1);
                        } else {
                            stringBuffer.append("X").append(j + 1).append(" * ");
                        }
                    }
                }
                if (i == truthTable.getCountOfStrings() - 1) {
                    stringBuffer.append(" )");
                } else {
                    stringBuffer.append(" ) + ").append(" (");
                }
            }
        }
        System.out.println(stringBuffer);
    }

    public void convertToConjuctiveNormalForm(int[][] array) {
        truthTable = new TruthTable();
        stringBuffer = new StringBuffer();
        stringBuffer.append("(");
        for (int i = 0; i < truthTable.getCountOfStrings(); i++) {
            if (array[i][truthTable.getCountOfColumns() - 1] == 0) {
                for (int j = 0; j < truthTable.getCountOfColumns() - 1; j++) {
                    if (array[i][j] == 0) {
                        if (j == truthTable.getCountOfColumns() - 2) {
                            stringBuffer.append("X").append(j + 1);
                        } else {
                            stringBuffer.append("X").append(j + 1).append(" + ");
                        }
                    }
                    if (array[i][j] == 1) {
                        if (j == truthTable.getCountOfColumns() - 2) {
                            stringBuffer.append("-X").append(j + 1);
                        } else {
                            stringBuffer.append("-X").append(j + 1).append(" + ");
                        }
                    }
                }
                if (i == truthTable.getCountOfStrings() - 1) {
                    stringBuffer.append(" )");
                } else {
                    stringBuffer.append(" ) * ").append(" ( ");
                }
            }
        }
        System.out.println(stringBuffer);
    }
}

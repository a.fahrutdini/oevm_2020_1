package com.company;

import java.util.Random;

public class TruthTable {
    private int countOfStrings = 16;
    private int countOfColumns = 5;
    private Random random = new Random();
    public int[][] truthTable = {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)},
    };

    public void printTruthTable() {
        for (int i = 0; i < countOfStrings; i++) {
            for (int j = 0; j < countOfColumns; j++) {
                System.out.print(truthTable[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public int getCountOfStrings() {
        return countOfStrings;
    }

    public int getCountOfColumns() {
        return countOfColumns;
    }
}

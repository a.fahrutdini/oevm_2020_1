Задание: Визуализировать программный продукт, разработанный на Лабораторной работе #4/

В программе 3 класса: Panel (панель отрисовки), BooleanFunctionsGUI(класс с методами преобразования и отрисовки) и Main(основной класс)

KNF - выводит конъюктивную нормальную форму вида (a + b) * (-a + b). В случае, если в доп. массиве RandomArray F = 0, то тогда X1, X2, X3 и X4 приводятся к нужной форме. Если в основном массиве Array X1-X4 = 0, то тогда он будет иметь положительный индекс, иначе отрицательный.

DNF - выводит дизъюнктивную нормульную форму вида (a * b) + (-a * b). В случае, если в доп. массиве RandomArray F = 1, то тогда X1, X2, X3 и X4 приводятся к нужной форме. Если в основном массиве Array X1-X4 = 1, то тогда он будет иметь положительный индекс, иначе отрицательный.

Ссылка на видео: https://www.youtube.com/watch?v=m7L43Q4MpQA
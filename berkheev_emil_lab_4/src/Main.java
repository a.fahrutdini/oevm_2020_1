import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();
        Scanner input = new Scanner(System.in);
        int j = 1;

        int[][] array = Methods.refreshArray(random);
        Methods.printArray(array);

        System.out.print("\nДНФ - 1 | КНФ - 2 | Обновить массив - 3 | Завершить - 0 \n");

        while (j != 0) {
            j = input.nextInt();

            if (j == 1) {
                System.out.print(Methods.DNF(array));
            }
            else if (j == 2) {
                System.out.print(Methods.KNF(array));
            }
            else if (j == 3) {
                array = Methods.refreshArray(random);
                Methods.printArray(array);
            }

            System.out.println();
        }
    }
}

import java.util.Random;

public class Methods {
    static int sizeI = 16;
    static int sizeJ = 5;
    static int lastElement = 3;
    static int flag = lastElement + 1;

    public static void printArray(int[][] array) {
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.print("\n");
        }
    }

    public static int[][] refreshArray(Random random) {
        int[][] Array = {
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},
        };
        return Array;
    }
    public static StringBuilder deleteLast(StringBuilder strBuilder){
        strBuilder.delete(strBuilder.toString().length() - 3, strBuilder.toString().length() - 1);
        return strBuilder;
    }
    public static String DNF(int[][] array) {
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < sizeI; i++) {
            if (array[i][flag] == 1) { //if flagged element == 1
                strBuilder.append("( ");
                for (int j = 0; j < lastElement; j++) {
                    if (array[i][j] == 1) { //if element == 1
                        strBuilder.append("X").append(j + 1).append(" * "); //Xnumber*
                    } else { //if element == 0
                        strBuilder.append("-X").append(j + 1).append(" * "); //-Xnumber*
                    }
                }

                if (array[i][lastElement] == 1) {//lastElement is 1
                    strBuilder.append("X" + flag); //type "Xlast"
                } else {//lastElement is 0
                    strBuilder.append("-X" + flag); //type "-Xlast"
                }
                strBuilder.append(" ) + ");//appending "+"
            }
        }
        deleteLast(strBuilder);// delete last operational "*"
        return strBuilder.toString();
    }

    public static String KNF(int[][] array) {
        StringBuilder strBuilder = new StringBuilder();

        for (int i = 0; i < sizeI; i++)
            if (array[i][flag] == 0) { //if flagged element == 0
                strBuilder.append("( ");
                for (int j = 0; j < lastElement; j++) {
                    if (array[i][j] == 0) { //if element == 0
                        strBuilder.append("X").append(j + 1).append(" + "); //Xnumber+
                    }
                    else { //если значение 1
                        strBuilder.append("-X").append(j + 1).append(" + "); //-Xnumber+
                    }
                }

                if (array[i][lastElement] == 0) { //lastElement is 0
                    strBuilder.append("X" + flag); //type "Xlast"
                }
                else { //если значение X4 1
                    strBuilder.append("-X" + flag);//type "-Xlast"
                }
                strBuilder.append(" ) * ");//appending "*"
            }

        deleteLast(strBuilder); // deleting last *
        return strBuilder.toString();
    }

}

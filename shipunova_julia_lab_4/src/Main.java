import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        TruthTable truthTable = TruthTable.getTruthTable();
        System.out.println(truthTable);
        System.out.println("Здравствуйте!\n Чтобы получить ДНФ введите 1\n Чтобы получить КНФ введите 2");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int operationIndex = scanner.nextInt();
            if (operationIndex == 1) {
                System.out.println(Converter.convertToDnf(truthTable.getTruthTableArray()));
            } else if (operationIndex == 2) {
                System.out.println(Converter.convertToKnf(truthTable.getTruthTableArray()));
            }
        }
    }
}

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class McClusky {
    static int _specChar=9;
    ArrayList<int[]> endList=new ArrayList<int[]>();
    static int countString=16;
    static int countColum=5;
    static int core=0;
    int a[][]=new int[countString][countColum];

    public String calcDNF(){
        core = 1;
        createArr(a);
        mainCalc();
        int[][] nf = mainCalc();
        return printDnf(nf);
    }

    public String calcKNF(){
        core = 0;
        createArr(a);
        int[][] nf = mainCalc();
        return printKnf(nf);
    }

    private int[][] mainCalc(){
        ArrayList<ArrayList<int[]>> list = createStrageTable();
        for (int i = 0; i < 5; i++) {
            list = formList1(list,endList);
        }
        ArrayList<int[]> secEndList= new ArrayList<int[]>();
        for (int i = 0; i < a.length; i++) {
            if(a[i][a[i].length-1]==core) {
                secEndList.add(a[i]);
            }
        }
        boolean[][] lastArr = new boolean[endList.size()][secEndList.size()];
        for (int i = 0; i < lastArr.length; i++) {
            for (int j = 0; j < lastArr[i].length; j++) {
                if (mask(endList.get(i), secEndList.get(j))) {
                    lastArr[i][j] = true;
                }
            }
        }
        boolean usedTerm[] = new boolean[secEndList.size()];
        int[][] nf = nf(secEndList,lastArr);
        return  nf;
    }

    private String printKnf(int[][] nf) {
        StringBuilder string = new StringBuilder("");
        for (int i = 0; i < nf.length; i++) {
            for (int j = 0; j < nf[0].length; j++) {
                if(nf[i][j]==0 || nf[i][j]==1) {
                    if(nf[i][j]==0) {
                        string.append("!X"+(j+1)+" ");
                    } else if(nf[i][j]==1) {
                        string.append("X"+(j+1)+" ");
                    }
                }
            }
            string.append("+");
        }
        return string.toString();
    }

    private String printDnf(int[][] dnf) {
        StringBuilder string = new StringBuilder("");
        for (int i = 0; i < dnf.length; i++) {
            for (int j = 0; j < dnf[0].length; j++) {
                if(dnf[i][j]==0 || dnf[i][j]==1) {
                    if(dnf[i][j]==0) {
                        string.append("X"+(j+1)+"+");
                    } else if(dnf[i][j]==1) {
                        string.append("!X"+(j+1)+"+");
                    }
                }
            }
            string.append("*");
        }
        return string.toString();
    }

    private int[][] nf(ArrayList<int[]> secEndList, boolean[][] lastArr) {
        boolean[] involved= new boolean[secEndList.size()];
        HashSet<int[]> dnf = new HashSet<int[]>();
        for (int i = 0; i < lastArr[0].length; i++) {
            int count=0;
            int index=0;
            for (int j = 0; j < lastArr.length; j++) {
                if(lastArr[j][i]) {
                    count++;
                    index = j;
                }
            }
            if(count==1) {
                dnf.add(endList.get(index));
            }
        }
        involved = createMapInvolved(endList,secEndList,lastArr,dnf);
        //printArr(involved);
        while (!checkFullInvolved(involved)) {
            int indexMaxCountDif=0;
            int maxCounDif=0;
            for (int i1 = 0; i1 < lastArr.length; i1++) {
                if(!dnf.contains(endList.get(i1))) {
                    int temp=countDif(involved,lastArr[i1]);
                    if(temp > maxCounDif) {
                        maxCounDif=temp;
                        indexMaxCountDif=i1;
                    }
                }
            }
            dnf.add(endList.get(indexMaxCountDif));
            involved = createMapInvolved(endList,secEndList,lastArr,dnf);
        }

        int[][] endDnf=new int[dnf.size()][4];
        int indexEndDnf = 0;
        for (int[] is : dnf) {
            for (int i = 0; i < is.length-1; i++) {
                endDnf[indexEndDnf][i]=is[i];
            }
            indexEndDnf++;
        }
        return endDnf;
    }

    private static int countDif(boolean[] involved, boolean[] bs) {
        int countDif=0;
        for (int i = 0; i < bs.length; i++) {
            if(!involved[i] && bs[i]) {
                countDif++;
            }
        }
        return countDif;

    }

    private static boolean checkFullInvolved(boolean[] involved) {
        for (int i = 0; i < involved.length; i++) {
            if(!involved[i]) {
                return false;
            }
        }
        return true;
    }

    private static void printArr(boolean[] involved) {
        System.out.print("\t");
        for (int i = 0; i < involved.length; i++) {
            System.out.print(involved[i]+"\t");
        }
        System.out.println();
    }

    private static boolean[] createMapInvolved(ArrayList<int[]> endList, ArrayList<int[]> secEndList,boolean[][] lastArr, HashSet<int[]> dnf) {
        boolean[] involved = new boolean[secEndList.size()];
        for (int i = 0; i < lastArr.length; i++) {
            if(dnf.contains(endList.get(i))) {
                for (int j = 0; j < lastArr[0].length; j++) {
                    if(lastArr[i][j]) {
                        involved[j]=true;
                    }
                }
            }
        }
        return involved;
    }

    private static void printLastArr(ArrayList<int[]> endList, ArrayList<int[]> secEndList, boolean[][] lastArr) {
        System.out.print("\t");
        for (int[] is : secEndList) {
            for (int i = 0; i < is.length-1; i++) {
                System.out.print(is[i]);
            }
            System.out.print("\t");
        }
        System.out.println();
        for (int i = 0; i < lastArr.length; i++) {
            {
                int[] temp = endList.get(i);
                for (int j = 0; j < temp.length-1; j++) {
                    System.out.print(temp[j]);
                }
            }
            System.out.print(":");
            for (int j = 0; j < lastArr[i].length; j++) {
                if(lastArr[i][j])
                    System.out.print("\tV");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }

    }

    private static boolean mask(int[] mask, int[] value) {
        for (int i = 0; i < value.length; i++) {
            if(mask[i]==_specChar) {
            } else if(mask[i]==value[i]) {

            } else {
                return false;
            }
        }
        return true;
    }

    private static ArrayList<ArrayList<int[]>> copyList(ArrayList<ArrayList<int[]>> list) {
        ArrayList<ArrayList<int[]>> temp = new ArrayList<ArrayList<int[]>>();
        for (ArrayList i : list) {
            temp.add(i);
        }
        return temp;
    }

    private static void createArr(int[][] a, String string) {
        int index=0;
        Random random =new Random();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int j2 = 0; j2 < 2; j2++) {
                    for (int k = 0; k < 2; k++) {
                        a[index][0]=i;
                        a[index][1]=j;
                        a[index][2]=j2;
                        a[index][3]=k;
                        int e=Integer.parseInt(string.charAt(index)+"");
                        //System.out.println(e);
                        a[index][4]=e;
                        index++;
                    }
                }
            }
        }
        printArr(a);
    }

    private static ArrayList<ArrayList<int[]>> formList1(ArrayList<ArrayList<int[]>> originalList, ArrayList<int[]> endList) {
        ArrayList<ArrayList<Boolean>> endTerm = new ArrayList<ArrayList<Boolean>>();
        for (int i = 0; i < originalList.size(); i++) {
            endTerm.add(new ArrayList<Boolean>());
            for (int j = 0; j < originalList.get(i).size(); j++) {
                endTerm.get(i).add(true);
            }
        }
        ArrayList<ArrayList<int[]>> list= copyList(originalList);
        ArrayList<ArrayList<int[]>> temp = new ArrayList<ArrayList<int[]>>();
        for (int i = 0; i < list.size(); i++) {
            temp.add(new ArrayList<int[]>());
        }
        for (int i = 0; i < list.size()-1; i++) {
            for (int j = 0; j < list.get(i).size(); j++) {
                for (int l = 0; l < list.get(i+1).size(); l++) {
                    int index = compArray((int[]) list.get(i).get(j),(int[]) list.get(i+1).get(l));
                    if(index != -404) {
                        endTerm.get(i).set(j, false);
                        endTerm.get(i+1).set(l, false);
                        int[] tempArr=(int[]) list.get(i).get(j).clone();
                        tempArr[index]=_specChar;
                        if(needAdd(tempArr,temp.get(i))) {
                            temp.get(i).add(tempArr);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < originalList.size(); i++) {
            for (int j = 0; j < originalList.get(i).size(); j++) {
                if(endTerm.get(i).get(j)) {
                    endList.add(originalList.get(i).get(j));
                }
            }
        }
        return temp;
    }

    private static void printListBool(ArrayList<ArrayList<Boolean>> list) {
        int index=0;
        for (ArrayList i : list) {
            System.out.print(index+++": ");
            for (boolean arr : (ArrayList<Boolean>) i) {
                System.out.print(arr+" ");
                System.out.print("| ");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static boolean needAdd(int[] tempArr, ArrayList<int[]> arrayList) {
        boolean flag;
        for (int i = 0; i < arrayList.size(); i++) {
            int ans=compArray(tempArr, arrayList.get(i));
            if (ans==-1) {
                return false;
            }
        }
        return true;
    }

    private static void printList(ArrayList<ArrayList<int[]>> list) {
        int index=0;
        for (ArrayList i : list) {
            System.out.print(index+++": ");
            for (int[] arr : (ArrayList<int[]>) i) {
                for (int j = 0; j < arr.length-1; j++) {
                    System.out.print(arr[j]+" ");
                }
                System.out.print("| ");
            }
            System.out.println();
        }
        System.out.println();
    }

    private ArrayList<ArrayList<int[]>> createStrageTable() {
        ArrayList<ArrayList<int[]>> list = new ArrayList<ArrayList<int[]>>(a[0].length);
        for (int i = 0; i < a[0].length; i++) {
            list.add(new ArrayList<int[]>());
        }
        for (int i = 0; i < a.length; i++) {
            if(a[i][a[i].length-1]==core) {
                int tempCount=0;
                for (int j = 0; j < a[i].length-1; j++) {
                    tempCount+=a[i][j];
                }
                list.get(tempCount).add(a[i].clone());
            }
        }
        return list;
    }

    private static void createArr(int[][] a) {
        int index=0;
        Random random =new Random();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int j2 = 0; j2 < 2; j2++) {
                    for (int k = 0; k < 2; k++) {
                        a[index][0]=i;
                        a[index][1]=j;
                        a[index][2]=j2;
                        a[index][3]=k;
                        a[index][4]=random.nextInt(2);
                        index++;
                    }
                }
            }
        }
        printArr(a);
    }

    private static int compArray(int[] a,int [] b) {
        boolean flag=false;
        int index=-404;

        for (int i = 0; i < b.length-1; i++) {
            if(a[i]==b[i]) {

            } else if (a[i]!=b[i] && !flag) {
                flag = true;
                index = i;
            } else {
                return -404;
            }
        }
        if(index==-404) return -1;
        return index;
    }

    private static void printArr(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}
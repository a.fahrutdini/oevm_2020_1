package com.company;

public class Helper {
    static final char[] SYMBOL = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static long numberToTen(String result, long number, int initialNotation) {
        for (int i = 0; i<result.length();i++) { // перевожу число в десятеричную систему счисления
            for (int j = 0; j<=15;j++) {
                if(result.charAt(i) == SYMBOL[j]) { // проверяю, если символы сходятся, считаю число в десятичной системе
                    number += j * Math.pow(initialNotation,i); // перебираю числа для того, чтобы получилось десятичное число
                }
            }
        }
        return number;
    }

    public static String convert(long finalNumber, String notFinalResult) { // перевод в двоичную систему
        while(((finalNumber / 2 != 0)) || (finalNumber % 2 != 0)) { // пока остаток не ноль делаю рил ток вещи
            notFinalResult += finalNumber % 2; // строка к которой на каждой
            // итерации прибавляется остаток от деления
            finalNumber = finalNumber / 2; // делю нацело, чтобы цикл не заклинило
        }
        return notFinalResult;
    }

    public static String addition(String notFinalResultOne, String notFinalResultTwo) { // сложение
        String tmp = "";
        String sum = ""; // итоговая сумма
        char numberOne = ' '; // цифра 1
        char numberTwo = ' '; // цифра 2
        char help = '0'; // символ, который отвечает за разряд (например при 1+1 разряд повышается на 1 и help будет = 1)
        if (notFinalResultOne.length() < notFinalResultTwo.length()) {  // если второе число больше первого по длине, то меняем их местами
            tmp = notFinalResultOne;
            notFinalResultOne = notFinalResultTwo;
            notFinalResultTwo = tmp;
        }

        for (int i = 0; i < notFinalResultOne.length();i++) { // цикл для посимвольного сложения

            numberOne = notFinalResultOne.charAt(i); // первая цифра
            if (i < notFinalResultTwo.length()) numberTwo = notFinalResultTwo.charAt(i); // вторая цифра
            else {
                numberTwo = '0'; // вторая цифра
            }

            if (i == notFinalResultOne.length() - 1 && help == '1' && numberOne == '1' && numberTwo == '1') {
                sum = "11" + sum;
                break;
            }
            else if (i == notFinalResultOne.length() - 1 && numberOne == '1' && numberTwo == '1' && help == '0') {
                sum = "10" + sum;
                break;
            }
            else if (i == notFinalResultOne.length() - 1 && numberOne == '1' && numberTwo == '0' && help == '1') {
                sum = "10" + sum;
                break;
            }

            else if (numberOne == '1' && numberTwo == '1' && help == '1') {
                sum = "1" + sum;
                help = '1';
            }
            else if ((((numberOne == '1' && numberTwo == '0') || (numberOne == '0' && numberTwo == '1')) && help == '1') || (numberOne == '1' && numberTwo == '1' && help == '0')) {
                sum = "0" + sum;
                help = '1';
            }
            else if (((numberOne == '1' && numberTwo == '0') || (numberOne == '0' && numberTwo == '1') && help == '0') || (numberOne == '0' && numberTwo == '0' && help == '1')) {
                sum = "1" + sum;
                help = '0';
            }
            else if (numberOne == '0' && numberTwo == '0' && help == '0') {
                sum = "0" + sum;
                help = '0';
            }
        }
        return sum; // возвращает итог сложения
    }

    public static String subtraction(String notFinalResultOne, String notFinalResultTwo, Integer firstN, Integer secondN) {
        String tmp = ""; // для того чтобы поменять числа, если это необходимо
        String misstake = ""; // массив символов, для удаления нулей
        boolean flag = false; // для удаления нулей
        char symbol = ' '; // для удаления нулей
        char numberOne = ' '; // цифра 1
        char numberTwo = ' '; // цифра 2
        char help = '0'; // переменная для запоминмания вычитания единицы из нуля
        char mark = ' '; // знак, если из меньшего вычитаем большее, то меняем знак на противоположный
        String difference = ""; // Разность конечная

        if (notFinalResultOne.length() < notFinalResultTwo.length() || firstN < secondN) {  // если второе число больше первого по длине, то меняем их местами
            tmp = notFinalResultOne;
            notFinalResultOne = notFinalResultTwo;
            notFinalResultTwo = tmp;
            mark = '-';
        }

        for (int i = 0; i < notFinalResultOne.length();i++) {
            numberOne = notFinalResultOne.charAt(i); // первая цифра
            if (i < notFinalResultTwo.length()) numberTwo = notFinalResultTwo.charAt(i); // вторая цифра
            else {
                numberTwo = '0'; // вторая цифра
            }

            if (numberOne == '0' && numberTwo == '1' && help == '1') {
                difference = "0" + difference;
                help = 1;
            }
            else if ((numberOne == '0' && numberTwo == '0' && help == '1')|| (numberOne == '1' && numberTwo == '1' && help == '1') || (numberOne == '0' && numberTwo == '1' && help == '0')) {
                difference = "1" + difference;
                help = '1';
            }
            else if ((numberOne == '1' && numberTwo == '1' && help == '0') || (numberOne == '1' && numberTwo == '0' && help == '1') || (numberOne == '0' && numberTwo == '0' && help == '0')) {
                difference = "0" + difference;
                help = '0';
            }
            else if (numberOne == '1' && numberTwo == '0' && help == '0') {
                difference = "1" + difference;
                help = '0';
            }

        }
        for (int j = 0; j < difference.length(); j++) { // цикл для удаления незначащих нулей
            symbol = difference.charAt(j);
            if (symbol == '1') {
                flag = true;
            }
            if (flag) { // пока ложь, нули не записываются
                misstake = misstake + symbol;
            }
        }
        misstake = mark + misstake;
        return misstake;
    }
}

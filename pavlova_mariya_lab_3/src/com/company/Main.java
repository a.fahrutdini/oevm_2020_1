package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        SystemOperations sys = new SystemOperations();
        Scanner in = new Scanner(System.in);
        System.out.println("Система счисления 2-16");
        int numSystem = in.nextInt();
        System.out.println("1 число");
        String num1 = in.next();
        System.out.println("2 число");
        String num2 = in.next();
        System.out.println("Арифметическая операция");
        String operation = in.next();
        in.close();

        int numberOneDec = sys.translateToDecSystem(numSystem, num1);
        int numberTwoDec = sys.translateToDecSystem(numSystem, num2);

        String numberOne = sys.translateToBinSystem(numberOneDec).toString();
        String numberTwo = sys.translateToBinSystem(numberTwoDec).toString();

        switch (operation) {
            case "+":
                System.out.println("Сумма: " + sys.add(numberOne, numberTwo));
                break;
            case "-":
                System.out.println("Разность: " + sys.subtract(numberOne, numberTwo));
                break;
            case "*":
                System.out.println("Произведение: " + sys.multiply(numberOne, numberTwo));
                break;
            case "/":
                System.out.println("Частное: " + sys.divide(numberOne, numberTwo));
                break;
            default:
                System.out.print("Неверный ввод операции");
                break;
        }

    }
}

package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws java.io.IOException {
        System.out.print("Введите систему счисления: ");
        Scanner scanner = new Scanner(System.in);
        int notation = scanner.nextInt();

        System.out.print("Введите первое число: ");
        Scanner s = new Scanner(System.in);
        char[] firstNumber = s.nextLine().toCharArray();

        System.out.print("Введите второе число: ");
        Scanner sс = new Scanner(System.in);
        char[] secondNumber = sс.nextLine().toCharArray();

        System.out.print("Введите арифметическую операцию: ");
        char operation = (char) System.in.read();

        long firstDecimal = Helper.convertTo10(firstNumber, notation);
        long secondDecimal = Helper.convertTo10(secondNumber, notation);

        char[] firstBinary = (Helper.convertTo2(firstDecimal)).toCharArray();
        char[] secondBinary = (Helper.convertTo2(secondDecimal)).toCharArray();

        if (operation == '+') {
            System.out.println(BinaryLogic.add(firstBinary, secondBinary));
        }
        if (operation == '-') {
            if (firstDecimal > secondDecimal) {
                System.out.println(BinaryLogic.subtract(firstBinary, secondBinary));
            }
            else {
                System.out.println("Упс, первое число меньше второго, я поменял их местами))");
                System.out.println(BinaryLogic.subtract(secondBinary, firstBinary));
            }
        }
        if (operation == '*') {
            System.out.println(BinaryLogic.multiply(firstBinary, secondBinary));
        }
        if (operation == '/') {

            if (firstDecimal > secondDecimal) {
                System.out.println(BinaryLogic.divide(firstBinary, secondBinary));
            }
            else {
                System.out.println("Упс, первое число меньше второго, я поменял их местами))");
                System.out.println(BinaryLogic.divide(secondBinary, firstBinary));
            };
        }
    }
}

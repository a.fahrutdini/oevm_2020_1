package com.company;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Form {
	private final JButton button_DNF_2 = new JButton("2");
    private final JButton button_KNF = new JButton("1");
    private final JTextArea result = new JTextArea();

    private final int[][] arrayForVisualization = Helper.generateArray();
    private final JPanel Table = new plane(arrayForVisualization);

    public static boolean knf;
    public static boolean dnf;
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form window = new Form();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Form() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 577, 666);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
       frame.setVisible(true);
        
        
        

        Table.setBounds(0, 34, 180, 638);
       frame.getContentPane().add(Table);
       
       JTextArea textArea = new JTextArea();
       Table.add(textArea);

        result.setBounds(190, 115, 180, 210);
        result.setEditable(false);
        frame.getContentPane().add(result);

        
        
        
        
        button_KNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                button_KNF.setEnabled(true);
                button_DNF_2.setEnabled(false);

                knf = true;

                result.setText((Helper.knf(arrayForVisualization)));
               Table.repaint();
            }
        });
        
        
        
        
        button_KNF.setBounds(219, 345, 46, 38);
        frame.getContentPane().add(button_KNF);

        button_DNF_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                button_DNF_2.setEnabled(true);
                button_KNF.setEnabled(false);

                dnf = true;

                result.setText((Helper.dnf(arrayForVisualization)));
                Table.repaint();
            }
        });
        button_DNF_2.setBounds(275, 345, 46, 38);
        frame.getContentPane().add(button_DNF_2);
    }
}



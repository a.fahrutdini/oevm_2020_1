package sample;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите исходную систему счисления числа: ");
        int initial = scanner.nextInt();
        System.out.println("Введите систему, в которую хотите перевести число: ");
        int desired = scanner.nextInt();
        System.out.println("Введите число, которое необходимо преобразовать: ");
        char[] number = scanner.next().toCharArray();
        Convertor convert = new Convertor(initial, desired, number);
        convert.checkResultOutput();
    }
}

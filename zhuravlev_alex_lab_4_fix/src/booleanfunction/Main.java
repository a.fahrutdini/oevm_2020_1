package booleanfunction;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        MinimizingBooleanFunctions mbf= new MinimizingBooleanFunctions();
        Scanner in = new Scanner(System.in);
        mbf.printTable();
        System.out.println("Выберете - ДНФ или КНФ?");
        String form = in.next();
        if (form.equals("ДНФ")) {
            mbf.DNF();
        }
        if (form.equals("КНФ")) {
            mbf.KNF();
        }
        if (!form.equals("ДНФ") && !form.equals("КНФ")) {
            System.out.println("Неверное значение!");
        }
    }
}

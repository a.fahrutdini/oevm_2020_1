import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Writer {

    private LinkedList<String> varStr = new LinkedList<>();
    private LinkedList<String> codeStr = new LinkedList<>();
    private LinkedList<String> vars = new LinkedList<>();
    private LinkedList<String> listOfVariables = new LinkedList<>();
    private HashMap<String, String> dictionaryOfStringVariables = new HashMap<>();
    private String pascalCode;
    private String blockOfVars;
    private String blockOfCods;

    public Writer(String pascalCode) throws IOException {
        this.pascalCode = pascalCode;
        parseParthBody();
        parseVarsStrings();
        parseVarsInteger();
        parseCodsStrings();
        setVariables(vars);
        createCode();
        addVariables();
        FileWriter writer = new FileWriter("Assembler.ASM", false);
        writer.append(starting);
        writer.append(variables);
        writer.append(code);
        writer.append(ending);
        writer.flush();
    }

    StringBuilder starting = new StringBuilder(
            "format PE console\n\n" +
                    "entry start\n\n" +
                    "include 'win32a.inc'\n\n" +
                    "section '.idata' import data readable\n\n" +
                    "\tlibrary kernel, 'kernel32.dll',\\\n" +
                    "\tmsvcrt, 'msvcrt.dll'\n\n" +
                    "\timport kernel,\\\n" +
                    "\tExitProcess, 'ExitProcess'\n\n" +
                    "\timport msvcrt,\\\n" +
                    "\tprintf, 'printf',\\\n" +
                    "\tscanf, 'scanf',\\\n" +
                    "\tgetch, '_getch'\\\n" +
                    "section '.data' data readable writable\n"
    );

    StringBuilder variables = new StringBuilder(
            "\tspaceStr db '%d', 0\n" +
                    "\tdopStr db '%d', 0ah, 0\n"
    );

    StringBuilder code = new StringBuilder(
            "section '.code' code readable executable\n\n" +
                    "\tstart:\n"
    );

    StringBuilder ending = new StringBuilder(
            "\tcall [getch]\n" +
                    "\tpush NULL\n" +
                    "\tcall [ExitProcess]"
    );

    public void parseParthBody() {
        Pattern patternForBlocOfVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherForBlocOfVariables = patternForBlocOfVariables.matcher(pascalCode);

        while (matcherForBlocOfVariables.find()) {
            blockOfVars = pascalCode.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end());
        }

        Pattern patternForBlocOfCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherForBlocOfCode = patternForBlocOfCode.matcher(pascalCode);

        while (matcherForBlocOfCode.find()) {
            blockOfCods = pascalCode.substring(matcherForBlocOfCode.start(), matcherForBlocOfCode.end());
        }

    }

    private void parseVarsStrings() {
        Pattern patternForBlocOfVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherForBlocOfVariables = patternForBlocOfVariables.matcher(blockOfVars);

        while (matcherForBlocOfVariables.find()) {
            varStr.add(blockOfVars.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end()));
        }
    }

    private void parseVarsInteger() {
        Pattern patternForBlocOfVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherForBlocOfVariables;
        String var;

        for (String string : varStr) {

            matcherForBlocOfVariables = patternForBlocOfVariables.matcher(string);

            while (matcherForBlocOfVariables.find()) {
                var = string.substring(matcherForBlocOfVariables.start(), matcherForBlocOfVariables.end());
                if (!var.equals("integer")) {
                    vars.add(var);
                }
            }
        }
        setVariables(vars);
    }

    private void parseCodsStrings() {

        Pattern patternForStringsOfCods = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherForStringsOfCods = patternForStringsOfCods.matcher(blockOfCods);

        while (matcherForStringsOfCods.find()) {
            codeStr.add(blockOfCods.substring(matcherForStringsOfCods.start(), matcherForStringsOfCods.end()));
        }
        createCode();
    }

    private void createCode() {

        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternReadLine = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternNumericOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");
        Pattern patternWriteLine = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        for (String string : codeStr) {

            if (string.matches(patternWrite.toString())) {
                Matcher matcherForCode = patternWrite.matcher(string);

                if (matcherForCode.find()) {
                    addWrite(matcherForCode.group(1));
                    continue;
                }
            } else if (string.matches(patternReadLine.toString())) {
                Matcher matcherForCode = patternReadLine.matcher(string);

                if (matcherForCode.find()) {
                    addReadLine(matcherForCode.group(1));
                    continue;
                }
            } else if (string.matches(patternWriteLine.toString())) {
                Matcher matcherForCode = patternWriteLine.matcher(string);

                if (matcherForCode.find()) {
                    addWriteLine(matcherForCode.group(1));
                    continue;
                }
            } else {
                Matcher matcherForCode = patternNumericOperation.matcher(string);

                if (matcherForCode.find()) {
                    addNumericOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                    continue;
                }
            }
        }
    }

    public void setVariables(LinkedList<String> linkedList) {
        listOfVariables = linkedList;
    }

    public void addVariables() {
        for (String string : listOfVariables) {
            variables.append("\t" + string + " dd ?\n");
        }
        for (String key : dictionaryOfStringVariables.keySet()) {
            variables.append("\t" + key + " db '" + dictionaryOfStringVariables.get(key) + "', 0\n");
        }
    }

    public void addWrite(String string) {
        dictionaryOfStringVariables.put("string" + (dictionaryOfStringVariables.size() + 1), string);
        code.append("\tpush string" + dictionaryOfStringVariables.size() + "\n" +
                "\tcall [printf]\n"
        );
    }

    public void addReadLine(String string) {
        if (listOfVariables.contains(string)) {
            code.append("\tpush " + string + "\n" +
                    "\tpush spaceString\n" +
                    "\tcall [scanf]\n\n"
            );
        }
    }

    public void addWriteLine(String string) {
        if (listOfVariables.contains(string)) {
            code.append("\tpush [" + string + "]\n" +
                    "\tpush dopStr\n" +
                    "\tcall [printf]\n\n"
            );
        }
    }

    public void addNumericOperation(String res, String firstNum, String operator, String secondNum) {
        if (listOfVariables.contains(res) && listOfVariables.contains(firstNum) && listOfVariables.contains(secondNum)) {
            switch (operator) {
                case "-":
                    code.append("\tmov ecx, [" + firstNum + "]\n" +
                            "\tsub ecx, [" + secondNum + "]\n" +
                            "\tmov [" + res + "], ecx\n"
                    );
                    break;
                case "*":
                    code.append("\tmov ecx, [" + firstNum + "]\n" +
                            "\timul ecx, [" + secondNum + "]\n" +
                            "\tmov [" + res + "], ecx\n"
                    );
                    break;
                case "/":
                    code.append("\tmov eax, [" + firstNum + "]\n" +
                            "\tmov ecx, [" + secondNum + "]\n" +
                            "\tdiv ecx\n" +
                            "\tmov [" + res + "], eax\n"
                    );
                    break;
                case "+":
                    code.append("\tmov ecx, [" + firstNum + "]\n" +
                            "\tadd ecx, [" + secondNum + "]\n" +
                            "\tmov [" + res + "], ecx\n"
                    );
                    break;
            }
        }
    }
}
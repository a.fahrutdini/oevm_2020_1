import java.io.FileReader;
import java.io.IOException;

public class Reader {

    public String read() throws IOException {
        String str = "";

        FileReader reader = new FileReader("Pascal.pas");
        int c;
        while ((c = reader.read()) != -1) {
            str += ((char) c);
        }
        return str.replaceAll(";", ";\n");
    }
}

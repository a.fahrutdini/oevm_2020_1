## Лабораторная работа №7
### Выполнила студентка группы ИСЭбд-21
### Коростелева Анастасия
### Видео: https://drive.google.com/file/d/1VulJ_qiQ0J7VpeFCM74l0pSmCgFp_fhb/view?usp=sharing
##### Задание:
Разработать транслятор программ Pascal-FASM. Предусмотреть
проверку синтаксиса и семантики исходной программы. Результатом
работы транслятора является программа для ассемблера FASM,
идентичная по функционалу исходной программе. Полученная
программа должна компилироваться и выполняться без ошибок.
##### Пример кода на Pascal:
    var
        x, y: integer;
        res1, res2, res3, res4: integer;
    begin
        write(‘input x: ’); readln(x);
        write(‘input y: ’); readln(y);
        res1 := x + y; write(‘x + y = ’); writeln(res1);
        res2 := x - y; write(‘x - y = ’); writeln(res2);
        res3 := x * y; write(‘x * y = ’); writeln(res3);
        res4 := x / y; write(‘x / y = ’); writeln(res4);
     end.

##### Преобразованный код на Assembler:
    format PE console
    
    entry start
    
    include 'win32a.inc'
    
    section '.idata' import data readable
    
    	library kernel, 'kernel32.dll',\
    	msvcrt, 'msvcrt.dll'
    
    	import kernel,\
    	ExitProcess, 'ExitProcess'
    
    	import msvcrt,\
    	printf, 'printf',\
    	scanf, 'scanf',\
    	getch, '_getch'\
    section '.data' data readable writable
    	spaceStr db '%d', 0
    	dopStr db '%d', 0ah, 0
    	x dd ?
    	y dd ?
    	res1 dd ?
    	res2 dd ?
    	res3 dd ?
    	res4 dd ?
    section '.code' code readable executable
    
    	start:
    	push x
    	push spaceString
    	call [scanf]
    
    	push y
    	push spaceString
    	call [scanf]
    
    	mov ecx, [x]
    	add ecx, [y]
    	mov [res1], ecx
    	push [res1]
    	push dopStr
    	call [printf]
    
    	mov ecx, [x]
    	sub ecx, [y]
    	mov [res2], ecx
    	push [res2]
    	push dopStr
    	call [printf]
    
    	mov ecx, [x]
    	imul ecx, [y]
    	mov [res3], ecx
    	push [res3]
    	push dopStr
    	call [printf]
    
    	mov eax, [x]
    	mov ecx, [y]
    	div ecx
    	mov [res4], eax
    	push [res4]
    	push dopStr
    	call [printf]
    
    	push x
    	push spaceString
    	call [scanf]
    
    	push y
    	push spaceString
    	call [scanf]
    
    	mov ecx, [x]
    	add ecx, [y]
    	mov [res1], ecx
    	push [res1]
    	push dopStr
    	call [printf]
    
    	mov ecx, [x]
    	sub ecx, [y]
    	mov [res2], ecx
    	push [res2]
    	push dopStr
    	call [printf]
    
    	mov ecx, [x]
    	imul ecx, [y]
    	mov [res3], ecx
    	push [res3]
    	push dopStr
    	call [printf]
    
    	mov eax, [x]
    	mov ecx, [y]
    	div ecx
    	mov [res4], eax
    	push [res4]
    	push dopStr
    	call [printf]
    
    	call [getch]
    	push NULL
    	call [ExitProcess]

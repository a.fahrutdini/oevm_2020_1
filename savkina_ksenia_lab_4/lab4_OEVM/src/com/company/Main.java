package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TruthTable table = new TruthTable();
        System.out.println("Сгенерированная таблица");
        System.out.println(table.printTruthTable());
        System.out.println("Нажмите d/k для представления в ДНФ/КНФ");
        String choice = scanner.nextLine();
        scanner.close();

        if (choice.equals("k"))
            System.out.println(table.presentInKNF());
        else if (choice.equals("d"))
            System.out.println(table.presentInDNF());
    }
}

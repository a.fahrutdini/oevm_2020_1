# Лабораторная работа №6

## ПИбд-21 Николаев Семён

Консольная программа, написана на FASM

Пользователь, при запуске программы, должен ввести два числа X и Y.
По итогу работы программы, в консоль выводятся результаты арифметических операций над числами X и Y.

Пример работы программы:
```
X = 10
Y = -5
X + Y = 5
X - Y = 15
X * Y = -50
X / Y = -2
```

**[Видео c обзором программы](https://drive.google.com/file/d/1Q76kAxsZpzyQ39SQdsOfu7v2y1sLxrE0/view?usp=sharing)**
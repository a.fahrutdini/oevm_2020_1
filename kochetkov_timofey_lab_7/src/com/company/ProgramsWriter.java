package com.company;

import java.io.*;
import java.util.*;

public class ProgramsWriter {

    ArrayList<String> arrayListOfVariables = new ArrayList<>();

    HashMap<String, String> arrayOfStringVariables = new HashMap<>();

    String start = "format PE console\n" + "\n" + "entry start\n" + "\n" + "include 'win32a.inc'\n" + "\n";

    String variables = "\nsection '.data' data readable writable\n" + "\tspaceStr db '%d', 0\n" + "\tdopStr db '%d', 0ah, 0\n";

    String finishVariables = "\tNULL = 0 \n";

    String code = "\nsection '.code' code readable executable\n" + "\n" + "\tstart:\n";

    String finishCode = "\n" + "\t\tcall [getch]\n" + "\n" + "\t\tpush NULL\n" + "\t\tcall [ExitProcess]\n" + "\n";

    String libraries = "section '.idata' import data readable\n" + "\n" +"\tlibrary kernel, 'kernel32.dll',\\\n" +
            "\t\tmsvcrt, 'msvcrt.dll'\n" + "\n" + "\timport kernel,\\\n" + "\t\tExitProcess, 'ExitProcess'\n" + "\n" +
            "\timport msvcrt,\\\n" + "\t\tprintf, 'printf',\\\n" + "\t\tscanf, 'scanf',\\\n" + "\t\tgetch, '_getch'\n";



    public void setArray(ArrayList<String> arrayList) {
        this.arrayListOfVariables = arrayList;
    }


    public void addToCodeOperation(String result, String firstNum, String operator, String secondNum) {

        if (arrayListOfVariables.contains(result) && arrayListOfVariables.contains(firstNum) && arrayListOfVariables.contains(secondNum)) {

            switch (operator) {
                case "+":
                    code += "\t\tmov ecx, [" + firstNum + "]\n" +
                            "\t\tadd ecx, [" + secondNum + "]\n" +
                            "\t\tmov [" + result + "], ecx\n";
                    break;
                case "-":
                    code += "\t\tmov ecx, [" + firstNum + "]\n" +
                            "\t\tsub ecx, [" + secondNum + "]\n" +
                            "\t\tmov [" + result + "], ecx\n";
                    break;
                case "*":
                    code += "\t\tmov ecx, [" + firstNum + "]\n" +
                            "\t\timul ecx, [" + secondNum + "]\n" +
                            "\t\tmov [" + result + "], ecx\n";
                    break;
                case "/":
                    code += "\t\tmov eax, [" + firstNum + "]\n" +
                            "\t\tmov ecx, [" + secondNum + "]\n" +
                            "\t\tdiv ecx\n" +
                            "\t\tmov [" + result + "], eax\n";
                    break;
            }
        }
    }


    public void addToCodeWrite(String string) {
        arrayOfStringVariables.put("str" + (arrayOfStringVariables.size() + 1), string);

        code += "\t\tpush " + "str" + arrayOfStringVariables.size() + "\n" + "\t\tcall [printf]\n";
    }

    public void addToCodeWriteLn(String string) {
        if (arrayListOfVariables.contains(string)) {

            code += "\t\tpush [" + string + "]\n" + "\t\tpush dopStr\n" + "\t\tcall [printf]\n\n";
        }
    }

    public void addToCodeReadLn(String string) {
        if (arrayListOfVariables.contains(string)) {
            code += "\t\tpush " + string + "\n" + "\t\tpush spaceStr\n" + "\t\tcall [scanf]\n\n";
        }
    }


    public void write() {
        addVariables();

        try (FileWriter writer = new FileWriter("AssemblerProgram.ASM", false)) {
            writer.write(start);
            writer.write(libraries);
            writer.write(variables);
            writer.write(finishVariables);
            writer.write(code);
            writer.write(finishCode);

            writer.flush();
        } catch (IOException ex) {
            System.out.println("Ошибка при создании файла");
        }
    }
    public void addVariables() {
        for(int i = 0; i < arrayListOfVariables.size();i++) {
            String string = arrayListOfVariables.get(i);
            variables += "\t" + string + " dd ?\n";
        }

        for (String keyString : arrayOfStringVariables.keySet()) {
            variables += "\t" + keyString + " db '" + arrayOfStringVariables.get(keyString) + "', 0\n";
        }
    }

}

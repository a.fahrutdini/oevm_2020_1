format PE console

entry start

include 'win32a.inc'

section '.data' data readable writable

        numberOne db 'X = ', 0
        numberTwo db 'Y = ', 0
        enterNum db '%d', 0

        X dd ?
        Y dd ?

        addMessage db 'X + Y = %d', 0dh, 0ah, 0
        subMessage  db 'X - Y = %d', 0dh, 0ah, 0
        imulMessage db 'X * Y = %d', 0dh, 0ah, 0
        idivMessage db 'X / Y = %d', 0dh, 0ah, 0

        NULL = 0

section '.code' code readable executable

        start:
                push numberOne
                call [printf]

                push X
                push enterNum
                call [scanf]

                push numberTwo
                call [printf]

                push Y
                push enterNum
                call [scanf]

                mov ecx, [X]
                add ecx, [Y]

                push ecx
                push addMessage
                call [printf]


                mov ecx, [X]
                sub ecx, [Y]
                push ecx
                push subMessage
                call [printf]


                mov ecx, [X]
                imul ecx, [Y]

                push ecx
                push imulMessage
                call [printf]


                mov eax, [X]
                mov ecx, [Y]
                idiv ecx

                push eax
                push idivMessage
                call [printf]

                call [getch]

                push NULL
                call [ExitProcess]


section '.idata' import data readable
        library kernel, 'kernel32.dll',\
                msvctr, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvctr,\
               printf, 'printf',\
               getch, '_getch',\
               scanf, 'scanf'

import java.util.ArrayList;
import java.util.Iterator;

public class BinaryNumber implements Comparable<BinaryNumber> {
    private static final int minNumSys = 2;
    private static final int maxNumSys = 16;
    private static final int decimalNumSys = 10;
    private final boolean[] value;

    public boolean[] getValue() {
        return this.value;
    }

    //CONSTRUCTORS

    public BinaryNumber(String num, int numSys) {
        checkNumSys(num, numSys);

        int decimal = toDecimal(num, numSys);

        int leng = countLength(decimal);

        this.value = new boolean[leng];

        for (int i = 0; i < leng; ++i) {
            if (decimal % 2 == 0) {
                this.value[leng - 1 - i] = false;
            } else {
                this.value[leng - 1 - i] = true;
            }

            decimal /= 2;
        }
    }

    private int countLength(int decimal) {
        int testNum = 2;
        int leng;
        for (leng = 1; testNum <= decimal; ++leng) {
            testNum *= 2;
        }
        return leng;
    }

    private int toDecimal(String num, int numSys) {
        int decimal = 0;
        int testNum;
        if (numSys != decimalNumSys) {
            for (testNum = 0; testNum < num.length(); ++testNum) {
                decimal = (int) ((double) decimal + (double) BinaryNumberHelperMethods.charToNum(num.charAt(testNum)) * Math.pow((double) numSys, (double) (num.length() - 1 - testNum)));
            }
        } else {
            decimal = Integer.parseInt(num);
        }
        return decimal;
    }

    private void checkNumSys(String num, int numSys) {
        if (numSys >= minNumSys && numSys <= maxNumSys) {
            int decimal;
            for (decimal = 0; decimal < num.length(); ++decimal) {
                if (BinaryNumberHelperMethods.charToNum(num.charAt(decimal)) >= numSys) {
                    throw new IndexOutOfBoundsException("Число находилось вне указанной системы счисления, либо ввод был не корректен.");
                }
            }
        } else {
            throw new IndexOutOfBoundsException("Система счисления находилась вне допустимых пределов.");
        }
    }

    public BinaryNumber(boolean[] arr) {
        this.value = arr;
    }

    //ACTIONS

    public BinaryNumber add(BinaryNumber number) {
        boolean[] result = new boolean[number.value.length + this.value.length];
        boolean[] bigger;
        boolean[] smaller;
        if (this.compareTo(number) > 0) {
            bigger = this.value;
            smaller = number.value;
        } else {
            smaller = this.value;
            bigger = number.value;
        }

        boolean isThereAdditionalHigherOrderNumber = false;

        for (int i = 0; i < result.length; ++i) {
            int biggerIndex = bigger.length - 1 - i;
            int smallerIndex = smaller.length - 1 - i;
            int resultIndex = result.length - 1 - i;
            boolean currentBigger = false;
            boolean currentSmaller = false;
            if (biggerIndex >= 0 && biggerIndex < bigger.length) {
                currentBigger = bigger[biggerIndex];
            }

            if (smallerIndex >= 0 && smallerIndex < smaller.length) {
                currentSmaller = smaller[smallerIndex];
            }

            if (isThereAdditionalHigherOrderNumber) {
                if (currentBigger & currentSmaller) {
                    result[resultIndex] = true;
                    isThereAdditionalHigherOrderNumber = true;
                } else if (!currentBigger & !currentSmaller) {
                    result[resultIndex] = true;
                    isThereAdditionalHigherOrderNumber = false;
                } else {
                    result[resultIndex] = false;
                    isThereAdditionalHigherOrderNumber = true;
                }
            } else if (currentBigger & currentSmaller) {
                result[resultIndex] = false;
                isThereAdditionalHigherOrderNumber = true;
            } else {
                result[resultIndex] = currentBigger | currentSmaller;
            }
        }

        BinaryNumber res = new BinaryNumber(BinaryNumberHelperMethods.trim(result));
        return res;
    }

    public BinaryNumber subtract(BinaryNumber subtrahend) throws IllegalArgumentException, IndexOutOfBoundsException {
        if (this.compareTo(subtrahend) < 0) {
            throw new IllegalArgumentException("Результатом будет отрицательное число.");
        } else {
            boolean[] val = this.value;
            boolean[] result = new boolean[this.value.length];
            boolean[] sub = subtrahend.value;

            for (int i = 0; i < result.length; ++i) {
                int valueIndex = val.length - 1 - i;
                int subIndex = sub.length - 1 - i;
                int resultIndex = result.length - 1 - i;
                if (subIndex < 0) {
                    break;
                }

                boolean currentVal = val[valueIndex];
                boolean currentSub = sub[subIndex];
                if (currentVal == currentSub) {
                    result[resultIndex] = false;
                } else if (currentVal & !currentSub) {
                    result[resultIndex] = true;
                } else {
                    int nextLeftTrueIndex = BinaryNumberHelperMethods.findLeftNextTrue(this.value, valueIndex);
                    val[nextLeftTrueIndex] = false;

                    for (int n = nextLeftTrueIndex + 1; n < valueIndex; ++n) {
                        val[n] = true;
                    }

                    result[resultIndex] = true;
                }
            }

            BinaryNumber res = new BinaryNumber(BinaryNumberHelperMethods.trim(result));
            return res;
        }
    }

    public BinaryNumber multiply(BinaryNumber multiplier) {
        ArrayList<BinaryNumber> terms = new ArrayList();

        for (int i = 0; i < multiplier.value.length; ++i) {
            int multIndex = multiplier.value.length - 1 - i;
            if (multiplier.value[multIndex]) {
                terms.add(new BinaryNumber(BinaryNumberHelperMethods.shiftLeft(this.value, i)));
            }
        }

        BinaryNumber result = new BinaryNumber(new boolean[0]);
        if (terms.size() > 0) {
            Iterator iterator = terms.iterator();
            while (iterator.hasNext()) {
                result = result.add((BinaryNumber) iterator.next());
            }
        }

        return result;
    }

    public BinaryNumber divide(BinaryNumber divisor) {
        BinaryNumber cur = divisor;
        int i;
        for (i = 0; this.compareTo(cur) > 0; ++i) {
            cur = cur.add(divisor);
        }
        return BinaryNumberHelperMethods.decimalToBinary(i);
    }

    //WRAPS

    public static BinaryNumber add(BinaryNumber a, BinaryNumber b) {
        return a.add(b);
    }

    public static BinaryNumber subtract(BinaryNumber a, BinaryNumber b) throws IllegalArgumentException, IndexOutOfBoundsException {
        return a.subtract(b);
    }

    public static BinaryNumber multiply(BinaryNumber a, BinaryNumber b) {
        return a.multiply(b);
    }

    public static BinaryNumber divide(BinaryNumber a, BinaryNumber b) {
        return a.divide(b);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < this.value.length; ++i) {
            if (this.value[i]) {
                sb.append('1');
            } else {
                sb.append('0');
            }
        }

        return sb.toString();
    }

    public int compareTo(BinaryNumber o) {
        if (this.value.length > o.value.length) {
            return 1;
        } else if (this.value.length < o.value.length) {
            return -1;
        } else {
            for (int i = 0; i < this.value.length; ++i) {
                if (this.value[i] != o.value[i]) {
                    if (this.value[i]) {
                        return 1;
                    }

                    return -1;
                }
            }

            return 0;
        }
    }
}

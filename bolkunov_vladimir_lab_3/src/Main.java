import java.io.PrintStream;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Выберите действие:");
            System.out.println("1 - сложение");
            System.out.println("2 - вычитание");
            System.out.println("3 - умножение");
            System.out.println("4 - деление");
            System.out.println("0 - выход");
            int input = scanner.nextInt();
            if (input == 0) {
                return;
            }

            if (input <= 4) {
                System.out.println("Введите исходную систему счисления (2-16):");
                int numSys = scanner.nextInt();
                System.out.println("Введите число A:");
                String a = scanner.next();
                BinaryNumber bn1 = new BinaryNumber(a, numSys);
                System.out.println("Введите число B:");
                String b = scanner.next();
                BinaryNumber bn2 = new BinaryNumber(b, numSys);
                PrintStream var10000;
                String var10001;
                BinaryNumber res;
                switch (input) {
                    case 1:
                        res = BinaryNumber.add(bn1, bn2);
                        var10000 = System.out;
                        var10001 = bn1.toString();
                        var10000.println(var10001 + " + " + bn2.toString() + " = " + res.toString());
                        break;
                    case 2:
                        res = BinaryNumber.subtract(bn1, bn2);
                        var10000 = System.out;
                        var10001 = bn1.toString();
                        var10000.println(var10001 + " - " + bn2.toString() + " = " + res.toString());
                        break;
                    case 3:
                        res = BinaryNumber.multiply(bn1, bn2);
                        var10000 = System.out;
                        var10001 = bn1.toString();
                        var10000.println(var10001 + " * " + bn2.toString() + " = " + res.toString());
                        break;
                    case 4:
                        res = BinaryNumber.divide(bn1, bn2);
                        var10000 = System.out;
                        var10001 = bn1.toString();
                        var10000.println(var10001 + " / " + bn2.toString() + " = " + res.toString());
                }
            }
        }
    }
}
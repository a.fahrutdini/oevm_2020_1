public class Methods {
    private int SS1;
    private int SS2;
    private char[] number;

    public void setSS1(int SS1) {
        this.SS1 = SS1;
    }

    public void setSS2(int SS2) {
        this.SS2 = SS2;
    }

    public void setNumber(String number) {
        this.number = number.toCharArray();
    }

    public String program() {
        StringBuilder strBuilder = new StringBuilder();
        int ifChar = 55;
        int ifDigit = 48;
        int newNumber = 0;
        //from SS1 to 10SS
        for (int i = 0; i < number.length; i++) {
            int thisSymbol;
            if (number[i] <= '9') {
                thisSymbol = (int) number[i] - ifDigit;
            } else {
                thisSymbol = (int) number[i] - ifChar;
            }
            newNumber += thisSymbol * (int) Math.pow(SS1, number.length - 1 - i);
        }
        //form 10SS to SS2
        if (newNumber == 0) {
            return strBuilder.append("0").toString();
        }
        while (newNumber > 0) {
            if (newNumber % SS2 <= 9) {
                strBuilder.append((char) (newNumber % SS2 + ifDigit));
            } else {
                strBuilder.append((char) (newNumber % SS2 + ifChar));
            }
            newNumber = newNumber / SS2;
        }

        return strBuilder.reverse().toString();
    }
}
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window {

    private JFrame frame;
    private JTextArea textAreaResult;
    private TruthTable truthTable;
    private JButton btnDNF;
    private JButton btnKNF;
    private TruthTablePanel panelTruthTable;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Window window = new Window();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Window() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        truthTable = new TruthTable();
        frame = new JFrame();
        frame.setTitle("\u041C\u0438\u043D\u0438\u043C\u0438\u0437\u0430\u0446\u0438\u044F \u0431\u0443\u043B\u0435\u0432\u044B\u0445 \u0444\u0443\u043D\u043A\u0446\u0438\u0439");
        frame.setBounds(100, 100, 662, 617);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        panelTruthTable = new TruthTablePanel();
        panelTruthTable.setBounds(10, 11, 230, 556);
        frame.getContentPane().add(panelTruthTable);

        textAreaResult = new JTextArea();
        textAreaResult.setFont(new Font("Dialog", Font.PLAIN, 15));
        textAreaResult.setEditable(false);
        textAreaResult.setBounds(250, 11, 168, 556);
        frame.getContentPane().add(textAreaResult);

        JButton btnCreateTableTruth = new JButton("\u0421\u043E\u0437\u0434\u0430\u0442\u044C \u0442\u0430\u0431\u043B\u0438\u0446\u0443 \u0438\u0441\u0442\u0438\u043D\u043D\u043E\u0441\u0442\u0438");
        btnCreateTableTruth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textAreaResult.setText("");
                truthTable.createNewTruthTable();
                panelTruthTable.setLogicalOperations("");
                btnDNF.setEnabled(true);
                btnKNF.setEnabled(true);
                panelTruthTable.setTruthTable(truthTable.getTruthTable());
                panelTruthTable.repaint();

            }
        });
        btnCreateTableTruth.setBounds(428, 11, 208, 40);
        frame.getContentPane().add(btnCreateTableTruth);

        btnKNF = new JButton("\u041A\u041D\u0424");
        btnKNF.setEnabled(false);
        btnKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                IMinimization Minimization = new KNFminimization();
                panelTruthTable.setLogicalOperations("КНФ");
                startMinimizing(Minimization);
                panelTruthTable.repaint();
            }
        });
        btnKNF.setBounds(428, 62, 208, 40);
        frame.getContentPane().add(btnKNF);

        btnDNF = new JButton("\u0414\u041D\u0424");
        btnDNF.setEnabled(false);
        btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                IMinimization Minimization = new DNFminimization();
                panelTruthTable.setLogicalOperations("ДНФ");
                startMinimizing(Minimization);
                panelTruthTable.repaint();
            }
        });
        btnDNF.setBounds(428, 113, 208, 40);
        frame.getContentPane().add(btnDNF);
    }

    private void startMinimizing(IMinimization operation) {
        operation.setTableTruth(truthTable.getTruthTable());
        operation.startMinimizing();
        textAreaResult.setText(operation.getResult().toString());
    }
}

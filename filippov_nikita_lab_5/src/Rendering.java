import java.awt.*;

public class Rendering {
    int marginLeft = 30;
    int marginLeftNumber = marginLeft + 12;
    int marginTop = 20;
    int marginTopNumber = marginTop + 22;
    int heightCell = 32;
    int widthCell = 32;

    public void drawTable(Graphics g, int[][] truthTable) {
        int marginLeft = 30;
        int marginTop = 20;

        for (int i = 0; i < truthTable.length; i++) {
            g.drawRect(marginLeft, (i * heightCell) + marginTop, widthCell * truthTable[i].length, heightCell);
            for (int j = 0; j < truthTable[i].length; j++) {
                g.drawRect((j * widthCell) + marginLeft, (i * heightCell) + marginTop, widthCell, heightCell);
            }
        }
    }

    public void drawNumbers(Graphics g, int[][] truthTable, LogicalOperations operation) {
        g.setFont(new Font("TimesRoman", Font.BOLD, 15));
        for (int i = 0; i < truthTable.length; i++) {
            if (operation == null) {
                g.setColor(Color.black);
            } else {
                switch (operation) {
                    case KNF: {
                        if (truthTable[i][truthTable[i].length - 1] == 0) {
                            g.setColor(new Color(0, 0, 255));
                        } else {
                            g.setColor(Color.black);
                        }
                        break;
                    }
                    case DNF: {
                        if (truthTable[i][truthTable[i].length - 1] == 1) {
                            g.setColor(new Color(255, 0, 255));
                        } else {
                            g.setColor(Color.black);
                        }
                        break;
                    }
                    case Empty: {
                        g.setColor(Color.black);
                        break;
                    }
                }

                for (int j = 0; j < truthTable[i].length; j++) {
                    String number = "";
                    number += truthTable[i][j];
                    g.drawString(number, (j * widthCell) + marginLeftNumber, (i * heightCell) + marginTopNumber);
                }
            }
        }
    }
}

import javax.swing.*;
import java.awt.*;

public class TruthTablePanel extends JPanel {
    private int[][] truthTable;
    private Rendering rendering = new Rendering();
    LogicalOperations operation;

    public void setTruthTable(int[][] truthTable) {
        this.truthTable = truthTable;
    }

    public void setLogicalOperations(String choice) {
        this.operation = LogicalOperations.getOperations(choice);
    }

    public void paint(Graphics g) {
        super.paint(g);
        if (truthTable != null) {
            rendering.drawTable(g, truthTable);
            rendering.drawNumbers(g, truthTable, this.operation);
        }
    }
}

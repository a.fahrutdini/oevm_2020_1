package com.company;

import java.util.Random;

public class TableOfTrue {

    private final int n = 16;
    private final int m = 4;
    private final int[][] table;
    private final int[] vector;

    /**
     * Конструктор класа
     */
    TableOfTrue() {
        table = new int[n][m];
        vector = new int[n];
        zapolnenTable();
        zapolnenVector();
    }

    /**
     * Метод заполнения заблицы
     */
    public void zapolnenTable() {

        int count;
        boolean flag;

        for (int j = m - 1; j >= 0; j--) {
            count = (int) Math.pow(2, m - j - 1);
            flag = false;
            for (int i = 0; i < n; i++) {
                if (count == 0) {
                    count = (int) Math.pow(2, m - j - 1);
                    flag = !flag;
                }
                if (flag) {
                    table[i][j] = 1;
                } else table[i][j] = 0;
                count--;
            }
        }
    }

    /**
     * Метод заполнения ответного вектора таблицы
     */
    public void zapolnenVector() {
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            vector[i] = random.nextInt(2);
        }
    }

    /**
     * Получить кол-во строк таблицы
     *
     * @return Кол-во строк
     */
    public int getN() {
        return n;
    }

    /**
     * Получить кол-во столбцов таблицы
     *
     * @return Кол-во столбцов
     */
    public int getM() {
        return m;
    }

    public int[][] getTable() {
        return table;
    }

    public int[] getVector() {
        return vector;
    }
}


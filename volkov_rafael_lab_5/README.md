# Лабораторная работа №5

## ПИбд-22 Волков Рафаэль

##### ТЗ:

Изучение простейших логических элементов. 
Разработать программный продукт для визуализации булевых функций с помощью простейших логических элементов. В качестве входных данных могут выступать результаты работы программы, разработанной в рамках ЛР №4, либо функция, сгенерированная случайным образом.

##### Работа программы:

В программе есть 5 классов:
* Main - точка запуска программы.
* TableOfTrue - таблица истинности и связанная с ней логика.
* GUITable - ГУИ форма
* PanelTable - панель с отрисовкой таблицы и нормальных форм

[Видео](https://drive.google.com/file/d/1lCryjvNCZFXlw6oOyrfXGvfOfHIHlikv/view?usp=sharing)

_Пример вывода Таблицы:_
![Пример](https://sun9-76.userapi.com/IxaOKIiOGRd57PXdxrCeXVeRl7EQin9IIZSayA/1UB_wiVOCC0.jpg)

_Пример вывода ДНФ:_
![Пример](https://sun9-25.userapi.com/5c7flsCBlOMcs20XzxU09s-HzmLt7cnXWBiGeQ/k_uMcSnzUMM.jpg)

_Пример вывода КНФ:_
![Пример](https://sun9-58.userapi.com/VKvGWZ1NhjJzr4pYSnAZ4imCyWwmAeZn5RQUog/BesLk0WUCHY.jpg)
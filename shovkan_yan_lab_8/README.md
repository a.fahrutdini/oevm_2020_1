## Лабораторная работа №8

### Студент группы ПИбд-21

### Шовкань Ян

##### Техническое задание:
Разобрать и собрать физический блок ЭВМ.

##### Ход выполнения: 
Я решил попробовать разобрать и собрать свой старый ноутбук HP Pavilion dv6-3070er. <br/>
![title](https://sun9-72.userapi.com/iAp0MQeLEPDY03XJ77E5-Y3ynJ6WjZX8Y56VjA/wGvhmQzjHuQ.jpg) <br/>
Мне удалось вытащить 2 плашки оперативной памяти: самсунговскую на 2 Гб и микроновскую на 2 ГБ. <br/>
![title](https://sun9-49.userapi.com/PBdogiBJawjAXPWFJ1mSGo-r7HK8zxJKdhYH9A/3ox0aXNjO5c.jpg) <br/>
Также там был установленный мной SSD диск от интела на 256 ГБ. <br/>
![title](https://sun9-31.userapi.com/KdwpRsoCNJMt_RJcsr-UXnzwzlsUPsix-wj5nA/K8-Dq3qb6iI.jpg) <br/>
Это материнская плата, процессор и видеочип. <br/>
![title](https://sun9-56.userapi.com/HRf3COOvPUipojU4Ip5L0QoqN6WYQ1HRLJvQAA/8F9o6EwRHn4.jpg) <br/>
А это доказательство того, что ноутбук действительно разбирался. <br/>
![title](https://sun9-69.userapi.com/-5X_xdNty9ziE-crin1BXiKA4KazlU3g3x913Q/X9gU3wSzX9A.jpg) <br/>
А это доказательство того, что ноутбук был собрн обратно. В процессе я так же почистил систему охлаждения, так же у меня была идея заменить термопасту, но мне сиало лень) <br/>
public class Converter {

    private static final int indexF = 4;

    public static String convertToDNF(int[][] truthTable) {
        StringBuilder result = new StringBuilder("(");
        boolean isStarted = false;
        for (int[] ints : truthTable) {
            if (ints[indexF] == 1) {
                if (isStarted) {
                    if (result.length() % 100 > 80) {
                        result.append(") + \n + (");
                    } else {
                        result.append(") + (");
                    }
                }
                for (int j = 0; j < ints.length - 1; j++) {
                    if (ints[j] == 0) {
                        result.append("!");
                    }
                    result.append("X").append(j + 1);
                    if (j != ints.length - 2) {
                        result.append(" * ");
                    }
                }
                isStarted = true;
            }
        }
        result.append(")");
        return result.toString();
    }

    public static String convertToKNF(int[][] truthTable) {
        StringBuilder result = new StringBuilder("(");
        boolean isStarted = false;
        for (int[] ints : truthTable) {
            if (ints[indexF] == 0) {
                if (isStarted) {
                    if (result.length() % 100 > 80) {
                        result.append(") * \n * (");
                    } else {
                        result.append(") * (");
                    }
                }
                for (int j = 0; j < ints.length - 1; j++) {
                    if (ints[j] == 1) {
                        result.append("!");
                    }
                    result.append("X").append(j + 1);
                    if (j != ints.length - 2) {
                        result.append(" + ");
                    }
                }
                isStarted = true;
            }
        }
        result.append(")");
        return result.toString();
    }
}

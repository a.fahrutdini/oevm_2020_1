package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Укажите исходную систему счисления (2-16): ");
        int startSystem = scanner.nextInt();

        System.out.print("Укажите конечную систему счисления (2-16): ");
        int finalSystem = scanner.nextInt();

        System.out.print("Укажите число в исходной системе счисления для перевода: ");
        char[] number = scanner.next().toCharArray();

        System.out.println("Исходное число в конечной системе счисления: " + convertToFinal(finalSystem, convertToDecimal(startSystem, number)));

    }

    /**
     * Этот метод вернет число в десятичной системе счисления
     * @param startSystem Исходная система счисления
     * @param number Число в исходной ситеме счисления
     */
    private static long convertToDecimal(int startSystem, char[] number) {
        long number10 = 0;

        for(int i = 0; i < number.length; ++i) {
            int currentDigit = 1;
            if (number[i] >= '0' && number[i] <= '9') {
                currentDigit = number[i] - '0';
            } else if (Character.isLetter(number[i])) {
                currentDigit = 10 + number[i] - 'A';
            }
            number10 = (long) ((double) number10 + (double) currentDigit * Math.pow(startSystem, number.length - i - 1));
        }
        return number10;
    }

    /**
     * Этот метод вернет число (в виде строки) в конечной системе счисления
     * @param finalSystem Конечная система счисления
     * @param number10 Число в десятичной системе счисления
     */
    private static String convertToFinal(int finalSystem, long number10) {
        StringBuilder result = new StringBuilder();
        long remainder;

        for(String currentChar; number10 > 0; number10 /= finalSystem) {

            remainder = number10 % (long) finalSystem;
            currentChar = "";
            if (number10 % (long) finalSystem < 10) {
                currentChar = Long.toString(remainder);
            } else {
                currentChar = currentChar + (char) ((int) ('A' + remainder - 10));
            }
            result.insert(0, currentChar);
        }
        return result.toString();
    }
}


**Лабораторная Работа №1.**

**Орлов Артем.**

**Группа ПИбд-21.**

| № | Комплектующие пк | Комментарии | Цена |
|:-:|:----------------:|:-----------:| :-------------:|
| 1 | **Процессор:** AMD Ryzen 5 3600| 6 ядер 3.6 ГГц 7-Нм техпроцесс| **16282 рубля** |
| 2 | **Материнская плата:** MSI B450M PRO-VDH | соответствует форм-фактору Micro-ATX |**8900 рублей**|
| 3 | **Видеокарта:** GIGABYTE Radeon RX 570 1244MHz PCI-E 3.0 4096MB 7000MHz 256 bit DVI | В вашем распоряжении будет 4 Гб памяти с частотой 7000 МГц  и разрядностью шины памяти 256 Бит   | **16190 рублей**  |
| 4 | **Оперативная память:** HyperX HX426C16FB3 | 8 Гб оперативной памяти DDR4 с частотой 2666 МГц и пропускной способностью 21300 МБ/с  | **2800 рублей** |
| 5 | **SSD:** SSD-диск Kingston SA400S37/240G | Для ускорения работы системы и быстрого запуска основных приложений будет использован SSD-накопитель с объемом 240 Гб  | **2500 рублей** |
| 6 | **Блок питания:** System Power 9 500W | Надежный блок с запасом на будущее  | **3900 рублей** |
| 7 | **Корпус:** Zalman N2 Black | Красивый корпус с 3 встроенными 120-мм вентиляторами, благодаря которым выбранные комплектующие не будут перегреваться  | **2500 рублей** |

**Итог:** Получилась сборка мощного ПК стоимостью в 53072 рубля


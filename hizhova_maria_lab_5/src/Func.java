import java.awt.*;

public class Func {

    private String func = "";

    public Func(String func) {
        this.func = func;
    }

    private String[] dev() {
        String[] ar = new String[8];
        String substr = "";
        boolean flag = false;
        int k = 0;
        for (int i = 0; i < func.length(); i++) {
            if (func.charAt(i) == '(') {
                flag = true;
            }
            if (func.charAt(i) == ')') {
                flag = false;
                ar[k] = substr;
                substr = "";
                k++;
            }
            if (flag && func.charAt(i) != ')' && func.charAt(i) != '(') {
                substr = substr + func.substring(i, i + 1);
            }
        }
        return ar;
    }

    public void paintFunc(Graphics g) {
        if (func.equals("F=0") || func.equals("F=") || func.equals("F=1")) {
            if (func.equals("F=0")) {
                drawPovt(g, 15, 10, "0");
            }
            if (func.equals("F=1")) {
                drawPovt(g, 15, 10, "1");
            }
            return;
        }
        String[] ar = dev();
        int k = 0;
        boolean flag = true;
        while (func.charAt(k) != '1' && func.charAt(k) != '2' && func.charAt(k) != '3' && func.charAt(k) != '3') {
            k++;
        }
        if (func.charAt(k + 1) == '+') {
            flag = false;
        }
        k = 0;
        while (k < ar.length && ar[k] != null) {
            int klv = 0;
            String x1 = "";
            String x2 = "";
            String x3 = "";
            String x4 = "";
            int i = 0;
            while (i < ar[k].length()) {
                if (ar[k].charAt(i) == '`') {
                    i += 2;
                } else if (ar[k].charAt(i) == '+') {
                    i++;
                    continue;
                } else {
                    i += 1;
                }
                if (i < ar[k].length())
                    switch (ar[k].charAt(i)) {
                        case '1':
                            if (i == 2) {
                                x1 = "ne";
                            } else {
                                x1 = "X1";
                            }
                            klv++;
                            break;
                        case '2':
                            if (i >= 2 && ar[k].charAt(i - 2) == '`') {
                                x2 = "ne";
                            } else {
                                x2 = "X2";
                            }
                            klv++;
                            break;
                        case '3':
                            if (i >= 2 && ar[k].charAt(i - 2) == '`') {
                                x3 = "ne";
                            } else {
                                x3 = "X3";
                            }
                            klv++;
                            break;
                        case '4':
                            if (i >= 2 && ar[k].charAt(i - 2) == '`') {
                                x4 = "ne";
                            } else {
                                x4 = "X4";
                            }
                            klv++;
                            break;
                        default:
                            break;
                    }
                if (flag) {
                    i++;
                } else {
                    i += 2;
                }
            }
            int x = 15;
            int y = 10 + k * 70;
            int klvSh = 0;
            if (x1 != "") {
                klvSh++;
                if (x1 == "ne") {
                    drawNOT(g, x, y, "X1");
                } else {
                    drawPovt(g, x, y, "X1");
                }
                if (klvSh % 2 == 1) {
                    y += 10;
                } else {
                    y += 25;
                }
            }
            if (x2 != "") {
                klvSh++;
                if (x2 == "ne") {
                    drawNOT(g, x, y, "X2");
                } else {
                    drawPovt(g, x, y, "X2");
                }
                if (klvSh % 2 == 1) {
                    y += 10;
                } else {
                    y += 25;
                }
            }
            if (x3 != "") {
                klvSh++;
                if (x3 == "ne") {
                    drawNOT(g, x, y, "X3");
                } else {
                    drawPovt(g, x, y, "X3");
                }
                if (klvSh % 2 == 1) {
                    y += 10;
                } else {
                    y += 25;
                }
            }
            if (x4 != "") {
                klvSh++;
                if (x4 == "ne") {
                    drawNOT(g, x, y, "X4");
                } else {
                    drawPovt(g, x, y, "X4");
                }
                if (klvSh % 2 == 1) {
                    y += 10;
                } else {
                    y += 25;
                }
            }
            x += 25;
            y = 4 + k * 70;
            if (klvSh == 1) {
                if (flag) {
                    drawAND1(g, x, y);
                } else {
                    drawOR1(g, x, y);
                }
                g.drawLine(x + 30, y + 15, x + 30, y + 27);
                g.drawLine(x + 30, y + 27, x + 40, y + 27);
                if (flag) {
                    drawAND1(g, x + 40, y + 17);
                } else {
                    drawOR1(g, x + 40, y + 17);
                }
                if (k % 2 == 0) {
                    g.drawLine(x + 70, y + 32, x + 70, y + 62);
                    g.drawLine(x + 70, y + 62, x + 80, y + 62);
                } else {
                    g.drawLine(x + 70, y + 32, x + 70, y + 2);
                    g.drawLine(x + 70, y + 2, x + 80, y + 2);
                }
            }
            if (klvSh == 2) {
                if (flag) {
                    drawAND2(g, x, y);
                } else {
                    drawOR2(g, x, y);
                }
                g.drawLine(x + 30, y + 15, x + 30, y + 27);
                g.drawLine(x + 30, y + 27, x + 40, y + 27);
                if (flag) {
                    drawAND1(g, x + 40, y + 17);
                } else {
                    drawOR1(g, x + 40, y + 17);
                }
                if (k % 2 == 0) {
                    g.drawLine(x + 70, y + 32, x + 70, y + 62);
                    g.drawLine(x + 70, y + 62, x + 80, y + 62);
                } else {
                    g.drawLine(x + 70, y + 32, x + 70, y + 2);
                    g.drawLine(x + 70, y + 2, x + 80, y + 2);
                }
            }
            if (klvSh == 3) {
                if (flag) {
                    drawAND2(g, x, y);
                } else {
                    drawOR2(g, x, y);
                }
                g.drawLine(x + 30, y + 15, x + 30, y + 27);
                g.drawLine(x + 30, y + 27, x + 40, y + 27);
                if (flag) {
                    drawAND2(g, x + 40, y + 17);
                } else {
                    drawOR2(g, x + 40, y + 17);
                }
                y += 35;
                if (flag) {
                    drawAND1(g, x, y);
                } else {
                    drawOR1(g, x, y);
                }
                g.drawLine(x + 30, y + 15, x + 30, y + 2);
                g.drawLine(x + 30, y + 2, x + 40, y + 2);
                y -= 35;
                if (k % 2 == 0) {
                    g.drawLine(x + 70, y + 32, x + 70, y + 62);
                    g.drawLine(x + 70, y + 62, x + 80, y + 62);
                } else {
                    g.drawLine(x + 70, y + 32, x + 70, y + 2);
                    g.drawLine(x + 70, y + 2, x + 80, y + 2);
                }
            }
            if (klvSh == 4) {
                if (flag) {
                    drawAND2(g, x, y);
                } else {
                    drawOR2(g, x, y);
                }
                g.drawLine(x + 30, y + 15, x + 30, y + 27);
                g.drawLine(x + 30, y + 27, x + 40, y + 27);
                if (flag) {
                    drawAND2(g, x + 40, y + 17);
                } else {
                    drawOR2(g, x + 40, y + 17);
                }
                y += 35;
                if (flag) {
                    drawAND2(g, x, y);
                } else {
                    drawOR2(g, x, y);
                }
                g.drawLine(x + 30, y + 15, x + 30, y + 2);
                g.drawLine(x + 30, y + 2, x + 40, y + 2);
                y -= 35;
                if (k % 2 == 0) {
                    g.drawLine(x + 70, y + 32, x + 70, y + 62);
                    g.drawLine(x + 70, y + 62, x + 80, y + 62);
                } else {
                    g.drawLine(x + 70, y + 32, x + 70, y + 2);
                    g.drawLine(x + 70, y + 2, x + 80, y + 2);
                }
            }
            k++;
        }
        if (k == 1) {
            g.setColor(Color.WHITE);
            g.fillRect(70, 0, 100, 100);
            return;
        }
        if (k == 2) {
            if (!flag) {
                drawAND2(g, 120, 56);
            } else {
                drawOR2(g, 120, 56);
            }
            return;
        }
        int klv = k / 2;
        if (k % 2 == 1) {
            klv++;
        }
        int z = 0;
        int kl = k;
        boolean a = false;
        while (klv != 1) {
            switch (kl) {
                case 1:
                case 3:
                case 5:
                case 7:
                    a = true;
                    break;
                case 2:
                case 4:
                case 6:
                case 8:
                    a = false;
                    break;
            }
            k = 0;
            if (a) {
                int x = 120 + z * 40;
                int y = 56 + z * 62;
                if (z > 0)
                    y += 8;
                while (k < klv - 1) {
                    if (!flag) {
                        drawAND2(g, x, y);
                    } else {
                        drawOR2(g, x, y);
                    }
                    if (k % 2 == 0) {
                        g.drawLine(x + 30, y + 15, x + 30, y + 80 + z * 69);
                    } else {
                        g.drawLine(x + 30, y + 15, x + 30, y - 50 - z * 69);
                    }
                    k++;
                    if (z == 0)
                        y += 140;
                    else
                        y += 280;
                }
                if (!flag) {
                    drawAND1(g, x, y);
                } else {
                    drawOR1(g, x, y);
                }
                if (k % 2 == 0) {
                    g.drawLine(x + 30, y + 15, x + 30, y + 80 + z * 69);
                } else {
                    g.drawLine(x + 30, y + 15, x + 30, y - 50 - z * 69);
                }
                k++;
                kl = k;
            } else {
                int x = 120 + z * 40;
                int y = 56 + z * 62;
                if (z > 0)
                    y += 8;
                while (k < klv) {
                    if (!flag) {
                        drawAND2(g, x, y);
                    } else {
                        drawOR2(g, x, y);
                    }
                    if (k % 2 == 0) {
                        g.drawLine(x + 30, y + 15, x + 30, y + 80 + z * 69);
                    } else {
                        g.drawLine(x + 30, y + 15, x + 30, y - 50 - z * 69);
                    }
                    k++;
                    if (z == 0)
                        y += 140;
                    else
                        y += 280;
                }
                kl = k;
            }
            if (klv % 2 == 1) {
                klv++;
            }
            klv /= 2;
            z++;
        }
        int x = 120 + z * 40;
        int y = 56 + z * 62;
        y += 8;
        if (z > 1)
            y += 78;
        if (!flag) {
            drawAND2(g, x, y);
        } else {
            drawOR2(g, x, y);
        }
    }

    private void drawAND1(Graphics g, int x, int y) {
        g.drawRect(x, y, 20, 30);
        g.drawLine(x - 10, y + 10, x, y + 10);
        g.drawLine(x - 5, y + 20, x, y + 20);
        g.drawLine(x - 5, y + 20, x - 5, y + 10);
        g.fillOval(x - 7, y + 8, 4, 4);
        g.setFont(new Font("Times New Roman", 0, 14));
        g.drawString("&", x + 3, y + 19);
        g.drawLine(x + 20, y + 15, x + 30, y + 15);
    }

    private void drawAND2(Graphics g, int x, int y) {
        g.drawRect(x, y, 20, 30);
        g.drawLine(x - 10, y + 10, x, y + 10);
        g.drawLine(x - 10, y + 20, x, y + 20);
        g.setFont(new Font("Times New Roman", 0, 14));
        g.drawString("&", x + 3, y + 19);
        g.drawLine(x + 20, y + 15, x + 30, y + 15);
    }

    private void drawOR1(Graphics g, int x, int y) {
        g.drawRect(x, y, 20, 30);
        g.drawLine(x - 10, y + 10, x, y + 10);
        g.drawLine(x - 5, y + 20, x, y + 20);
        g.drawLine(x - 5, y + 20, x - 5, y + 10);
        g.fillOval(x - 7, y + 8, 4, 4);
        g.setFont(new Font("Times New Roman", 0, 14));
        g.drawString("1", x + 3, y + 19);
        g.drawLine(x + 20, y + 15, x + 30, y + 15);
    }

    private void drawOR2(Graphics g, int x, int y) {
        g.drawRect(x, y, 20, 30);
        g.drawLine(x - 10, y + 10, x, y + 10);
        g.drawLine(x - 10, y + 20, x, y + 20);
        g.setFont(new Font("Times New Roman", 0, 14));
        g.drawString("1", x + 3, y + 19);
        g.drawLine(x + 20, y + 15, x + 30, y + 15);
    }

    private void drawNOT(Graphics g, int x, int y, String x1) {
        g.drawRect(x, y, 15, 8);
        g.drawLine(x - 10, y + 4, x, y + 4);
        g.setFont(new Font("Times New Roman", 0, 8));
        g.drawString("1", x + 6, y + 8);
        g.setFont(new Font("Times New Roman", 0, 8));
        g.drawString(x1, x - 10, y + 4);
        g.drawLine(x + 15, y + 4, x + 25, y + 4);
        g.setColor(Color.WHITE);
        g.fillOval(x + 13, y + 2, 4, 4);
        g.setColor(Color.BLACK);
        g.drawOval(x + 13, y + 2, 4, 4);
    }

    private void drawPovt(Graphics g, int x, int y, String x1) {
        g.drawRect(x, y, 15, 8);
        g.drawLine(x - 10, y + 4, x, y + 4);
        g.setFont(new Font("Times New Roman", 0, 8));
        g.drawString("1", x + 6, y + 8);
        g.setFont(new Font("Times New Roman", 0, 8));
        g.drawString(x1, x - 10, y + 4);
        g.drawLine(x + 15, y + 4, x + 25, y + 4);
    }

}

import javax.swing.*;
import java.awt.*;

public class Win {

    private JFrame frame;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Win window = new Win();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Win() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 892, 525);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        Panel panel = new Panel();
        Bite b = new Bite();
        int[] s ={1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0};
        b.END(s);
        Func F = new Func(b.global1());
        panel.setBackground(Color.WHITE);
        panel.updatePanel(F);
        panel.setBounds(10, 11, 423, 464);
        frame.getContentPane().add(panel);

        Panel panel_1 = new Panel();
        panel_1.setBackground(Color.WHITE);
        Func T = new Func(b.global2());
        panel_1.updatePanel(T);
        panel_1.updatePanel(T);
        panel_1.setBounds(444, 11, 423, 464);
        frame.getContentPane().add(panel_1);
    }
}

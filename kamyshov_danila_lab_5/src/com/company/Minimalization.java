package com.company;

public class Minimalization {

    private StringBuffer stringBuffer = new StringBuffer();
    private String stringResult = "";
    private TruthTable truthTable = new TruthTable();
    private int iteration = -1;

    public void disjunctiveNormalForm(int[][] array) {
        if (array[iteration][truthTable.getCountOfColumns() - 1] == 1) {
            stringBuffer.append("(");
            for (int j = 0; j < truthTable.getCountOfColumns() - 1; j++) {
                if (array[iteration][j] == 0) {
                    if (j == truthTable.getCountOfColumns() - 2) {
                        stringBuffer.append("-X").append(j + 1);
                    } else {
                        stringBuffer.append("-X").append(j + 1).append(" * ");
                    }
                }
                if (array[iteration][j] == 1) {
                    if (j == truthTable.getCountOfColumns() - 2) {
                        stringBuffer.append("X").append(j + 1);
                    } else {
                        stringBuffer.append("X").append(j + 1).append(" * ");
                    }
                }
            }
            if (iteration == truthTable.getCountOfStrings() - 1) {
                stringBuffer.append(" )");
            } else {
                stringBuffer.append(" ) + ");
            }
        }
        stringResult = String.valueOf(stringBuffer);
    }

    public void сonjunctiveNormalForm(int[][] array) {
        if (array[iteration][truthTable.getCountOfColumns() - 1] == 0) {
            stringBuffer.append("(");
            for (int j = 0; j < truthTable.getCountOfColumns() - 1; j++) {
                if (array[iteration][j] == 0) {
                    if (j == truthTable.getCountOfColumns() - 2) {
                        stringBuffer.append("X").append(j + 1);
                    } else {
                        stringBuffer.append("X").append(j + 1).append(" + ");
                    }
                }
                if (array[iteration][j] == 1) {
                    if (j == truthTable.getCountOfColumns() - 2) {
                        stringBuffer.append("-X").append(j + 1);
                    } else {
                        stringBuffer.append("-X").append(j + 1).append(" + ");
                    }
                }
            }
            if (iteration == truthTable.getCountOfStrings() - 1) {
                stringBuffer.append(" )");
            } else {
                stringBuffer.append(" ) * ");
            }
        }
        stringResult = String.valueOf(stringBuffer);
    }


    public String getStringResult() {
        return stringResult;
    }

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    public int getIteration() {
        return iteration;
    }

}

## Лабораторная работа №7 

**ПИбд-22 Майоров Юрий**

### Задание:

Создать транслятор программ (Pascal-FASM)
Предусмотреть проверку синтаксиса и семантики исходной программы.
Результатом работы транслятора является программа для ассемблера FASM,
идентичная по функционалу исходной программе.
Полученная программа должна компилироваться и выполняться без ошибок.

Ссылка на видео: https://yadi.sk/i/dl7ENAND-IUkEg

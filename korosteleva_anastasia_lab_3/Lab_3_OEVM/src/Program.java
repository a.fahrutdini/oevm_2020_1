public class Program {

    private int startSS;
    private String firstDigit;
    private String secondDigit;

    public Program(int startSS, String firstDigit, String secondDigit) {
        this.startSS = startSS;
        this.firstDigit = carryToBinarySS(carryToDecimalSS(firstDigit));
        this.secondDigit = carryToBinarySS(carryToDecimalSS(secondDigit));
    }

    private void swap() {
        if (!compare(firstDigit, secondDigit)) {
            String temp = firstDigit;
            firstDigit = secondDigit;
            secondDigit = temp;
        }
    }


    private Boolean compare(String strFirstNumber, String strSecondNumber) {
        char[] strFirstArray = new StringBuilder(strFirstNumber).reverse().toString().toCharArray();
        char[] strSecondArray = new StringBuilder(strSecondNumber).reverse().toString().toCharArray();

        if (strFirstArray.length > strSecondArray.length) {
            return true;
        }
        if (strFirstArray.length < strSecondArray.length) {
            return false;
        }
        if (strFirstArray.length == strSecondArray.length) {

            for (int i = 0; i < strFirstArray.length; i++) {

                if (strFirstArray[i] == '1' && strSecondArray[i] == '0') {
                    return true;
                }
                if (strFirstArray[i] == '0' && strSecondArray[i] == '1') {
                    return false;
                }
            }
        }
        return true;
    }

    private int carryToDecimalSS(String strNumber) {
        int intNumber = 0;
        char charZero = '0';
        char charNine = '9';
        char letterInSS = 'A' + 10;
        char[] DigitArray = strNumber.toCharArray();

        for (int i = 0; i < DigitArray.length; i++) {
            int temp;
            if (DigitArray[i] >= charZero && DigitArray[i] <= charNine) {
                temp = DigitArray[i] - charZero;
            } else {
                temp = DigitArray[i] - letterInSS;
            }
            intNumber += temp * Math.pow(startSS, DigitArray.length - i - 1);
        }
        return intNumber;
    }

    private String carryToBinarySS(int intNumber) {
        StringBuilder strNumber = new StringBuilder();

        char charZero = '0';

        if (intNumber == 0) {
            strNumber.append("0");
        }

        for (int i = 0; intNumber > 0; i++) {
            strNumber.append((char) (intNumber % 2 + charZero));
            intNumber /= 2;
        }

        return strNumber.toString();
    }

    public String calculate(char action) {
        StringBuilder str = new StringBuilder();

        switch (action) {
            case '+':
                swap();
                str.append(add(firstDigit, secondDigit));
                return str.reverse().toString();
            case '-':
                str.append(subtract(firstDigit, secondDigit)).reverse();
                return removeZeros(str.toString());
            case '*':
                swap();
                str.append(multiply(firstDigit, secondDigit)).reverse();
                return str.toString();
            case '/':
                str.append(divide(firstDigit, secondDigit)).reverse();
                return str.toString();
            default:
                throw new IllegalArgumentException("You enter wrong action");
        }
    }

    private String add(String strFirstNumber, String strSecondNumber) {
        char[] firstDigitArray = strFirstNumber.toCharArray();
        char[] secondDigitArray = strSecondNumber.toCharArray();
        StringBuilder result = new StringBuilder();
        int transfer = 0;
        int i = 0;
        char charZero = '0';
        char charOne = '1';

        //из-за того, что по ходу работы программы меняется transfer и добавляется 0 или 1 в строку,
        //чтобы разбить этот метод на подметоды надо будет сделать String метод, который бы возвращал 0 или 1
        //и делать ГЛОБАЛЬНУЮ переменную transfer, я думаю, что это нецелесеобразно, можно ли избежать разбиения?
        while (i < secondDigitArray.length) {
            if (transfer == 0) {
                if (firstDigitArray[i] == charZero && secondDigitArray[i] == charZero) {
                    result.append("0");
                    transfer = 0;
                }
                if ((firstDigitArray[i] == charZero && secondDigitArray[i] == charOne) || (firstDigitArray[i] == charOne && secondDigitArray[i] == charZero)) {
                    result.append("1");
                    transfer = 0;
                }
                if (firstDigitArray[i] == charOne && secondDigitArray[i] == charOne) {
                    result.append("0");
                    transfer = 1;
                }
            } else {
                if (firstDigitArray[i] == charZero && secondDigitArray[i] == charZero) {
                    result.append("1");
                    transfer = 0;
                }
                if ((firstDigitArray[i] == charZero && secondDigitArray[i] == charOne) || (firstDigitArray[i] == charOne && secondDigitArray[i] == charZero)) {
                    result.append("0");
                    transfer = 1;
                }
                if (firstDigitArray[i] == '1' && secondDigitArray[i] == charOne) {
                    result.append("1");
                    transfer = 1;
                }
            }
            i++;
        }

        while (i < firstDigitArray.length) {
            if (firstDigitArray[i] == charZero && transfer == 0) {
                result.append("0");
                transfer = 0;
            }
            if ((firstDigitArray[i] == charZero && transfer == 1) || (firstDigitArray[i] == '1' && transfer == 0)) {
                result.append("1");
                transfer = 0;
            }
            if (firstDigitArray[i] == charOne && transfer == 1) {
                result.append("0");
                transfer = 1;
            }
            i++;
        }

        if (transfer == 1) {
            result.append("1");
        }
        return result.toString();
    }

    private String subtract(String strFirstNumber, String strSecondNumber) {
        StringBuilder result = new StringBuilder();

        strSecondNumber = flipCode(strSecondNumber);
        result.append(add(strFirstNumber, strSecondNumber));
        result.deleteCharAt(result.length() - 1);

        return result.toString();
    }

    private String multiply(String strFirstNumber, String strSecondNumber) {
        char[] strArray2 = strSecondNumber.toCharArray();
        StringBuilder strArray1 = new StringBuilder(strFirstNumber);
        String result = "0";
        int iIndex = 0;

        for (int i = 0; i < strArray2.length; i++) {
            if (strArray2[i] == '1') {
                for (int j = 0; j < i - iIndex; j++) {
                    strArray1.insert(0, '0');
                }
                result = add(strArray1.toString(), result);
                iIndex = i;
            }
        }

        return result;
    }

    private String divide(String strFirstNumber, String strSecondNumber) {
        String comparison = strSecondNumber;
        String result = "0";

        if (strSecondNumber.toCharArray().length == 1 && strSecondNumber.toCharArray()[0] != '1') {
            throw new ArithmeticException("You try division by zero");
        }

        while (compare(strFirstNumber, comparison)) {
            result = add(result, "1");
            comparison = add(comparison, strSecondNumber);
        }
        return result;
    }

    private String flipCode(String strNumber) {
        StringBuilder string = new StringBuilder();
        StringBuilder result = new StringBuilder();
        char[] digitArray = strNumber.toCharArray();

        int i = 0;
        while (i < secondDigit.length()) {
            if (digitArray[i] == '1') {
                string.append("0");
            } else {
                string.append("1");
            }
            i++;
        }
        while (i < firstDigit.length()) {
            string.append("1");
            i++;
        }
        result.append(add(string.toString(), "1"));
        return result.toString();
    }

    private String removeZeros(String strNumber) {
        StringBuilder result = new StringBuilder();
        char[] digitArray = strNumber.toCharArray();
        int i = 0;
        while (digitArray[i] == '0') {
            i++;
            if (i == strNumber.length()) {
                return "0";
            }
        }
        while (i < strNumber.length()) {
            result.append(digitArray[i]);
            i++;
        }
        return result.toString();
    }
}
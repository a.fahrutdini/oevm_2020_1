package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int N = 16;
        int M = 5;
        int[][] truthTable = new int[N][M];
        int enter = 0;
        Scanner in = new Scanner(System.in);
        while (enter != 1 && enter != 2) {
            System.out.printf("enter 1 for DNF\nenter 2 for CNF\nenter: ");
            enter = in.nextInt();
        }
        TruthTable table = new TruthTable(N, M, truthTable);
        truthTable = table.truthTable;
        table.printTruthTable(truthTable);
        if (enter == 1) {
            System.out.print("\nDNF: ");
            table.toDisjunctiveNormalForm(truthTable);
        } else if (enter == 2) {
            System.out.print("\nCNF: ");
            table.toConjunctiveNormalForm(truthTable);
        }
    }
}


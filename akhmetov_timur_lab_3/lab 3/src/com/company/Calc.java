package com.company;


import java.util.Scanner;

public class Calc {

    public Calc(String strFirst, String strSecond, int base){
        System.out.println("Выберите действие:");
        Scanner sc = new Scanner(System.in);
        String action = sc.next();

        long first = Converter.convertToDec(strFirst, base);
        long second = Converter.convertToDec(strSecond, base);

        strFirst = Converter.convertToBinary(first);
        strSecond = Converter.convertToBinary(second);

        switch (action){
            case "+":
                System.out.println(Calc.addition(strFirst, strSecond));
                break;
            case "-":
                System.out.println(Calc.subtraction(strFirst, strSecond));
                break;
            case "*":
                System.out.println(Calc.multiplication(strFirst, strSecond));
                break;
            case "/":
                System.out.println(Calc.division(strFirst, strSecond));
                break;
            default:
                System.out.println("Некорректное действие!");
                break;
        }
    }

    private static String addition(String strFirst, String strSecond) {
        int len1 = strFirst.length();
        int len2 = strSecond.length();
        int carry = 0;
        String res = "";
        int maxLen = Math.max(len1, len2);
        for (int i = 0; i < maxLen; i++) {
            int p = i < len1 ? strFirst.charAt(len1 - 1 - i) - '0' : 0;
            int q = i < len2 ? strSecond.charAt(len2 - 1 - i) - '0' : 0;

            int tmp = p + q + carry;
            carry = tmp / 2;
            res = tmp % 2 + res;
        }
            return (carry == 0) ? res : "1" + res;
    }

    private static String subtraction(String strFirst, String strSecond) {

        String res = "";

        int cnt = strFirst.length() - strSecond.length();

        char[] additionalCode = new char[strFirst.length()];

        int firstInt = Math.toIntExact(Converter.convertToDec(strFirst, 2));
        int secondInt = Math.toIntExact(Converter.convertToDec(strSecond, 2));


        for (int i = 0; i < cnt; i++){
            additionalCode[i] = '1';
        }
        for (int i = cnt; i < additionalCode.length; ++i) {
            if (strSecond.charAt(i - cnt) == '1') {
                additionalCode[i] = '0';
            } else if (strSecond.charAt(i - cnt) == '0') {
                additionalCode[i] = '1';
            }
        }
        String strAdditionalCode = String.valueOf(additionalCode);
        strAdditionalCode = addition(strAdditionalCode, "1");
        String sum = addition(strAdditionalCode, strFirst);

        if (sum.length() > 1) {
            boolean zeros = true;
            for (int i = 1; i < sum.length(); i++) {
                if (sum.charAt(i) == '1'){
                    zeros = false;
                }
                else {
                    if (zeros) {
                        continue;
                    }
                }
                res += sum.charAt(i);
            }
        }
        if (secondInt > firstInt){
            res = "-" + res;
        }
        return res;
    }

    private static String multiplication(String strFirst, String strSecond){
        int secondInt = Math.toIntExact(Converter.convertToDec(strSecond, 2));
        String res = "";
        for(int i = 0; i < secondInt; i++){
            res = addition(res, strFirst);
        }
        return res;
    }

    private static String division(String strFirst, String strSecond){
        long cnt = 0;
        while (strFirst.length() > strSecond.length()) {
            strFirst = subtraction(strFirst, strSecond);
            cnt++;
        }
        String result = Converter.convertToBinary(cnt + 1);
        return result;

    }
}

package com.company;


public class Converter {
    public static long convertToDec(String num, int base) {
        String numRev = new StringBuilder(new String(num)).reverse().toString();
        char[] charArrNum = numRev.toCharArray();
        long res = 0;

        for(int i = charArrNum.length - 1; i >= 0; i--){
            switch (charArrNum[i]){
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    res += (charArrNum[i] - '0') * Math.pow(base, i);
                    break;
                case 'A':
                    res += 10 * Math.pow(base, i);
                    break;
                case 'B':
                    res += 11 * Math.pow(base, i);
                    break;
                case 'C':
                    res += 12 * Math.pow(base, i);
                    break;
                case 'D':
                    res += 13 * Math.pow(base, i);
                    break;
                case 'E':
                    res += 14 * Math.pow(base, i);
                    break;
                case 'F':
                    res += 15 * Math.pow(base, i);
                    break;
            }


        }
        return res;
    }

    public static String convertToBinary(long num){
        String res = "";
        while(num != 0){
            res += Integer.toString(Math.toIntExact(num % 2));
            num /= 2;
        }
        String resRev = new StringBuilder(new String(res)).reverse().toString();
        return resRev;
    }
}

import java.util.Scanner;

public class Main {
    private static Scanner input = new Scanner( System.in );

    public static void main(String[] args) {
        MinForm minForm = new MinForm();
        int[][] truthTable = new int[16][5];
        minForm.data( truthTable );
        minForm.matrixFilling();
        System.out.printf( "\n1 - DNF\n2 - KNF\nEnter: " );
        int command = input.nextInt();
        while (command != 1 && command != 2) {
            System.out.printf( "1 - DNF\n2 - KNF\nEnter: " ); // если вводим неверную команду то вновь просим ввод
            command = input.nextInt();
        }
        if (command == 1) // DNF
            System.out.printf( "Result: %s", minForm.disjunctiveNormalForm() );
        else
            System.out.printf( "Result: %s", minForm.conjunctiveNormalForm() ); // KNF
    }
}

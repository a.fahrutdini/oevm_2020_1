import java.util.Scanner;

public class MinForm {
    private String[] codeX = {"X1", "X2", "X3", "X4"};             // Элементы таблицы истинности
    private int[][] _matrix;                                       // Основная матрица
    private String _resultString;                                 // Результат
    final int N = 16;                                              // Кол-во строк
    final int M = 5;                                               // Кол-во столбцов

    public void data(int[][] TruthTable) {
        _matrix = TruthTable;
        _resultString = "";
    }

    private int[] convertTo2NS(int value) {
        int[] elements = {0, 0, 0, 0};
        int i = elements.length - 1;

        while (value >= 2) {
            elements[i] = value % 2;
            value /= 2;
            i--;
        }
        elements[i] = value;
        return elements;
    }

    public void matrixFilling() {
        int[] tablesElements;
        for (int i = 0; i < N; i++) {
            tablesElements = convertTo2NS( i );
            for (int j = 0; j < tablesElements.length; j++) {
                System.out.printf( "%d ", tablesElements[j] );
                _matrix[i][j] = tablesElements[j];
            }
            _matrix[i][M - 1] = (int) (Math.random() * 2);
            System.out.printf( " = %d\n", _matrix[i][M - 1] );
        }
    }

    public String conjunctiveNormalForm() {
        Boolean firstElement = false;                // переменная для определения первой скобки
        // перед которой не ставится знак "*"
        for (int i = 0; i < N; i++) {
            if (_matrix[i][M - 1] == 0)           // КНФ производит поиск по 0
            {
                if (!firstElement)                    // проверяем: является i-ая группа переменных первой скобкой
                    _resultString += "(";
                else
                    _resultString += " * (";

                for (int j = 0; j < M - 1; j++)    // в данном цикле происходит кодирование 1 и 0 i-ой переменной
                {                                 // в "X_i" с нужным знаком
                    if (j == 0)                   // проверка на первый элемент в группе (перед ним не ставится "+")
                    {
                        if (_matrix[i][j] == 1)                       // если 1 => "-"
                            _resultString += "-" + codeX[j];
                        else                                          // если 0 => знак у переменной не ставим
                            _resultString += codeX[j];
                    } else                                              // если элемент не первый в группе (X2, X3, X4)
                    {
                        if (_matrix[i][j] == 1)                       // если 1 => "-"
                            _resultString += " + -" + codeX[j];
                        else                                          // если 0 => знак у переменной не ставим
                            _resultString += " + " + codeX[j];
                    }
                }
                _resultString += ")";                                 // в конце i-ой группы закрываем скобку
                firstElement = true;
            }
        }
        return _resultString;
    }

    public String disjunctiveNormalForm()            // аналогичный метод, только ДНФ поиск производится по 1
    {                                                // "+" меняем на "*", и в обратную сторону так же
        Boolean firstGroup = false;

        for (int i = 0; i < N; i++) {
            if (_matrix[i][M - 1] == 1) {
                if (!firstGroup)
                    _resultString += "(";
                else
                    _resultString += " + (";

                for (int j = 0; j < M - 1; j++) {
                    if (j == 0) {
                        if (_matrix[i][j] == 0)
                            _resultString += "-" + codeX[j];
                        else
                            _resultString += codeX[j];
                    } else {
                        if (_matrix[i][j] == 0)
                            _resultString += " * -" + codeX[j];
                        else
                            _resultString += " * " + codeX[j];
                    }
                }
                _resultString += ")";
                firstGroup = true;
            }
        }
        return _resultString;
    }
}

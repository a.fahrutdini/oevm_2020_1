package sample;

import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PascalReader {

    public static void readPascalFile(String name, DataBase dataBase) {
        try (FileReader reader = new FileReader(name)) {
            Scanner scanner = new Scanner(reader);
            StringBuilder textStringBuilder = new StringBuilder();
            while (scanner.hasNextLine()) {
                textStringBuilder.append(scanner.nextLine()).append("\n");
            }
            String code = textStringBuilder.toString();
            codeProcessing(code, dataBase);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void codeProcessing(String code, DataBase dataBase) {
        variableProcessing(code, dataBase);
        commandProcessing(code, dataBase);
    }

    private static void variableProcessing(String code, DataBase dataBase) {
        Pattern patternVariablesString = Pattern.compile("(?<=var\\n)[\\w\\W]*(?=\\nbegin)");
        Matcher matcherVariablesString = patternVariablesString.matcher(code);

        if (matcherVariablesString.find()) {
            String variables = matcherVariablesString.group();

            Pattern patternVariables = Pattern.compile("\\b[a-zA-Z][\\w]*\\b");
            Matcher matcherVariables = patternVariables.matcher(variables);

            while (matcherVariables.find()) {
                if (!matcherVariables.group().equals("integer")) {
                    dataBase.addVariable(matcherVariables.group() + " dd ?\n");
                }
            }
        }
    }

    private static void commandProcessing(String code, DataBase dataBase) {
        Pattern patternCommandsString = Pattern.compile("(?<=begin\\n)[\\w\\W]*(?=\\nend.)");
        Matcher matcherCommandString = patternCommandsString.matcher(code);
        if (matcherCommandString.find()) {
            String commands = matcherCommandString.group();

            Pattern patternCommands = Pattern.compile("[\\w][\\w /,=*+:()'-]*;");
            Matcher matcherCommands = patternCommands.matcher(commands);

            while (matcherCommands.find()) {

                String command = matcherCommands.group();
                String valueWithoutQuotes = null;
                if (command.contains("(")) {
                    valueWithoutQuotes = command.substring(command.indexOf('(') + 1, command.indexOf(')'));
                }

                if (command.contains("readln")) {
                    dataBase.addReadCommand(valueWithoutQuotes);
                } else if (command.contains("write")) {
                    writeProcessing(command, dataBase, valueWithoutQuotes);
                } else if (command.contains(":=")) {
                    operationProcessing(command, dataBase);
                }
            }
        }
    }

    private static int newVariablesCount = 0;

    private static void writeProcessing(String command, DataBase dataBase, String valueWithoutQuotes) {
        String endVariable = "', 0\n";
        String endVariableNewLine = ", 0dh, 0ah, 0\n";
        if (command.contains("'")) {
            String value = command.substring(command.indexOf('(') + 2, command.indexOf(')') - 1);
            String newVariable = "string" + newVariablesCount + " db '" + value;

            if (command.contains("writeln")) {
                newVariable += endVariableNewLine;
            } else {
                newVariable += endVariable;
            }

            dataBase.addVariable(newVariable);
            dataBase.addWriteCommand("string" + newVariablesCount);
            newVariablesCount++;
        } else {
            if (command.contains("writeln")) {
                dataBase.addWritelnCommand(valueWithoutQuotes);
            } else {
                dataBase.addWriteCommand(valueWithoutQuotes);
            }
        }
    }

    private static void operationProcessing(String command, DataBase dataBase) {
        String result = command.substring(0, command.indexOf(":") - 1);
        if (command.contains("+")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("+") - 1);
            String secondElement = command.substring(command.indexOf("+") + 2, command.length() - 1);
            dataBase.addMathsCommand(result + " " + firstElement + " " + secondElement + " add");
        } else if (command.contains("-")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("-") - 1);
            String secondElement = command.substring(command.indexOf("-") + 2, command.length() - 1);
            dataBase.addMathsCommand(result + " " + firstElement + " " + secondElement + " sub");
        } else if (command.contains("*")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("*") - 1);
            String secondElement = command.substring(command.indexOf("*") + 2, command.length() - 1);
            dataBase.addMathsCommand(result + " " + firstElement + " " + secondElement + " imul");
        } else if (command.contains("div")) {
            String firstElement = command.substring(command.indexOf("=") + 2, command.indexOf("div") - 1);
            String secondElement = command.substring(command.indexOf("div") + 4, command.length() - 1);
            dataBase.addMathsCommand(result + " " + firstElement + " " + secondElement + " div");
        }
    }
}
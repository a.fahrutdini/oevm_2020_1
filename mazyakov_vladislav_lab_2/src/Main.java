import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        Scanner sc = new Scanner(System.in);
        StringBuffer answer = new StringBuffer();
        String start_system_st = sc.nextLine();
        String finish_system_st = sc.nextLine();
        String number = sc.nextLine();
        int start_system = Integer.parseInt(start_system_st);
        int finish_system = Integer.parseInt(finish_system_st);
        solution.transferToThe10Digit(start_system, number);
        if (solution.err == 0) {
            solution.endTransfer(finish_system, answer);
        }
    }
}
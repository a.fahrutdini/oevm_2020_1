Лабораторная работа 1

Комплектующие|Kомментарии
:-|-:
Процессор: AMD Ryzen 9 3900XT, BOX |процессоры amd являются лучшими в соотношение цена/качество, данная модель одна из новых и будет актуально еще минимум 5 лет
Материнская плата: ASUS TUF B450M-PLUS GAMING | сочетается с процессором от amd и считается надежной и долго служащей
Видеокарта: nVidia GeForce GTX 1050TI 4GT OC | самая мощная среди бюджетных игровых видеокарт
Модуль памяти: PATRIOT Viper Steel PVS432G300C6 DDR4 — 32ГБ | более, чем достаточное количество памяти
Жесткий диск: WD Caviar Blue WD10EZEX | хватит объема для всего необходимого
Корпус: ATX AEROCOOL Cylon, белый | красивый корпус
Блок питания: THERMALTAKE Smart RGB 700, 700Вт, черный | надежный и прослужит долго
Система охлаждения: Кулер AeroCool Verkho 3 | хорошая защита от перегрева
общая цена: +-100000р.
package com.company;
import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
    MinimizingBooleanFunctions MBF;
    public MyPanel(MinimizingBooleanFunctions MBF) {
        this.MBF = MBF;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        MBF.draw(g);
    }
}

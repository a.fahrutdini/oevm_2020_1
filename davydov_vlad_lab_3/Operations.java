package com.company;

import java.util.Scanner;

public class Operations {
    public String changeTheNotation(int finiteNotation, int initialNotation, String number) {
        long number_ = 0;
        final int numberLength = number.length();
        for (int i = 0; i < numberLength; i++) {
            long symbol = number.charAt(i) - 48;
            if (symbol > 9) symbol -= 7;
            for (int j = numberLength - 1; j > i; j--) {
                symbol *= initialNotation;
            }
            number_ += symbol;
        }
        long auxiliaryForCount = number_;
        int count = 0;
        while (auxiliaryForCount > 0) {
            auxiliaryForCount /= finiteNotation;
            count++;
        }
        if (count == 0) count++;
        char[] resultArr = new char[count];
        for (int i = 0; i < count; i++) {
            resultArr[i] += number_ % finiteNotation;
            resultArr[i] += 48;
            if (resultArr[i] > 57) resultArr[i] += 7;
            number_ /= finiteNotation;
        }
        StringBuilder result = new StringBuilder();
        for (int i = count - 1; i >= 0; i--) {
            result.append(resultArr[i]);
        }
        return result.toString();
    }

    public char[] flipTheArray(char[] massive) {
        for (int i = 0; i < massive.length / 2; i++) {
            char tmp = massive[i];
            massive[i] = massive[massive.length - i - 1];
            massive[massive.length - i - 1] = tmp;
        }
        return massive;
    }

    public String performSum(String binary_number_1, String binary_number_2) {
        final int length = binary_number_1.length() + binary_number_2.length();
        char[] result = new char[length];

        char[] number_1_ = new char[length]; // увеличиваем кол-во ячеек в массиве number_1_ до binary_number_1.length()+binary_number_2.length()
        char[] number_2_ = new char[length];
        for (int j = 0; j < length; j++) {
            number_1_[j] = '0';
            number_2_[j] = '0';
        }
        for (int j = 0; j < binary_number_1.length(); j++) {
            number_1_[j] = flipTheArray(binary_number_1.toCharArray())[j];
        }
        for (int j = 0; j < binary_number_2.length(); j++) {
            number_2_[j] = flipTheArray(binary_number_2.toCharArray())[j];
        }
        boolean flag = false; // flag , отвечающий за перенос в следующий разряд
        for (int d = 0; d < length; d++) {
            result[d] = '0';
        }
        for (int i = 0; i < length; i++) {
            if ((number_1_[i] == '0' && number_2_[i] == '0') && (!flag)) {
                result[i] = '0';
                flag = false;
            } else if ((number_1_[i] == '0' && number_2_[i] == '0') && (flag)) {
                result[i] = '1';
                flag = false;
            } else {
                boolean b = (number_1_[i] == '1' && number_2_[i] == '0') || (number_1_[i] == '0' && number_2_[i] == '1');
                if (b && (flag)) {
                    result[i] = '0';
                    flag = true;
                } else if (b && (!flag)) {
                    result[i] = '1';
                    flag = false;
                } else if (((number_1_[i] == '1' && number_2_[i] == '1')) && (flag)) {
                    result[i] = '1';
                    flag = true;
                } else if (((number_1_[i] == '1' && number_2_[i] == '1')) && (!flag)) {
                    result[i] = '0';
                    flag = true;
                }
            }
        }
        result = flipTheArray(result);
        int i = 0;
        while (result[i] == '0') {
            i++;
        }
        StringBuilder res = new StringBuilder("");
        for (int k = i; k < length; k++) {
            res = res.append(result[k]);
        }
        return res.toString();
    }

    public String performSubtraction(String number1, String number2) {
        if (number1.equals(number2)) {
            return "0";
        }
        StringBuilder result = new StringBuilder("");
        boolean isNegative = false;
        if (Long.valueOf(changeTheNotation(10, 2, number1)) < Long.valueOf(changeTheNotation(10, 2, number2))) {
            isNegative = true;
        }
        char[] number1Arr;
        char[] number2Arr;
        int endValue = number1.length();
        if (isNegative) {
            endValue = number2.length();
            number2Arr = number2.toCharArray();
            number1Arr = getDopCode(number1, number2Arr.length).toCharArray();
        } else {
            number1Arr = number1.toCharArray();
            number2Arr = getDopCode(number2, number1Arr.length).toCharArray();
        }
        int number1Current = 0;
        if (number1Arr[number1Arr.length - 1] == '1') {
            number1Current = 1;
        }
        int number2Current = 0;
        if (number2Arr[number2Arr.length - 1] == '1') {
            number2Current = 1;
        }
        int dopCurrent = 0;
        int i = 2;
        while (true) {
            int currentValue = number1Current + number2Current + dopCurrent;
            number1Current = 0;
            number2Current = 0;
            if (currentValue > 1) {
                result.append(currentValue % 2);
                dopCurrent = 1;
            } else {
                result.append(currentValue);
                dopCurrent = 0;
            }
            int possibleLength = number1Arr.length - i;
            if (possibleLength >= 0) {
                number1Current = 1;
                if (number1Arr[possibleLength] == '0') {
                    number1Current = 0;
                }
            }
            possibleLength = number2Arr.length - i;
            if (possibleLength >= 0) {
                number2Current = 1;
                if (number2Arr[possibleLength] == '0') {
                    number2Current = 0;
                }
            }
            if (i > endValue) {
                break;
            }
            i++;
        }
        if (isNegative) {
            result.append('-');
        }
        return deleteZeros(result.reverse(), isNegative).toString();
    }

    public String performMultiply(String number1, String number2) {
        final long number1Dec = Long.valueOf(changeTheNotation(10, 2, number1));
        final long number2Dec = Long.valueOf(changeTheNotation(10, 2, number2));
        if (number1Dec == 0 || number2Dec == 0) {
            return "0";
        }
        String result = number1;
        String multiplyValue = number1;
        long endMultiplyValue = number2Dec;
        if (number1Dec < number2Dec) {
            result = number2;
            multiplyValue = number2;
            endMultiplyValue = number1Dec;
        }
        endMultiplyValue--;
        for (long i = 0; i < endMultiplyValue; i++) {
            result = performSum(result, multiplyValue);
        }
        return result;
    }

    public String performDivide(String number1, String number2) {
        final long number1Dec = Long.valueOf(changeTheNotation(10, 2, number1));
        final long number2Dec = Long.valueOf(changeTheNotation(10, 2, number2));
        if (number2Dec == 0) {
            return "На ноль делить нельзя!";
        }
        if (number1Dec < number2Dec) {
            return "0";
        }
        if (number1Dec == number2Dec) {
            return "1";
        }
        if (number2Dec == 1) {
            return number1;
        }
        String appendResult = number2;

        long result = 0;
        while (Long.valueOf(changeTheNotation(10, 2, appendResult)) <= number1Dec) {
            appendResult = performSum(appendResult, number2);
            result++;
        }
        return changeTheNotation(2, 10, Long.toString(result));
    }

    private StringBuilder deleteZeros(StringBuilder number, boolean isNegative) {
        int zeros = 0;
        for (int i = 0; i < number.length(); i++) {
            if (number.charAt(i) == '1') {
                break;
            }
            zeros++;
        }
        if (isNegative) {
            number.delete(1, zeros);
        } else {
            number.delete(0, zeros);
        }
        return number;
    }

    public String getDopCode(String number, int length) {
        StringBuilder result = new StringBuilder("");
        char[] numberArr = number.toCharArray();
        for (int i = 0; i < (length - numberArr.length); i++) {
            result.append(1);
        }
        for (int i = 0; i < numberArr.length; i++) {
            int currentNumber = 0;

            if (numberArr[i] == '0') {
                currentNumber = 1;
            }
            result.append(currentNumber);
        }
        return performSum(result.toString(), "1");
    }

    public String getCorrectNum(int notation) {
        String input = "";
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите число в исходной системе счисления: ");
        input = scan.nextLine();
        input = input.trim();
        input = input.toLowerCase();
        while (!isCorrectNum(notation, input)) {
            System.out.print("Повторите ввод: ");
            input = scan.nextLine();
            input = input.trim();
            input = input.toLowerCase();
        }
        return input;
    }

    public boolean isCorrectNum(int notation, String number) {
        char[] numberArr = number.toCharArray();
        int currentNumber = 0;
        for (int i = 0; i < numberArr.length; i++) {
            if (Character.isDigit(numberArr[i])) {
                currentNumber = (int) (numberArr[i]) - 48;
            } else {
                if (numberArr[i] > 96 && numberArr[i] < 103) {
                    currentNumber = (int) numberArr[i] - 87;
                } else return false;
            }
            if (currentNumber >= notation) {
                return false;
            }
        }
        return true;
    }

    public String getCorrectNotation() {
        String input = "";
        Scanner scan = new Scanner(System.in);
        System.out.print("Ведите исходную систему счисления(2-16): ");
        input = scan.nextLine();
        input = input.trim();
        while (!isCorrectNotation(input)) {
            System.out.print("Повторите ввод: ");
            input = scan.nextLine();
            input = input.trim();
        }
        return input;
    }


    public boolean isCorrectNotation(String input) {
        char[] inputArr = input.toCharArray();
        if (inputArr.length > 2 || inputArr.length < 1) {
            return false;
        }
        for (int i = 0; i < inputArr.length; i++) {
            if (!Character.isDigit(inputArr[i])) {
                return false;
            }
        }
        if (Integer.parseInt(input) > 16 || Integer.parseInt(input) < 2) {
            return false;
        }
        return true;
    }

    public boolean isCorrectOperation(String input) {
        if (input.length() > 1 || input.length() < 1) {
            return false;
        }
        if (input.equals("+") || input.equals("*") || input.equals("-") || input.equals("/")) {
            return true;
        }
        return false;
    }

    public String getCorrectOperation() {
        String input = "";
        Scanner scan = new Scanner(System.in);
        System.out.print("Укажите операцию(+ - * /): ");
        input = scan.nextLine();
        input = input.trim();
        input = input.toLowerCase();
        while (!isCorrectOperation(input)) {
            System.out.print("Повторите ввод: ");
            input = scan.nextLine();
            input = input.trim();
        }
        return input;
    }

    public String performAnOperation(String operation, String number1, String number2) {
        switch (operation) {
            case "+":
                return performSum(number1, number2);
            case "-":
                return performSubtraction(number1, number2);
            case "/":
                return performDivide(number1, number2);
            case "*":
                return performMultiply(number1, number2);
            default:
                return "";
        }
    }

}

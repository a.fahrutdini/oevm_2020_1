package com.company;

public class Main {

    public static void main(String[] args) {
        Operations arithmetic = new Operations();
        int initialNotation = 0;
        final int binaryNotation = 2;
        String operation = "";
        String number1 = "";
        String number2 = "";
        initialNotation = Byte.parseByte(arithmetic.getCorrectNotation());
        number1 = arithmetic.changeTheNotation(binaryNotation, initialNotation, arithmetic.getCorrectNum(initialNotation));
        number2 = arithmetic.changeTheNotation(binaryNotation, initialNotation, arithmetic.getCorrectNum(initialNotation));
        operation = arithmetic.getCorrectOperation();
        System.out.println("Ответ: " + arithmetic.performAnOperation(operation, number1, number2));
    }
}
public class Converter {
    protected static int convertTo10(char[] number, int initialSN) throws NotValidateException {
        int result = 0;
        int k = 0;
        for (int i = number.length - 1; i >= 0; i--, k++) {
            result += validate(number[i]) * Math.pow(initialSN, k);
        }
        return result;
    }

    protected static int validate(char digit) throws NotValidateException {
        int number;
        if (digit >= '0' && digit <= '9') {
            number = digit - '0';
        } else {
            if (digit >= 'A' && digit <= 'J') {
                number = digit - 'A' + 10;
            } else throw new NotValidateException();
        }
        return  number;
    }

    protected static String convertToFinal(int number) {
        StringBuilder result = new StringBuilder();
        String letter;
        while (number > 0) {
            int res = number / 2;
            int remainder = number % 2;
            letter = Integer.toString(remainder);
            result.insert(0, letter);
            number = res;
        }
        return result.toString();
    }

    protected static String convert(int initialSN, char[] number) throws NotValidateException {
        int k = convertTo10(number, initialSN);
        return convertToFinal(k);
    }
}

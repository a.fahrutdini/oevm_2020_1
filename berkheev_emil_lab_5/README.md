## Лабораторная работа №5

### Студент группы ПИбд-21

### Берхеев Эмиль

### Ссылка на видео: https://youtu.be/0Uj-PbW9LQk

##### Техническое задание:
Разработать программный продукт для визуализации работы
булевых функций. Входные данные из 4 лабы.

##### Пример работы программы: 
* ДНФ:
![title](https://sun9-3.userapi.com/impg/1j5BvyUgAm7O3mQaNzHD4NWR7zZUz79P5WdIZA/7-SqToid6YU.jpg?size=535x593&quality=96&proxy=1&sign=79808cfa11c5f597934a785a1a84337e)
* КНФ:
![title](https://sun9-50.userapi.com/impg/rRwa1lI9Dq6EjGeEGHxdHA7FYu8FH9NfsVu9Ow/6Hee2wv77L4.jpg?size=537x597&quality=96&proxy=1&sign=b8f0c1851b0947c6d9f9c34c612a3575)
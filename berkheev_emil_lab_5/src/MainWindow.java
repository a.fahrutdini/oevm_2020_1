import java.util.Random;
import javax.swing.*;

public class MainWindow {
    static int _step = -1;
    static boolean _DNF = false;
    static boolean _KNF = false;
    Methods methods = new Methods();
    Random random = new Random();
    StringBuilder strBuilder = new StringBuilder();
    private int[][] array = {
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)},
    };
    private JPanel MyPanel = new CustomPanel(array);
    private JFrame frame;
    private JButton btnNextStep = new JButton("Шаг");
    private JButton btnDNF = new JButton("ДНФ");
    private JButton btnKNF = new JButton("КНФ");
    private JTextArea txtFunction = new JTextArea();

    public MainWindow() {
        initialize();
    }

    public void hideButtons() {
        btnDNF.setVisible(false);
        btnKNF.setVisible(false);
    }

    private void initialize() {

        frame = new JFrame();
        frame.setBounds(100, 100, 550, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        MyPanel.setBounds(0, 0, 170, 600);
        frame.getContentPane().add(MyPanel);

        txtFunction.setEditable(false);
        txtFunction.setBounds(290, 10, 200, 510);
        frame.getContentPane().add(txtFunction);

        btnNextStep.setEnabled(false);
        btnNextStep.addActionListener(e -> {
            _step++;
            if (_DNF == true) {
                txtFunction.setText(strBuilder.append(methods.normalForm(array, false)).toString());
            }
            if (_KNF == true) {
                txtFunction.setText(strBuilder.append(methods.normalForm(array, true)).toString());
            }
            if (_step == 15) {
                btnNextStep.setEnabled(false);
                txtFunction.setText(strBuilder.delete(strBuilder.toString().length() - 3, strBuilder.toString().length() - 1).toString());
            }

            MyPanel.repaint();
        });
        btnNextStep.setBounds(180, 10, 90, 30);
        frame.getContentPane().add(btnNextStep);

        btnDNF.addActionListener(e -> {
            btnNextStep.setEnabled(true);
            btnKNF.setEnabled(false);
            btnDNF.setEnabled(false);
            _step = 0;
            _DNF = true;
            hideButtons();
            MyPanel.repaint();
        });
        btnDNF.setBounds(180, 50, 90, 30);
        frame.getContentPane().add(btnDNF);

        btnKNF.addActionListener(e -> {
            btnNextStep.setEnabled(true);
            btnKNF.setEnabled(false);
            btnDNF.setEnabled(false);
            _step = 0;
            _KNF = true;
            hideButtons();
            MyPanel.repaint();
        });
        btnKNF.setBounds(180, 90, 90, 30);
        frame.getContentPane().add(btnKNF);
        frame.setVisible(true);
    }
}
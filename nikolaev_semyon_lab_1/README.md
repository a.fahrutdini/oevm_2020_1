# Лабораторная работа №1
# ПИбд-21 Николаев Семён
### Список комплектующих для ПК:
* Процессор: Intel Core i5 9400F частота процессора: 2.9 ГГц (сокет LGA 1151 v2)
* Видеокарта: NVIDIA GeForce RTX 1660 — 6144 Мб
* Материнская плата: ASRock B360-HDV (сокет LGA 1151) 
* Оперативная память: PATRIOT Signature DDR4 - 8ГБ 2400, 288 pin 
* Система охлаждения: DEEPCOOL GAMMAXX GT BLACK, 120мм. (совместим с сокетом LGA 1151)
* Жесткий диск: WD Caviar Blue, 1ТБ, SATA III, 3.5"
* Твердотельный диск:  CRUCIAL BX500 CT480BX500SSD1 480ГБ, 2.5", SATA III
* Твердотельный диск: SILICON POWER M-Series 256ГБ, M.2 2280, PCI-E x4 (диск для хранения игровых билдов)
* Блок питания: AEROCOOL VX PLUS 600W, 600Вт, 120мм
* Корпус: ATX ZALMAN S3, Midi-Tower 
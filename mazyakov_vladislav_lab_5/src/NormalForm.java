public class NormalForm {
    private String[] variables = {"X1", "X2", "X3", "X4"};
    StringBuilder DNFInc = new StringBuilder();
    StringBuilder CNFInc = new StringBuilder();
    int DNFIncRow=0;
    int CNFIncRow=0;
    int DNFIncColumn=0;
    int CNFIncColumn=0;
    boolean DNFIncPlus=false;
    boolean CNFIncMultiplication=false;

    public void setDNFInc(StringBuilder DNFInc) {
        this.DNFInc = DNFInc;
    }

    public void setDNFIncRow(int DNFIncRow) {
        this.DNFIncRow = DNFIncRow;
    }

    public void setDNFIncColumn(int DNFIncColumn) {
        this.DNFIncColumn = DNFIncColumn;
    }

    public void setDNFIncPlus(boolean DNFIncPlus) {
        this.DNFIncPlus = DNFIncPlus;
    }

    public void setCNFInc(StringBuilder CNFInc) {
        this.CNFInc = CNFInc;
    }

    public void setCNFIncRow(int CNFIncRow) {
        this.CNFIncRow = CNFIncRow;
    }

    public void setCNFIncColumn(int CNFIncColumn) {
        this.CNFIncColumn = CNFIncColumn;
    }

    public void CNFIncMultiplication(boolean CNFIncMultiplication) {
        this.CNFIncMultiplication = CNFIncMultiplication;
    }

    public StringBuilder convertToDisjunctiveNormalForm(int[][] truthTable) {
        StringBuilder DNF = new StringBuilder();
        int countColumns = truthTable.length;
        int countRows = truthTable[0].length;
        boolean plus = false;

        for (int i = 0; i < countRows; ++i) {
            if (truthTable[countColumns - 1][i] == 1) {
                if (plus) {
                    DNF.append(" + ");
                    DNF.append("\n");
                }
                DNF.append("(");
                plus = true;
                for (int j = 0; j < countColumns - 1; ++j) {
                    if (truthTable[j][i] == 0) {
                        DNF.append("!");
                    }
                    DNF.append(variables[j]);
                    if (j < countColumns - 2) {
                        DNF.append(" * ");
                    } else {
                        DNF.append(")");
                    }
                }
            }
        }
        return DNF;
    }

    public StringBuilder convertToDisjunctiveNormalFormInc(int[][] truthTable) {
        int countColumns = truthTable.length;
        int countRows = truthTable[0].length;

        if (DNFIncRow<countRows) {
            if (truthTable[countColumns - 1][DNFIncRow] == 0) {
                DNFIncRow++;
                return DNFInc;
            }
            if (truthTable[countColumns - 1][DNFIncRow] == 1) {
                if (DNFIncPlus) {
                    DNFInc.append(" + ");
                    DNFInc.append("\n");
                    DNFIncPlus = false;
                }
                if (DNFIncColumn<countColumns) {
                    if (DNFIncColumn==0) {
                        DNFInc.append("(");
                    }
                    if (truthTable[DNFIncColumn][DNFIncRow] == 0) {
                        DNFInc.append("!");
                    }
                    DNFInc.append(variables[DNFIncColumn]);
                    if (DNFIncColumn < countColumns - 2) {
                        DNFInc.append(" * ");
                        DNFIncColumn++;
                    } else {
                        DNFIncRow++;
                        DNFIncColumn=0;
                        DNFInc.append(")");
                        DNFIncPlus = true;
                    }
                }
            }
        }
        return DNFInc;
    }

    public StringBuilder convertToConjunctiveNormalFormInc(int[][] truthTable) {
        int countColumns = truthTable.length;
        int countRows = truthTable[0].length;
        boolean multiplication = false;
        if (CNFIncRow<countRows) {
            if (truthTable[countColumns - 1][CNFIncRow] == 1) {
                CNFIncRow++;
                return CNFInc;
            }
            if (truthTable[countColumns - 1][CNFIncRow] == 0) {
                if (CNFIncMultiplication) {
                    CNFInc.append(" * ");
                    CNFInc.append("\n");
                    CNFIncMultiplication=false;
                }
                if (CNFIncColumn<countColumns) {
                    if (CNFIncColumn==0) {
                        CNFInc.append("(");
                    }
                    if (truthTable[CNFIncColumn][CNFIncRow] == 1) {
                        CNFInc.append("!");
                    }
                    CNFInc.append(variables[CNFIncColumn]);
                    if (CNFIncColumn < countColumns - 2) {
                        CNFInc.append(" + ");
                        CNFIncColumn++;
                    } else {
                        CNFIncRow++;
                        CNFIncColumn=0;
                        CNFInc.append(")");
                        CNFIncMultiplication = true;
                    }
                }
            }
        }
        return CNFInc;
    }

    public StringBuilder convertToConjunctiveNormalForm(int[][] truthTable) {
        StringBuilder CNF = new StringBuilder();
        int countColumns = truthTable.length;
        int countRows = truthTable[0].length;
        boolean multiplication = false;
        for (int i = 0; i < countRows; ++i) {
            if (truthTable[countColumns - 1][i] == 0) {
                if (multiplication) {
                    CNF.append(" * ");
                    CNF.append("\n");
                }
                CNF.append("(");
                multiplication = true;
                for (int j = 0; j < countColumns - 1; ++j) {
                    if (truthTable[j][i] == 1) {
                        CNF.append("!");
                    }
                    CNF.append(variables[j]);
                    if (j < countColumns - 2) {
                        CNF.append(" + ");
                    } else {
                        CNF.append(")");
                    }
                }
            }
        }
        return CNF;
    }
}

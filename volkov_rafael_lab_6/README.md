# Лабораторная работа №6

## ПИбд-22 Волков Рафаэль

##### ТЗ:

Основы программирования на языке ассемблера.
Разработать с помощью ассемблера FASM (http://flatassembler.net/) программу для сложения, вычитания, умножения и деления двух целых чисел. Данные могут быть введены с клавиатуры либо заданы в виде констант.

##### Работа программы:

[Видео](https://drive.google.com/file/d/1zeJ3DJF7ypVivQaoA-7ZuNSdmrdg2RkT/view?usp=sharing)

_Пример 1:_
![Пример](https://sun9-8.userapi.com/e6amwSe0n1M2hFrSZrLqHGu-ITO17e5565EIHw/SBQ5T1Oa4Xc.jpg)

_Пример 2:_
![Пример](https://sun9-68.userapi.com/-3KE3e-9xxSDDmWfjqdET_OEN3mOZwnd6vQT3Q/82wxhqbxNnc.jpg)

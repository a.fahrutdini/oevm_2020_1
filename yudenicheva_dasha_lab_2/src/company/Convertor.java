package company;
import javax.print.DocFlavor;
import java.sql.Array;

public class Convertor {
    private int initial;
    private int desired;
    private char[] number;

    public Convertor(int initial, int desired, char[] number){
        this.initial = initial;
        this.desired = desired;
        this.number = number;
    }
    /**
 * функция перевода числа в 10-ую систему счисления
 */
    public static long convertToDec(char[] number, int initial) {
        long numeral10 = 0;

        for (int i = 0; i < number.length; i++) {
            int currentNumeral = 1;
            if (number[i] >= '0' && number[i] <= '9') {
                currentNumeral = number[i] - '0';
            } else if (Character.isLetter(number[i])) {
                currentNumeral = 10 + number[i] - 'A';
            }
            numeral10 = (long) ((double) numeral10 + (double) currentNumeral * Math.pow(initial, number.length - i - 1));
        }
        return numeral10;
    }
    /**
     * функция перевода в нужную СС
     */
    public static String convertToDesiredNumericSystem(int desired, long numeral10) {
        StringBuilder finish = new StringBuilder();
        long residue;

        for (String Char = ""; numeral10 > 0; numeral10 /= desired) {
            residue = numeral10 % (long) desired; //остаток
            Char = "";
            if (numeral10 % (long) desired < 10) {
                Char = Long.toString(residue);
            } else {
                Char = Char + (char) ((int)'A' + residue - 10);
            }
            finish.insert(0, Char);
        }
        return finish.toString();
    }
    /**
     * проверка СС и вывод результата
     */
    public void checkResultOutput(){
        int minLimit = 2, maxLimit = 16;
        if ((initial >= minLimit && initial <= maxLimit) && (desired >= minLimit && desired <= maxLimit)) {
            System.out.println("Получившиеся число: " + convertToDesiredNumericSystem(desired, convertToDec(number, initial)));
        } else {
            System.out.println("Ошибка!Введите другую систему счисления!");
        }
    }
}





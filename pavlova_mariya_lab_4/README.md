### Павлова Мария ПИбд-22
# Лабораторная работа №4 "Минимизация булевых функций"

#### Задание
Разработать программный продукт для минимизации булевой функции из четырех переменных (любым методом). При запуске программы случайным образом заполняются значения функции в таблице истинности (таблица 1). Пользователь указывает конечную форму логической функции (ДНФ или КНФ). После расчетов программа выводит функцию в выбранной форме.

#### Запуск
Найти в проекте Main.java и запустить

#### Технологии 
 IntelliJ IDEA Community
 Java 

#### Пример работы 
![ДНФ](https://sun9-41.userapi.com/impf/UKdjThrkisGOYuI1-9P6tyMsaewKvGt_CcugUg/JzkzO9E6g-M.jpg?size=356x578&quality=96&proxy=1&sign=5d27f337aa3522b14ec4805ac9e0dad8)
![КНФ](https://sun9-35.userapi.com/impf/k9aPFpuXYARjni17lav-tGXpaSdjUbxF8hkVoA/nKNmD8filSE.jpg?size=311x621&quality=96&proxy=1&sign=d76ba6bb53ce1c0730d27b7704332593)

#### Видео 

[Открыть](https://www.dropbox.com/s/gwtl11lrp0wvi06/4%20%D0%BB%D0%B0%D0%B1%D0%BE%D1%80%D0%B0%D1%82%D0%BE%D1%80%D0%BD%D0%B0%D1%8F%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0.mp4?dl=0")



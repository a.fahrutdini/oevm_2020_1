package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        LogicFunction function = new LogicFunction(4);
        int[][] truthTable = function.createTableTruth();
        function.print(truthTable);

        Scanner input = new Scanner(System.in);
        System.out.println("Выберите форму: 1. ДНФ; 2.КНФ");
        int typeForm = input.nextInt();

        switch (typeForm) {
            case 1:
                System.out.println("ДНФ: " + function.getDisjunctiveNormalForm(truthTable));
                break;
            case 2:
                System.out.println("КНФ: " + function.getConjunctiveNormalForm(truthTable));
                break;
            default:
                System.out.print("Неверный ввод");
                break;
        }
    }
}

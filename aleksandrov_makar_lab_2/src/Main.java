import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите исходную систему счисления: ");
        int originalSON = in.nextInt();

        System.out.print("Введите конечную систему счисления: ");
        int newSON = in.nextInt();

        System.out.print("Введите число, которое хотите перевести: ");
        String num = in.next();

        Convertor convertor = new Convertor(originalSON, newSON, num);
        String result = convertor.convert();
        System.out.printf("Результат: %s", result);
        }

        catch (Exception e){
            System.out.println("Введите корректные данные");
        }
    }
}

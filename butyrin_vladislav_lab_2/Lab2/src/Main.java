import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите изначальную систему счисления: ");
        int startSystem = input.nextInt();

        System.out.print("Введите конечную систему счисления: ");
        int lastSystem = input.nextInt();

        System.out.print("Введите число для перевода: ");
        char[] number = input.next().toCharArray();

        System.out.print("Результат перевода: ");
        String answer = Convertor.convert10SSToEndSS(startSystem, lastSystem, number);
        System.out.print(answer);
    }
}

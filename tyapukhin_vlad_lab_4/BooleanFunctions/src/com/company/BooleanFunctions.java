package com.company;

import java.util.Random;

public class BooleanFunctions {

    private int[][] array = {

            {0, 0, 0, 0},
            {0, 0, 0, 1},
            {0, 0, 1, 0},
            {0, 0, 1, 1},
            {0, 1, 0, 0},
            {0, 1, 0, 1},
            {0, 1, 1, 0},
            {0, 1, 1, 1},
            {1, 0, 0, 0},
            {1, 0, 0, 1},
            {1, 0, 1, 0},
            {1, 0, 1, 1},
            {1, 1, 0, 0},
            {1, 1, 0, 1},
            {1, 1, 1, 0},
            {1, 1, 1, 1},
    };

    private int sizeI = 16;
    private int sizeJ = 4;
    private int[] randomArray = new int[sizeI];
    Random random = new Random();

    public void print() {
        StringBuilder table = new StringBuilder();
        table.append("X1  X2  X3  X4  F\n");

        for(int i = 0; i < sizeI; i++) {
            randomArray[i] = random.nextInt(2);
        }

        for(int i = 0; i < sizeI; i++) {
            for(int j = 0; j < sizeJ; j++) {
                table.append(array[i][j] + "   ");
            }
            table.append(randomArray[i] + "\n");
        }
        System.out.println(table);
    }

    public void dnf() {
        StringBuilder dnf_str = new StringBuilder();
        int counter = 0;

        for(int i = 0; i < sizeI; i++) {
            if(randomArray[i] == 1) {
                if(counter > 0) {
                    dnf_str.append(" + ");
                }
                dnf_str.append("(");
                for(int j = 0; j < sizeJ; j++) {
                    if(array[i][j] == 0) {
                        dnf_str.append("-");
                    }
                    dnf_str.append("X" + (j + 1));
                    if(j < sizeJ - 1) {
                        dnf_str.append(" * ");
                    }
                }
                dnf_str.append(")");
                counter++;
            }
        }
        System.out.println(dnf_str);
    }

    public void knf() {

        StringBuilder knf_str = new StringBuilder();
        int counter = 0;

        for(int i = 0; i < sizeI; i++) {
            if(randomArray[i] == 0) {
                if(counter > 0) {
                    knf_str.append(" * ");
                }
                knf_str.append("(");
                for(int j = 0; j < sizeJ; j++) {
                    if(array[i][j] == 1) {
                        knf_str.append("-");
                    }
                    knf_str.append("X" + (j + 1));
                    if(j < sizeJ - 1) {
                        knf_str.append(" + ");
                    }
                }
                knf_str.append(")");
                counter++;
            }
        }
        System.out.println(knf_str);
    }
}


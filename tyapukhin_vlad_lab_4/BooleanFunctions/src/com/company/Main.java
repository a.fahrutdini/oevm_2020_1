package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        BooleanFunctions function = new BooleanFunctions();
        Scanner in = new Scanner(System.in);
        function.print();
        System.out.println("ДНФ : 1\nКНФ : 2\n");
        String str = in.next();

        if(str.equals("1")) {
            function.dnf();
        } else if(str.equals("2")) {
            function.knf();
        } else {
            System.out.println("Неверный ввод");
        }
    }
}



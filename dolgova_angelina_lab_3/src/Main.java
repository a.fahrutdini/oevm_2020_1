import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Converter converter = new Converter();
        BinaryCalculator calculator = new BinaryCalculator();
        System.out.println("\t\t\tМОДЕЛИРОВАНИЕ ВЫПОЛНЕНИЯ АРИФМЕТИЧЕСКИХ ОПЕРАЦИЙ\t\t\t");
        System.out.println();

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите исходную систему счисления (2-16): ");
        int originalSystem = scanner.nextInt();

        System.out.print("Введите первое число: ");
        String firstNumber = scanner.next();
        String firstNumberBinary;
        if (converter.checkInputData(firstNumber.toCharArray(), originalSystem)) {
            String firstNumberDecimal = String.valueOf(converter.convertToDecimalSystem(firstNumber.toCharArray(), originalSystem));
            firstNumberBinary = String.valueOf(converter.convertToBinarySystem(firstNumberDecimal.toCharArray()));
        }
        else return;

        System.out.print("Введите второе число: ");
        String secondNumber = scanner.next();
        String secondNumberBinary;
        if (converter.checkInputData(secondNumber.toCharArray(), originalSystem)) {
            String secondNumberDecimal = String.valueOf(converter.convertToDecimalSystem(secondNumber.toCharArray(), originalSystem));
            secondNumberBinary = String.valueOf(converter.convertToBinarySystem(secondNumberDecimal.toCharArray()));
        }
        else return;

        System.out.println("Введите номер необходимой операции: ");
        System.out.println("1) +\n2) -\n3) *\n4) /\n5) Ввести новые данные");

        while (true) {
            int x = scanner.nextInt();
            switch (x) {
                case 1 -> System.out.println(firstNumberBinary + " + " + secondNumberBinary + " = " + calculator.addNumbers(firstNumberBinary.toCharArray(), secondNumberBinary.toCharArray()));
                case 2 -> System.out.println(firstNumberBinary + " - " + secondNumberBinary + " = " + calculator.subtractNumbers(firstNumberBinary.toCharArray(), secondNumberBinary.toCharArray()));
                case 3 -> System.out.println(firstNumberBinary + " * " + secondNumberBinary + " = " + calculator.multiplyNumbers(firstNumberBinary.toCharArray(), secondNumberBinary.toCharArray()));
                case 4 -> System.out.println(firstNumberBinary + " / " + secondNumberBinary + " = " + calculator.divideNumbers(firstNumberBinary.toCharArray(), secondNumberBinary.toCharArray()));
                case 5 -> {
                    System.out.print("Введите исходную систему счисления (2-16): ");
                    originalSystem = scanner.nextInt();
                    System.out.print("Введите первое число: ");
                    firstNumber = scanner.next();
                    if (converter.checkInputData(firstNumber.toCharArray(), originalSystem)) {
                        String firstNumberDecimal = String.valueOf(converter.convertToDecimalSystem(firstNumber.toCharArray(), originalSystem));
                        firstNumberBinary = String.valueOf(converter.convertToBinarySystem(firstNumberDecimal.toCharArray()));
                    } else return;
                    System.out.print("Введите второе число: ");
                    secondNumber = scanner.next();
                    if (converter.checkInputData(secondNumber.toCharArray(), originalSystem)) {
                        String secondNumberDecimal = String.valueOf(converter.convertToDecimalSystem(secondNumber.toCharArray(), originalSystem));
                        secondNumberBinary = String.valueOf(converter.convertToBinarySystem(secondNumberDecimal.toCharArray()));
                    } else return;
                }
            }
        }
    }
}
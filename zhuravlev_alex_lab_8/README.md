## Лабораторная работа №8 

## "Физические компоненты ЭВМ"
Выполнил студент группы **ИСЭбд-21: Журавлев Александр**

### Задание:

*Разобрать и собрать ПК*

*Порядок выполнения работы:*

**[1) До разборки](https://drive.google.com/file/d/1U2McQ02Ef0JfOtd7F7xNz9rdyqDVxp5u/view?usp=sharing)**

**[2) Комлектующие](https://drive.google.com/file/d/1nwJiBmlrHCklxANe0F1f_SXIN7xpT4x2/view?usp=sharing)**

**[3) После сборки](https://drive.google.com/file/d/1uAqErkibkK_x9AJuvJL7XCSr6QV4lUv2/view?usp=sharing)**

**[В итоге после разборки и сборки компьютер остался жив.](https://drive.google.com/file/d/1hKO0tKrRSAaGu37oNX-UEUehld0espPk/view?usp=sharing)**


# Лабораторная работа №4

## ИСЭбд-21 Александров Макар

**[Видео-пример работы программы](https://www.dropbox.com/s/lin3j6y61fufs5y/lab4.mkv?dl=0)**

Консольная программа, написана на Java

Пользователь, при запуске программы, должен ввести ДНФ, либо же КНФ.
По итогу работы программы, в консоль выводится ДНФ или КНФ сгенерированной таблицы.

Пример работы программы:
```
Введите ДНФ или КНФ для осуществления соответсвующей опрации: ДНФ
0 0 0 0 = 1
0 0 0 1 = 1
0 0 1 0 = 1
0 0 1 1 = 0
0 1 0 0 = 1
0 1 0 1 = 0
0 1 1 0 = 0
0 1 1 1 = 0
1 0 0 0 = 1
1 0 0 1 = 1
1 0 1 0 = 0
1 0 1 1 = 0
1 1 0 0 = 1
1 1 0 1 = 0
1 1 1 0 = 0
1 1 1 1 = 1
( -x1 * -x2 * -x3 * -x4 ) + ( -x1 * -x2 * -x3 * x4 ) + ( -x1 * -x2 * x3 * -x4 ) +
( -x1 * x2 * -x3 * -x4 ) + ( x1 * -x2 * -x3 * -x4 ) + ( x1 * -x2 * -x3 * x4 ) +
( x1 * x2 * -x3 * -x4 ) + ( x1 * x2 * x3 * x4 )

```

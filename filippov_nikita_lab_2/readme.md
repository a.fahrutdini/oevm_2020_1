<!DOCTYPE html>
<html>
<body>
Лабораторная работа №2<br>
выполнил: Филиппов Никита<br>
группа: ПИбд-22<br>

Ссылка на видео: https://drive.google.com/file/d/1vNeuwjivzjVs4ilO4It4M2O7yQ6flzqB/view?usp=sharing <br>
<H3> Как запустить?</H3>
Запустить лабу через Main.java
<H3>Какие технологии использовались?</H3>
Использовалась IntelliJ IDEA и стандартные библиотеки Java. Использовал StringBuilder
<H3>Функционал программы</H3>
Переводит введенное число из введенной с.с во введенную с.с

<table bgcolor="#DCDCDC" border="2">
    <caption >
        Пример
    </caption>
    <tr>
        <th>№</th>
        <th>Входные данные</th>
        <th>Выходные данные</th>
    </tr>
    <tr>
        <td>1.</td>
        <td>16 4 ABCD</td>
        <td>22233031</td>
    </tr>
    <tr>
        <td>2.</td>
        <td>13 5 1865</td>
        <td>104012</td>
    </tr>
    <tr>
        <td>3.</td>
        <td>32 5 FILIPPOV</td>
        <td>32233044113311031</td>
    </tr>
    <tr>
        <td>4.</td>
        <td>4 53 95</td>
        <td>Ошибка ввода (Используются цифры >= с.с)</td>
    </tr>
    <tr>
        <td>5.</td>
        <td>32 7 OFFSPRING</td>
        <td>5445650141451542</td>
    </tr>
    <tr>
        <td>6.</td>
        <td>9 7 178</td>
        <td>305</td></td>
    </tr>
</table>
</body>
</html>
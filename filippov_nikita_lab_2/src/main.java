
import java.util.InputMismatchException;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int originalSystem = 0;
        int endSystem = 0;
        String number = "";

        try {
            originalSystem = in.nextInt();
            endSystem = in.nextInt();
            number = in.next();
        } catch (InputMismatchException e) {
            System.out.printf("Ошибка ввода с.с");
            return;
        }

        in.close();
        Translator translator = new Translator(originalSystem, endSystem, number);
        translator.translateToEnd();
        System.out.print(translator.getResult());
    }
}

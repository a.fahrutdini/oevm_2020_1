public class DrawElements {

    private String string;
    private int posY;

    public DrawElements(String string, int posY) {
        this.string = string;
        this.posY = posY;
    }

    public String getString() {
        return string;
    }

    public void addPlusToDNF() {
        string += " +";
    }

    public void addMultiplyToKNF() {
        string += " *";
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
}

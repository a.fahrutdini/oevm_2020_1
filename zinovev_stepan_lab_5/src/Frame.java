import javax.swing.*;

public class Frame {

    private JFrame frame;
    private final int frameWidth = 630;
    private final int frameHeight = 850;

    private JFrame frameInit() {
        JFrame frame = new JFrame("5thLaboratoryWork");
        frame.setSize(frameWidth, frameHeight);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(null);
        return frame;
    }

    private void addElementsToFrame() {
        DrawPanel drawPanel = new DrawPanel(frame);
        JButton buttonRefresh = new JButton("Refresh Table");
        JButton buttonDrawElements = new JButton("Convert to DNF / KNF");

        frame.getContentPane().add(buttonRefresh);
        frame.getContentPane().add(buttonDrawElements
        );
        frame.getContentPane().add(drawPanel);

        buttonRefresh.setBounds(30, 760, 200, 30);
        buttonDrawElements.setBounds(260, 760, 340, 30);
        drawPanel.setBounds(0, 0, frameWidth, frameHeight);

        buttonRefresh.addActionListener(e -> {
            drawPanel.refreshTable();
            buttonDrawElements.setEnabled(true);
            frame.repaint();
        });

        buttonDrawElements.addActionListener(e -> {
            drawPanel.fillStrings();
            buttonDrawElements.setEnabled(false);
        });

        frame.repaint();
    }

    public void showFrame() {
        frame = frameInit();
        addElementsToFrame();
    }
}
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("Введите исходную систему счисления (от 2 до 16): ");
            int initialSN = scanner.nextInt();
            if (initialSN > 20 || initialSN < 2) throw new NotValidateException();
            System.out.print("Введите конечную систему счисления (от 2 до 16): ");
            int finalSN = scanner.nextInt();
            if (finalSN > 20 || finalSN < 2) throw new NotValidateException();
            System.out.print("Введите число в исходной системе счисления: ");
            String line = scanner.next();
            char[] number = line.toUpperCase().toCharArray();
            String result = Converter.convert(initialSN, finalSN, number);
            System.out.println("Число в конечной системе счисления: " + result);
        } catch (NotValidateException e) {
            System.out.println(e.getMessage());
        }
    }
}

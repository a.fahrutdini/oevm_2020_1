import javax.swing.*;

public class Frame {

    private JFrame frame;
    private JButton btnShowNextStep;
    private JButton btnCreateNewTable;
    private JRadioButton btnBringToDNF;
    private JRadioButton btnBringToCNF;

    public Frame() {
        initialize();
    }

    private void initialize() {

        frame = new JFrame("Визуализация булевых функций");
        frame.setBounds(0, 0, 600, 750);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setVisible(true);
        frame.setResizable(false);

        DrawPanel panel = new DrawPanel();
        panel.setBounds(10, 10, 600, 600);
        frame.getContentPane().add(panel);

        btnCreateNewTable = new JButton("Создать новую таблицу истинности");
        btnCreateNewTable.addActionListener(e -> {
            btnShowNextStep.setText("Минимизировать");
            btnShowNextStep.setEnabled(true);
            btnBringToDNF.setEnabled(true);
            btnBringToCNF.setEnabled(true);
            panel.updateTable();
            panel.repaint();
            panel.resetStep();
        });
        frame.getContentPane().add(btnCreateNewTable);
        btnCreateNewTable.setBounds(10, 620, 280, 40);
        btnCreateNewTable.setEnabled(true);

        JPanel radioPanel = new JPanel();
        ButtonGroup group = new ButtonGroup();
        btnBringToDNF = new JRadioButton("ДНФ", true);
        group.add(btnBringToDNF);
        btnBringToCNF = new JRadioButton("КНФ", false);
        group.add(btnBringToCNF);
        radioPanel.add(btnBringToCNF);
        radioPanel.add(btnBringToDNF);
        frame.getContentPane().add(radioPanel);
        radioPanel.setBounds(300, 620, 300, 50);
        btnBringToDNF.setEnabled(true);
        btnBringToCNF.setEnabled(true);

        btnShowNextStep = new JButton("Минимизировать");
        btnShowNextStep.setEnabled(true);
        btnShowNextStep.addActionListener(e -> {
            btnShowNextStep.setText("Следующий шаг");
            btnBringToDNF.setEnabled(false);
            btnBringToCNF.setEnabled(false);

            if (btnBringToDNF.isSelected()) {
                panel.setChooseDNF(true);
            } else panel.setChooseDNF(false);

            panel.increaseStep();
            panel.repaint();

            if (panel.isLastStep()) {
                btnShowNextStep.setEnabled(false);
            }
        });
        frame.getContentPane().add(btnShowNextStep);
        btnShowNextStep.setBounds(10, 670, 560, 40);
    }

    public static void main(String[] args) {
        Frame frame = new Frame();
    }
}
public class FormConverter {
    private final int NUMBER_OF_COLUMNS;
    public final String[] VARIABLES = {"X_1", "X_2", "X_3", "X_4" };
    private int firstStringIndex = -1;

    public FormConverter(int countOfColumns){
        this.NUMBER_OF_COLUMNS = countOfColumns;
    }

    /*
    Метод для получения строки ДНФ определённого шага
     */
    public String bringToDNF(int[][] truthTable, int step) {
        StringBuilder result = new StringBuilder();
        String emptyString = "-1";
        int i = step - 1;
        if (truthTable[i][4] == 0) {
            return emptyString;
        }
        else {
            if (firstStringIndex == -1 || step == firstStringIndex) {
                result.append("(");
            }
            else {
                result.append("+ (");
            }
            for (int j = 0; j < NUMBER_OF_COLUMNS - 1; j++) {
                if (truthTable[i][j] == 0) {
                    if (j == NUMBER_OF_COLUMNS - 2) {
                        result.append("!" + VARIABLES[j]);
                    }
                    else result.append("!" + VARIABLES[j] + " * ");
                }
                else {
                    if (j == NUMBER_OF_COLUMNS - 2) {
                        result.append(VARIABLES[j]);
                    }
                    else result.append(VARIABLES[j] + " * ");
                }
            }
            result.append(")");
        }
        return result.toString();
    }

    /*
    Метод для получения строки КНФ определённого шага
     */
    public String bringToCNF(int[][] truthTable, int step) {
        StringBuilder result = new StringBuilder();
        String emptyString = "-1";
        int i = step - 1;
        if (truthTable[i][4] == 1) {
            return emptyString;
        }
        else {
            if (firstStringIndex == -1 || step == firstStringIndex) {
                result.append("(");
            }
            else {
                result.append("* (");
            }
            for (int j = 0; j < NUMBER_OF_COLUMNS - 1; j++) {
                if (truthTable[i][j] == 1) {
                    if (j == NUMBER_OF_COLUMNS - 2) {
                        result.append("!" + VARIABLES[j]);
                    }
                    else result.append("!" + VARIABLES[j] + " + ");
                }
                else {
                    if (j == NUMBER_OF_COLUMNS - 2) {
                        result.append(VARIABLES[j]);
                    }
                    else result.append(VARIABLES[j] + " + ");
                }
            }
            result.append(")");
        }
        return result.toString();
    }

    public void setFirstStringIndex(int firstStringIndex) {
        this.firstStringIndex = firstStringIndex;
    }

    public int getFirstStringIndex() {
        return firstStringIndex;
    }
}
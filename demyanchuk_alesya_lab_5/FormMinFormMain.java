import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormMinFormMain {
    private JFrame frame;
    private PanelMinForm panelMinForm;
    private MinForm minForm = new MinForm();
    int[][] truthTable = new int[16][5];

    public static void main(String[] args) {
        EventQueue.invokeLater( new Runnable() {
            public void run() {
                try {
                    FormMinFormMain window = new FormMinFormMain();
                    window.frame.setVisible( true );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } );
    }

    public FormMinFormMain() {
        initialize();
    }

    public void initialize() {
        frame = new JFrame();
        frame.setBounds( 0, 0, 500, 550 );
        frame.setVisible( true );
        frame.getContentPane().setLayout( null );

        panelMinForm = new PanelMinForm();
        panelMinForm.setBounds( 0, 0, 160, 500 );
        frame.getContentPane().add( panelMinForm );

        minForm.setData( truthTable );
        minForm.matrixFilling();
        panelMinForm.setArrayNumbers( minForm.getData() );

        JButton btnAddMassNumber = new JButton( "Добавить массив" );
        btnAddMassNumber.setBounds( 200, 30, 200, 20 );
        btnAddMassNumber.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                minForm.setData( truthTable );
                minForm.matrixFilling();
                panelMinForm.setArrayNumbers( minForm.getData() );
                panelMinForm.setCNFstatus( false );
                panelMinForm.setDNFstatus( false );
                panelMinForm.repaint();
            }
        } );
        frame.getContentPane().add( btnAddMassNumber );
        frame.repaint();

        JTextArea textAreaResult = new JTextArea();
        textAreaResult.setBounds( 200, 100, 280, 300 );
        frame.getContentPane().add( textAreaResult );

        JButton btnCNF = new JButton( "CNF" );
        btnCNF.setBounds( 200, 50, 100, 20 );
        btnCNF.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String result = minForm.conjunctiveNormalForm();
                panelMinForm.setCNFstatus( true );
                panelMinForm.setDNFstatus( false );
                textAreaResult.selectAll();
                textAreaResult.replaceSelection( "" );
                textAreaResult.setText( result );
                panelMinForm.repaint();
            }
        } );
        frame.getContentPane().add( btnCNF );

        JButton btnDNF = new JButton( "DNF" );
        btnDNF.setBounds( 200, 70, 100, 20 );
        btnDNF.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String result = minForm.disjunctiveNormalForm();
                panelMinForm.setCNFstatus( false );
                panelMinForm.setDNFstatus( true );
                textAreaResult.selectAll();
                textAreaResult.replaceSelection( "" );
                textAreaResult.setText( result );
                panelMinForm.repaint();
            }
        } );
        frame.getContentPane().add( btnDNF );
        frame.repaint();
    }
}

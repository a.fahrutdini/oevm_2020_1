import javax.swing.*;
import java.awt.*;

public class PanelMinForm extends JPanel {
    private int arrayNumbers[][];
    private int rows = 16;
    private int columns = 5;
    private boolean DNFstate = false;
    private boolean CNFstate = false;

    public void paint(Graphics g) {
        super.paint( g );
        int sizeX = 30;
        int sizeY = 30;
        int x = 0;
        int y = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                g.setColor( Color.lightGray );
                g.fillRect( x + sizeX * j, y, sizeX, sizeY );
                g.setColor( Color.blue );
                g.drawString( arrayNumbers[i][j] + "", 10 + j * sizeX, 15 + i * sizeY );
                if (CNFstate == true) {
                    if (arrayNumbers[i][columns - 1] == 1) {
                        g.setColor( Color.blue );
                        g.fillRect( x + sizeX * j, y, sizeX, sizeY );
                        g.setColor( Color.white );
                        g.drawString( arrayNumbers[i][j] + "", 10 + j * sizeX, 15 + i * sizeY );
                    }
                } else if (DNFstate == true) {
                    if (arrayNumbers[i][columns - 1] == 0) {
                        g.setColor( Color.blue );
                        g.fillRect( x + sizeX * j, y, sizeX, sizeY );
                        g.setColor( Color.white );
                        g.drawString( arrayNumbers[i][j] + "", 10 + j * sizeX, 15 + i * sizeY );
                    }
                }
                g.setColor( Color.blue );
                g.drawRect( x + sizeX * j, y, sizeX, sizeY );
            }
            y += sizeY;
        }
    }

    public void setArrayNumbers(int arrayNumbers[][]) {
        this.arrayNumbers = arrayNumbers;
    }

    public void setDNFstatus(boolean DNFstate) {
        this.DNFstate = DNFstate;
    }

    public void setCNFstatus(boolean CNFstate) {
        this.CNFstate = CNFstate;
    }
}

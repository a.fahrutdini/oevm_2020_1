public class MinForm {
    private String[] codeX = {"X1", "X2", "X3", "X4"};             // Элементы таблицы истинности
    private int[][] _matrix;                                       // Основная матрица
    final int countRows = 16;                                              // Кол-во строк
    final int countColumn = 5;                                               // Кол-во столбцов

    public void setData(int[][] TruthTable) {
        _matrix = TruthTable;
    }

    public int[][] getData() {
        return _matrix;
    }

    private int[] convertTo2NS(int value) {
        int[] elements = {0, 0, 0, 0};
        int i = elements.length - 1;
        while (value >= 2) {
            elements[i] = value % 2;
            value /= 2;
            i--;
        }
        elements[i] = value;
        return elements;
    }

    public void matrixFilling() {
        int[] tablesElements;
        for (int i = 0; i < countRows; i++) {
            tablesElements = convertTo2NS( i );
            for (int j = 0; j < tablesElements.length; j++) {
                _matrix[i][j] = tablesElements[j];
            }
            _matrix[i][countColumn - 1] = (int) (Math.random() * 2);
        }
    }

    public String conjunctiveNormalForm() {
        Boolean firstElement = false;
        String _resultString = "";
        for (int i = 0; i < countRows; i++) {
            if (_matrix[i][countColumn - 1] == 0)           // КНФ производит поиск по 0
            {
                if (!firstElement)                    // проверяем: является i-ая группа переменных первой скобкой
                    _resultString += "\n" + "(";
                else
                    _resultString += "\n" + " * (";
                for (int j = 0; j < countColumn - 1; j++)    // в данном цикле происходит кодирование 1 и 0 i-ой переменной
                {                                 // в "X_i" с нужным знаком
                    if (j == 0)                   // проверка на первый элемент в группе (перед ним не ставится "+")
                    {
                        if (_matrix[i][j] == 1)                       // если 1 => "-"
                            _resultString += "-" + codeX[j];
                        else                                          // если 0 => знак у переменной не ставим
                            _resultString += codeX[j];
                    } else                                              // если элемент не первый в группе (X2, X3, X4)
                    {
                        if (_matrix[i][j] == 1)                       // если 1 => "-"
                            _resultString += " + -" + codeX[j];
                        else                                          // если 0 => знак у переменной не ставим
                            _resultString += " + " + codeX[j];
                    }
                }
                _resultString += ")";                                 // в конце i-ой группы закрываем скобку
                firstElement = true;
            }
        }
        return _resultString;
    }

    public String disjunctiveNormalForm()            // аналогичный метод, только ДНФ поиск производится по 1
    {                                                // "+" меняем на "*", и в обратную сторону так же
        Boolean firstGroup = false;
        String _resultString = "";
        for (int i = 0; i < countRows; i++) {
            if (_matrix[i][countColumn - 1] == 1) {
                if (!firstGroup)
                    _resultString += "\n" + "(";
                else
                    _resultString += "\n" + " + (";
                for (int j = 0; j < countColumn - 1; j++) {
                    if (j == 0) {
                        if (_matrix[i][j] == 0)
                            _resultString += "-" + codeX[j];
                        else
                            _resultString += codeX[j];
                    } else {
                        if (_matrix[i][j] == 0)
                            _resultString += " * -" + codeX[j];
                        else
                            _resultString += " * " + codeX[j];
                    }
                }
                _resultString += ")";
                firstGroup = true;
            }
        }
        return _resultString;
    }
}

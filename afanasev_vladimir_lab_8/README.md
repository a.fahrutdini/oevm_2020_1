# Лабораторная работа №8

## ИСЭбд-21 Афанасьев Владимир

##### Техническое задание: 

Физические компоненты ЭВМ.

Разобрать и собрать системный блок ЭВМ.

_Таблица комплектующих_

| № | Комплектующие пк | Комментарии |
|:-:|:----------------:|:-----------:|
| 1 | **Процессор:** AMD Ryzen 5 2600, SocketAM4, OEM. | Ядер — 6, потоков — 12.|
| 2 | **Материнская плата:** gigabyte b450m s2h. | Плата совместима с процессорами AMD Ryzen 3-поколения/ AMD Ryzen 2-поколения/ AMD Ryzen 1-поколения. SocketAM4 (Что, собственно, подхдит для данного процессора). |
| 3 | **Видеокарта:** ASUS GeForce GTX 1650 SUPER. | Тихая и холодная, не требовательна к питанию, в общем за свои деньги СУПЕР вариант. |
| 4 | **Оперативная память:** CORSAIR Vengeance LPX CMK8GX4M1A2666C16 DDR4 - 8ГБ 2666. | Конечно, хотелось бы 2 планки CORSAIR по 8 гб, но пока что было решено взять 1 планку на 8гб и позже докупить 2-ю. |
| 5 | **Кулер для процессора:**  DEEPCOOL GAMMAXX 300 FURY. | Подходит для охлаждения процессора, многим этот кулер нравится. |
| 6 | **SSD:** WD Blue WDS500G2B0B 500ГБ. | Под систему, программы. Для быстрой работы самого пк. |
| 7 | **Блок питания:** AEROCOOL VX PLUS 600W. | Бюджетный блок питания, хорошо справляющийся со своей задачей. |
| 8 | **Корпус:** mATX AEROCOOL Qs-240. | Обычный корпус, хорошо подошел для этих комплектующих. |

**[До сборки](https://www.dropbox.com/s/po29sahgcqnsxv6/%D0%94%D0%BE%20%D1%81%D0%B1%D0%BE%D1%80%D0%BA%D0%B8.jpg?dl=0)**<br>
**Комплектующие:**<br>
**[Материнская плата, процессор и кулер для процессора](https://www.dropbox.com/sh/rhtsk1w97q6466y/AACCEvZCfpHJYJFP89E0BTGFa?dl=0)**<br>
**[Оперативная память](https://www.dropbox.com/s/zwxu136wtbdsv45/%D0%9E%D0%BF%D0%B5%D1%80%D0%B0%D1%82%D0%B8%D0%B2%D0%BD%D0%B0%D1%8F%20%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D1%8C.jpg?dl=0)**<br>
**[SSD](https://www.dropbox.com/s/3fw6ixcz753e667/SSD.jpg?dl=0)**<br>
**[Видеокарта](https://www.dropbox.com/s/27e1xeb0kagyskk/%D0%92%D0%B8%D0%B4%D0%B5%D0%BE%D0%BA%D0%B0%D1%80%D1%82%D0%B0.jpg?dl=0)**<br>
**[После сборки](https://www.dropbox.com/s/ph39ihin295c0s7/%D0%9F%D0%BE%D1%81%D0%BB%D0%B5%20%D1%81%D0%B1%D0%BE%D1%80%D0%BA%D0%B8.jpg?dl=0)**<br>
**[Всё работает](https://www.dropbox.com/sh/sz20ay0ny2lebuh/AABy4YDd39cJlhrtuxr03Onsa?dl=0)**<br>
package com.company;

public class HelperClass {

    static int X1 = 0;
    static int X2 = 1;
    static int X3 = 2;
    static int X4 = 3;

    public static String functionKNF (int[][] arr, int sizeColumn, int sizeStr) {
        StringBuilder result = new StringBuilder();
        int count = 0;
        for (int i = 0; i < sizeStr; i++) {
            if (arr[i][sizeColumn - 1] == 0){
                if(count > 0){
                    result.append("*");
                }
                if(arr[i][X1] == 1){
                    result.append("(-X1");
                }
                else {
                    result.append("(X1");
                }
                if(arr[i][X2] == 1){
                    result.append("-X2");
                }
                else {
                    result.append("+X2");
                }
                if(arr[i][X3] == 1){
                    result.append("-X3");
                }
                else {
                    result.append("+X3");
                }
                if(arr[i][X4] == 1){
                    result.append("-X4)");
                    result.append("\n");
                }
                else {
                    result.append("+X4)");
                    result.append("\n");
                }
                count++;
            }
        }
        return result.toString();
    }

    public static String functionDNF (int[][] arr, int sizeColumn, int sizeStr){
        StringBuilder result = new StringBuilder();
        int count = 0;
        for (int i = 0; i < sizeStr; i++) {
            if (arr[i][sizeColumn - 1] == 1){
                if(count > 0){
                    result.append("+");
                }
                if(arr[i][X1] == 0){
                    result.append("(-X1");
                }
                else {
                    result.append("(X1");
                }
                if(arr[i][X2] == 0){
                    result.append("*-X2");
                }
                else {
                    result.append("*X2");
                }
                if(arr[i][X3] == 0){
                    result.append("*-X3");
                }
                else {
                    result.append("*X3");
                }
                if(arr[i][X4] == 0){
                    result.append("*-X4)");
                    result.append("\n");
                }
                else {
                    result.append("*X4)");
                    result.append("\n");
                }
                count++;
            }
        }
        return result.toString();
    }

}

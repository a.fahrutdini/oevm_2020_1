package com.company;
import javax.swing.*;
import java.awt.Color;
import javax.swing.border.*;

public class Screen {

    HelperClass helperVariable = new HelperClass();
    public JFrame frame;
    private MyPanel myPanel;

    private JTextArea result = new JTextArea();
    private JButton buttonCreate = new JButton("Создать заново");
    private JButton buttonDNF = new JButton("ДНФ");
    private JButton buttonKNF = new JButton("КНФ");

    Color blueColor = new Color(161, 245, 249);

    int sizeColumn = 5;
    int sizeStr = 16;
    int[][] array = {
            {1, 0, 0, 0, (int) (Math.random() * 2)},
            {0, 1, 0, 1, (int) (Math.random() * 2)},
            {0, 0, 0, 0, (int) (Math.random() * 2)},
            {0, 0, 0, 1, (int) (Math.random() * 2)},
            {0, 1, 1, 1, (int) (Math.random() * 2)},
            {0, 0, 0, 0, (int) (Math.random() * 2)},
            {1, 1, 1, 1, (int) (Math.random() * 2)},
            {0, 1, 0, 1, (int) (Math.random() * 2)},
            {1, 0, 1, 0, (int) (Math.random() * 2)},
            {1, 0, 1, 1, (int) (Math.random() * 2)},
            {1, 0, 1, 0, (int) (Math.random() * 2)},
            {1, 0, 0, 1, (int) (Math.random() * 2)},
            {1, 1, 1, 0, (int) (Math.random() * 2)},
            {1, 1, 0, 1, (int) (Math.random() * 2)},
            {0, 0, 0, 0, (int) (Math.random() * 2)},
            {1, 1, 0, 1, (int) (Math.random() * 2)},
    };

    Screen(){

        frame = new JFrame( "Минимизация булевых выражений");
        frame.setBounds(0, 0, 500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(null);
        frame.getContentPane().setBackground(Color.white);

        myPanel = new MyPanel(sizeStr,sizeColumn);
        myPanel.doNewArray(array);
        myPanel.setBounds(200, 10, 350, 450);
        myPanel.setBackground(Color.white);
        frame.getContentPane().add(myPanel);

        result.setBounds(20, 150, 150, 250);
        result.setBackground(blueColor);
        result.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        frame.getContentPane().add(result);

        buttonCreate.addActionListener(e -> updateArray());
        buttonCreate.setBounds(20, 10, 150, 30);
        frame.getContentPane().add(buttonCreate);

        buttonDNF.addActionListener(e -> drawDNF());
        buttonDNF.setBounds(20, 50, 150, 30);
        frame.getContentPane().add(buttonDNF);

        buttonKNF.addActionListener(e -> drawKNF());
        buttonKNF.setBounds(20, 90, 150, 30);
        frame.getContentPane().add(buttonKNF);

    }

    private void updateArray() {

        int[][] newArray = {
                {1, 0, 0, 0, (int) (Math.random() * 2)},
                {0, 1, 0, 1, (int) (Math.random() * 2)},
                {0, 0, 0, 0, (int) (Math.random() * 2)},
                {0, 0, 0, 1, (int) (Math.random() * 2)},
                {0, 1, 1, 1, (int) (Math.random() * 2)},
                {0, 0, 0, 0, (int) (Math.random() * 2)},
                {1, 1, 1, 1, (int) (Math.random() * 2)},
                {0, 1, 0, 1, (int) (Math.random() * 2)},
                {1, 0, 1, 0, (int) (Math.random() * 2)},
                {1, 0, 1, 1, (int) (Math.random() * 2)},
                {1, 0, 1, 0, (int) (Math.random() * 2)},
                {1, 0, 0, 1, (int) (Math.random() * 2)},
                {1, 1, 1, 0, (int) (Math.random() * 2)},
                {1, 1, 0, 1, (int) (Math.random() * 2)},
                {0, 0, 0, 0, (int) (Math.random() * 2)},
                {1, 1, 0, 1, (int) (Math.random() * 2)},
        };
        array = newArray;
        myPanel.doNewArray(array);
        result.setText("");
        myPanel.repaint();
    }
    private void drawKNF() {
        result.setText(helperVariable.functionKNF(array,sizeColumn,sizeStr));
    }

    private void drawDNF() {
        result.setText(helperVariable.functionDNF(array,sizeColumn,sizeStr));
    }

}

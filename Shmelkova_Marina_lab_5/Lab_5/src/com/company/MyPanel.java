package com.company;
import javax.swing.*;
import java.awt.*;


public class MyPanel extends JPanel {

    private int[][] array;
    private int height;
    private int width;
    private int oneVariable = 25;

    MyPanel(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public void paint(Graphics g) {
        super.paint(g);
        for (int i = 0; i < height; i++) {
            if (array[i][width - 1] == 0) {
                g.setColor(Color.red);
            }
            else {
                g.setColor(Color.BLUE);
            }
            for (int j = 0; j < width; j++) {
                g.drawRect(80 + j * oneVariable, 10 + i * oneVariable, oneVariable, oneVariable);
                g.drawString(array[i][j] + "", 45 * 2 + j * oneVariable, 10 * 3 + i * oneVariable);
            }
        }
    }

    public void doNewArray(int[][] array) {
        this.array = array;
    }

}


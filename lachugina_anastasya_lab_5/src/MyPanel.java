import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {

    private int[][] array;
    private int sizeI;
    private int sizeJ;
    private int indent = 10;
    private int sizeOfOneCell = 30;

    MyPanel(int sizeI, int sizeJ) {
        this.sizeI = sizeI;
        this.sizeJ = sizeJ;
    }

    public void paint(Graphics g) {
        super.paint(g);
        for (int i = 0; i < sizeI; i++) {
            if (array[i][sizeJ - 1] == 0) {
                g.setColor(Color.BLUE);
            } else {
                g.setColor(Color.RED);
            }
                for (int j = 0; j < sizeJ; j++) {
                g.drawRect(indent + j * sizeOfOneCell, indent + i * sizeOfOneCell, sizeOfOneCell, sizeOfOneCell);
                g.drawString(array[i][j] + "", indent * 2 + j * sizeOfOneCell, indent * 3 + i * sizeOfOneCell);
            }
        }
    }

    public void setNewArray(int[][] array) {
        this.array = array;
    }
}

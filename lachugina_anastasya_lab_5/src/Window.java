import javax.swing.*;
import java.awt.Color;
import javax.swing.border.*;

public class Window {
    Minimization minimization = new Minimization();

    public static void main(String[] args) {
        Window window = new Window();
        window.frame.setVisible(true);
    }

    private JFrame frame;
    private MyPanel myPanel;
    Color yellowColor = new Color(248, 253, 145);
    Color blueColor = new Color(153, 217, 234);
    private JButton buttonNewArray = new JButton("Новые значения");
    private JButton buttonDNF = new JButton("ДНФ");
    private JButton buttonKNF = new JButton("КНФ");
    private JTextArea minimizedFunction = new JTextArea();

    private int sizeI = 16;
    private int sizeJ = 5;
    private int[][] array = {
            {0, 0, 0, 0, (int) (Math.random() * 2)},
            {0, 0, 0, 1, (int) (Math.random() * 2)},
            {0, 0, 1, 0, (int) (Math.random() * 2)},
            {0, 0, 1, 1, (int) (Math.random() * 2)},
            {0, 1, 0, 0, (int) (Math.random() * 2)},
            {0, 1, 0, 1, (int) (Math.random() * 2)},
            {0, 1, 1, 0, (int) (Math.random() * 2)},
            {0, 1, 1, 1, (int) (Math.random() * 2)},
            {1, 0, 0, 0, (int) (Math.random() * 2)},
            {1, 0, 0, 1, (int) (Math.random() * 2)},
            {1, 0, 1, 0, (int) (Math.random() * 2)},
            {1, 0, 1, 1, (int) (Math.random() * 2)},
            {1, 1, 0, 0, (int) (Math.random() * 2)},
            {1, 1, 0, 1, (int) (Math.random() * 2)},
            {1, 1, 1, 0, (int) (Math.random() * 2)},
            {1, 1, 1, 1, (int) (Math.random() * 2)},
    };

    Window() {
        frame = new JFrame();
        frame.setBounds(100, 100, 360, 550);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.getContentPane().setBackground(yellowColor);

        myPanel = new MyPanel(sizeI,sizeJ);
        myPanel.setNewArray(array);
        myPanel.setBounds(0, 0, 180, 500);
        myPanel.setBackground(yellowColor);
        frame.getContentPane().add(myPanel);

        minimizedFunction.setBounds(180, 130, 150, 362);
        minimizedFunction.setBackground(blueColor);
        minimizedFunction.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        frame.getContentPane().add(minimizedFunction);

        buttonNewArray.addActionListener(e -> makeNewArray());
        buttonNewArray.setBounds(180, 10, 150, 30);
        frame.getContentPane().add(buttonNewArray);

        buttonDNF.addActionListener(e -> drawDNF());
        buttonDNF.setBounds(180, 50, 150, 30);
        frame.getContentPane().add(buttonDNF);

        buttonKNF.addActionListener(e -> drawKNF());
        buttonKNF.setBounds(180, 90, 150, 30);
        frame.getContentPane().add(buttonKNF);
    }

    private void makeNewArray() {
        int[][] newArray = {
                {0, 0, 0, 0, (int) (Math.random() * 2)},
                {0, 0, 0, 1, (int) (Math.random() * 2)},
                {0, 0, 1, 0, (int) (Math.random() * 2)},
                {0, 0, 1, 1, (int) (Math.random() * 2)},
                {0, 1, 0, 0, (int) (Math.random() * 2)},
                {0, 1, 0, 1, (int) (Math.random() * 2)},
                {0, 1, 1, 0, (int) (Math.random() * 2)},
                {0, 1, 1, 1, (int) (Math.random() * 2)},
                {1, 0, 0, 0, (int) (Math.random() * 2)},
                {1, 0, 0, 1, (int) (Math.random() * 2)},
                {1, 0, 1, 0, (int) (Math.random() * 2)},
                {1, 0, 1, 1, (int) (Math.random() * 2)},
                {1, 1, 0, 0, (int) (Math.random() * 2)},
                {1, 1, 0, 1, (int) (Math.random() * 2)},
                {1, 1, 1, 0, (int) (Math.random() * 2)},
                {1, 1, 1, 1, (int) (Math.random() * 2)},
        };
        array = newArray;
        myPanel.setNewArray(array);
        minimizedFunction.setText("");
        myPanel.repaint();
    }

    private void drawKNF() {
        minimizedFunction.setText(minimization.conNF(array,sizeI,sizeJ));
    }

    private void drawDNF() {
        minimizedFunction.setText(minimization.disNF(array,sizeI,sizeJ));
    }
}


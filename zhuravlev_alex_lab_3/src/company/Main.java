package company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            System.out.print("Введите исходную c.c (2-16): ");
            int originalSON = in.nextInt();
            System.out.print("Введите первое число: ");
            String firstNum = in.next();
            System.out.print("Введите второе число: ");
            String secondNum = in.next();

            System.out.print("Введите операцию котороую нужно выполнить: | + | - | * | / |→ ");
            String operation = in.next();

            System.out.println("Результат: " + BinaryOperations.calculate(originalSON, firstNum, secondNum, operation));
        }
        catch (Exception e) {
            System.out.println("Ошибка! Введите корректное значение!");
        }
    }
}

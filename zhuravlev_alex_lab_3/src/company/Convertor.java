package company;

public class Convertor {

    private static final char[] CodeChar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final int[] CodeInt = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

    public static String convertToUnsignedBinaryNumber (int originalSON, String num) {
        int value = convertToDecimalNumber(originalSON, unsignedFormat(num));
        int ranks = 0;
        //перевод в 2 с.с
        int newSS = 2;
        while (value >= newSS) {
            ranks++;
            value /= newSS;
        }
        ranks++;

        char[] Result = new char[ranks];
        value = convertToDecimalNumber(originalSON, num);

        for (int i = ranks - 1; i > -1; i--) {
            for (int j = 0; j < newSS; j++) {
                if (value % newSS == CodeInt[j]) {
                    Result[i] = CodeChar[j];
                    value /= newSS;
                    break;
                }
            }
        }
        return new String(Result);
    }

    private static int convertToDecimalNumber(int originalSON, String num) {
        char[] valueCharArray = num.toCharArray();
        int value = 0;

        for (int i = 0; i < valueCharArray.length; i++) {
            int power = valueCharArray.length - i - 1;

            for (int j = 0; j < originalSON; j++) {
                if (valueCharArray[i] == CodeChar[j]) {
                    value += CodeInt[j] * (int) Math.pow(originalSON, power);
                    break;
                }
            }
        }
        return value;
    }

    private static String unsignedFormat(String value) { //метод для присваивания беззнаковых значений
        StringBuilder unsignedValue = new StringBuilder();

        if (value.toCharArray()[0] == '-') {
            char[] valueArray = value.toCharArray();

            for (int i = 1; i < valueArray.length; i++) {
                unsignedValue.append(valueArray[i]);
            }
            return unsignedValue.toString();
        }
        else {
            return value;
        }
    }
}

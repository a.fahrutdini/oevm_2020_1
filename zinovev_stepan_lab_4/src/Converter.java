public class Converter {

    private static final int fIndex = 4;

    public static String convertToDNF(int[][] truthTable) {
        StringBuilder result = new StringBuilder("(");
        boolean inProcess = false;
        for (int[] ints : truthTable) {
            if (ints[fIndex] == 1) {
                if (inProcess) {
                        result.append(") + (");
                }
                for (int j = 0; j < ints.length - 1; j++) {
                    if (ints[j] == 0) {
                        result.append("!");
                    }
                    result.append("X").append(j + 1);
                    if (j != ints.length - 2) {
                        result.append(" * ");
                    }
                }
                inProcess = true;
            }
        }
        result.append(")");
        return result.toString();
    }

    public static String convertToKNF(int[][] truthTable) {
        StringBuilder result = new StringBuilder("(");
        boolean inProcess = false;
        for (int[] ints : truthTable) {
            if (ints[fIndex] == 0) {
                if (inProcess) {
                        result.append(") * (");
                }
                for (int j = 0; j < ints.length - 1; j++) {
                    if (ints[j] == 1) {
                        result.append("!");
                    }
                    result.append("X").append(j + 1);
                    if (j != ints.length - 2) {
                        result.append(" + ");
                    }
                }
                inProcess = true;
            }
        }
        result.append(")");
        return result.toString();
    }
}
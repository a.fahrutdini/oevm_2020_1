package com.company;

import java.util.Random;

public class TableOfTrue {

    private final int n;            //Кол-во строк таблицы
    private final int m;            //Кол-во столбцов таблицы
    private final int[][] table;    //Таблица истинности
    private final int[] vector;     //Вектор - ответ таблицы истинности

    /**
     * Метод объявления таблицы
     *
     * @param n кол-во строк
     * @param m кол-во столбцов
     */
    public TableOfTrue(int n, int m) {
        this.n = n;
        this.m = m;
        table = new int[n][m];
        vector = new int[n];
        fillingTable();             //Заполняем таблицу
        fillingVector();            //Задаём решение таблцы
    }

    /**
     * Метод заполнения заблицы
     */
    public void fillingTable() {

        int count;
        boolean flag;

        for (int j = m - 1; j >= 0; j--) {
            count = (int) Math.pow(2, m - j - 1);
            flag = false;
            for (int i = 0; i < n; i++) {
                if (count == 0) {
                    count = (int) Math.pow(2, m - j - 1);
                    flag = !flag;
                }
                if (flag) {
                    table[i][j] = 1;
                } else {
                    table[i][j] = 0;
                }
                count--;
            }
        }
    }

    /**
     * Метод задания ответа таблице
     */
    public void fillingVector() {
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            vector[i] = random.nextInt(2);
        }
    }

    /**
     * Вывод таблицы
     */
    public void printf() {
        for (int i = 0; i < n; i++) {
            System.out.print("|");
            for (int j = 0; j < m; j++) {
                System.out.print(" " + table[i][j]);
            }
            System.out.print(" | " + vector[i] + " |\n");
        }
    }

    /**
     * Метод минимизации таблицы истинности - КНФ
     */
    public void KNF() {
        StringBuilder sOut = new StringBuilder("KNF is : ");
        boolean flag = true;
        int countS = 0;
        for (int i = 0; i < n; i++) {
            if (vector[i] == 0) {
                if (flag) {
                    sOut.append("(");
                    countS += 1;
                    flag = false;
                } else {
                    sOut.append("*(");
                    countS += 2;
                }
                for (int j = 0; j < m; j++) {
                    if (table[i][j] == 1) {
                        sOut.append("-X");
                        countS += 2;
                    } else {
                        sOut.append("X");
                        countS += 1;
                    }
                    sOut.append(j + 1);
                    if (j < m - 1) {
                        sOut.append(" + ");
                        countS += 3;
                    }
                }
                sOut.append(")");
                countS += 1;
            }
            if (countS >= 80) {
                sOut.append("\n");
                countS = 0;
            }
        }
        System.out.println(sOut.append("\n"));
    }

    /**
     * Метод минимизации таблицы истинности - ДНФ
     */
    public void DNF() {
        StringBuilder sOut = new StringBuilder("DNF is : ");
        boolean flag = true;
        int countS = 0;
        for (int i = 0; i < n; i++) {
            if (vector[i] == 1) {
                if (flag) {
                    sOut.append("(");
                    countS++;
                    flag = false;
                } else {
                    sOut.append("+(");
                    countS += 2;
                }
                for (int j = 0; j < m; j++) {
                    if (table[i][j] == 0) {
                        sOut.append("-X");
                        countS += 2;
                    } else {
                        sOut.append("X");
                        countS++;
                    }
                    sOut.append(j + 1);
                    if (j < m - 1) {
                        sOut.append(" * ");
                        countS += 3;
                    }
                }
                sOut.append(")");
                countS++;
            }
            if (countS >= 80) {
                sOut.append("\n");
                countS = 0;
            }
        }
        System.out.println(sOut.append("\n"));
    }
}

package com;

public class ArithmeticOperation {

    private int _numArithmeticOperation;     // номер операции
    private String _firstValue;             //  первое число
    private String _secondValue;            //  второе число
    private static String _result = "";      //  результата

    public ArithmeticOperation(int checkArithmeticOperation, String firstValue, String secondValue) {
        _numArithmeticOperation = checkArithmeticOperation;
        _firstValue = firstValue;
        _secondValue = secondValue;

        if (_numArithmeticOperation == 1) {
            swapValue(_firstValue, _secondValue);
            addZeroSecondValue(_firstValue, _secondValue);
            sumValue(_firstValue, _secondValue);
            System.out.println("Результат:" + _result);
        } else if (_numArithmeticOperation == 2) {
            swapValue(_firstValue, _secondValue);
            addZeroSecondValue(_firstValue, _secondValue);
            subtractionValue(_firstValue, _secondValue);
            if (secondValue.length() > firstValue.length()) {
                System.out.println("Результат:-" + _result);
            } else
                System.out.println("Результат:" + _result);

        } else if (_numArithmeticOperation == 3) {
            multiplyValue(_firstValue, _secondValue);
            System.out.println("Результат:" + _result);
        } else if (_numArithmeticOperation == 4) {
            divideValue(_firstValue, _secondValue);
            System.out.println("Результат:" + _result);
        }
        return;
    }

    private String sumValue(String firstValue, String secondValue) {
        char[] firstValueArray = new StringBuffer(firstValue).reverse().toString().toCharArray();
        char[] secondValueArray = new StringBuffer(secondValue).reverse().toString().toCharArray();
        boolean flag = false;     // локальная переменная для отслеживания переходящей еденицы
        for (int i = 0; i < secondValueArray.length; i++) {
            if (firstValueArray[i] == '0' && secondValueArray[i] == '0') {
                if (flag == false) {
                    flag = false;
                    _result += "0";
                } else if (flag == true) {
                    flag = false;
                    _result += "1";
                }
            } else if ((firstValueArray[i] == '1' && secondValueArray[i] == '0') || (firstValueArray[i] == '0' && secondValueArray[i] == '1')) {
                if (flag == false) {
                    flag = false;
                    _result += "1";
                } else if (flag == true) {
                    flag = true;
                    _result += "0";
                }
            } else if (firstValueArray[i] == '1' && secondValueArray[i] == '1') {
                if (flag == false) {
                    flag = true;
                    _result += "0";
                } else if (flag == true) {
                    flag = true;
                    _result += "1";
                }
            }
        }
        for (int i = firstValueArray.length - 1; i < firstValueArray.length; i++) {
            if (firstValueArray[i] == 0) {
                if (flag == false) {
                    flag = false;
                    _result += "0";
                } else if (flag == true) {
                    flag = false;
                    _result += "1";
                }
            } else if (firstValueArray[i] == 1) {
                if (flag == false) {
                    flag = false;
                    _result = "1";
                } else if (flag == true) {
                    flag = true;
                    _result += "0";
                }
            }
        }
        if (flag == true) {
            _result += "10";
        }
        _result = new StringBuffer(result).reverse().toString();
        return _result;
    }

    private String subtractionValue(String firstValue, String secondValue) {
        char[] firstValueArray = new StringBuffer(firstValue).reverse().toString().toCharArray();
        char[] secondValueArray = new StringBuffer(secondValue).reverse().toString().toCharArray();
        boolean flag = false;   // локальная переменная для отслеживания еденицы при заеме
        for (int i = 0; i < secondValueArray.length; i++) {
            if (firstValueArray[i] == '0' && secondValueArray[i] == '0') {
                if (flag == false) {
                    flag = false;
                    _result += "0";
                } else if (flag == true) {
                    flag = true;
                    _result += "0";
                }
            } else if (firstValueArray[i] == '1' && secondValueArray[i] == '0') {
                if (flag == false) {
                    flag = false;
                    _result += "1";
                } else if (flag == true) {
                    flag = false;
                    _result += "0";
                }
            } else if (firstValueArray[i] == '0' && secondValueArray[i] == '1') {
                if (flag == false) {
                    flag = true;
                    _result += "1";
                } else if (flag == true) {
                    flag = true;
                    _result += "0";
                }
            } else if (firstValueArray[i] == '1' && secondValueArray[i] == '1') {
                if (flag == false) {
                    flag = true;
                    _result += "0";
                } else if (flag == true) {
                    flag = true;
                    _result += "0";
                }
            }
        }
        for (int i = firstValueArray.length - 1; i < firstValueArray.length; i++) {
            if (firstValueArray[i] == 0) {
                if (flag == false) {
                    flag = false;
                    _result += "0";
                } else if (flag == true) {
                    flag = true;
                    _result += "1";
                }
            } else if (firstValueArray[i] == 1) {
                if (flag == false) {
                    flag = false;
                    _result = "1";
                } else if (flag == true) {
                    flag = false;
                    _result += "0";
                }
            }
        }
        _result = new StringBuffer(result).reverse().toString();
        return _result;
    }

    private String multiplyValue(String firstValue, String secondValue) {
        char[] secondNumArray = new StringBuffer(secondValue).reverse().toString().toCharArray();
        String helpResult = "0";        // локальная переменная для промежуточного результата в умножении
        int helpSymbol = 0;             //вспомогательная переменная для прохождения по циклу присвоением значения первому массиву символов(первой переменной)
        for (int i = 0; i < secondNumArray.length; i++) {
            if (secondNumArray[i] == '1') {
                for (int j = 0; j < i - helpSymbol; j++) {
                    firstValue += "0";
                }
                new ArithmeticOperation(1, firstValue, helpResult);
                helpResult = _result;
                helpSymbol = i;
            }
        }
        return helpResult;
    }

    private String divideValue(String firstValue, String secondValue) {
        String comparison = secondValue;
        while (compareNumbers(firstValue, comparison)) {
            swapValue(result, "1");
            addZeroSecondValue(_firstValue, _secondValue);
            sumValue(_firstValue, _secondValue);
            comparison = sumValue(comparison, secondValue);
        }
        System.out.print(_result);
        return _result;

    }

    /*
     * Функция которая меняет строки местами,
     * ставя на первую позицию самую длинну строку
     */
    private String swapValue(String firstValue, String secondValue) {
        String temporaryString = "";
        if (firstValue.length() < secondValue.length()) {
            temporaryString = firstValue;
            firstValue = secondValue;
            secondValue = temporaryString;
        }
        _firstValue = firstValue;
        _secondValue = secondValue;
        return temporaryString;
    }

    /*
     * Функция добавления нулей в незначащие позиции,
     * чтобы было удобнее производить сложение
     */
    private String addZeroSecondValue(String firstValue, String secondValue) {
        int col = firstValue.length() - secondValue.length();
        while (col > 0) {
            secondValue = "0" + secondValue;
            col--;
        }
        _firstValue = firstValue;
        _secondValue = secondValue;
        return _secondValue;
    }

    private static boolean compareNumbers(String firstValue, String secondValue) {
        char[] firstNumArray = firstValue.toCharArray();
        char[] secondNumArray = secondValue.toCharArray();

        if (firstNumArray.length > secondNumArray.length) {
            return true;
        } else if (firstNumArray.length < secondNumArray.length) {
            return false;
        } else {
            for (int i = 0; i < firstNumArray.length; i++) {
                if (firstNumArray[i] == '1' && secondNumArray[i] == '0') {
                    return true;
                } else if (firstNumArray[i] == '0' && secondNumArray[i] == '1') {
                    return false;
                }
            }
        }
        return true;
    }
}




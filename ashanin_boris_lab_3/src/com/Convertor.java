package com;

public class Convertor {
    private static int _startSS;        // начальная система счисления
    private static char[] _value;       // переменная для конвертации
    private static String _otvet = "";  // результат
    public static String _valueIn2SS;   // переменная в конечной системе счисления

    public Convertor(int startSS, String value) {
        _value = value.toCharArray();
        _startSS = startSS;
        convertTo10SS(_startSS, _value);
        return;
    }

    public static char[] convertTo10SS(int _startSS, char[] _value) {
        int res = 0;
        char[] simValue = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        int[]  chValue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

        for (int i = 0; i < _value.length; i++) {
            for (int j = 0; j < simValue.length; j++) {
                if (_value[i] == simValue[j]) {
                    res += chValue[j] * Math.pow(_startSS, _value.length - i - 1);
                }
            }
        }
        Convertor.convertToEndSS(res);
        return _value;
    }

    public static int convertToEndSS(int res) {

        int osn = 2;    // основание
        int ost;        // остаток при делении
        do {
            ost = res % osn;
            res = res / osn;
            if (ost < 10) {
                otvet = ost + _otvet;
            }
        }
        while (res / osn != 0);
        ost = res % osn;
        res = res / osn;
        if (ost < 10) {
            otvet = ost + otvet;
        }
        assignValue(_otvet);
        _otvet = "";
        return res;
    }

    public static String assignValue(String otvet) {
        _valueIn2SS = otvet;
        return _valueIn2SS;
    }
}









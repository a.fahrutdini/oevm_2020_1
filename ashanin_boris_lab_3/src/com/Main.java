package com;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int _startSS = 0;                       // начальная система счисления
        String _firstValue                      // первое число
        String _secondValue                     // второе число
        String _firstValueIn2SS                 // первое число в конечной системе счисления
        String _secondValueIn2SS                // второе число в конечной системе счисления
        int _checkArithmeticOperation           // номер операции

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите исходную систему счисления\n");
        _startSS = scanner.nextInt();

        System.out.print("Введите первое число\n");
        Scanner s = new Scanner(System.in);
        _firstValue = s.nextLine();

        System.out.print("Введите второе число\n");
        _secondValue = s.nextLine();

        Convertor First = new Convertor(_startSS, _firstValue);
        _firstValueIn2SS = First._valueIn2SS;
        Convertor Second = new Convertor(_startSS, _secondValue);
        _secondValueIn2SS = Second._valueIn2SS;

        System.out.print("Введите номер арифметическое дейсвия из следующего списка:\n1)|+|\n2) |-|\n3) |*|\n4)|/|");
        _checkArithmeticOperation = scanner.nextInt();
        ArithmeticOperation top = new ArithmeticOperation(_checkArithmeticOperation, _firstValueIn2SS, _secondValueIn2SS);

    }
}

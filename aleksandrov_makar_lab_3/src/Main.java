import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            System.out.print("Введите исходную систему счисления: ");
            int originalSON = in.nextInt();

            System.out.print("Введите первое число: ");
            String firstNum = in.next();

            System.out.print("Введите второе число: ");
            String secondNum = in.next();

            System.out.print("Введите арифметическую операцию: ");
            String operation = in.next();

            System.out.println("Результат: " + BinaryCalculator.calculate(originalSON, firstNum, secondNum, operation));
        }

        catch (Exception e) {
            System.out.println("Введите корректные данные");
        }
    }
}

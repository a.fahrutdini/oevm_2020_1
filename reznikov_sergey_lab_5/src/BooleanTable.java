import java.awt.*;

public class BooleanTable {

    char[][] table;
    public final int ELEMENTS_NUMBER = 4;
    final int COLUMN_SIZE = 60;
    final int STRING_SIZE = 40;
    final private char[] words = {'a', 'b', 'c', 'd', 'e', 'f', 'g'};
    /*1 - Дизъюнктивная форма
     * 0 - Конъюнктивная форма
     * 2 - и вовсе был дурак*/
    private char colorField = '2';

    public int checkXSize(){
        int field =(ELEMENTS_NUMBER+1)*COLUMN_SIZE;
        return field+10;
    }

    public int checkYSize(){
        int field = (int)(Math.pow(2, ELEMENTS_NUMBER)+1)*STRING_SIZE;
        return field+10;
    }

    public void changeNumbers(){
        table = new char[(int) Math.pow(2, ELEMENTS_NUMBER)][];

        for (int i = 0; i < table.length; i++) {
            table[i] = new char[ELEMENTS_NUMBER + 1];
            for (int k = 0; k < ELEMENTS_NUMBER + 1; k++) {
                String tmp = "" + (int) (Math.random() * 2);
                table[i][k] = tmp.toCharArray()[0];
            }
        }

        colorField='2';
    }

    public BooleanTable() {
        table = new char[(int) Math.pow(2, ELEMENTS_NUMBER)][];

        for (int i = 0; i < table.length; i++) {
            table[i] = new char[ELEMENTS_NUMBER + 1];
            for (int k = 0; k < ELEMENTS_NUMBER + 1; k++) {
                String tmp = "" + (int) (Math.random() * 2);
                table[i][k] = tmp.toCharArray()[0];
            }
        }
    }

    public String getKForm() {
        StringBuffer str = new StringBuffer("");
        str.append("f(");
        for(int i=0;i<ELEMENTS_NUMBER;i++){
            str.append(words[i] + ", ");
        }
        str.delete(str.length()-2,str.length());
        str.append(") = \n");

        for (int i = 0; i < table.length; i++) {
            if (table[i][ELEMENTS_NUMBER] == '0') {
                str.append("(");
                for (int k = 0; k < ELEMENTS_NUMBER; k++) {
                    if (table[i][k] == '1') {
                        str.append("-" + words[k]);
                    } else {
                        str.append(words[k]);
                    }
                    if (k != ELEMENTS_NUMBER - 1) str.append(" + ");
                }
                str.append(") * \n * ");
            }
        }
        str.delete(str.length() - 6, str.length());
        return str.toString();
    }

    public String getDForm() {
        StringBuffer str = new StringBuffer("");
        str.append("f(");
        for(int i=0;i<ELEMENTS_NUMBER;i++){
            str.append(words[i] + ", ");
        }
        str.delete(str.length()-2,str.length());
        str.append(") = \n");

        for (int i = 0; i < table.length; i++) {
            if (table[i][ELEMENTS_NUMBER] == '1') {
                str.append("(");
                for (int k = 0; k < ELEMENTS_NUMBER; k++) {
                    if (table[i][k] == '0') {
                        str.append("-" + words[k]);
                    } else {
                        str.append(words[k]);
                    }
                    if (k != ELEMENTS_NUMBER - 1) str.append(" * ");
                }
                str.append(") + \n + ");
            }
        }

        str.delete(str.length() - 6, str.length());
        return str.toString();
    }

    public void Draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Courier", Font.BOLD + Font.ITALIC, 16));
        int i = 0;
        while(i<ELEMENTS_NUMBER){
            g.drawRect(i * COLUMN_SIZE, 0, COLUMN_SIZE, STRING_SIZE);
            i++;
        }
        g.drawRect(i * COLUMN_SIZE, 0, COLUMN_SIZE, STRING_SIZE);

        for (int k = 0; k < table.length; k++) {
            for (int t = 0; t < table[k].length; t++) {
                g.drawRect(t * COLUMN_SIZE, STRING_SIZE+k*STRING_SIZE, COLUMN_SIZE, STRING_SIZE);
            }
        }

        g.setColor(Color.GREEN);
        for (int k = 0; k < table.length; k++) {
            if(table[k][ELEMENTS_NUMBER]==colorField) {
                for (int t = 0; t < table[k].length; t++) {
                    g.drawRect(t * COLUMN_SIZE, STRING_SIZE + k * STRING_SIZE, COLUMN_SIZE, STRING_SIZE);
                }
            }
        }

        g.setColor(Color.BLACK);
        g.setFont(new Font("Courier", Font.BOLD + Font.ITALIC, 16));
        i = 0;
        while(i<ELEMENTS_NUMBER){
            g.drawString("" + words[i], (int) (i * COLUMN_SIZE + 0.5 * COLUMN_SIZE), (int) STRING_SIZE/2);
            i++;
        }
        g.drawString("" + words[i], (int) (i * COLUMN_SIZE + 0.5 * COLUMN_SIZE), (int) STRING_SIZE/2);

        for (int k = 0; k < table.length; k++) {
            for (int t = 0; t < table[k].length; t++) {
                g.drawString("" + table[k][t], (int) (t * COLUMN_SIZE + 0.5 * COLUMN_SIZE), (int) (k+1)*STRING_SIZE+STRING_SIZE/2);
            }
        }

    }

    public void KFormColor() {
        colorField='0';
    }

    public void DFormColor() {
        colorField='1';
    }
}

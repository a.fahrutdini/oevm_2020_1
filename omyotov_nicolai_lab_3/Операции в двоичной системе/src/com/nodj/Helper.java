package com.nodj;

import java.util.Scanner;

public class Helper {
    void checkBase(String message, Scanner sc, Converter converter, Converter converter2) {
        while (true) {
            System.out.println(message);
            try {
                int number = sc.nextInt();
                if (number < 2 || number > 16) {
                    System.out.println("Неверно! Число должно быть в диапозоне (2-16)!");
                } else {
                    converter.setBaseIn(number);
                    converter.setBaseOut(2);
                    converter2.setBaseIn(number);
                    converter2.setBaseOut(2);
                    break;
                }
            } catch (Exception e) {
                System.out.println("Вы ввели не число!");
                sc.next();
            }
        }
    }

    void checkNum(String message, Scanner sc, Converter converter) {
        while (true) {
            System.out.println(message);
            try {
                converter.setNumIn(sc.next().toUpperCase());
                break;
            } catch (Exception e) {
                System.out.println("Вы ввели не число!");
                sc.next();
            }
        }
    }

    void checkOperation(String message, Scanner sc, Calculator calc) {
        while (true) {
            System.out.println(message);
            boolean isSuccess = false;
            String operation = sc.next();
            switch (operation) {
                case "+":
                case "-":
                case "*":
                case "/":
                    calc.setOperation(operation.charAt(0));
                    isSuccess = true;
                    break;
                default:
                    System.out.println("Вы ввели не операцию!");
                    sc.next();
            }
            if (isSuccess) {
                break;
            }
        }
    }
}

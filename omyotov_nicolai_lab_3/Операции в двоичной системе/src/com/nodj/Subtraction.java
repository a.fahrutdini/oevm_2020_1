package com.nodj;

import java.util.ArrayList;
import java.util.Collections;

public class Subtraction {
    public boolean isNegative = false;
    private ArrayList<Integer> fNum;
    private ArrayList<Integer> sNum;
    private boolean isEqual = false;

    Subtraction(ArrayList<Integer> firstNum, ArrayList<Integer> secondNum) {
        if (firstNum.size() == secondNum.size()) {
            for (int j = 0; j < firstNum.size(); j++) {
                if (firstNum.get(j) > secondNum.get(j)) {
                    fNum = new ArrayList<>(firstNum);
                    sNum = new ArrayList<>(secondNum);
                    isEqual = false;
                    break;
                } else if (firstNum.get(j) < secondNum.get(j)) {
                    isNegative = true;
                    fNum = new ArrayList<>(secondNum);
                    sNum = new ArrayList<>(firstNum);
                    isEqual = false;
                    break;
                }
                isEqual = true;
            }
        } else if (firstNum.size() < secondNum.size()) {
            isNegative = true;
            fNum = new ArrayList<>(secondNum);
            sNum = new ArrayList<>(firstNum);
        } else {
            fNum = new ArrayList<>(firstNum);
            sNum = new ArrayList<>(secondNum);
        }
    }

    public ArrayList<Integer> sub() {
        if (isEqual) {
            ArrayList<Integer> zero = new ArrayList<>();
            zero.add(0);
            return zero;
        }
        Collections.reverse(sNum);
        makeAdditionOfNumber();
        Collections.reverse(sNum);

        addOne();
        Summation sum2 = new Summation(fNum, sNum);
        ArrayList<Integer> sub = sum2.sum();
        removeSuperfluousFigures(sub);
        return sub;
    }

    private void makeAdditionOfNumber() {
        for (int i = 0; i < fNum.size(); i++) {
            if (i < sNum.size()) {
                if (sNum.get(i) == 1) {
                    sNum.set(i, 0);
                } else {
                    sNum.set(i, 1);
                }
            } else {
                sNum.add(1);
            }
        }
    }

    private void addOne() {
        ArrayList<Integer> one = new ArrayList<>();
        one.add(1);
        Summation sum = new Summation(sNum, one);
        sNum = sum.sum();
    }

    private void removeSuperfluousFigures(ArrayList<Integer> sub) {
        sub.remove(0);
        while (sub.get(0) == 0) {
            sub.remove(0);
        }
    }
}

package EVM_Lab_3;

public class Operations {

    static final int COLUMN = 15;
    static final int LEN = 4;
    static final int A = 0;
    static final int B = 1;
    static final int C = 2;
    static final int D = 3;

    public static String convertToBin(int num, String result) {
        while (((num / 2 != 0)) || (num % 2 != 0)) {
            result += num % 2;
            num = num / 2;
        }
        result = new StringBuffer(result).reverse().toString();
        while (result.length() < LEN) {
            result = "0" + result;
        }
        return result;
    }

    public static void printTable(int[][] table) {
        String str = "";
        char oneSymbolOfLine = ' ';
        int randNum = 0;
        for (int i = 0; i <= COLUMN; i++) {
            str = "";
            str = convertToBin(i, str);
            for (int j = 0; j < LEN; j++) {
                oneSymbolOfLine = str.charAt(j);
                if (oneSymbolOfLine == '1') {
                    table[i][j] = 1;
                } else {
                    table[i][j] = 0;
                }
                System.out.print(" " + oneSymbolOfLine + " ");
            }
            randNum = (int) (Math.random() * 2);
            table[i][LEN] = randNum;
            System.out.println(" " + randNum);
        }
    }

    public static void getKNF(int[][] table) {
        char signA = ' ';
        char signB = ' ';
        char signC = ' ';
        char signD = ' ';
        boolean firstIteration = true;
        for (int i = 0; i <= COLUMN; i++) {
            if (table[i][LEN] == 0) {
                if (table[i][A] == 1) {
                    signA = '-';
                }
                if (table[i][B] == 1) {
                    signB = '-';
                }
                if (table[i][C] == 1) {
                    signC = '-';
                }
                if (table[i][D] == 1) {
                    signD = '-';
                }
                if (firstIteration) {
                    System.out.print("(" + signA + "A +" + signB + "B +" + signC + "C +" + signD + "D)");
                    firstIteration = false;
                } else {
                    System.out.print(" * (" + signA + "A +" + signB + "B +" + signC + "C +" + signD + "D)");
                }
                signA = ' ';
                signB = ' ';
                signC = ' ';
                signD = ' ';
            }
        }
    }

    public static void getDNF(int[][] table) {
        char signA = ' ';
        char signB = ' ';
        char signC = ' ';
        char signD = ' ';
        boolean firstIteration = true;

        for (int i = 0; i <= COLUMN; i++) {
            if (table[i][LEN] == 1) {
                if (table[i][A] == 0) {
                    signA = '-';
                }
                if (table[i][B] == 0) {
                    signB = '-';
                }
                if (table[i][C] == 0) {
                    signC = '-';
                }
                if (table[i][D] == 0) {
                    signD = '-';
                }
                if (firstIteration) {
                    System.out.print("(" + signA + "A *" + signB + "B *" + signC + "C *" + signD + "D)");
                    firstIteration = false;
                } else {
                    System.out.print(" + (" + signA + "A *" + signB + "B *" + signC + "C *" + signD + "D)");
                }
                signA = ' ';
                signB = ' ';
                signC = ' ';
                signD = ' ';
            }
        }
    }
}

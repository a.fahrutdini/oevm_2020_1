import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Введите систему счисления : ");
        int SS = in.nextInt();

        System.out.print("Введите первое число: ");
        String firstNum = in.next();

        System.out.print("Введите второе число: ");
        String secondNum = in.next();

        System.out.print("Введите знак операции (+,-,*,/) : ");
        char action = in.next().charAt(0);

        Helper help = new Helper(SS, firstNum, secondNum, action);
        System.out.print(help.calculate());
    }
}
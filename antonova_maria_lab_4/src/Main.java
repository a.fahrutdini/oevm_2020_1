package com.company;

public class Main {

    public static void main(String[] args) {
        try {
            int tWidth = 5;
            int tLength = 16;

            TruthTable table = new TruthTable(tLength, tWidth);
            table.fillTable();
            table.printTable();

            Getter g = new Getter();
            g.showMenu();
            int function;
            do{
                function = g.getValue();
                if (function == 1){
                    DNF dnf = new DNF(table);
                    dnf.printResult();
                }
                if (function == 2) {
                    CNF cnf = new CNF(table);
                    cnf.printResult();
                }
            } while (function != 0);
        }
        catch (Exception l) {
            System.out.println(l);
        }
    }
}
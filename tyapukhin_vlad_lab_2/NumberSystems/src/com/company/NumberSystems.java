package com.company;

public class NumberSystems {

    private int a;
    private int b;
    private char[] arr;

    NumberSystems(int a, int b, char[] arr) {

        this.a = a;
        this.b = b;
        this.arr = arr;
    }

    public void number_Systems() {

        StringBuilder endNumStr;

        //Получаем число в десятичной CC
        int number10 = transIn10SS(arr);

        //Переводим его в нужную СС
        endNumStr = transInEndSS(number10);
        System.out.println(endNumStr);
    }

    private int transIn10SS(char[] arr) {

        int number = 0;

        for (int i = 0; i < arr.length; i++) {
            int temp = 1;
            if (((int)arr[i] >= (int)'0') && ((int)arr[i] <= (int)'9'))
                temp = (int)arr[i] - (int)'0';
            else if (((int)arr[i] >= (int)'A') && ((int)arr[i] <= (int)'Z'))
                temp = (int)arr[i] + 10 - (int)'A';

            number += temp * (Math.pow(a, arr.length - i - 1));
        }

        return number;
    }

    private StringBuilder transInEndSS(int number) {

        StringBuilder endNumStr = new StringBuilder();

        String result = "";
        int tmp = 0;
        String tmp_str = "";

        while (number >= b) {
            tmp = number % b;
            tmp_str = "";

            if ((number % b) < 10)
                tmp_str = String.valueOf(tmp);
            else
                tmp_str += (char)((int)'A' - 10 + tmp);

            result = tmp_str + result;
            number /= b;
        }

        if ((number % b) < 10)
            tmp_str = String.valueOf(number);
        else
            tmp_str = String.valueOf((char)((int)'A' - 10 + number));

        endNumStr.append(tmp_str + result);

        return endNumStr;
    }
}


package EVm_lab_2;

public class Main {

    public static void main(String[] args) {
        int initialNot = UserInput.getNotation("исходную");
        if (initialNot == 0) {
            System.out.println("Некорректный ввод");
            return;
        }

        int finalNot = UserInput.getNotation("конечную");
        if (finalNot == 0) {
            System.out.println("Некорректный ввод");
            return;
        }

        String num, result;
        try {
            num = UserInput.getNum(initialNot);
        } catch (Exception e) {
            System.out.println("Некорректный ввод");
            return;
        }
        try {
            result = Converter.convertTo(num, initialNot, finalNot);
        } catch (Exception e) {
            System.out.println("Некорректный ввод");
            return;
        }
        System.out.println(result);
    }
}

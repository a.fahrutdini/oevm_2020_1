import java.io.IOException;
import java.util.Scanner;

public class Laba {

    public static void main(String[] args) throws IOException {

        Converter cv = new Converter();

        cv.setNumbers(10, "333", "222");
        System.out.println("10, 333 + 222");
        cv.sumNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "-333", "222");
        System.out.println("10, -333 + 222");
        cv.sumNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "222", "-333");
        System.out.println("10, 222 + -333");
        cv.sumNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "-333", "-222");
        System.out.println("10, -333 + -222");
        cv.sumNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        System.out.println("___________________________");
        //__________________________________

        cv.setNumbers(10, "333", "222");
        System.out.println("10, 333 - 222");
        cv.dimNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "-333", "222");
        System.out.println("10, -333 - 222");
        cv.dimNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "333", "-555");
        System.out.println("10, 333 - -555");
        cv.dimNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "-333", "-222");
        System.out.println("10, -333 - -222");
        cv.dimNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        System.out.println("___________________________");
        //__________________________________

        cv.setNumbers(10, "33", "22");
        System.out.println("10, 33 * 22");
        cv.multiplyNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "-103", "77");
        System.out.println("10, -103 * 77");
        cv.multiplyNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "33", "-22");
        System.out.println("10, 33 * -22");
        cv.multiplyNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "-33", "-22");
        System.out.println("10, -33 * -22");
        cv.multiplyNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        System.out.println("___________________________");
        //__________________________________

        cv.setNumbers(10, "333", "22");
        System.out.println("10, 333 / 22");
        cv.splitNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "103", "77");
        System.out.println("10, 103 / 77");
        cv.splitNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "11", "-22");
        System.out.println("10, 11 / -22");
        cv.splitNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        cv.setNumbers(10, "-33", "-22");
        System.out.println("10, -33 / -22");
        cv.splitNumbers();
        System.out.println(cv.getResult());
        System.out.println(cv.getDecimalResult());

        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.println("___________________________");
            System.out.println("Введите основу системы счисления");
            int base;
            base = Integer.parseInt(sc.nextLine());
            System.out.println("Введите первое число в заданной системе счисления");
            String num1 = sc.nextLine();
            System.out.println("Введите второе число в заданной системе счисления");
            String num2 = sc.nextLine();
            cv.setNumbers(base, num1, num2);
            System.out.println("1 - сложение \n 2 - вычитание \n 3 - умножение \n 4 - деление");
            char tmp = sc.next().toCharArray()[0];
            if(tmp == '1'){
                cv.sumNumbers();
                System.out.println("Сложение");
            }
            else if(tmp == '2'){
                cv.dimNumbers();
                System.out.println("Вычитание");
            }
            else if(tmp == '3'){
                cv.multiplyNumbers();
                System.out.println("Умножение");
            }
            else if(tmp == '4'){
                cv.splitNumbers();
                System.out.println("Деление");
            }
            System.out.println(cv.getResult());
            System.out.println(cv.getDecimalResult());
            sc.nextLine();
        }
    }

}

package com.company;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;


public class Window {

    NormalForm form = new NormalForm();
    Table tTable = new Table();
    DrawTable dTable = new DrawTable();
    private int[][] table;
    private JFrame frame = new JFrame();
    private JButton CreateTable_button = new JButton("вывести таблицу");
    private JButton CreateDNF_button = new JButton("вывести ДНФ целиком");
    private JButton CreateCNF_button = new JButton("вывести КНФ целиком");
    private JButton CreateDNFInc_button = new JButton("вывести ДНФ пошагово");
    private JButton CreateCNFInc_button = new JButton("вывести КНФ пошагово");
    private JLabel DNF_label = new JLabel("ДНФ");
    private JLabel CNF_label = new JLabel("КНФ");
    private JTextArea DNF_text = new JTextArea();
    private JTextArea CNF_text = new JTextArea();


    public Window() {
        int width = 1500;
        frame.setVisible(true);
        frame.setBounds(0, 0, width, 800);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel Table_label = new JLabel("таблица истинности");
        CreateTable_button.setBounds(10, 100, 180, 70);
        CreateDNF_button.setBounds(10, 300, 180, 70);
        CreateCNF_button.setBounds(10, 400, 180, 70);
        CreateDNFInc_button.setBounds(10, 500, 180, 70);
        CreateCNFInc_button.setBounds(10, 600, 180, 70);
        Table_label.setBounds(250, 70, 200, 20);
        DNF_label.setBounds(600, 70, 100, 20);
        CNF_label.setBounds(1000, 70, 100, 20);
        DNF_text.setBounds(480, 100, 300, 500);
        CNF_text.setBounds(880, 100, 300, 500);

        frame.getContentPane().add(CreateTable_button);
        frame.getContentPane().add(CreateDNF_button);
        frame.getContentPane().add(CreateCNF_button);
        frame.getContentPane().add(CreateDNFInc_button);
        frame.getContentPane().add(CreateCNFInc_button);
        frame.getContentPane().add(Table_label);
        frame.getContentPane().add(DNF_label);
        frame.getContentPane().add(CNF_label);
        frame.getContentPane().add(DNF_text);
        frame.getContentPane().add(CNF_text);
        CreateDNF_button.setVisible(false);
        CreateCNF_button.setVisible(false);
        CreateDNFInc_button.setVisible(false);
        CreateCNFInc_button.setVisible(false);
        dTable.setBounds(250, 100, 1000, 650);
        frame.getContentPane().add(dTable);
        frame.repaint();
    }

    public void buttons() throws IOException {
        CreateTable_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tTable.fillTable();
                tTable.printTable();
                table = tTable.gettTable();
                dTable.setTable(table);
                DNF_text.setText("");
                CNF_text.setText("");
                zeroingDNF();
                zeroingCNF();
                CreateDNF_button.setVisible(true);
                CreateCNF_button.setVisible(true);
                CreateDNFInc_button.setVisible(true);
                CreateCNFInc_button.setVisible(true);
                frame.repaint();
            }
        });
        CreateDNF_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DNF_text.setText(form.convertDisjunctiveForm(table).toString());
                CreateDNFInc_button.setVisible(false);
                dTable.setDNFInc(false);
                dTable.setActiveCellDNF(form.DNFRow,form.DNFColumn);
                frame.repaint();
            }
        });
        CreateCNF_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CNF_text.setText(form.convertConjunctiveForm(table).toString());
                CreateCNFInc_button.setVisible(false);
                dTable.setCNF(false);
                dTable.setActiveCellCNF(form.CNFRow,form.CNFColumn);
                frame.repaint();
            }
        });
        CreateDNFInc_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String dnf=form.convertDisjunctiveFormInc(table).toString();
                DNF_text.setText(dnf);
                System.out.println(dnf);
                dTable.setDNFInc(true);
                dTable.setActiveCellDNF(form.DNFRow,form.DNFColumn);
                frame.repaint();
            }
        });
        CreateCNFInc_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String CNF=form.convertConjunctiveFormInc(table).toString();
                CNF_text.setText(CNF);
                System.out.println(CNF);
                dTable.setCNF(true);
                dTable.setActiveCellCNF(form.CNFRow,form.CNFColumn);
                frame.repaint();
            }
        });
    }
    public void zeroingDNF() {
        StringBuilder empty = new StringBuilder();
        form.setDNF(empty);
        form.setDNFColumn(0);
        form.setDNFRow(0);
        form.setDNFPlus(false);
        dTable.setDNFInc(false);
        dTable.setActiveCellDNF(form.DNFRow,form.DNFColumn);
    }
    public void zeroingCNF() {
        StringBuilder empty = new StringBuilder();
        form.setCNF(empty);
        form.setCNFColumn(0);
        form.setCNFRow(0);
        form.CNFMultiplication(false);
        dTable.setCNF(false);
        dTable.setActiveCellCNF(form.CNFRow,form.CNFColumn);
    }



}

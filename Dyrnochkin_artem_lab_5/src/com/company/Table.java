package com.company;

import java.util.Random;

public class Table {

    Random random = new Random();
    int countRows = 16;
    int countColumns = 5;
    private int[][] tTable = new int[countColumns][countRows];

    public int[][] gettTable() {
        return tTable;
    }

    public void fillTable() {
        for (int i = 0; i < countRows; i++) {
            char[] number = transferBinary(i);
            for (int j = 0; j < countColumns - 1; j++) {
                tTable[j][i] = number[j] - 48;
            }
            tTable[4][i] = random.nextInt(2);
        }
    }

    public char[] transferBinary(int number) {
        int finish_system = 2;
        StringBuilder answer = new StringBuilder();
        while (number >= finish_system) {
            answer.append(number % finish_system);
            number /= finish_system;
        }
        answer.append(number);
        answer.reverse();
        while (answer.length() < 4) {
            answer.insert(0, '0');
        }
        return answer.toString().toCharArray();
    }

    public void printTable() {
        System.out.println(" x1 | x2 | x3 | x4 | F");
        for (int i = 0; i < countRows; i++) {
            for (int j = 0; j < countColumns - 1; j++) {
                System.out.print("  " + tTable[j][i] + " |");
            }
            System.out.print(" " + tTable[countColumns - 1][i] + "\n");
        }
    }


}

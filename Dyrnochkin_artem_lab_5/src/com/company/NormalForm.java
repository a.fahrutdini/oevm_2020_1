package com.company;

public class NormalForm {

    private String[] variables = {"x1", "x2", "x3", "x4"};
    StringBuilder DNF = new StringBuilder();
    StringBuilder CNF = new StringBuilder();
    int DNFRow= 0;
    int CNFRow = 0;
    int DNFColumn = 0;
    int CNFColumn = 0;
    boolean DNFPlus = false;
    boolean CNFMultiplication = false;

    public void setDNF(StringBuilder DNF) {
        this.DNF = DNF;
    }

    public void setDNFRow(int DNFRow) {
        this.DNFRow = DNFRow;
    }

    public void setDNFColumn(int DNFColumn) {
        this.DNFColumn = DNFColumn;
    }

    public void setDNFPlus(boolean DNFPlus) {
        this.DNFPlus = DNFPlus;
    }

    public void setCNF(StringBuilder CNF) {
        this.CNF = CNF;
    }

    public void setCNFRow(int CNFRow) {
        this.CNFRow = CNFRow;
    }

    public void setCNFColumn(int CNFColumn) {
        this.CNFColumn = CNFColumn;
    }

    public void CNFMultiplication(boolean CNFMultiplication) {
        this.CNFMultiplication = CNFMultiplication;
    }

    public StringBuilder convertDisjunctiveForm(int[][] tTable) {
        StringBuilder DNF = new StringBuilder();
        int countColumns = tTable.length;
        int countRows = tTable[0].length;
        boolean plus = false;

        for (int i = 0; i < countRows; ++i) {
            if (tTable[countColumns - 1][i] == 1) {
                if (plus) {
                    DNF.append(" + ");
                    DNF.append("\n");
                }
                DNF.append("(");
                plus = true;
                for (int j = 0; j < countColumns - 1; ++j) {
                    if (tTable[j][i] == 0) {
                        DNF.append("!");
                    }
                    DNF.append(variables[j]);
                    if (j < countColumns - 2) {
                        DNF.append(" * ");
                    } else {
                        DNF.append(")");
                    }
                }
            }
        }
        return DNF;
    }

    public StringBuilder convertDisjunctiveFormInc(int[][] tTable) {
        int countColumns = tTable.length;
        int countRows = tTable[0].length;

        if (DNFRow<countRows) {
            if (tTable[countColumns - 1][DNFRow] == 0) {
                DNFRow++;
                return DNF;
            }
            if (tTable[countColumns - 1][DNFRow] == 1) {
                if (DNFPlus) {
                    DNF.append(" + ");
                    DNF.append("\n");
                    DNFPlus = false;
                }
                if (DNFColumn < countColumns) {
                    if (DNFColumn == 0) {
                        DNF.append("(");
                    }
                    if (tTable[DNFColumn][DNFRow] == 0) {
                        DNF.append("!");
                    }
                    DNF.append(variables[DNFColumn]);
                    if (DNFColumn < countColumns - 2) {
                        DNF.append(" * ");
                        DNFColumn++;
                    } else {
                        DNFRow++;
                        DNFColumn = 0;
                        DNF.append(")");
                        DNFPlus = true;
                    }
                }
            }
        }
        return DNF;
    }

    public StringBuilder convertConjunctiveFormInc(int[][] tTable) {
        int countColumns = tTable.length;
        int countRows = tTable[0].length;
        boolean multiplication = false;
        if (CNFRow < countRows) {
            if (tTable[countColumns - 1][CNFRow] == 1) {
                CNFRow++;
                return CNF;
            }
            if (tTable[countColumns - 1][CNFRow] == 0) {
                if (CNFMultiplication) {
                    CNF.append(" * ");
                    CNF.append("\n");
                    CNFMultiplication = false;
                }
                if (CNFColumn < countColumns) {
                    if (CNFColumn == 0) {
                        CNF.append("(");
                    }
                    if (tTable[CNFColumn][CNFRow] == 1) {
                        CNF.append("!");
                    }
                    CNF.append(variables[CNFColumn]);
                    if (CNFColumn < countColumns - 2) {
                        CNF.append(" + ");
                        CNFColumn++;
                    } else {
                        CNFRow++;
                        CNFColumn = 0;
                        CNF.append(")");
                        CNFMultiplication = true;
                    }
                }
            }
        }
        return CNF;
    }

    public StringBuilder convertConjunctiveForm(int[][] tTable) {
        StringBuilder CNF = new StringBuilder();
        int countColumns = tTable.length;
        int countRows = tTable[0].length;
        boolean multiplication = false;
        for (int i = 0; i < countRows; ++i) {
            if (tTable[countColumns - 1][i] == 0) {
                if (multiplication) {
                    CNF.append(" * ");
                    CNF.append("\n");
                }
                CNF.append("(");
                multiplication = true;
                for (int j = 0; j < countColumns - 1; ++j) {
                    if (tTable[j][i] == 1) {
                        CNF.append("!");
                    }
                    CNF.append(variables[j]);
                    if (j < countColumns - 2) {
                        CNF.append(" + ");
                    } else {
                        CNF.append(")");
                    }
                }
            }
        }
        return CNF;
    }


}

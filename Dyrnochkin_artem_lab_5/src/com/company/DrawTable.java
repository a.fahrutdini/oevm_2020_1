package com.company;
import javax.swing.*;
import java.awt.*;


public class DrawTable extends JPanel{

    private int[] activeCellDNF;
    private int[] activeCellCNF;
    private int[][] tTable;
    boolean tableExist = false;
    boolean DNFInc = false;
    boolean CNFInc = false;

    public void setDNFInc(boolean DNFInc) {
        this.DNFInc = DNFInc;
    }
    public void setActiveCellDNF(int row, int column) {
        this.activeCellDNF = new int[2];
        this.activeCellDNF[0] = row;
        this.activeCellDNF[1] = column;
    }

    public void setActiveCellCNF(int row, int column) {
        this.activeCellCNF = new int[2];
        this.activeCellCNF[0] = row;
        this.activeCellCNF[1] = column;
    }

    public void setCNF(boolean CNFInc) {
        this.CNFInc = CNFInc;
    }

    private int countRows = 16;
    private int countColumns = 5;

    public void setTable(int[][] table) {
        this.tTable = table;
        tableExist = true;
    }

    public void paint(Graphics g) {
        g.setColor(Color.black);
        int widthCell = 20;
        int heightCell = 20;
        if (tableExist) {
            for (int i = 0; i < countRows; i++) {
                for (int j = 0; j < countColumns; j++) {
                    g.drawRect(widthCell * j, heightCell * i, widthCell, heightCell);
                    g.drawString(String.valueOf(tTable[j][i]), widthCell * j + widthCell * 2 / 5, heightCell * i + heightCell * 4 / 5);
                }
            }
            if (DNFInc && activeCellDNF[0] < countRows) {
                g.setColor(Color.blue);
                for (int j = 0; j < countColumns; j++) {
                    g.drawRect(widthCell * j, heightCell * activeCellDNF[0], widthCell, heightCell);
                }
                g.setColor(Color.green);
                g.drawRect(widthCell * activeCellDNF[1], heightCell * activeCellDNF[0], widthCell, heightCell);
                g.setColor(Color.red);
                g.drawRect(widthCell * 4, heightCell * activeCellDNF[0], widthCell, heightCell);
            }
            if (CNFInc && activeCellCNF[0] < countRows) {
                g.setColor(Color.red);
                for (int j = 0; j < countColumns; j++) {
                    g.drawRect(widthCell * j, heightCell * activeCellCNF[0], widthCell, heightCell);
                }
                g.setColor(Color.green);
                g.drawRect(widthCell * activeCellCNF[1], heightCell * activeCellCNF[0], widthCell, heightCell);
                g.setColor(Color.blue);
                g.drawRect(widthCell * 4, heightCell * activeCellCNF[0], widthCell, heightCell);
            }
        }
    }


}

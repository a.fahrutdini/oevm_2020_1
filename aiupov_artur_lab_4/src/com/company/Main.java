package com.company;
import java.util.*;

public class Main {
    static final int COLUMN = 16; // длина столбца
    static final int SYMBOL = 5; // длина строки

    public static void main(String[] args) {
        System.out.println("if you wanna use KNF print 1");
        System.out.println("if you wanna use DNF print 2");

        Scanner number = new Scanner(System.in);
        int executionOption = number.nextInt(); // переменная которая будет определять по кнф решаем или по днф

        int[][] arrayOfNumbers = new int[COLUMN][SYMBOL]; // массив всех чисел
        Helper.outputOnDisplay(arrayOfNumbers); // функция для вывода таблицы
        if (executionOption == 1) {
            Helper.knfFormula(arrayOfNumbers); // функция для кнф формулы
        } else {
            Helper.dnfFormula(arrayOfNumbers); // функция для днф формулы
        }
    }
}

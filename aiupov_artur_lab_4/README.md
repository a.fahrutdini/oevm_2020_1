### Лабораторная работа №4, выполнил Аюпов Артур из группы ИСЭбд-21

**Ссылка на видео:** https://drive.google.com/file/d/1xhe7DU4bcmVn3U6wuMMvvDhI-xT6iCq3/view?usp=sharing

### 1. Как запустить
Запустить лабу можно найди файл **Main.java** и запустив его в компиляторе. 

### 2. Какие технологии использовали
Среду разработки **IntelliJ IDEA Community** и несколько стандартных библиотек ***java***, одна из которых переворачивает строку (StringBuffer).

### 3. Что она делает
В этой лабораторной работе реализована минимизация булевых функций. В таблице случайным образом заполняются значения функций, а пользователь указывает конечную форму кнф или днф

### 4. Примеры
#### Пример работы KNF формулы
https://drive.google.com/file/d/1Xq0zPl3FbvxjuVUnTphVEcQATw7Sg1HJ/view?usp=sharing
#### Пример работы DNF формулы
https://drive.google.com/file/d/1e951xSad3nQZaE-DeO743lZqtqyNG747/view?usp=sharing

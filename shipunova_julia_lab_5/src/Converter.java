public class Converter {
    public static String getConvertedString(int[][] array, int row) {
        StringBuilder answer = new StringBuilder("(");
        int valueIndex = 4;
        if (array[row][valueIndex] == 1) {
            for (int j = 0; j < array[row].length - 1; j++) {
                if (array[row][j] == 0) {
                    answer.append("!");
                }
                answer.append("x").append(j + 1);
                if (j < array[row].length - 2) {
                    answer.append(" * ");
                }
            }
        } else {
            for (int j = 0; j < array[row].length - 1; j++) {
                if (array[row][j] == 1) {
                    answer.append("!");
                }
                answer.append("x").append(j + 1);
                if (j < array[row].length - 2) {
                    answer.append(" + ");
                }
            }
        }
        answer.append(")");
        return answer.toString();
    }

    public static String[] convertToDnf(int[][] array) {
        int valueIndex = 4;
        String[] result = new String[array.length];
        int k = 0;
        for (int[] ints : array) {
            if (ints[valueIndex] == 1) {
                if (k > 0) {
                    result[k - 1] += " +";
                }
                result[k] = "(";
                for (int j = 0; j < ints.length - 1; j++) {
                    if (ints[j] == 0) {
                        result[k] += "!";
                    }
                    result[k] += "x" + (j + 1);
                    if (j < ints.length - 2) {
                        result[k] += " * ";
                    }
                }
                result[k] += ")";
                k++;
            }
        }
        return result;
    }

    public static String[] convertToKnf(int[][] array) {
        int valueIndex = 4;
        String[] result = new String[array.length];
        int k = 0;
        for (int[] ints : array) {
            if (ints[valueIndex] == 0) {
                if (k > 0) {
                    result[k - 1] += " *";
                }
                result[k] = "(";
                for (int j = 0; j < ints.length - 1; j++) {
                    if (ints[j] == 1) {
                        result[k] += "!";
                    }
                    result[k] += "x" + (j + 1);
                    if (j < ints.length - 2) {
                        result[k] += " + ";
                    }
                }
                result[k] += ")";
                k++;
            }
        }
        return result;
    }
}

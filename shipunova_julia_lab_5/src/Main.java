public class Main {
    public static void main(String[] args) {
        Frame frame = Frame.createFrame();
        VisualizePanel visualizePanel = new VisualizePanel();
        frame.initButtons(visualizePanel);
        frame.setVisualizePanel(visualizePanel);
        frame.repaint();
    }
}

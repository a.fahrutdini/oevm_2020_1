import javax.swing.*;
import java.awt.*;

public class VisualizePanel extends JPanel {
    private Timer timer;
    private Frame frame;
    private TruthTable truthTable;
    private int countOfString = 0;
    private String[] stringArray;
    private String[] dnfStrings;
    private String[] knfStrings;

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setFont(new Font("Bahnschrift Light", Font.PLAIN, 18));
        g2.drawString("Таблица истинности", 12, 25);
        g2.setFont(new Font("Bahnschrift Light", Font.PLAIN, 20));
        int cellSize = 30;
        for (int i = 0; i < truthTable.getTruthTableArray().length; i++) {
            for (int j = 0; j < truthTable.getTruthTableArray()[i].length; j++) {
                g2.drawString(String.valueOf(truthTable.getTruthTableArray()[i][j]), (j + 1) * cellSize, (i + 2) * cellSize);
                g2.drawRect((j + 1) * cellSize - 12, (i + 1) * cellSize + 7, cellSize, cellSize);
            }
        }
        if (stringArray != null)
            drawStrings(g2);
    }

    public VisualizePanel() {
        truthTable = TruthTable.getTruthTable();
        timer = new Timer(100, e -> minimize());
    }

    public void setTruthTable(TruthTable truthTable) {
        this.truthTable = truthTable;
    }

    private void minimize() {
        if (countOfString < stringArray.length) {
            stringArray[countOfString] = Converter.getConvertedString(truthTable.getTruthTableArray(), countOfString);
            countOfString++;
            frame.repaint();
        } else {
            dnfStrings = Converter.convertToDnf(truthTable.getTruthTableArray());
            knfStrings = Converter.convertToKnf(truthTable.getTruthTableArray());
            frame.repaint();
            timer.stop();
        }
    }

    public void minimizeStart(Frame frame) {
        this.frame = frame;
        timer.start();
        clear();
    }

    private void drawStrings(Graphics2D g) {
        for (int i = 0; i < stringArray.length; i++) {
            if (stringArray[i] != null) {
                g.drawString(stringArray[i], 180, (i + 2) * 30);
            }
        }
        if (dnfStrings != null && knfStrings != null) {
            int k = 0;
            g.drawString("ДНФ", 400, (k + 2) * 30);
            k++;
            for (String dnfString : dnfStrings) {
                if (dnfString != null) {
                    g.drawString(dnfString, 400, (k + 2) * 30);
                    k++;
                } else {
                    break;
                }
            }
            g.drawString("КНФ", 400, (k + 2) * 30);
            k++;
            for (String knfString : knfStrings) {
                if (knfString != null) {
                    g.drawString(knfString, 400, (k + 2) * 30);
                    k++;
                } else {
                    break;
                }
            }
        }
    }

    public void clear() {
        countOfString = 0;
        stringArray = new String[truthTable.getTruthTableArray().length];
        dnfStrings = null;
        knfStrings = null;
    }
}

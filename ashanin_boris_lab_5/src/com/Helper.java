package com;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.SystemColor;
import javax.swing.SwingConstants;

public class Helper {

    private JFrame frame;
    final int _sizeCol = 5;                          // количество столбцов
    final int _sizeStr = 16;                         // количество строk
    private String _result = " ";                    // результат
    private String _nameResult = "";                // имя результата

    Random random = new Random();

    private int[][] _table = {                      // таблица
            {0, 0, 0, 0, random.nextInt(2)},
            {0, 0, 0, 1, random.nextInt(2)},
            {0, 0, 1, 0, random.nextInt(2)},
            {0, 0, 1, 1, random.nextInt(2)},
            {0, 1, 0, 0, random.nextInt(2)},
            {0, 1, 0, 1, random.nextInt(2)},
            {0, 1, 1, 0, random.nextInt(2)},
            {0, 1, 1, 1, random.nextInt(2)},
            {1, 0, 0, 0, random.nextInt(2)},
            {1, 0, 0, 1, random.nextInt(2)},
            {1, 0, 1, 0, random.nextInt(2)},
            {1, 0, 1, 1, random.nextInt(2)},
            {1, 1, 0, 0, random.nextInt(2)},
            {1, 1, 0, 1, random.nextInt(2)},
            {1, 1, 1, 0, random.nextInt(2)},
            {1, 1, 1, 1, random.nextInt(2)},};

    private int X1 = 0;
    private int X2 = 1;
    private int X3 = 2;
    private int X4 = 3;

    JLabel[] labelsAdd = new JLabel[_sizeCol * _sizeStr];
    JPanel panelTable = new JPanel();
    JTextPane textPaneResult = new JTextPane();
    JButton btnRestart = new JButton("\u041E\u0431\u043D\u043E\u0432\u0438\u0442\u044C \u0434\u0430\u043D\u043D\u044B\u0435");
    JLabel lblTable = new JLabel("\u0422\u0430\u0431\u043B\u0438\u0446\u0430");
    JLabel lblResult = new JLabel("\u0420\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442");

    public Helper() {
        initialize();
    }

    private String DNF() {
        _nameResult = "ДНФ:";
        int count = 0;
        for (int i = 0; i < _sizeStr; i++) {
            if (_table[i][_sizeCol - 1] == 1) {
                if (count > 0) {
                    _result = _result + " + ";
                }
                if (_table[i][X1] == 0) {
                    _result = _result + "( - X1 ";
                } else {
                    _result = _result + "( X1 ";
                }
                if (_table[i][X2] == 0) {
                    _result = _result + " * -X2 ";
                } else {
                    _result = _result + " * X2 ";
                }
                if (_table[i][X3] == 0) {
                    _result = _result + " * -X3 ";
                } else {
                    _result = _result + " * X3 ";
                }
                if (_table[i][X4] == 0) {
                    _result = _result + "* -X4 )\n";
                } else {
                    _result = _result + "* X4 )\n";
                }
                count++;
            }
        }
        return _result;
    }

    private String KNF() {
        _nameResult = "КНФ:";
        int count = 0;
        for (int i = 0; i < _sizeStr; i++) {
            if (_table[i][_sizeCol - 1] == 0) {
                if (count > 0) {
                    _result = _result + " * ";
                }
                if (_table[i][X1] == 1) {
                    _result = _result + "( - X1 ";
                } else {
                    _result = _result + " ( X1 ";
                }
                if (_table[i][X2] == 1) {
                    _result = _result + " - X2 ";
                } else {
                    _result = _result + " + X2 ";
                }
                if (_table[i][X3] == 1) {
                    _result = _result + " - X3 ";
                } else {
                    _result = _result + " + X3 ";
                }
                if (_table[i][X4] == 1) {
                    _result = _result + " - X4 )\n";
                } else {
                    _result = _result + " + X4 )\n";
                }
                count++;
            }
        }
        return _result;
    }

    private void createTable() {
        Random random = new Random();
        int[][] newTable = {
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},};
        _table = newTable;
    }

    private void printTable(int checkNumber) {
        boolean helpFlag = false; // для выделения строк
        String helpValues = "";   // для добавления строки в лэйбд
        switch (checkNumber) {
            case 1:
                for (int i = 0; i < _sizeStr; i++) {
                    helpValues = "";
                    for (int j = 0; j < _sizeCol; j++) {
                        helpValues += Integer.toString(_table[i][j]) + " ";
                        if (j == _sizeCol - 1 && _table[i][j] == 1) {
                            helpFlag = true;
                        }
                    }
                    labelsAdd[i] = new JLabel();
                    labelsAdd[i].setText(helpValues);
                    if (helpFlag) {
                        labelsAdd[i].setForeground(Color.RED);
                        helpFlag = false;
                    }
                    panelTable.add(labelsAdd[i]);
                }
                break;
            case 2:
                for (int i = 0; i < _sizeStr; i++) {
                    helpValues = "";
                    for (int j = 0; j < _sizeCol; j++) {
                        helpValues += Integer.toString(_table[i][j]) + " ";
                        if (j == _sizeCol - 1 && _table[i][j] == 0) {
                            helpFlag = true;
                        }
                    }
                    labelsAdd[i] = new JLabel();
                    labelsAdd[i].setText(helpValues);
                    if (helpFlag) {
                        labelsAdd[i].setForeground(Color.RED);
                        helpFlag = false;
                    }
                    panelTable.add(labelsAdd[i]);
                }
                break;
            case 3:
                for (int i = 0; i < _sizeStr; i++) {
                    helpValues = "";
                    for (int j = 0; j < _sizeCol; j++) {
                        helpValues += Integer.toString(_table[i][j]) + " ";
                    }
                    labelsAdd[i] = new JLabel();
                    labelsAdd[i].setText(helpValues);
                    panelTable.add(labelsAdd[i]);
                }
                break;
        }
    }

    private void initialize() {

        JButton btnDNF = new JButton("\u0414\u041D\u0424");
        JButton btnKNF = new JButton("\u041A\u041D\u0424");

        setFrame(new JFrame());
        getFrame().setBounds(100, 100, 585, 610);
        getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        panelTable.setBackground(SystemColor.info);

        panelTable.setBounds(124, 36, 108, 363);
        frame.getContentPane().add(panelTable);
        textPaneResult.setBackground(SystemColor.window);

        textPaneResult.setBounds(247, 36, 213, 363);
        frame.getContentPane().add(textPaneResult);

        btnDNF.setBounds(257, 415, 69, 59);
        btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _result = "";
                DNF();
                panelTable.removeAll();
                printTable(1);
                textPaneResult.repaint();
                textPaneResult.setText(_nameResult + "\n" + _result);
            }
        });
        frame.getContentPane().add(btnDNF);

        btnKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _result = "";
                KNF();
                panelTable.removeAll();
                printTable(2);
                textPaneResult.repaint();
                textPaneResult.setText(_nameResult + "\n" + _result);
            }
        });
        btnKNF.setBounds(354, 415, 69, 59);
        frame.getContentPane().add(btnKNF);

        btnRestart.setVerticalAlignment(SwingConstants.TOP);
        btnRestart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                createTable();
                panelTable.removeAll();
                textPaneResult.setText(null);
                _result = "";
                printTable(3);
            }
        });

        btnRestart.setBounds(32, 415, 176, 51);
        frame.getContentPane().add(btnRestart);

        lblTable.setBounds(139, 16, 69, 20);
        frame.getContentPane().add(lblTable);

        lblResult.setBounds(298, 16, 98, 20);
        frame.getContentPane().add(lblResult);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }
}

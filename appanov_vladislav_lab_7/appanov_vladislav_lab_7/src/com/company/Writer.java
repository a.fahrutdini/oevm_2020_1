package com.company;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

public class Writer {
    LinkedList<String> listOfVariables = new LinkedList<>();
    HashMap<String, String> dictionaryOfStringVariables = new HashMap<>();

    StringBuilder starting = new StringBuilder(
            "format PE console\n\n" +
                    "entry start\n\n" +
                    "include 'win32a.inc'\n\n" +
                    "section '.idata' import data readable\n\n" +
                    "\tlibrary kernel, 'kernel32.dll',\\\n" +
                    "\tmsvcrt, 'msvcrt.dll'\n\n" +
                    "\timport kernel,\\\n" +
                    "\tExitProcess, 'ExitProcess'\n\n" +
                    "\timport msvcrt,\\\n" +
                    "\tprintf, 'printf',\\\n" +
                    "\tscanf, 'scanf',\\\n" +
                    "\tgetch, '_getch'\\\n" +
                    "section '.data' data readable writable\n"
    );

    public void write() throws IOException {
        addVariables();
        FileWriter writer = new FileWriter("assemblerCode.ASM", false);
        writer.append(starting);
        writer.append(variables);
        writer.append(code);
        writer.append(ending);
        writer.flush();
    }

    public void setVariables(LinkedList<String> linkedList) {
        listOfVariables = linkedList;
    }

    public void addVariables() {
        for (String string : listOfVariables) {
            variables.append("\t" + string + " dd ?\n");
        }

        for (String key : dictionaryOfStringVariables.keySet()) {
            variables.append("\t" + key + " db '" + dictionaryOfStringVariables.get(key) + "', 0\n");
        }
    }

    StringBuilder variables = new StringBuilder(
            "\tspaceStr db '%d', 0\n" +
                    "\tdopStr db '%d', 0ah, 0\n"
    );

    StringBuilder code = new StringBuilder(
            "section '.code' code readable executable\n\n" +
                    "\tstart:\n"
    );

    StringBuilder ending = new StringBuilder(
            "\tcall [getch]\n" +
                    "\tpush NULL\n" +
                    "\tcall [ExitProcess]"
    );

    public void addWrite(String string) {
        dictionaryOfStringVariables.put("string" + (dictionaryOfStringVariables.size() + 1), string);
        code.append("\tpush string" + dictionaryOfStringVariables.size() + "\n" +
                "\tcall [printf]\n"
        );
    }

    public void addNumericOperation(String res, String firstNum, String operator, String secondNum) {
        if (listOfVariables.contains(res) && listOfVariables.contains(firstNum) && listOfVariables.contains(secondNum)) {
            switch (operator) {
                case "-":
                    code.append("\tmov ecx, [" + firstNum + "]\n" +
                            "\tsub ecx, [" + secondNum + "]\n" +
                            "\tmov [" + res + "], ecx\n"
                    );
                    break;
                case "*":
                    code.append("\tmov ecx, [" + firstNum + "]\n" +
                            "\timul ecx, [" + secondNum + "]\n" +
                            "\tmov [" + res + "], ecx\n"
                    );
                    break;
                case "/":
                    code.append("\tmov eax, [" + firstNum + "]\n" +
                            "\tmov ecx, [" + secondNum + "]\n" +
                            "\tdiv ecx\n" +
                            "\tmov [" + res + "], eax\n"
                    );
                    break;
                case "+":
                    code.append("\tmov ecx, [" + firstNum + "]\n" +
                            "\tadd ecx, [" + secondNum + "]\n" +
                            "\tmov [" + res + "], ecx\n"
                    );
                    break;
            }
        }
    }

    public void addReadLine(String string) {
        if (listOfVariables.contains(string)) {
            code.append("\tpush " + string + "\n" +
                    "\tpush spaceString\n" +
                    "\tcall [scanf]\n\n"
            );
        }
    }

    public void addWriteLine(String string) {
        if (listOfVariables.contains(string)) {
            code.append("\tpush [" + string + "]\n" +
                    "\tpush dopStr\n" +
                    "\tcall [printf]\n\n"
            );
        }
    }
}


format PE console

entry start

;����������� ��������� ��� ������� printf, scanf � getch
include 'win32a.inc'
section '.idata' import data readable

                
        library kernel, 'kernel32.dll',\
                msvcrt, 'msvcrt.dll'

        import kernel,\
               ExitProcess, 'ExitProcess'

        import msvcrt,\
               printf, 'printf',\
               scanf, 'scanf',\
               getch, '_getch'



section '.code' code readable executable

        start: ;����� (�� ����, ��� main)
                
                ;���� � �������
                push numX
                call [printf] ;����� ������ � �������

                push X
                push enterNum
                call [scanf] ;������ �������� � �������

                push numY
                call [printf]

                push Y
                push enterNum
                call [scanf]

                                        ;��������
                    mov ecx, [X]
                    add ecx, [Y]

                    push ecx
                    push addMSG
                    call [printf]

                                        ;���������
                    mov ecx, [X]
                    sub ecx, [Y]

                    push ecx
                    push subMSG
                    call [printf]

                                        ;���������
                    mov ecx, [X]
                    imul ecx, [Y]

                    push ecx
                    push mulMSG
                    call [printf]

                                        ;�������
                    mov eax, [X]
                    cdq
                    mov ecx, [Y]
                    idiv ecx

                    push eax
                    push divMSG
                    call [printf]

                ;���� ������ �� ����� �������, �� ��������� ����� �� �������
                push endMSG
                call [printf]
                call [getch]

                push NULL
                call [ExitProcess]

section '.data' data readable writable

                ; db == 1 ����
        numX db 'Enter X = ', 0 ;��������� � ������� 'X = '
        numY db 'Enter Y = ', 0 ;��������� � ������� 'Y = '
        enterNum db ' %d', 0 ;��� ����� �������� �� �������

        addMSG db 'X + Y = %d', 0dh, 0ah, 0 ;��������� � ������� 'X + Y = sum', 0dh,0ah, - ��� �������� �������
        subMSG db 'X - Y = %d', 0dh, 0ah, 0
        mulMSG db 'X * Y = %d', 0dh, 0ah, 0
        divMSG db 'X / Y = %d', 0dh, 0ah, 0
        endMSG db 'Click on any key to shutdown program', 0dh, 0ah, 0

        X dd ?
        Y dd ?

        NULL = 0
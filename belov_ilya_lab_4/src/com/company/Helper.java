package com.company;

public class Helper {
    public static char[] convertTo2(int number10) {
        String result = "";
        int remainder = 1;
        int binaryNotation = 2;

        for (String сurChar = ""; number10 > 0; number10 /= binaryNotation) {
            remainder = number10 % binaryNotation;
            сurChar = "";
            if (number10 % binaryNotation < 10) {
                сurChar = Integer.toString(remainder);
            } else {
                сurChar = сurChar + (char)((int)('A' + remainder - 10));
            }

            result = сurChar + result;
        }
        int fourDigitsNumber = 4;
        while (result.length() < fourDigitsNumber) {
            result = '0' + result;
        }
        return result.toCharArray();
    }

    public static int[][] fillTruthTable(int n, int m, int[][] truthTable) {
        for (int i = 0; i < n; ++i) {
            char[] strX = convertTo2(i);
            for (int j = 0; j < m - 1; ++j) {
                truthTable[i][j] = strX[j] - '0';
            }
            truthTable[i][m - 1] = (int)(Math.random() * 2); // рандомим значение функции
        }
        return truthTable;
    }

    public static void printTruthTable(int n, int m, int[][] truthTable) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m - 1; ++j) {
                System.out.print(truthTable[i][j] + " ");
            }
            System.out.println("= " + truthTable[i][m - 1]);
        }
    }
}

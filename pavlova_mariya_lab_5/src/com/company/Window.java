package com.company;
import java.awt.EventQueue;
import javax.swing.*;

public class Window {

    private JFrame frame;
    private JButton btnGenerateTruthTable = new JButton("Generate");
    private JButton btnGetDisjunctiveNormalForm = new JButton("Get DNF");
    private JButton btnGetConjunctiveNormalForm = new JButton("Get KNF");
    private JTextArea results = new JTextArea();
    private LogicFunction function = new LogicFunction(4);
    private JPanel panelFunction = new PanelFunction(function);

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Window window = new Window();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Window() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 500, 650);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        panelFunction.setBounds(0, 60, 210, 600);
        frame.getContentPane().add(panelFunction);

        results.setEditable(false);
        results.setBounds(210, 80, 200, 480);
        frame.getContentPane().add(results);

        btnGenerateTruthTable.addActionListener(e -> {
            function.createTableTruth();
            btnGetDisjunctiveNormalForm.setEnabled(true);
            btnGetConjunctiveNormalForm.setEnabled(true);
            results.setText("");
            panelFunction.repaint();
        });
        btnGenerateTruthTable.setBounds(20, 10, 150, 30);
        frame.getContentPane().add(btnGenerateTruthTable);

        btnGetDisjunctiveNormalForm.addActionListener(e -> {
            String str = function.getDisjunctiveNormalForm();
            results.append(str);
            panelFunction.repaint();
            btnGetDisjunctiveNormalForm.setEnabled(false);
        });
        btnGetDisjunctiveNormalForm.setBounds(210, 10, 90, 30);
        frame.getContentPane().add(btnGetDisjunctiveNormalForm);

        btnGetConjunctiveNormalForm.addActionListener(e -> {
            String str = function.getConjunctiveNormalForm();
            results.append(str);
            panelFunction.repaint();
            btnGetConjunctiveNormalForm.setEnabled(false);
        });
        btnGetConjunctiveNormalForm.setBounds(310, 10, 90, 30);
        frame.getContentPane().add(btnGetConjunctiveNormalForm);
    }
}

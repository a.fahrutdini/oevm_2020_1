# Лабораторная работа №8 "Физические компоненты ЭВМ" 

* К сожалению, у меня нет своего компьютера,
поэтому использовала компьютер своего товарища

Деталь | Модель
----|----
Процессор | AMD FX 8320 8 ядер базовая частота 3,5 гигагерц 
Видеокарта | RADEON RX 570 4GB 
Оперативка | KINGSTON 2 планки по 4 гига DDR3 
Материнская плата | GIGABYTE 970A - DS3P

### До разборки
![ДО](https://sun9-34.userapi.com/impg/ooBaxfZdkQExCqeEitYS9KJ-SUuo3kHncznz1A/m97r8UmdoS8.jpg?size=1620x2160&quality=96&proxy=1&sign=5ceca207faf3852217a1cd135fce8770)

### После разборки
![ПОСЛЕ](https://sun9-75.userapi.com/impg/RAwT0DYOLXPzzcHET6nQj0kYoI1IWScIs8bxDA/EOkJOYigPts.jpg?size=2560x1920&quality=96&proxy=1&sign=fbae4f865dcdc481688304eec976585c)
![ПОСЛЕ](https://sun9-61.userapi.com/impg/c6T9l2L4lVLffQjJCuPYBwXKlZb4QjHOsB9vZw/rEoCF7wDTCI.jpg?size=2560x1920&quality=96&proxy=1&sign=cb6a971c42511ee37507938a54b0508d)
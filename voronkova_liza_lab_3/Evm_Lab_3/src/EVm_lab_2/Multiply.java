package EVm_lab_2;

import java.util.ArrayList;

public class Multiply {

    private ArrayList<Integer> result;
    private Addition addition;
    private Compare compare;
    private ArrayList<Integer> Unit;


    public Multiply() {
        this.addition = new Addition();
        this.compare = new Compare();
        this.Unit = new ArrayList<>(1);
        this.Unit.add(1);
    }

    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> a, ArrayList<Integer> b) {
        this.result = new ArrayList<>();
        ArrayList<Integer> tmp = new ArrayList<>();
        tmp.add(0);
        if (compare.equals(tmp, b, 0) < 0) {
            while (compare.equals(tmp, b, 0) == -1) {
                result = addition.startArithmeticOperation(a, result);
                tmp = addition.startArithmeticOperation(tmp, Unit);
            }
        } else {
            this.result.add(0);
        }
        return result;
    }
}

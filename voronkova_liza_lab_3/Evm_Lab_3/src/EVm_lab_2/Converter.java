package EVm_lab_2;

import static java.lang.Math.pow;

public class Converter {

    private StringBuilder result;

    public long convertTo10(int sSystem, String enter) {
        long dSystem = 0;
        for (int i = 0; i < enter.length(); i++) {
            if (Character.isLetter(enter.charAt(i))) {
                if (enter.charAt(i) - 55 < sSystem) {
                    dSystem += (enter.charAt(i) - 55) * pow(sSystem, enter.length() - 1 - i);
                } else {
                    return 0;
                }
            } else {
                if (Character.getNumericValue(enter.charAt(i)) < sSystem) {
                    dSystem += (enter.charAt(i) - 48) * pow(sSystem, enter.length() - 1 - i);
                } else {
                    return 0;
                }
            }
        }
        return dSystem;
    }


    public String convert(int startSystem, int endSystem, String enter) {
        result = new StringBuilder();
        long decimalSystem = convertTo10(startSystem, enter);

        if (decimalSystem < endSystem) {
            if (decimalSystem < 10) {
                this.result = this.result.append(decimalSystem);
            } else {
                this.result = this.result.append((char) (decimalSystem + 55));
            }
        } else {
            while (decimalSystem / endSystem != 0) {
                if (decimalSystem % endSystem < 10) {
                    this.result.append(decimalSystem % endSystem);
                } else {
                    this.result.append((char) (decimalSystem % endSystem + 55));
                }
                decimalSystem /= endSystem;
            }
            if (decimalSystem < 10) {
                this.result.append(decimalSystem);
            } else {
                this.result.append((char) (decimalSystem + 55));
            }
        }

        if (this.result.length() == 0) {
            this.result.replace(0, result.length(), "Ошибка ввода");
        } else {
            this.result.reverse();
        }
        return this.result.toString();
    }
}

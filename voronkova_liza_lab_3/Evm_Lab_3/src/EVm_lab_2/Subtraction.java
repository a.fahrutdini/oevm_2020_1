package EVm_lab_2;

import java.util.ArrayList;

public class Subtraction {
    private ArrayList<Integer> result;
    private Addition addition;
    private ArrayList<Integer> unit;


    public Subtraction() {
        this.addition = new Addition();
        this.unit = new ArrayList<>(1);
        this.unit.add(1);
    }

    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> a, ArrayList<Integer> b) {
        this.result = new ArrayList<>();
        ArrayList<Integer> backB = new ArrayList<>();
        for (int i = 0; i < b.size(); i++) {
            if (b.get(i) == 1) {
                backB.add(0);
            } else {
                backB.add(1);
            }
        }
        for (int i = backB.size(); i < a.size(); i++) {
            backB.add(1);
        }
        backB = this.addition.startArithmeticOperation(backB, this.unit);
        this.result = this.addition.startArithmeticOperation(a, backB);
        this.result.remove(this.result.size() - 1);
        return this.result;
    }
}

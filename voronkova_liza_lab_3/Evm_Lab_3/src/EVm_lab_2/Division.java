package EVm_lab_2;

import java.util.ArrayList;

public class Division {

    private ArrayList<Integer> result;
    private Addition addition;
    private Subtraction subtraction;
    private Compare compare;
    private ArrayList<Integer> unit;


    public Division() {
        this.addition = new Addition();
        this.result = new ArrayList();
        this.compare = new Compare();
        this.subtraction = new Subtraction();
        this.unit = new ArrayList<>(1);
        this.unit.add(1);
    }

    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> a, ArrayList<Integer> b) {
        this.result = new ArrayList<>();
        if (b.get(b.size() - 1) == 0) {
            System.out.printf("Деление на ноль!\n");
            return this.result;
        } else {
            ArrayList<Integer> tmp = a;
            if (compare.equals(tmp, b, 0) >= 0) {
                while (compare.equals(tmp, b, 0) >= 0) {
                    tmp = subtraction.startArithmeticOperation(tmp, b);
                    result = addition.startArithmeticOperation(unit, result);
                }
            } else {
                this.result.add(0);
            }
        }
        return result;
    }
}

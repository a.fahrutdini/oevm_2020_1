package EVm_lab_2;

import java.util.ArrayList;
import java.util.HashMap;

public class Calculator {

    private Addition addition;
    private Subtraction subtraction;
    private Multiply multiplication;
    private Division division;
    private Compare compare;
    private Converter converter = new Converter();
    private Operations operations;

    private ArrayList<Integer> aBinary = new ArrayList();
    private ArrayList<Integer> bBinary = new ArrayList<>();
    private ArrayList<Integer> result = new ArrayList();
    private StringBuilder A = new StringBuilder("");
    private StringBuilder B = new StringBuilder("");

    private boolean orientResult = true;
    private String choice;
    private int scaleOfNotation = -1;


    public Calculator() {
        addition = new Addition();
        subtraction = new Subtraction();
        multiplication = new Multiply();
        division = new Division();
        compare = new Compare();
    }


    public void setChoice(String choice) {
        this.choice = choice;
    }


    public void setArguments(String first, String second, int ScaleOfNotation) {
        this.A.append(first);
        this.B.append(second);
        if (ScaleOfNotation <= 0) {
            System.out.printf("Ошибка введения с.с");
            return;
        }
        this.scaleOfNotation = ScaleOfNotation;
        convertToArrayList();
    }

    public void toCalculate() {
        this.result = new ArrayList<>();
        if (!checkInvalidInput()) {
            getBinaryAandB();
            switch (operations.getOperations(choice)) {
                case Addition: {
                    this.result = addition.startArithmeticOperation(aBinary, bBinary);
                    break;
                }
                case Subtraction: {
                    if (compare.equals(aBinary, bBinary, 0) == -1) {
                        this.orientResult = false;
                        ArrayList<Integer> tmp = aBinary;
                        aBinary = bBinary;
                        bBinary = tmp;
                    } else {
                        this.orientResult = true;
                    }
                    this.result = subtraction.startArithmeticOperation(aBinary, bBinary);
                    break;
                }
                case Multiplication: {
                    this.result = multiplication.startArithmeticOperation(aBinary, bBinary);
                    break;
                }
                case Division: {
                    this.result = division.startArithmeticOperation(aBinary, bBinary);
                    break;
                }
                default:
                    break;
            }
            getResult();
        }
    }


    private boolean checkInvalidInput() {
        return (aBinary == null || bBinary == null || scaleOfNotation == -1);
    }


    private void convertToArrayList() {
        String A_BinaryStr = converter.convert(scaleOfNotation, 2, A.toString());
        String B_BinaryStr = converter.convert(scaleOfNotation, 2, B.toString());
        for (int i = A_BinaryStr.length() - 1; i >= 0; i--) {
            if (A_BinaryStr.charAt(i) == '0' || A_BinaryStr.charAt(i) == '1') {
                this.aBinary.add(Character.getNumericValue(A_BinaryStr.charAt(i)));
            } else {
                aBinary = null;
                return;
            }
        }
        for (int i = B_BinaryStr.length() - 1; i >= 0; i--) {
            if (B_BinaryStr.charAt(i) == '0' || B_BinaryStr.charAt(i) == '1') {
                this.bBinary.add(Character.getNumericValue(B_BinaryStr.charAt(i)));
            } else {
                bBinary = null;
                return;
            }
        }
    }


    private void getBinaryAandB() {
        System.out.println("первое число в 2 с.с");
        for (int i = aBinary.size() - 1; i >= 0; i--) {
            System.out.print(aBinary.get(i));
        }
        System.out.printf("\n");
        System.out.println("второе число в 2 с.с");
        for (int i = bBinary.size() - 1; i >= 0; i--) {
            System.out.print(bBinary.get(i));
        }
        System.out.printf("\n");
    }


    public void getResult() {
        if (this.result.size() != 0) {
            System.out.println("Результат:");
            for (int i = this.result.size() - 1; ((i >= 0) && (this.result.get(i) == 0)); i--) {
                this.result.remove(i);
            }
            if (this.result.size() == 0) {
                result.add(0);
            }
            if (!orientResult) {
                System.out.printf("-");
                this.orientResult = true;
            }
            for (int i = this.result.size() - 1; i >= 0; i--) {
                System.out.print(this.result.get(i));
            }
            System.out.printf("\n");
        }
    }
}

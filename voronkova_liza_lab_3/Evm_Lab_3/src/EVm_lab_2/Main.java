package EVm_lab_2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Calculator calculator = new Calculator();

        Scanner in = new Scanner(System.in);
        int originalSystem = 0;
        String a = "";
        String b = "";
        String choice = "";
        System.out.println("Введите систему счисления");
        try {
            originalSystem = in.nextInt();
            System.out.println("Введите первое число");
            a = in.next();
            System.out.println("Введите второе число");
            b = in.next();
            System.out.println("Введите знак операции");
            choice = in.next();
        } catch (InputMismatchException e) {
            System.out.printf("Ошибка ввода");
            return;
        }
        in.close();

        calculator.setArguments(a, b, originalSystem);
        calculator.setChoice(choice);
        calculator.toCalculate();
    }
}

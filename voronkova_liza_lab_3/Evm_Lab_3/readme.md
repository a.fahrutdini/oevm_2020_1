<!DOCTYPE html>
<html>
<head>
    <meta charset ="utf-8">
</head>
<body>
Лабораторная работа №3<br>
выполнила: Воронкова Лиза<br>
группа: ИСЭбд-21<br>

Ссылка на видео: https://drive.google.com/file/d/1HDQuAlzJ8uJUjS6qqfiIddQpxxqE-WEp/view?usp=sharing<br>
<H3> Как запустить?</H3>
Запустить через Main.java
<H3>Какие технологии использовались?</H3>
Использовалась IntelliJ IDEA и стандартные библиотеки Java. Использовала StringBuilder,ArrayList
<H3>Функционал программы</H3>
Переводит 2 введенных числа из введенной с.с во 2 с.с и выполняет арифметическую операцию. Результатом арифм.операции является число в 2 с.с.<br>
Доступные операции: Сложение, вычитание, умножение, деление

<table bgcolor="#DCDCDC" border="2">
    <caption >
        Пример
    </caption>
    <tr>
        <th rowspan="2">№</th>
        <th colspan="4">Входные данные</th>
        <th colspan="3">Выходные данные</th>
    </tr>
    <tr>
			<th>с.с</th>
            <th>A</th>
            <th>B</th>
			<th>Операция</th>
			<th>А в 2 с.с</th>
            <th>B в 2 с.с</th>
            <th>Результат</th>
    </tr>
    <tr>
        <td>1.</td>
        <td>8</td>
        <td>714</td>
        <td>53</td>
		<td>*</td>
        <td>111001100</td>
        <td>101100</td>
		<td>100111100010000</td>
    </tr>
    <tr>
        <td>2.</td>
        <td>10</td>
        <td>938</td>
        <td>49</td>
		<td>/</td>
        <td>1110101010</td>
        <td>110001</td>
		<td>10011</td>
    </tr>
    <tr>
        <td>3.</td>
        <td>32</td>
        <td>FILIPPOV</td>
        <td>LABA3</td>
		<td>-</td>
        <td>111110010101011001011001110011100011111</td>
        <td>1010101010010110101000011</td>
		<td>111110010101001110101111011100111011100</td>
    </tr>
    <tr>
        <td>4.</td>
        <td>3</td>
        <td>20210</td>
        <td>100021</td>
		<td>-</td>
        <td>10110111</td>
        <td>11111010</td>
		<td>-1000011</td>
    </tr>
    <tr>
        <td>5.</td>
        <td>7</td>
        <td>654</td>
        <td>0</td>
		<td>/</td>
        <td>101001101</td>
        <td>0</td>
		<td>Деление на ноль!</td>
    </tr>
    <tr>
        <td>6.</td>
		<td>5</td>
        <td>41230</td>
        <td>101</td>
		<td>+</td>
        <td>101010000010</td>
        <td>11010</td>
		<td>101010011100</td>
    </tr>
</table>
</body>
</html>
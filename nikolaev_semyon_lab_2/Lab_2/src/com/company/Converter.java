package com.company;

import java.util.Scanner;

public class Converter {

    private static String number;
    private int inputNumSystem;
    private int outputNumSystem;
    private int decimalNotation;
    private int[] digitsOfNum;
    private String resultNumber;

    Converter(){
        resultNumber = "";
    }

    public String convert(){
        input();
        digitsOfNum = new int[number.length()];
        toIntArray();
        toDecimalNotation();
        toOutputNumSystem();
        return resultNumber;
    }

    public String convert(int n, int m, String number){
        inputNumSystem = n;
        outputNumSystem = m;
        this.number = number.toUpperCase();

        digitsOfNum = new int[number.length()];
        toIntArray();
        toDecimalNotation();
        toOutputNumSystem();
        return resultNumber;
    }

    /** Input variable @inputNumSystem @outputNumSystem @numInt*/
    private void input(){
        Scanner in = new Scanner(System.in);
        System.out.print("Input number system N: ");
        inputNumSystem = in.nextInt();

        System.out.print("Input number system M: ");
        outputNumSystem = in.nextInt();

        System.out.print("Input number for convert: ");
        number = in.next();

        number = number.toUpperCase();
    }

    /** Load numInt array, with change A-F alpha on the number 10-15*/
    private void toIntArray(){
        char[] firstNumChar = number.toCharArray();

        for (int i = 0; i < firstNumChar.length; i++) {
            if(firstNumChar[i] - '0' <= 9 && firstNumChar[i] - '0' >= 0){
                digitsOfNum[i] =  (int)(firstNumChar[i] - '0');
            }else if(firstNumChar[i] - 'A' <= 5 && firstNumChar[i] - 'A' >= 0){
                digitsOfNum[i] =  (int)(firstNumChar[i] - 'A' + 10);
            }else {
                System.out.println("Error input!");
                break;
            }
        }
    }

    /** Convert @number in @inputNumSystem to decimal notation*/
    private  void toDecimalNotation(){
        int k = 1;
        decimalNotation = digitsOfNum[digitsOfNum.length - 1];

        for(int i = digitsOfNum.length - 2; i >= 0; i--){
            k *= inputNumSystem;
            decimalNotation += digitsOfNum[i] * k;
        }
    }

    /** Convert @number in @decimalNotation to @outputNumSystem*/
    private void  toOutputNumSystem(){
        int[] outIntNumArr = new int[decimalNotation / outputNumSystem + 2];
        int k;
        {
            int i = 0;
            while (decimalNotation != 0) {
                outIntNumArr[i] = decimalNotation % outputNumSystem;
                decimalNotation /= outputNumSystem;
                //System.out.println(" ->" + i);
                i++;
            }
            k = i - 1;
        }

        char[] outNumber = new char[outIntNumArr.length ];

        for(int i = k; i >= 0; i--){
            if(outIntNumArr[i]  >= 10 && outIntNumArr[i] <= 15){
                outNumber[i] = (char)(outIntNumArr[i] + 'A' - 10);
            }else if(outIntNumArr[i]  >= 0 && outIntNumArr[i] <= 9){
                outNumber[i] = (char)(outIntNumArr[i] + '0');
            }else{
                System.out.print("Error output");
            }
            resultNumber += outNumber[i];
        }
    }
}

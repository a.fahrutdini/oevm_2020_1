package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int inNumSystem = input.nextInt(); //исходная система счисления
        int outNumSystem = input.nextInt(); //конечная система счисления
        String number = input.next(); //число для перевода
        input.close();

        if (inNumSystem >= 2 && inNumSystem <= 16) {
            TranslationSystem tr = new TranslationSystem();
            StringBuffer result = tr.translation(inNumSystem, outNumSystem, number);
            System.out.println(result);
        } else
            System.out.println("Основание системы счисления должно быть 2-16");
    }
}

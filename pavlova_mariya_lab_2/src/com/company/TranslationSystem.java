package com.company;

public class TranslationSystem {

    public static int translateToDecSystem(int inNumSystem, String number) {
        int res = 0;
        char[] numberArr = number.toCharArray();

        for (int i = 0; i < numberArr.length; i++) {
            if (Character.getNumericValue(numberArr[i]) >= inNumSystem) {
                return -1;
            }
            int power = (int)Math.pow(inNumSystem, number.length()-i-1);
            if (numberArr[i] >= '0' && numberArr[i] < '9') {
                res += Character.getNumericValue(numberArr[i])*power;
            }
            else if (numberArr[i] >= 'A' && numberArr[i] <= 'F') {
                res += (numberArr[i] - 'A' + 10) * power;
            }
        }
        return res;
    }

    public static StringBuffer translateFromDecSystem(int outNumSystem, int number) {
        if (number == -1) {
            return new StringBuffer("Введенная цифра отсутствует в исходной СС");
        }
        StringBuffer res = new StringBuffer();
        if (number/outNumSystem == 0) {
            return res.append(number);
        }

        while (number/outNumSystem != 0 ) {
            int remainder = number%outNumSystem;
            if (remainder < 10) {
                res.append(remainder);
            } else if (remainder >= 10 &&  remainder <= 15) {
                char subRes = (char)(remainder + 'A' - 10);
                res.append(subRes);
            }
            number = number/outNumSystem;
        }

        if (number > 9) {
            res.append((char)(number + 'A' - 10));
            return res.reverse();
        }

        res.append(number);
        return res.reverse();
    }

    public static StringBuffer translation(int inNumSystem, int outNumSystem, String number) {
        int decNumber = translateToDecSystem(inNumSystem,number); //перевод из исходной СС в 10 СС
        return translateFromDecSystem(outNumSystem, decNumber); //перевод из 10 СС в выходную СС
    }

}

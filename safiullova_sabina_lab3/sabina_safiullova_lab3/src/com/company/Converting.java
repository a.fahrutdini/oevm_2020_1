package com.company;

import java.util.Arrays;

public class Converting {
    public static String convertToDecimal(int base, char[] number) {
        long decimal = 0;
        int length= number.length;

        for (int i = 0; i < length; i++) {
            char digit = Character.toUpperCase(number[i]);

            if (Character.isLetter(digit)) {
                decimal += (digit - 'A' + Main.OFFSET) * (Math.pow(base, length - 1 - i));
            } else if (Character.isDigit(digit)) {
                decimal += (number[i] - '0') * (Math.pow(base, length - 1 - i));
            } else {
                System.exit(1);
            }
        }

        String reverseNumber = convertToBinary(decimal);
        return reverseNumber;
    }
    public static String  convertToBinary(long decimal) {
        long divide = decimal;
        int quantity = 1;

        while (Math.pow(2,quantity) <= decimal) {
            quantity++;
        }

        int [] binaryNumber = new int[quantity];
        int n = 0;

        while (divide >= 2) {
            binaryNumber[n] = (int) divide % 2;

            n++;
            divide= divide / 2;
        }

        binaryNumber[quantity-1] = (int)divide % 2;

        String stringNum = new String(); ;
        stringNum = Arrays.toString(binaryNumber);
        String reverseNumber = new StringBuffer(stringNum).reverse().toString();

        return reverseNumber;
    }
}

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.concurrent.ForkJoinWorkerThread;

import javax.swing.JPanel;

public class MyJPanel extends JPanel {
	String str = "!1!4+23+1!34+1!2!3";
	int countPer = 4;
	int sizeYterm = 15;
	int Y = 75;
	int X=30;
	
	@Override
	public void paint(Graphics g) {
		X=30;
		super.paint(g);
		setGraphix(g);
	}

	private void setGraphix(Graphics g) {
		int countTerm = 1;
		boolean[] inv = new boolean[countPer+1]; 
		boolean[] per = new boolean[countPer+1];
		countTerm = CountTerm(inv,per,countTerm);
		int[] xper = new int[countPer+1];
		int[] nexper = new int[countPer+1];
		CreateInput(g,inv,per,xper,nexper);
		Y=75;
		boolean ontr=false;
		int[][] outTerm= new int[countPer+1][2];
		DrawTerms(g,nexper,xper,ontr,outTerm,countTerm);
	}

	private void DrawTerms(Graphics g, int[] nexper, int[] xper, boolean ontr, int[][] outTerm, int countTerm) {
		g.drawLine(X, Y-15, X+30, Y-15);
		g.drawString("+",X+15,Y);
		int j = 0;
		for (int i = 0; i < str.length(); i++) {
			switch (str.charAt(i)) {
				case '!':
					ontr=true;
					break;
				case '1':
				case '2':
				case '3':
				case '4':
					int temp = Integer.parseInt(str.charAt(i)+"");
					if(ontr) {
						sizeYterm+=15;
						g.drawLine(X, Y+15, X, Y-15);
						g.drawLine(X+30, Y+15, X+30, Y-15);
						g.drawLine(nexper[temp], Y, X, Y);
					}
					else {
						sizeYterm+=15;
						g.drawLine(X, Y+15, X, Y-15);
						g.drawLine(X+30, Y+15, X+30, Y-15);
						g.drawLine(xper[temp], Y, X, Y);
					}
					Y+=15;
					ontr=false;
					break;
				case '+':
					g.drawLine(X, Y, X+30, Y);
					g.drawLine(X, Y, X, Y-15);
					g.drawLine(X, Y+15, X+30, Y+15);
					g.drawString("+",X+15,Y+15+15);
					outTerm[j][0]=X+30;
					outTerm[j][1]=Y-sizeYterm/2;
					j++;
					sizeYterm=15;
					Y+=30;
					break;
				default:
					break;
			}
		}
		outTerm[j][0]=X+30;
		outTerm[j][1]=Y-sizeYterm/2;
		g.drawLine(X, Y, X+30, Y);
		g.drawLine(X+30, Y-sizeYterm/2, X+30+15, Y-sizeYterm/2);
		X+=60*2;
		Y=60+((Y-60)/2)-15*countTerm/2;
		g.drawRect(X, Y, 30, 10*(countTerm+1));
		g.drawString("*",X+15,Y+15+15);
		g.drawLine(X+30, Y+15*countTerm/2, X+45, Y+15*countTerm/2);
		DrawConnect(outTerm,Y,g);
	}

	private void CreateInput(Graphics g, boolean[] inv, boolean[] per, int[] xper, int[] nexper) {
		for (int i = 0; i < per.length; i++) {
			if(per[i]) {
				g.drawLine(X, 30, X, 1000);
				xper[i]=X;
				if(inv[i]) {
					X+=30;
					g.drawLine(X, 45, X, 1000);
					nexper[i]=X;
					X+=30;
				} else {
					X+=60;
				}

			}
		}
	}

	private int CountTerm(boolean[] inv, boolean[] per, int countTerm) {
		for (int i = 0; i < str.length(); i++) {
			if(str.charAt(i)=='!') {
				inv[Integer.parseInt(str.charAt(i+1)+"")]=true;
			}
			else if(str.charAt(i)=='1') {
				per[1]=true;
			}
			else if(str.charAt(i)=='2') {
				per[2]=true;
			}
			else if(str.charAt(i)=='3') {
				per[3]=true;
			}
			else if(str.charAt(i)=='4') {
				per[4]=true;
			} else if(str.charAt(i)=='+') {
				countTerm++;
			}
		}
		return countTerm;
	}

	private void DrawConnect(int[][] outTerm, int y,Graphics g) {
		int smesh = 60;
		y+=10;
		for (int i = 0; i < outTerm.length; i++) {
			if(outTerm[i][0]==0) continue;
			g.drawLine(outTerm[i][0], outTerm[i][1],outTerm[i][0]+smesh, outTerm[i][1] );
			g.drawLine(outTerm[i][0]+smesh, outTerm[i][1],outTerm[i][0]+smesh, y );
			g.drawLine(outTerm[i][0]+smesh, y ,X,y);
			smesh-=10;
			y+=10;
		}
	}
}

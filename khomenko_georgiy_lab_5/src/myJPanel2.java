import java.awt.Graphics;

import javax.swing.JPanel;

public class MyJPanel2 extends JPanel {
	String str = "X1+X3+!X4+*!X1+X2+!X3";
	int countPer = 4;
	int x = 30;
	int sizeYterm = 15;
	int y = 75;

	@Override
	public void paint(Graphics g) {
		x =30;
		super.paint(g);
		SetGraphix(g);
	}

	private void SetGraphix(Graphics g) {
		int countTerm = 1;
		boolean[] inv = new boolean[countPer+1]; 
		boolean[] per = new boolean[countPer+1];
		countTerm = CountTerm(inv,per,countTerm);
		int[] xper = new int[countPer+1];
		int[] nexper = new int[countPer+1];
		boolean ontr=false;
		int[][] outTerm= new int[countTerm+1][2];
		CreateInput(g,inv,per,xper,nexper);
		DrawTerms(g,nexper,xper,ontr,outTerm,countTerm);
	}

	private int[][] DrawTerms(Graphics g, int[] nexper, int[] xper, boolean ontr, int[][] outTerm, int countTerm) {

		g.drawLine(x, y -15, x +30, y -15);
		g.drawString("*", x +15, y);
		int j = 0;
		for (int i = 0; i < str.length(); i++) {
			switch (str.charAt(i)) {
				case '!':
					ontr=true;
					break;
				case '1':
				case '2':
				case '3':
				case '4':
					int temp = Integer.parseInt(str.charAt(i)+"");
					if(ontr) {
						sizeYterm+=15;
						g.drawLine(x, y +15, x, y -15);
						g.drawLine(x +30, y +15, x +30, y -15);
						g.drawLine(nexper[temp], y, x, y);
					}
					else {
						sizeYterm+=15;
						g.drawLine(x, y +15, x, y -15);
						g.drawLine(x +30, y +15, x +30, y -15);
						g.drawLine(xper[temp], y, x, y);
					}
					y +=15;
					ontr=false;
					break;
				case '*':
					g.drawLine(x, y, x +30, y);
					g.drawLine(x, y, x, y -15);
					g.drawLine(x, y +15, x +30, y +15);
					g.drawString("*", x +15, y +15+15);
					outTerm[j][0]= x +30;
					outTerm[j][1]= y -sizeYterm/2;
					j++;
					y +=30;
					break;
				default:
					break;
			}
		}
		outTerm[j][0]= x +30;
		outTerm[j][1]= y -sizeYterm/2;
		g.drawLine(x, y, x +30, y);
		g.drawLine(x +30, y -sizeYterm/2, x +30+15, y -sizeYterm/2);
		x +=300;
		y =60+((y -60)/2)-15*countTerm/2;
		g.drawRect(x, y, 30, 10*(countTerm+1));
		g.drawString("+", x +15, y +15+15);
		g.drawLine(x +30, y +15*countTerm/2, x +45, y +15*countTerm/2);
		DrawConnect(outTerm,g);
		return null;
	}

	private void CreateInput(Graphics g, boolean[] inv, boolean[] per, int[] xper, int[] nexper) {
		for (int i = 0; i < per.length; i++) {
			if(per[i]) {
				g.drawLine(x, 30, x, 1000);
				xper[i]= x;
				if(inv[i]) {
					x +=30;
					g.drawLine(x, 45, x, 1000);
					nexper[i]= x;
					x +=30;
				} else {
					x +=60;
				}
			}
		}
	}

	private int CountTerm(boolean[] inv, boolean[] per, int countTerm) {
		for (int i = 0; i < str.length(); i++) {
			char temp =str.charAt(i);
			if(temp=='!') {
				inv[Integer.parseInt(str.charAt(i+2)+"")]=true;
			}
			else if(temp=='1') {
				per[1]=true;
			}
			else if(temp=='2') {
				per[2]=true;
			}
			else if(temp=='3') {
				per[3]=true;
			}
			else if(temp=='4') {
				per[4]=true;
			} else if(temp=='*') {
				countTerm++;
			}
		}
		return  countPer;
	}
	
	private void DrawConnect(int[][] outTerm,Graphics g) {
		int smesh = 120;
		y +=10;
		for (int i = 0; i < outTerm.length; i++) {
			if(outTerm[i][0]==0) continue;
			g.drawLine(outTerm[i][0], outTerm[i][1],outTerm[i][0]+smesh, outTerm[i][1] );
			g.drawLine(outTerm[i][0]+smesh, outTerm[i][1],outTerm[i][0]+smesh, y);
			g.drawLine(outTerm[i][0]+smesh, y, x, y);
			if(i>=outTerm.length/2) {
				smesh +=10;
			} else {
				smesh -=10;
			}
			y +=10;
		}
		
	}

	
}

import java.util.Random;

public class DNFandKNF {
    public static int[][] generateArray() {
        Random random = new Random();

        return new int[][]{
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},
        };
    }

    public static String DNF(int [][] generatedArray) {
        String result = new String();

        for (int[] item: generatedArray ) {
            if (item[4] == 1) {
                if (result.isEmpty()) {//если это первая итерация
                    result += ((item[0] == 0) ? "( -x1 * " : "( x1 * ");
                }
                else {
                    result += ((item[0] == 0) ? " + \n( -x1 * " : " + \n( x1 * ");
                }
                result += ((item[1] == 0) ? "-x2 * " : "x2 * ");
                result += ((item[2] == 0) ? "-x3 * " : "x3 * ");
                result += ((item[3] == 0) ? "-x4 )" : "x4 )");
            }
        }
        return result;
    }

    public static String KNF(int [][] generatedArray) {
        String result = new String();

        for (int[] item: generatedArray ) {
            if (item[4] == 0) {
                if (result.isEmpty()) {//если это первая итерация
                    result += ((item[0] == 0) ? "( x1 + " : "( -x1 + ");
                }
                else {
                    result += ((item[0] == 0) ? " * \n( -x1 + " : " * \n( -x1 + ");
                }
                result += ((item[1] == 0) ? "x2 + " : "-x2 + ");
                result += ((item[2] == 0) ? "x3 + " : "-x3 + ");
                result += ((item[3] == 0) ? "x4 )" : "-x4 )");
            }
        }
        return result;
    }
}


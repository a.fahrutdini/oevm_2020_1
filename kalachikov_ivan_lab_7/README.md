# Лабораторная работа №7 "Изучение методов трансляции" 

* Использовал язык Java и регулярные выражения для обработки текста
* Запускается через program.EXE файл, сам транслятор запускается через среду разработки IDEA
* Класс Reader отвечает за чтение кода на языке pascal и занесения переменных и команд в базу данных DataBase
После чего класс Writer на основании всех данных из объекта класса DataBase генерирует FASM код.
* Тесты:

![Картинка](https://sun9-74.userapi.com/impg/pI3KpNF5CN-2AALmFglRaiZGDWNgYONUh7cUXA/IxvWn2_BkdA.jpg?size=453x305&quality=96&proxy=1&sign=ebbe8acee585e7a24e21b66f674f91a9)
![Картинка](https://sun9-63.userapi.com/impg/jqp_4bWjoOiHEmCgVx8QY1-DhEgGxAA0Mf9VYA/Suz3gM79bYM.jpg?size=532x910&quality=96&proxy=1&sign=32237ce4145fd35226df9391271efb61)  

[Видео](https://drive.google.com/file/d/1HAv9EtoGW2blmcz2hWkZR6GzFnCYX-oV/view?usp=sharing)

## Лабораторная работа №4 "Минимизация булевых функций"

Выполнила студентка группы **ПИбд-22 Долгова Ангелина**


### Задание:

Разработать программный продукт для минимизации булевой функции
из четырех переменных. При запуске программы
случайным образом заполняются значения функции в таблице
истинности. 

*Пользователь указывает конечную форму логической функции (ДНФ или КНФ). 
После расчетов программа выводит функцию в выбранной форме.*

---------------------------------------------------------------

**Тесты:**

*Таблица истинности:*

| **X<sub>1</sub>** | **X<sub>2</sub>** | **X<sub>3</sub>** | **X<sub>4</sub>** | **F** 
| :---: | :---: | :---: | :---: | :---: 
| 0 | 0 | 0 | 0 | 0 
| 0 | 0 | 0 | 1 | 1
| 0 | 0 | 1 | 0 | 0
| 0 | 0 | 1 | 1 | 0
| 0 | 1 | 0 | 0 | 1
| 0 | 1 | 0 | 1 | 1
| 0 | 1 | 1 | 0 | 0
| 0 | 1 | 1 | 1 | 0
| 1 | 0 | 0 | 0 | 1
| 1 | 0 | 0 | 1 | 0
| 1 | 0 | 1 | 0 | 1
| 1 | 0 | 1 | 1 | 0
| 1 | 1 | 0 | 0 | 0
| 1 | 1 | 0 | 1 | 0
| 1 | 1 | 1 | 0 | 1
| 1 | 1 | 1 | 1 | 1

ДНФ: *(!X_1*!X_2*!X_3*X_4) + (!X_1*X_2*!X_3*!X_4) + (!X_1*X_2*!X_3*X_4) + (X_1*!X_2*!X_3*!X_4) + (X_1*!X_2*X_3*!X_4) + (X_1*X_2*X_3*!X_4) + (X_1*X_2*X_3*X_4)*

КНФ: *(X_1 + X_2 + X_3 + X_4)*(X_1 + X_2 + !X_3 + X_4)*(X_1 + X_2 + !X_3 + !X_4)*(X_1 + !X_2 + !X_3 + X_4)*(X_1 + !X_2 + !X_3 + !X_4)*(!X_1 + X_2 + X_3 + !X_4)*(!X_1 + X_2 + !X_3 + !X_4)*(!X_1 + !X_2 + X_3 + X_4)*(!X_1 + !X_2 + X_3 + !X_4)*

| **X<sub>1</sub>** | **X<sub>2</sub>** | **X<sub>3</sub>** | **X<sub>4</sub>** | **F** 
| :---: | :---: | :---: | :---: | :---: 
| 0 | 0 | 0 | 0 | 1 
| 0 | 0 | 0 | 1 | 0
| 0 | 0 | 1 | 0 | 0
| 0 | 0 | 1 | 1 | 0
| 0 | 1 | 0 | 0 | 1
| 0 | 1 | 0 | 1 | 1
| 0 | 1 | 1 | 0 | 1
| 0 | 1 | 1 | 1 | 0
| 1 | 0 | 0 | 0 | 0
| 1 | 0 | 0 | 1 | 1
| 1 | 0 | 1 | 0 | 0
| 1 | 0 | 1 | 1 | 0
| 1 | 1 | 0 | 0 | 1
| 1 | 1 | 0 | 1 | 0
| 1 | 1 | 1 | 0 | 1
| 1 | 1 | 1 | 1 | 0

ДНФ: *(!X_1 * !X_2 * !X_3 * !X_4) + (!X_1 * X_2 * !X_3 * !X_4) + (!X_1 * X_2 * !X_3 * X_4) + (!X_1 * X_2 * X_3 * !X_4) + (X_1 * !X_2 * !X_3 * X_4) + (X_1 * X_2 * !X_3 * !X_4) + (X_1 * X_2 * X_3 * !X_4)*

КНФ:  *(X_1 + X_2 + X_3 + !X_4) * (X_1 + X_2 + !X_3 + X_4) * (X_1 + X_2 + !X_3 + !X_4) * (X_1 + !X_2 + !X_3 + !X_4) * (!X_1 + X_2 + X_3 + X_4) * (!X_1 + X_2 + !X_3 + X_4) * (!X_1 + X_2 + !X_3 + !X_4) * (!X_1 + !X_2 + X_3 + !X_4) * (!X_1 + !X_2 + !X_3 + !X_4)*

---------------------------------------------------------------

[Видео c демонстрацией работы](https://goo.su/35cK)

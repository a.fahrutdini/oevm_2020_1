public class TruthTable {
    private final int numberOfRows;
    private final int numberOfColumns;
    public int[][] truthTable;

    public TruthTable(int countOfRows, int countOfColumns){
        this.numberOfRows = countOfRows;
        this.numberOfColumns = countOfColumns;
        truthTable = new int[countOfRows][countOfColumns];
    }

    //Метод для заполнения таблицы истинности
    public void fillTruthTable() {
        for (int i = 0; i < numberOfRows; i++) {
            char[] fillChar = Converter.convertToBinary(i).toCharArray();
            for (int j = 0; j < numberOfColumns - 1; j++) {
                truthTable[i][j] = fillChar[j] - '0';
                truthTable[i][numberOfColumns - 1] = (int) (Math.random() * 2);
            }
        }
    }

    //Метод для вывода таблицы истинности в консоль
    public void printTruthTable() {
        System.out.println("_____________________________");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("| X_1 | X_2 | X_3 | X_4 | F |\n");
        for (int i = 0; i < numberOfRows; ++i) {
            stringBuilder.append("|");
            for (int j = 0; j < numberOfColumns - 1; ++j) {
                stringBuilder.append("  ").append(truthTable[i][j]).append("  |");
            }
            stringBuilder.append(" ").append(truthTable[i][numberOfColumns - 1]).append(" |\n");
        }
        System.out.print(stringBuilder);
        System.out.println("_____________________________");
    }

    public int[][] getTruthTable() {
            return truthTable;
    }
}

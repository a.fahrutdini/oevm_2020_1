import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("\t\t\tМИНИМИЗАЦИЯ БУЛЕВЫХ ФУНКЦИЙ\t\t\t");
        System.out.println();
        Scanner scanner = new Scanner(System.in);

        final int NUMBER_OF_ROWS = 16;
        final int NUMBER_OF_COLUMNS = 5;

        TruthTable truthTable = new TruthTable(NUMBER_OF_ROWS, NUMBER_OF_COLUMNS);
        FormConverter formConverter = new FormConverter(NUMBER_OF_ROWS, NUMBER_OF_COLUMNS);

        System.out.println();
        System.out.println("Таблица истинности, заполненная случайным образом:");
        truthTable.fillTruthTable();
        truthTable.printTruthTable();

        while (true) {
            System.out.println();
            System.out.print("\nУкажите конечную форму логической функции (ДНФ/КНФ) \n(Для генерации новой таблицы введите \"new\". Для выхода введите \"exit\"): ");
            String form = scanner.nextLine();

            switch (form) {
                case "ДНФ":
                    System.out.println("ДНФ: ");
                    formConverter.bringToDNF(truthTable.getTruthTable());
                    break;
                case "КНФ":
                    System.out.println("КНФ: ");
                    formConverter.bringToCNF(truthTable.getTruthTable());
                    break;
                case "new":
                    System.out.println("Таблица истинности, заполненная случайным образом:");
                    truthTable.fillTruthTable();
                    truthTable.printTruthTable();
                    break;
                case "exit":
                    return;
                default:
                    System.out.println("Ошибка! Введены неккоректные данные.");
                    break;
            }
        }
    }
}
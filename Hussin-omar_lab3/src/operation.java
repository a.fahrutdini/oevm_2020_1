import java.util.Arrays;
import java.util.Scanner;
public class binaryoperrations {
	
	
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        System.out.print("Enter the original number system: ");
	        int firstSystem = scanner.nextInt();
	        System.out.print("Enter your first : ");
	        char[] firstDigit = scanner.next().toCharArray();
	        System.out.print("Enter your second : ");
	        char[] secondDigit = scanner.next().toCharArray();
	        System.out.print("The operation: \n- - - - - - - - - - - \n + \n- - - - - - - - -  \n - \n- - - - - - - \n * \n- - - - - \n / \n- - - \n");
	        char x = scanner.next().charAt(0);
	        int firstNumber = ConvertingToBinarySystem.convertingToDecimalSystem(firstSystem, firstDigit);
	        int secondNumber = ConvertingToBinarySystem.convertingToDecimalSystem(firstSystem, secondDigit);
	        char[] firstNumberA =ConvertingToBinarySystem.convertingToBinarySystem(firstNumber).toCharArray();
	        char[] secondNumberBinary = ConvertingToBinarySystem.convertingToBinarySystem(secondNumber).toCharArray();

	        switch (x) {
	            case '+':System.out.print("sum = " + binaryoperrations.sum(firstNumberA, secondNumberBinary));
	                break;   
	            case '-': System.out.print(" difference = " + binaryoperrations.differencebtweewnbinaryNumbers(firstNumberA, secondNumberBinary));
	                break;
	            case '*':  System.out.print(" multiplication = " + binaryoperrations.multiplication(firstNumberA, secondNumberBinary));
	                break;
	            case '/':System.out.print(" divide = " + binaryoperrations.divide(firstNumberA, secondNumberBinary));
	                break;
	            default: System.out.print(" Symbol");
	                break;
	        }
	    }
 public static String multiplication(char[] firstBinary, char[] secondBinary) {
 	
     char[] ResultOfMultiplicationOfBinary = {};
     
     for (int i = 0; i < ConvertingToBinarySystem.convertingToDecimalSystem(2, secondBinary); i++) {
     	
         ResultOfMultiplicationOfBinary = (sum(ResultOfMultiplicationOfBinary, firstBinary).toCharArray());
     }
     return new String(ResultOfMultiplicationOfBinary);
 }

 public static String divide(char[] Dividend, char[] Divider) {
     int ResultDivide = 0;
     while (ConvertingToBinarySystem.convertingToDecimalSystem(2, Dividend) >= ConvertingToBinarySystem.convertingToDecimalSystem(2, Divider) &&!Arrays.equals(Dividend, new char[]{'0'})) {
        Dividend = ( differencebtweewnbinaryNumbers(Dividend, Divider).toCharArray());
         ResultDivide++;
     }
     return ConvertingToBinarySystem.convertingToBinarySystem(ResultDivide);
 }
 
 public static String differencebtweewnbinaryNumbers(char[] minuend, char[] subtrahends) {
     boolean minu = false;
     if (ConvertingToBinarySystem.convertingToDecimalSystem(2, minuend) < ConvertingToBinarySystem.convertingToDecimalSystem(2, subtrahends)) {
         minu = true;
         char[] tmp = subtrahends;
         subtrahends = minuend;
         minuend = tmp;
     } else if (ConvertingToBinarySystem.convertingToDecimalSystem(2, minuend) == 
    		 ConvertingToBinarySystem.convertingToDecimalSystem(2, subtrahends)) {
         return "0";
     }
     StringBuilder ResultDifferenceBetweenBinaryNumbers = new StringBuilder();
     int differenceDischarges = minuend.length - subtrahends.length;
     char[] reverseSubtrahend = new char[minuend.length];

     for (int i = 0; i < reverseSubtrahend.length; i++) {
         if (i < differenceDischarges) {
             reverseSubtrahend[i] = '1';
         } else if (subtrahends[i - differenceDischarges] == '0') {
             reverseSubtrahend[i] = '1';
         } else {
             reverseSubtrahend[i] = '0';
         }
     }
     //additional code that contains the reverse code + 1
     
     
     
     char[] auxiliaryCode = sum(reverseSubtrahend, new char[]{'1'}).toCharArray();
     
     //addition of additional code and minified
     char[] auxiliarySum = sum(auxiliaryCode, minuend).toCharArray();
     
     
     //checking for trailing zeros
     boolean helperFindZeros = true;

     for (int i = 1; i < auxiliarySum.length; i++) {
         if (auxiliarySum[i] == '1') {
             helperFindZeros = false;
         } else if (helperFindZeros) {
             continue;
         }
         ResultDifferenceBetweenBinaryNumbers.append(auxiliarySum[i]);
     }
     return ResultDifferenceBetweenBinaryNumbers.toString();
 }

 
// Method for adding binary numbers
 //Contains the variable discharge, which retains the remainder when added and transfers it to the next discharge
 public static String sum(char[] firstdi, char[] seconddi) {
     StringBuilder sb = new StringBuilder();
     char currentDigit;
     int discharge = 0;

     for (int i = firstdi.length - 1, j = seconddi.length - 1; i >= 0 || j >= 0 || discharge == 1; --i, --j) {

         int firstBinaryDigit;
         int secondBinarydigit;

         if (i < 0) {
             firstBinaryDigit = 0;
         } else if (firstdi[i] == '0') {
             firstBinaryDigit = 0;
         } else {
             firstBinaryDigit = 1;
         }

         if (j < 0) {
             secondBinarydigit = 0;
         } else if (seconddi[j] == '0') {
             secondBinarydigit = 0;
         } else {
             secondBinarydigit = 1;
         }
         //the current amount contains the first and second binary numbers, as well as the remainder when adding the first and second binary numbers
         int currentSum = firstBinaryDigit + secondBinarydigit + discharge;

         if (currentSum == 0) {
             currentDigit = '0';
             discharge = 0;
         } else if (currentSum == 1) {
             currentDigit = '1';
             discharge = 0;
         } else if (currentSum == 2) {
             currentDigit = '0';
             discharge = 1;
         } else {
             currentDigit = '1';
             discharge = 1;
         }
         sb.insert(0, currentDigit);
     }
     return sb.toString();
 }
 
}

	
	
	
	
	
	



# Лабораторная работа 5
# Изучение простейших логических элементов
#####Выполнил: Журавлев Александр | ИСЭбд-21
***
* Программа разработана в среде IDEA с конфигурацией **Main**.
* Программа создает таблицу истинности и приводит ее к ДНФ и КНФ. 
* Пример работы: 

**[Скрин](https://drive.google.com/file/d/1BHiTTGEXNJR0wLokqXxzgodPvSNNqSDD/view?usp=sharing)**

**[Видео](https://drive.google.com/file/d/1-7yWTZ6zq79TAVoCjRHU3td6oo0YvKx9/view?usp=sharing)**
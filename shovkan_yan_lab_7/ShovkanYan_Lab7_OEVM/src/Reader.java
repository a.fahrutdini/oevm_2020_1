import java.io.FileReader;
import java.io.IOException;

public class Reader {

    public String read() throws IOException {
        StringBuilder str = new StringBuilder();

        FileReader reader = new FileReader("PascalCode.pas");
        int c;
        while ((c = reader.read()) != -1) {
            str.append((char) c);
        }
        str.toString().replaceAll(";", ";\n");
        return str.toString();
    }
}

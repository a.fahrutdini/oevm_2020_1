package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        final int countOfRows = 16;
        final int countOfColumns = 5;
        TruthTable truthTable = new TruthTable(countOfRows, countOfColumns);

        System.out.print("Введите 1 для рачёта ДНФ или 2 для рачёта КНФ: ");
        int choice = scanner.nextInt();

        System.out.print("\nСлучайно заполненная таблица истинности:\n");

        truthTable.fillTruthTable();
        truthTable.printTruthTable();

        NormalForm normalForm = new NormalForm(countOfRows, countOfColumns);

        switch (choice){
            case 1:
                System.out.print("\nДНФ: ");
                normalForm.convertToDisjunctiveNormalForm(truthTable.getTruthTable());
                break;
            case 2:
                System.out.print("\nКНФ: ");
                normalForm.convertToConjunctiveNormalForm(truthTable.getTruthTable());
                break;
            default:
                System.out.print("Ошибка! Нужно было нажать на кнопки 1 или 2");
        }
    }
}

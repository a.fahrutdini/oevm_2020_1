import java.util.ArrayList;

public class Subtraction implements IArithmetic {
    private ArrayList<Integer> result;
    private Addition addition;
    private ArrayList<Integer> unit;

    /**
     * Конструктор, инициализирующий все вспомогательные классы;
     */
    public Subtraction() {
        this.addition = new Addition();
        this.unit = new ArrayList<>(1);
        this.unit.add(1);
    }

    /**
     * Метод разности 2х чисел в 2с.с, передающихся в ArrayList
     */
    @Override
    public ArrayList<Integer> startArithmeticOperation(ArrayList<Integer> a, ArrayList<Integer> b) {
        this.result = new ArrayList<>();
        ArrayList<Integer> backB = new ArrayList<>();

        //Обратный код B_Binary
        for (int i = 0; i < b.size(); i++) {
            if (b.get(i) == 1) {
                backB.add(0);
            } else {
                backB.add(1);
            }
        }

        //Дополняем нулями
        for (int i = backB.size(); i < a.size(); i++) {
            backB.add(1);
        }

        //Дополнительный код B_Binary(+1)
        backB = this.addition.startArithmeticOperation(backB, this.unit);

        //Сложение А и дополнительного кода B
        this.result = this.addition.startArithmeticOperation(a, backB);
        this.result.remove(this.result.size() - 1);
        return this.result;
    }
}

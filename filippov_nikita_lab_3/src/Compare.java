import java.util.ArrayList;

import static java.lang.Math.min;

public class Compare {

    /**
     * Метод, возвращающий длину без незначущих нулей впереди
     */
    private int lengthWithoutZeros(ArrayList<Integer> arrayList) {
        int result = arrayList.size();
        for (int i = result - 1; ((result > 0) && (arrayList.get(i) == 0)); i--) {
            result--;
        }
        return result;
    }

    /**
     * Метод побитового сравнения 2х ArrayList. Вернет 1, если A>B; 0 - A=B, -1 A<B
     */
    public int equals(ArrayList<Integer> a, ArrayList<Integer> b, int step) {
        int aSize = lengthWithoutZeros(a);
        int bSize = lengthWithoutZeros(b);

        if (aSize > bSize) {
            return 1;
        }
        if (bSize > aSize) {
            return -1;
        }

        if (step >= min(aSize, bSize)) {
            if (a.get(aSize - step) > b.get(bSize - step)) {
                return 1;
            } else {
                if (b.get(bSize - step) > a.get(aSize - step)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }

        if (a.get(aSize - step - 1) > b.get(bSize - step - 1)) {
            return 1;
        } else {
            if (b.get(bSize - step - 1) > a.get(aSize - step - 1)) {
                return -1;
            } else {
                return equals(a, b, step + 1);
            }
        }
    }
}

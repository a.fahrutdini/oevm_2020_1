import java.util.Scanner;

public class Helper
{
    private Scanner in = new Scanner(System.in);
    private String[] codeX = {"X1", "X2", "X3", "X4"};             // Элементы таблицы истинности
    private int[][] _matrix;                                       // Основная матрица
    private  String _resultString;                                 // Результат
    final int N = 16;                                              // Кол-во строк в матрице
    final int M = 5;                                               // Кол-во столбцов в матрице


    public void data(int[][] TruthTable)
    {
        _matrix = TruthTable;
        _resultString = "";
    }

    private int[] convertTo2NS(int value)
    {
        int[] elems = {0, 0, 0, 0};
        int i = elems.length - 1;

        while (value >= 2)
        {
            elems[i] = value % 2;
            value /= 2;
            i--;
        }
        elems[i] = value;

        return  elems;
    }

    public void matrixFilling()
    {
        int[] tablesElems;
        for (int i = 0; i < N; i++)
        {
            tablesElems = convertTo2NS(i);

            for(int j = 0; j < tablesElems.length; j++)
            {
                System.out.printf("%d ", tablesElems[j]);
                _matrix[i][j] = tablesElems[j];
            }
            _matrix[i][M - 1] = (int)(Math.random() * 2);
            System.out.printf(" = %d\n", _matrix[i][M - 1]);
        }
    }

    public String conjunctiveNormalForm()
    {
        Boolean firstElem = false;                // переменная для определения первой скобки,
                                                  // перед которой не ставится знак "*"
        for(int i = 0; i < N; i++)
        {
            if (_matrix[i][M - 1] == 0)           // КНФ производит поиск по 0
            {
                if(!firstElem)                    // проверяем: является i-ая группа переменных первой скобкой
                    _resultString += "(";
                else
                    _resultString += " * (";

                for(int j = 0; j < M - 1; j++)    // в данном цикле происходит кодирование 1 и 0 i-ой переменной
                {                                 // в "X_i" с нужным знаком
                    if (j == 0)                   // проверка на первый элемент в группе (перед ним не ставится "+")
                    {
                        if (_matrix[i][j] == 1)                       // если 1 => "-"
                            _resultString += "-" + codeX[j];
                        else                                          // если 0 => знак у переменной не ставим
                            _resultString += codeX[j];
                    }
                    else                                              // если элемент не первый в группе (X2, X3, X4)
                    {
                        if (_matrix[i][j] == 1)                       // если 1 => "-"
                            _resultString += " + -" + codeX[j];
                        else                                          // если 0 => знак у переменной не ставим
                            _resultString += " + " + codeX[j];
                    }
                }
                _resultString += ")";                                 // в конце i-ой группы закрываем скобку
                firstElem = true;
            }
        }
        return _resultString;
    }

    public String disjunctiveNormalForm()            // аналогичный метод, только ДНФ поиск производится по 1
    {                                                // "+" меняем на "*", и в обратную сторону так же
        Boolean firstGroup = false;

        for(int i = 0; i < N; i++)
        {
            if (_matrix[i][M - 1] == 1)
            {
                if(!firstGroup)
                    _resultString += "(";
                else
                    _resultString += " + (";

                for(int j = 0; j < M - 1; j++)
                {
                    if (j == 0)
                    {
                        if (_matrix[i][j] == 0)
                            _resultString += "-" + codeX[j];
                        else
                            _resultString += codeX[j];
                    }
                    else
                    {
                        if (_matrix[i][j] == 0)
                            _resultString += " * -" + codeX[j];
                        else
                            _resultString += " * " + codeX[j];
                    }
                }
                _resultString += ")";
                firstGroup = true;
            }
        }
        return _resultString;
    }
}

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        NumberSystemsConverter converter = new NumberSystemsConverter();
        System.out.println("ПЕРЕВОД ЧИСЛА В ДРУГУЮ СИСТЕМУ СЧИСЛЕНИЯ");
        System.out.println();

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите исходную систему счисления (2-16): ");
        int originalSystem = scanner.nextInt();

        System.out.print("Введите конечную систему счисления (2-16): ");
        int finalSystem = scanner.nextInt();

        System.out.print("Введите число в исходной системе счисления: ");
        String originalNumber = scanner.next();

        if (converter.checkInputData(originalNumber.toCharArray(), originalSystem)) {
            System.out.print("Число " + originalNumber + " в " + finalSystem + " с.с. равно: ");
            System.out.print(converter.convertToFinalSystem(converter.convertToDecimalSystem(originalNumber.toCharArray(), originalSystem), finalSystem));
        }
    }
}

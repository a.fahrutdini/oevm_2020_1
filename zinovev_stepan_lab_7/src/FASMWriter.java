import java.io.FileWriter;
import java.io.IOException;

public class FASMWriter {
    private String path;

    public FASMWriter(String path) {
        this.path = path;
    }

    public void writeFile(CommandStorage commandStorage) {
        try (FileWriter writer = new FileWriter(path)) {
            writer.write("format PE console\n\n" +
                    "entry start\n\n" +
                    "\tinclude 'win32a.inc'\n\n" +
                    "section '.data' data readable writable\n\n" +
                    "\tspaceString db '%d', 0\n" +
                    "\tnewLine db '%d', 0ah, 0\n");

            while (commandStorage.hasVar()) {
                writer.write("\t" + commandStorage.removeVar(0));
            }

            writer.write("\nsection '.code' data readable executable\n\n" +
                    "\tstart:\n\n");

            commandsProcessing(commandStorage, writer);

            writer.write("\tcall [getch]\n" +
                    "\tpush NULL\n" +
                    "\tcall[ExitProcess]\n\n" +
                    "section '.idata' import data readable\n\n" +
                    "\tlibrary kernel, 'kernel32.dll',\\\n" +
                    "\t\tmsvcrt, 'msvcrt.dll'\n\n" +
                    "\timport kernel,\\\n" +
                    "\t\tExitProcess, 'ExitProcess'\n\n" +
                    "\timport msvcrt,\\\n" +
                    "\t\tprintf, 'printf',\\\n" +
                    "\t\tscanf, 'scanf',\\\n" +
                    "\t\tgetch, '_getch'");
            System.out.println("Код успешно преобразован и записан в файл " + path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void commandsProcessing(CommandStorage commandStorage, FileWriter writer) throws IOException {
        while (commandStorage.hasCommand()) {
            switch (commandStorage.removeCommand()) {
                case "read":
                    writer.write("\tpush " + commandStorage.removeReadCommand() + "\n" +
                            "\tpush spaceString\n" +
                            "\tcall [scanf]\n\n");
                    break;
                case "write":
                    writer.write("\tpush " + commandStorage.removeWriteCommand() + "\n" +
                            "\tcall [printf]\n\n");
                    break;
                case "writeln":
                    writer.write("\tpush [" + commandStorage.removeWritelnCommand() + "]\n" +
                            "\tpush newLine\n" +
                            "\tcall [printf]\n\n");
                    break;
                case "math":
                    String command = commandStorage.removeMathsCommand();
                    String[] arguments = command.split(" ");
                    if (!command.contains("div")) {
                        writer.write("\tmov ecx, [" + arguments[1] + "]\n" +
                                "\t" + arguments[3] + " ecx, [" + arguments[2] + "]\n" +
                                "\tmov [" + arguments[0] + "], ecx\n\n");
                    } else {
                        writer.write("\tmov eax, [" + arguments[1] + "]\n" +
                                "\tmov ecx, [" + arguments[2] + "]\n" +
                                "\tdiv ecx\n" +
                                "\tmov [" + arguments[0] + "], eax\n\n");
                    }
                    break;
            }
        }
    }
}
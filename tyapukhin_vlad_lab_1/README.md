
|  #  | Комплектующие                                                            | Комментарий |
| :----:  |:-------------------------------------------------------------------:| :----------------------------------------------------------------------------------------------------------------:|
|  1    | Процессор Ryzen 5 2600 | Хороший 6-ти ядерный процессор с тактовой частотой 3,4 ГГц.  |
|  2    | Материнская плата MSI B450 Tomahawk, AM4+ | Форм фактор ATX, Чипсет AMD B450, Тип поддерживаемой памяти DDR4 |
|  3    | Видеокарта MSI Nvidia GTX 1060 6GB | Достаточно компактна. Подходит для гейминга |
|  4    | Твердотельный накопитель Western Digital WD BLUE 3D NAND SATA SSD 250 GB | Высокая производительность |
|  5    | Жесткий диск WD Blue [WD10EZEX] | Неплохой объем памяти 1 ТБ и высокая скорость работы - 7200 об/мин. |
|  6    | Оперативная память Corsair 16 GB DDR4 3000 MHz Vengeance LPX Black | 4-ое поление памяти за счет высокой тактовой частоты обеспечивает хорошую производительность |
|  7    | Кулер для процессора SilentiumPC Grandis 2 XE1436 | Данный кулер справляется со своей основной задачей - охлаждением; уровень шума в норме |
|  8    | Блок питания EVGA SuperNOVA 750W | Суммарная мощность 750 Вват, чего вполне должно хватить с запасом |
|  9    | Корпус Aerocool Cylon RGB | Просторный и минималистичный. Вентилятор в комплекте на 120 мм. 1 разъем USB 3.0 и 2 разъема USB 2.0. Форм фактор также соблюден |
|  10  | Итоги | Эта сборка нацелена на получение максимального количества возможностей и производительности за минимальную цену. SSD позволяет запустить винду в течение 5-10 секунд, а связка комплектующих позволяет использовать компьютер не только для работы с десктопными приложениями без лагов, но и для игр. |



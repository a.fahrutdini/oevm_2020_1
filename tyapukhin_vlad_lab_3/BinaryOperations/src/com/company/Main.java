package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String system = in.nextLine();
        //Исходная СС
        int a = Integer.parseInt(system);
        String[] numbers = in.nextLine().split(" ");
        //Первое и второе число
        char[] numberOne = (numbers[0]).toCharArray();
        char[] numberTwo = (numbers[1]).toCharArray();
        //Выбранная операция
        String operation = in.nextLine();

        Transfer transfer = new Transfer(a, numberOne, numberTwo, operation);
        transfer.numberSystems();
    }
}



package com.company;

public class BinaryOperations {

    public static String add(int[] numberArrOne, int[] numberArrTwo, int maxLength) {

        int[] result = new int[maxLength + 1];
        int saveRank = 0, number = 0;

        for(int i = maxLength; i >= 0; i--) {
            /*
             * Если сумма ячеек первого и второга числа + значение числа, которое мы держим в уме >= 2,
             * тогда в ячеку массива результат запишется сумма условия - 2,
             * иначе в ячеку результата запишется сумма ячеек первого и второго числа + число, которое у нас в уме
             */
            if(numberArrOne[i] + numberArrTwo[i] + saveRank >= 2) {
                number = numberArrOne[i] + numberArrTwo[i] + saveRank - 2;
                saveRank = 1;
            } else {
                number = numberArrOne[i] + numberArrTwo[i] + saveRank;
                saveRank = 0;
            }
            result[i] = number;
        }

        String out = transferToString(result, maxLength);

        return out;
    }

    public static String subtract(int[] numberArrOne, int[] numberArrTwo, int maxLength) {

        int[] result = new int[maxLength + 1];

        for(int i = maxLength; i > 0; i--) {
            if(numberArrOne[i] == 1 && numberArrTwo[i] == 1) {
                result[i] = 0;
            } else if(numberArrOne[i] == 0 && numberArrTwo[i] == 1 && numberArrOne[i - 1] == 1) {
                numberArrOne[i - 1] = 0;
                result[i] = 1;
            } else if(numberArrOne[i] == 0 && numberArrTwo[i] == 1 && numberArrOne[i - 1] == 0) {
                int k = i - 1;
                while(numberArrOne[k] != 1) {
                    numberArrOne[k] = 1;
                    k--;
                }
                numberArrOne[k] = 0;
                result[i] = 1;
            } else if(numberArrOne[i] == 1 && numberArrTwo[i] == 0) {
                result[i] = 1;
            } else if(numberArrOne[i] == 0 && numberArrTwo[i] == 0) {
                result[i] = 0;
            }
        }

        String out = transferToString(result, maxLength);

        return out;
    }

    public static String multiply(int[] numberArrOne, int maxLength, int count) {

        int[] result = new int[maxLength + 1];

        for(int i = 0; i < maxLength + 1; i++) {
            result[i] = 0;
        }

        while (count > 0) {
            //Складываем первое число count раз
            String str = add(numberArrOne, result, maxLength);
            String[]str_arr = str.split("");
            for (int j = 0; j < str_arr.length; j++) {
                result[j] = Integer.parseInt(str_arr[j]);
            }
            count--;
        }

        String out = transferToString(result, maxLength);

        return out;
    }

    public static String divide(int[] numberArrTwo, int maxLength, String numberOneIn2) {

        int count = 1;

        //Пока делимое больше произведения делителя и частного, частное возрастает (частное в произведении представлено счетчиком)
        while(Long.parseLong(numberOneIn2) > Long.parseLong(multiply(numberArrTwo, maxLength, count))) {
            count++;
        }

        if(Long.parseLong(numberOneIn2) != Long.parseLong(multiply(numberArrTwo, maxLength, count))) {
            count--;
        }

        //Переводим частное в 2 СС
        String out = transferIn2SS(count);

        return out;
    }

    public static String transferIn2SS(int number) {

        String out = "";
        int tmp = 0;

        while (number >= 2) {
            tmp = number % 2;
            out = String.valueOf(tmp) + out;
            number /= 2;
        }

        out = String.valueOf(number) + out;

        return out;
    }

    public static String transferToString(int[] result, int maxLength) {

        String out = "";

        for(int i = maxLength; i >= 0; i--) {
            out = result[i] + out;
        }

        return out;
    }
}


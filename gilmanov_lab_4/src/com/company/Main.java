package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int [] resultColumn;
        int[][] table = {
                {0, 0, 0, 0},
                {0, 0, 0, 1},
                {0, 0, 1, 0},
                {0, 0, 1, 1},
                {0, 1, 0, 0},
                {0, 1, 0, 1},
                {0, 1, 1, 0},
                {0, 1, 1, 1},
                {1, 0, 0, 0},
                {1, 0, 0, 1},
                {1, 0, 1, 0},
                {1, 0, 1, 1},
                {1, 1, 0, 0},
                {1, 1, 0, 1},
                {1, 1, 1, 0},
                {1, 1, 1, 1},
        };
        Random random = new Random ();
        resultColumn = new int [table.length];
        StringBuilder printTable = new StringBuilder();
        for (int i = 0; i < table.length; i++){
            resultColumn[i] = random.nextInt(2);
        }
        printTable.append("x1 x2 x3 x4 F");
        printTable.append('\n');
        for (int i = 0; i < table.length; i++){
            for (int j = 0; j < table[0].length; j++){
                printTable.append(table[i][j] + "  ");
            }
            printTable.append(resultColumn[i]);
            printTable.append('\n');
        }
        System.out.println(printTable);
        DNF dnf = new DNF();
        dnf.DNF(table, resultColumn);
        System.out.println();
        KNF knf = new KNF();
        knf.KNF(table, resultColumn);
    }
}

package com.exmple.program;
import java.util.Scanner;

    public class Program {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            Helper helper = new Helper();

            System.out.printf("Enter numeral system (from): ");
            int fromNS = in.nextInt();
            System.out.printf("Enter value: ");
            String value = in.next();
            System.out.printf("Enter numeral system (to): ");
            int toNS = in.nextInt();

            helper.data(fromNS, toNS, value);
            String result = helper.convertToNumeralSystemString();
            System.out.printf("Result: %s", result);
        }
}

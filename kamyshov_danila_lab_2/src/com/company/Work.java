package com.company;

public class Work {

    public void transfer() {
        Check check = new Check();
        Input input = new Input();
        StringBuffer answer = new StringBuffer();
        try {
            input.initialization();
            if (!check.checkValidSystem(input.getStartSystemStr(), input.getFinishSystemStr())) {
                throw new InvalidSystemExeption();
            }
            if (!check.checkValidNumber(input.getNumber(), input.getStartSystemStr())) {
                throw new InvalidNumberExeption();
            }
            int startSystem = Integer.parseInt(input.getStartSystemStr());
            int finishSystem = Integer.parseInt(input.getFinishSystemStr());
            int buffer = 0;
            char[] Array = input.getNumber().toCharArray();
            for (int i = Array.length - 1; i >= 0; i--) {
                double pow = Math.pow(startSystem, Math.abs(i - Array.length + 1));
                if (Character.isDigit(Array[i])) {
                    buffer += ((int) Array[i] - 48) * pow;
                } else {
                    switch (Array[i]) {
                        case 'A' -> buffer += 10 * pow;
                        case 'B' -> buffer += 11 * pow;
                        case 'C' -> buffer += 12 * pow;
                        case 'D' -> buffer += 13 * pow;
                        case 'E' -> buffer += 14 * pow;
                        case 'F' -> buffer += 15 * pow;
                    }
                }
            }
            while (buffer >= finishSystem) {
                if (buffer % finishSystem >= 10) {
                    switch (buffer % finishSystem) {
                        case 10 -> answer.append("A");
                        case 11 -> answer.append("B");
                        case 12 -> answer.append("C");
                        case 13 -> answer.append("D");
                        case 14 -> answer.append("E");
                        case 15 -> answer.append("F");
                    }
                } else {
                    answer.append(buffer % finishSystem);

                }
                buffer /= finishSystem;
            }
            if (buffer >= 10) {
                switch (buffer) {
                    case 10 -> answer.append("A");
                    case 11 -> answer.append("B");
                    case 12 -> answer.append("C");
                    case 13 -> answer.append("D");
                    case 14 -> answer.append("E");
                    case 15 -> answer.append("F");
                }
            } else {
                answer.append(buffer);
            }
            answer.reverse();
            System.out.println(answer);
        } catch (InvalidNumberExeption e) {
            System.out.println("Введено неверное число");
        } catch (InvalidSystemExeption e) {
            System.out.println("Введена неверная система");
        }
    }

    public static class InvalidSystemExeption extends Exception {

    }

    public static class InvalidNumberExeption extends Exception {

    }
}

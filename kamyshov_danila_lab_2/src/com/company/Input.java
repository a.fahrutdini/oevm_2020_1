package com.company;

import java.util.Scanner;

public class Input {

    private final Scanner sc = new Scanner(System.in);
    private String startSystemStr;
    private String finishSystemStr;
    private String number;

    public void initialization() {
        System.out.println("Введите начальную СС (2-16): ");
        startSystemStr = sc.nextLine();
        System.out.println("Введите конечную СС (2-16): ");
        finishSystemStr = sc.nextLine();
        System.out.println("Введите число в начальной СС: ");
        number = sc.nextLine();

    }

    public String getStartSystemStr() {
        return startSystemStr;
    }

    public String getFinishSystemStr() {
        return finishSystemStr;
    }

    public String getNumber() {
        return number;
    }
}

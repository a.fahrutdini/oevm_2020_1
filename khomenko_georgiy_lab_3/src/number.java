public class Number {
    private final int Binary = 2;
    private final int Octal = 8;
    private final int Hexadecimal = 16;
    private final int Decimal = 10;
    public int numberDecimal;
    public String numberBinary = "";
    public String numberOctal = "";
    public String numberHexadecimal = "";

    public Number(String number, int fund) {
        switch (fund) {
            case Binary:
                numberBinary = number;
                numberInOctal();
                numberInDecimal();
                numberInHexadecimal();
                break;
            case Octal:
                numberOctal = number;
                numberInBinaryFOctal();
                numberInDecimal();
                numberInHexadecimal();
                break;
            case Decimal:
                numberDecimal = Integer.parseInt(number);
                numberInBinaryFDecimal();
                numberInOctal();
                numberInHexadecimal();
                break;
            case Hexadecimal:
                numberHexadecimal = number;
                numberInBinaryFHexadecimal();
                numberInOctal();
                numberInDecimal();
                break;
        }

    }

    private void numberInBinaryFOctal() {
        String[][] table = {
                        {"000", "0"},
                        {"001", "1"},
                        {"010", "2"},
                        {"011", "3"},
                        {"100", "4"},
                        {"101", "5"},
                        {"110", "6"},
                        {"111", "7"},
        };
        String tempNumber = numberOctal;
        for (int i = 0; i < table.length; i++) {
            tempNumber = tempNumber.replace(table[i][1], table[i][0]);
        }
        numberBinary = tempNumber;

    }

    private void numberInBinaryFHexadecimal() {
        String[][] table = {
                        {"0000", "0"},
                        {"0001", "1"},
                        {"0010", "2"},
                        {"0011", "3"},
                        {"0100", "4"},
                        {"0101", "5"},
                        {"0110", "6"},
                        {"0111", "7"},
                        {"1000", "8"},
                        {"1001", "9"},
                        {"1010", "A"},
                        {"1011", "B"},
                        {"1100", "C"},
                        {"1101", "D"},
                        {"1110", "E"},
                        {"1111", "F"},
        };
        String tempNumber = numberHexadecimal;
        for (int i = 0; i < table.length; i++) {
            tempNumber = tempNumber.replace(table[i][1], table[i][0]);
        }
        numberBinary = tempNumber;

    }

    private void numberInDecimal() {
        numberDecimal = 0;
        char[] arr = numberBinary.toCharArray();

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == '1') {
                numberDecimal += pow(2, arr.length - i - 1);
            }
        }
    }

    private int pow(int a, int b) {
        if (b == 0) return 1;
        if (b == 1) return a;
        int numb = a;
        for (int i = 1; i < b; i++) {
            numb *= a;
        }
        return numb;
    }

    private void numberInHexadecimal() {
        numberHexadecimal = "";
        String[][] table = {
                        {"0000", "0"},
                        {"0001", "1"},
                        {"0010", "2"},
                        {"0011", "3"},
                        {"0100", "4"},
                        {"0101", "5"},
                        {"0110", "6"},
                        {"0111", "7"},
                        {"1000", "8"},
                        {"1001", "9"},
                        {"1010", "A"},
                        {"1011", "B"},
                        {"1100", "C"},
                        {"1101", "D"},
                        {"1110", "E"},
                        {"1111", "F"},
        };
        String tempNumber2 = numberBinary;
        while (tempNumber2.length() % 4 != 0) {
            tempNumber2 = "0" + tempNumber2;
        }
        char[] arr = tempNumber2.toCharArray();
        for (int i = 0; i < tempNumber2.length(); i += 4) {
            String temp = arr[i] + "" + arr[i + 1] + "" + arr[i + 2] + "" + arr[i + 3] + "";
            for (int j = 0; j < 16; j++) {
                if (temp.equals(table[j][0])) {
                    numberHexadecimal += table[j][1];
                }
            }
        }
    }

    private void numberInOctal() {
        numberOctal = "";
        String[][] table = {
                        {"000", "0"},
                        {"001", "1"},
                        {"010", "2"},
                        {"011", "3"},
                        {"100", "4"},
                        {"101", "5"},
                        {"110", "6"},
                        {"111", "7"},
        };
        String tempNumber2 = numberBinary;
        while (tempNumber2.length() % 3 != 0) {
            tempNumber2 = "0" + tempNumber2;
        }
        char[] arr = tempNumber2.toCharArray();
        for (int i = 0; i < tempNumber2.length(); i += 3) {
            String temp = arr[i] + "" + arr[i + 1] + "" + arr[i + 2] + "";
            for (int j = 0; j < 8; j++) {
                if (temp.equals(table[j][0])) {
                    numberOctal += table[j][1];
                }
            }
        }
    }

    private void numberInBinaryFDecimal() {
        if (numberDecimal == 0) {
            numberBinary = "0";
            return;
        }
        int a = numberDecimal;
        numberBinary = "";
        while (a > 1) {
            numberBinary = (a % 2) + "" + numberBinary;
            a /= 2;
        }
        numberBinary = 1 + numberBinary;
    }

    public String toString() {
        return "\n" +
                " 10 - " + numberDecimal + "\n" +
                " 2 - " + numberBinary + "\n" +
                " 8 - " + numberOctal + "\n" +
                " 16 - " + numberHexadecimal + "\n";
    }

    public void add(Number number) {
        char[] num1 = number.numberBinary.toCharArray();
        char[] num2 = numberBinary.toCharArray();
        char[] result;
        if (num1.length > num2.length) {
            result = new char[num1.length + 1];
        } else {
            result = new char[num2.length + 1];
        }
        for (int i = 0; i < num1.length; i++) {
            if (num1[num1.length - 1 - i] == '1') {
                addDischarge(result, result.length - 1 - i);
            }
        }
        for (int i = 0; i < num2.length; i++) {
            if (num2[num2.length - 1 - i] == '1') {
                addDischarge(result, result.length - 1 - i);
            }
        }
        numberBinary = "";
        Boolean flagStart = true;
        for (int i = 0; i < result.length; i++) {
            if ((result[i] == '\0' && flagStart)) {

            } else if (result[i] == '1') {
                flagStart = false;
                numberBinary += result[i];
            } else if (result[i] == '\0' || result[i] == '0') {
                numberBinary += 0 + "";
            }
        }
        numberInOctal();
        numberInDecimal();
        numberInHexadecimal();
    }

    private void addDischarge(char[] number, int Discharge) {
        if (number[Discharge] == '1') {
            number[Discharge] = '0';
            addDischarge(number, Discharge - 1);
        } else {
            number[Discharge] = '1';
        }
    }


    private void borrowDischarge(char[] number, int Discharge) {
        if (number[Discharge] == '1') {
            number[Discharge] = '0';
            number[Discharge + 1] = '1';
            return;
        } else {
            borrowDischarge(number, Discharge - 1);
            number[Discharge] = '1';
        }
    }

    public void sub(Number number) {
        char[] num1 = numberBinary.toCharArray();
        char[] num2 = number.numberBinary.toCharArray();
        for (int i = 0; i < num2.length; i++) {
            if (num1[num1.length - 1 - i] == '0' && num2[num2.length - 1 - i] == '0') {
                num1[num1.length - 1 - i] = '0';
            } else if (num1[num1.length - 1 - i] == '0' && num2[num2.length - 1 - i] == '1') {
                borrowDischarge(num1, num1.length - 1 - i);
                num1[num1.length - 1 - i] = '1';
            } else if (num1[num1.length - 1 - i] == '1' && num2[num2.length - 1 - i] == '1') {
                num1[num1.length - 1 - i] = '0';
            }
        }
        numberBinary = "";
        int j = 0;
        while (true) {
            if (j == num1.length) {
                numberBinary = "0";
                break;
            }
            if (num1[j] != '0') {
                for (; j < num1.length; j++) {
                    numberBinary += num1[j];
                }
                break;
            }
            j++;
        }
        numberInOctal();
        numberInDecimal();
        numberInHexadecimal();
    }

    public void multiply(Number number) {
        Number numberTemp = new Number(this.numberDecimal + "", 10);
        for (; !number.numberBinary.equals("1"); number.sub(new Number(1 + "", 10))) {
            this.add(numberTemp);
        }
    }

    public Boolean comparisonNumberBinary(Number number) {
        if (this.numberBinary.length() > number.numberBinary.length())
            return true;
        if (this.numberBinary.length() == number.numberBinary.length()) {
            char[] num1 = numberBinary.toCharArray(), num2 = number.numberBinary.toCharArray();
            for (int i = 0; i < num1.length; i++) {
                if (num1[i] != num2[i]) {
                    break;
                }
            }
            return true;
        }
        return false;
    }

    public void divide(Number number) {
        Number Count = new Number("0", 10);
        while (comparisonNumberBinary(number)) {
            sub(number);
            Count.add(new Number("1", 10));
        }
        numberBinary = Count.numberBinary;
        numberInOctal();
        numberInDecimal();
        numberInHexadecimal();
    }
}
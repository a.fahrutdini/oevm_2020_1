import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Введите исходную систему счисления: ");
        Scanner system = new Scanner(System.in);
        int SystemSs = system.nextInt();

        System.out.print("Введите операцию: ");
        Scanner operations = new Scanner(System.in);
        String operation = operations.nextLine();

        System.out.print("Введите первое число: ");
        Scanner firstNums = new Scanner(System.in);
        String firstNum = firstNums.nextLine();

        System.out.print("Введите второе число: ");
        Scanner secondNums = new Scanner(System.in);
        String secondNum = secondNums.nextLine();

        char[] firstNumberChar = firstNum.toCharArray();
        char[] secondNumberChar = secondNum.toCharArray();
        char[] operationChar = operation.toCharArray();
        int firstNumberInt=0;
        int secondNumberInt=0;
        String firstNumberBinary = "";
        String secondNumberBinary = "";
        String result = "";

        firstNumberChar = ClassSupport.toUpper(firstNum, firstNumberChar);
        firstNumberInt = ClassSupport.convertToTen(firstNum, firstNumberChar, firstNumberInt, SystemSs);

        secondNumberChar = ClassSupport.toUpper(secondNum, secondNumberChar);
        secondNumberInt = ClassSupport.convertToTen(secondNum, secondNumberChar, secondNumberInt, SystemSs);

        firstNumberBinary = ClassSupport.convertToBinary(firstNumberInt, firstNumberBinary);
        secondNumberBinary = ClassSupport.convertToBinary(secondNumberInt, secondNumberBinary);

        try {
            result = ClassSupport.operations(operationChar, firstNumberBinary, secondNumberBinary, result, firstNumberInt, secondNumberInt);
            System.out.print(firstNum + " " + operation + " " + secondNum + " = " + result);
        } catch (Exception e) {
            System.out.println("Выберите другую операцию");
        }
    }
}

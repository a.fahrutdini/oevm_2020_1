package com.company;
import java.awt.*;
import java.lang.StringBuilder;



public class Helper {

    /*
    функция для перевода в двоичную систему
     */
    public static String convert(int finalNumber, String notFinalResult) {
        while(((finalNumber / 2 != 0)) || (finalNumber % 2 != 0)) {
            notFinalResult += finalNumber % 2;
            finalNumber = finalNumber / 2;
        }
        notFinalResult = new StringBuffer(notFinalResult).reverse().toString();
        while(notFinalResult.length() < new Variables().getPropertyValue("widthArray")){
            notFinalResult = "0" + notFinalResult;
        }
        return notFinalResult;
    }

    public static int[][] outputOnDisplay(int[][] arrayOfNumbers) {
        String line = "";
        char oneSymbolOfLine = ' ';
        int randNumber = 0;

        for (int i = 0; i <= new Variables().getPropertyValue("heightArray"); i++) {
            line = "";
            line = convert(i, line);
            for (int j = 0; j < new Variables().getPropertyValue("widthArray"); j++) {
                oneSymbolOfLine = line.charAt(j);
                int n = Character.getNumericValue(oneSymbolOfLine);
                arrayOfNumbers[i][j] = n;
            }
            randNumber = (int)(Math.random()*2);
            arrayOfNumbers[i][new Variables().getPropertyValue("widthArray")] = randNumber;
        }
        return arrayOfNumbers;
    }

    /*
        функция для расчёта по формуле КНФ
    */
    public static String knfСalculations(int[][] arrayOfNumbers) {
        // знак первого икса
        char signX1 = ' ';
        // знак второго икса
        char signX2 = ' ';
        // знак третьего икса
        char signX3 = ' ';
        // знак четвертого икса
        char signX4 = ' ';
        // проверкан на первую итерацию
        boolean firstIteration = true;

        StringBuilder strBuilder = new StringBuilder();

        for (int i = 0; i <= new Variables().getPropertyValue("heightArray"); i++) {
            if (arrayOfNumbers[i][new Variables().getPropertyValue("widthArray")] == 0) {
                if (arrayOfNumbers[i][new Variables().getPropertyValue("firstIndex")] == 1) {
                    signX1 = '-';
                }
                if (arrayOfNumbers[i][new Variables().getPropertyValue("secondIndex")] == 1) {
                    signX2 = '-';
                }
                if (arrayOfNumbers[i][new Variables().getPropertyValue("thirdIndex")] == 1) {
                    signX3 = '-';
                }
                if (arrayOfNumbers[i][new Variables().getPropertyValue("fourthIndex")] == 1) {
                    signX4 = '-';
                }
                if (firstIteration) {
                    strBuilder.append("( " + signX1 + "X1 + " + signX2 + "X2 + " + signX3 + "X3 + " + signX4 + "X4 )\n");
                    firstIteration = false;
                } else {
                    strBuilder.append(" * ( " + signX1 + "X1 + " + signX2 + "X2 + " + signX3 + "X3 + " + signX4 + "X4 )\n");
                }
            }
            signX1 = ' ';
            signX2 = ' ';
            signX3 = ' ';
            signX4 = ' ';
        }
        return strBuilder.toString();
    }

    /*
    * формула для расчётав по формуле ДНФ
    * */

    public static String dnfСalculations(int[][] arrayOfNumbers) {
        // знак первого икса
        char signX1 = ' ';
        // знак второго икса
        char signX2 = ' ';
        // знак третьего икса
        char signX3 = ' ';
        // знак четвёртого икса
        char signX4 = ' ';
        // проверкан на первую итерацию
        boolean firstIteration = true;
        StringBuilder strBuilder = new StringBuilder();

        for (int i = 0; i <= new Variables().getPropertyValue("heightArray"); i++) {
            if (arrayOfNumbers[i][new Variables().getPropertyValue("widthArray")] == 1) {  // проверка последней цифры в строке
                if (arrayOfNumbers[i][new Variables().getPropertyValue("firstIndex")] == 0) { // проверка на знак первого икса
                    signX1 = '-';
                }
                if (arrayOfNumbers[i][new Variables().getPropertyValue("secondIndex")] == 0) { // проверка на знак второго икса
                    signX2 = '-';
                }
                if (arrayOfNumbers[i][new Variables().getPropertyValue("thirdIndex")] == 0) { // проверка на знак третьего икса
                    signX3 = '-';
                }
                if (arrayOfNumbers[i][new Variables().getPropertyValue("fourthIndex")] == 0) { // проверка на знак четвёртого икса
                    signX4 = '-';
                }
                if (firstIteration) { // проверка на первую итерацию
                    strBuilder.append("( " + signX1 + "X1 * " + signX2 + "X2 * " + signX3 + "X3 * " + signX4 + "X4 )\n");
                    firstIteration = false;
                } else {
                    strBuilder.append(" + ( " + signX1 + "X1 * " + signX2 + "X2 * " + signX3 + "X3 * " + signX4 + "X4 )\n");
                }
            }
            signX1 = ' ';
            signX2 = ' ';
            signX3 = ' ';
            signX4 = ' ';
        }
        return strBuilder.toString();
    }

    /*
    *  функция для отрисовки массива
    * */

    public static void draw(int arrayOfNumbers[][], Graphics g) {
        int Y = 10;
        int width = 30;
        int height = 30;

        for (int i = 0; i < new Variables().getPropertyValue("widthArray"); i++) {
            g.drawRect(new Variables().getPropertyValue("startX") + width * i, Y, width, height);
            g.drawString("X" + (i + 1), new Variables().getPropertyValue("startX") * 2 + width * i, Y * 3);
        }

        g.drawRect(new Variables().getPropertyValue("startX") + width * (new Variables().getPropertyValue("widthArray")), Y, width, height);
        g.drawString("F", new Variables().getPropertyValue("startX") + 10 + width * (new Variables().getPropertyValue("widthArray")), Y + 20);

        Y += 30;

        for (int i = 0; i <= new Variables().getPropertyValue("heightArray"); i++) {
            for (int j = 0; j <= new Variables().getPropertyValue("widthArray"); j++) {
                g.drawRect(new Variables().getPropertyValue("startX") + width * j, Y + height * i, width, height);
                g.drawString(arrayOfNumbers[i][j] + "", new Variables().getPropertyValue("startX") + 10 + width * j, Y + 20 + height * i);
            }
        }

        if (Main.knf) {
            for (int i = 0; i <= new Variables().getPropertyValue("heightArray"); i++) {
                if (arrayOfNumbers[i][new Variables().getPropertyValue("widthArray")] == 0) {
                    g.setColor(Color.red);
                    for (int j = 0; j <= new Variables().getPropertyValue("widthArray"); j++) {
                        g.drawRect(new Variables().getPropertyValue("startX") + width * j, Y + height * i, width, height);
                    }
                }
            }
        } else if (Main.dnf) {
            for (int i = 0; i <= new Variables().getPropertyValue("heightArray"); i++) {
                if (arrayOfNumbers[i][new Variables().getPropertyValue("widthArray")] == 1) {
                    g.setColor(Color.red);
                    for (int j = 0; j <= new Variables().getPropertyValue("widthArray"); j++) {
                        g.drawRect(new Variables().getPropertyValue("startX") + width * j, Y + height * i, width, height);
                    }
                }
            }
        }
    }
}

package com.company;

import java.util.Scanner;

public class Main {
    static int scale;
    static String firstNumber;
    static String secondNumber;
    static String operation;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        if(input()) {

        }
        else {
            return;
        }

        convertNumber();
    }

    public static boolean input() {

        System.out.println("Input scale of notation:");

        scale = scanner.nextInt();

        System.out.println("Input the first number:");
        firstNumber = scanner.next();

        System.out.println("Input the second number:");
        secondNumber = scanner.next();

        System.out.println("Input the operation(+ - * /):");
        operation = scanner.next();
        if(operation.equals("+")  || operation.equals("-") || operation.equals("*") || operation.equals("/")){

        }
        else {
            System.out.println("You entered wrong operation");
            System.out.println("Don't do this anymore, please");
            return false;
        }
        return true;
    }

    public static void convertNumber() {
        Convertor convertFirstNumber = new Convertor(scale, firstNumber);
        Convertor convertSecondNumber = new Convertor(scale, secondNumber);
        int firstDecimalNumber = convertFirstNumber.convertToDecimal();
        int secondDecimalNumber = convertSecondNumber.convertToDecimal();

        if (firstDecimalNumber == -1 || secondDecimalNumber == -1) {
            System.out.println("Wrong");
            return;
        }

        String firstBinaryNumber = convertFirstNumber.convertToBinaryScale(firstDecimalNumber).toString();
        String secondBinaryNumber = convertSecondNumber.convertToBinaryScale(secondDecimalNumber).toString();


        OperationClass operationClass = new OperationClass(firstBinaryNumber,secondBinaryNumber);

        if(operation.equals("+") ) {
            System.out.print("A + B = ");
            System.out.println(operationClass.add());
        }
        else if(operation.equals("-")) {
            System.out.print("A - B = ");
            System.out.println(operationClass.deduction());
        }
        else if(operation.equals("*")){
            System.out.print("A * B = ");
            System.out.println(operationClass.multiply());
        }
        else {
            System.out.print("A / B = ");
            System.out.println(operationClass.divide());
        }
    }

}

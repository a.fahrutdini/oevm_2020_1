import java.util.Random;

public class TruthTable {

    private final int[][] truthTable;

    private int[][] arrayInit() {
        Random random = new Random();
        return new int[][]{
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {0, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)}
        };
    }

    public TruthTable() {
        truthTable = arrayInit();
    }

    public int[][] getTruthTable() {
        return truthTable;
    }
}

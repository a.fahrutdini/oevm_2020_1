public class AnimatedString {

    private String string;
    private int posY;
    private int limitY;

    public AnimatedString(String string, int posY, int limitY) {
        this.string = string;
        this.posY = posY;
        this.limitY = limitY;
    }

    public AnimatedString(String string, int posY) {
        this.string = string;
        this.posY = posY;
        this.limitY = posY;
    }

    public String getString() {
        return string;
    }

    public void addPlusToDNF() {
        string += " +";
    }

    public void addMultiplyToKNF() {
        string += " *";
    }

    public int getPosY() {
        return posY;
    }

    public int getLimitY() {
        return limitY;
    }

    public void setLimitY(int limitY) {
        this.limitY = limitY;
    }

    public void increaseY() {
        posY += 5;
    }

    public void decreaseY() {
        posY -= 5;
    }
}

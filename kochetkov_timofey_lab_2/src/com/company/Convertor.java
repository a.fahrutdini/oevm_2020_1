package com.company;

public class Convertor {
    private int firstScale = 0;
    private int secondScale = 0;
    private String number = "";

    public Convertor(int first, int second, String num) {
        firstScale = first;
        secondScale = second;
        number = num;
    }

    public int convertToDecimal() {
        number = new StringBuffer(number).reverse().toString(); // переворачиваем строку для удобного понимания
        char[] numberInCharArray = number.toCharArray();
        int numberInDecimal = 0;
        int degree = 0;
        int numberBetweenConversion = 0;
        int maxSizeOfScale = 16;
        int minSizeOfScale = 2;

        if (firstScale < minSizeOfScale || firstScale > maxSizeOfScale || secondScale < minSizeOfScale || secondScale > maxSizeOfScale) {
            System.out.println("You entered the wrong scale of notation!");
            System.out.println("Don't do this anymore, please");
            return -1;
        }
        if (number == "") {
            System.out.println("You don't entered the number!");
            System.out.println("Don't do this anymore, please");
            return -1;
        }

        for (int i = 0; i < numberInCharArray.length; i++) {
            if (numberInCharArray[i] >= '0' && numberInCharArray[i] <= '9') {
                numberBetweenConversion = (int) (numberInCharArray[i] - 48);
            } else if (numberInCharArray[i] >= 'A' && numberInCharArray[i] <= 'F' && numberInCharArray[i] - 64 + 10 <= firstScale) {
                numberBetweenConversion = numberInCharArray[i] - 64 + 10;
            } else {
                System.out.println("You entered the wrong digit!");
                System.out.println("Don't do this anymore, please");
                return -1;
            }

            numberInDecimal += numberBetweenConversion * (int) Math.pow(firstScale, degree);
            degree++;
        }

        return numberInDecimal;
    }

    public StringBuilder convertToSecondScale(int numberInDecimal) {
        StringBuilder stringBuilder = new StringBuilder();

        if (numberInDecimal == 0) {
            return stringBuilder.append(0);
        }

        while (numberInDecimal != 0) {
            int step = numberInDecimal % secondScale;
            if (step >= 10) {
                stringBuilder.append((char) (step + 65 - 10));
            } else {
                stringBuilder.append(step);
            }
            numberInDecimal /= secondScale;
        }
        stringBuilder.reverse();
        return stringBuilder;
    }

}

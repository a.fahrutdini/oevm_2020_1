package com.company;

import java.io.FileReader;
import java.io.IOException;

public class Pascal {
    public String read(){
        StringBuilder string = new StringBuilder();

        try(FileReader reader = new FileReader("pascalProgram.pas"))
        {
            int ch;
            while((ch = reader.read()) != - 1){

                string.append((char) ch);
            }
        }

        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        return string.toString().replaceAll(";", ";\n");
    }

}

package EVM_Lab_3;

import java.util.Scanner;

public class UserInput {

    public static String getCorrectNum(byte not) {
        String input = "";
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите число в исходной системе счисления: ");
        input = scan.nextLine();
        input = input.trim();
        input = input.toLowerCase();
        while (!isCorrectNum(not, input)) {
            System.out.print("Повторите ввод: ");
            input = scan.nextLine();
            input = input.trim();
            input = input.toLowerCase();
        }
        return input;
    }

    private static boolean isCorrectNum(byte not, String num) {
        char[] numArr = num.toCharArray();
        int currentNum = 0;
        for (int i = 0; i < numArr.length; i++) {
            if (Character.isDigit(numArr[i])) {
                currentNum = (int) (numArr[i]) - 48;
            } else {
                if (numArr[i] > 96 && numArr[i] < 103) {
                    currentNum = (int) numArr[i] - 87;
                } else return false;
            }
            if (currentNum >= not) {
                return false;
            }
        }
        return true;
    }

    public static String getCorrectNotation(boolean isFirstInput) {
        String input = "";
        Scanner scan = new Scanner(System.in);
        if (isFirstInput)
            System.out.print("Ведите исходную систему счисления(2-16): ");
        else
            System.out.print("Ведите конечную систему счисления(2-16): ");
        input = scan.nextLine();
        input = input.trim();
        while (!isCorrectNotation(input)) {
            System.out.print("Повторите ввод: ");
            input = scan.nextLine();
            input = input.trim();
        }
        return input;
    }

    private static boolean isCorrectNotation(String input) {
        char[] inputArr = input.toCharArray();
        if (inputArr.length > 2 || inputArr.length < 1) {
            return false;
        }
        for (int i = 0; i < inputArr.length; i++) {
            if (!Character.isDigit(inputArr[i])) {
                return false;
            }
        }
        if (Integer.parseInt(input) > 16 || Integer.parseInt(input) < 2) {
            return false;
        }
        return true;
    }

    private static boolean isCorrectOperation(String input) {
        if (input.length() > 1 || input.length() < 1) {
            return false;
        }
        if (input.equals("+") || input.equals("*") || input.equals("-") || input.equals("/")) {
            return true;
        }
        return false;
    }

    public static String getCorrectOperation() {
        String input = "";
        Scanner scan = new Scanner(System.in);
        System.out.print("Укажите операцию(+ - * /): ");
        input = scan.nextLine();
        input = input.trim();
        input = input.toLowerCase();
        while (!isCorrectOperation(input)) {
            System.out.print("Повторите ввод: ");
            input = scan.nextLine();
            input = input.trim();
        }
        return input;
    }

}

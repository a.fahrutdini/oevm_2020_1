package EVM_Lab_3;

public class Converter {

    public static long convertToDec(byte notOld, String num) {
        if (notOld == 10) {
            return Long.parseLong(num);
        }
        long result = 0;
        int specNum = 0;
        char[] numArr = num.toCharArray();
        for (int i = numArr.length - 1, k = 0; i >= 0; i--, k++) {
            if (Character.isDigit(numArr[i])) {
                specNum = 48;
            } else {
                specNum = 87;
            }
            result += (Math.pow(notOld, k) * (numArr[i] - specNum));
        }
        return result;
    }


    public static String convertToNewNotation(byte notOld, byte newNot, String stringNum) {
        if (stringNum.equals("0")) {
            return "0";
        }
        long num = convertToDec(notOld, stringNum);
        StringBuilder result = new StringBuilder("");
        while (num != 0) {
            if (num % newNot > 9) {
                result.append((char) (num % newNot + 87));
            } else {
                result.append(num % newNot);
            }
            num /= newNot;
        }
        char[] resultStr = result.toString().toCharArray();
        for (int i = 0, k = resultStr.length - 1; k > i; i++, k--) {
            char temp = resultStr[i];
            resultStr[i] = resultStr[k];
            resultStr[k] = temp;
        }
        return new String(resultStr);
    }

}

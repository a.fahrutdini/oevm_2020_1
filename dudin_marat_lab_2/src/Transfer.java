public class Transfer {
    private int startSS = 0;
    private int endSS = 0;
    private String numStartString = "";

    public Transfer(int startSS, int endSS, String numStartString) {
        this.startSS = startSS;
        this.endSS = endSS;
        this.numStartString = numStartString;
    }

    private int numInTenSs(char[] arrChar) {
        int cash = 0;
        int degree = 0;
        int numInTenSs = 0;
        int minSS = 2;
        int maxSS = 16;
        int assistan = 10;

        if(startSS < minSS || startSS > maxSS || endSS < minSS || endSS > maxSS) {
            return -1;
        }

        for(int i = arrChar.length - 1; i >= 0; i--) {
            if(arrChar[i] >= '0' && arrChar[i] <= '9') {
                cash = Character.getNumericValue(arrChar[i]);
            } else {
                if(arrChar[i] >= 'A' && arrChar[i] <= 'F') {
                    cash = arrChar[i] - 'A' + assistan;
                } else {
                    return -1;
                }
            }

            if(cash >= startSS) {
                return -1;
            }

            numInTenSs += cash * Math.pow(startSS, degree);
            degree++;
        }

        return numInTenSs;
    }

    private StringBuilder transferNumInEndSS(int numInTenSs) {
        int assistan = 10;
        int assistan2 = 9;
        StringBuilder endNumStr = new StringBuilder();

        if(numInTenSs == 0) {
            endNumStr.append(0);
            return endNumStr;
        }

        while(numInTenSs != 0) {
            if(numInTenSs % endSS > assistan2) {
                endNumStr.append((char)(numInTenSs - assistan + 'A'));
            } else {
                endNumStr.append(numInTenSs % endSS);
            }
            numInTenSs = numInTenSs / endSS;
        }

        endNumStr.reverse();
        return endNumStr;
    }

    public void transfer() {
        char[] arrChar = numStartString.toCharArray();
        int numInTenSs = 0;
        StringBuilder endNumStr;

        numInTenSs = numInTenSs(arrChar);

        if(numInTenSs == -1) {
            System.out.println("Ошибка");
            return;
        }

        endNumStr = transferNumInEndSS(numInTenSs);
        System.out.println(endNumStr);
    }



}
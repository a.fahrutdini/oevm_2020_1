package com.company;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadAndWrite {

    /**
     * Чтение файла
     *
     * @param fileName путь к файлу
     */
    public String readFileAsString(String fileName) throws Exception {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data.replaceAll(";", ";\n");
    }

    /**
     * Запись файла
     *
     * @param path путь к файлу
     * @param a строка для записи
     */
    public File writeFile(String path, String a) throws IOException {
        String newString = a;
        File file = new File(path);
        FileWriter fw = null;
        try {
            fw = new FileWriter(file);
            fw.write(newString);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fw.close();
        return file;
    }
}

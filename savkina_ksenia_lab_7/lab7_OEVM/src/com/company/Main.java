package com.company;

import java.io.File;

public class Main {

    public static void main(String[] args) throws Exception {
        ReadAndWrite readAndWrite = new ReadAndWrite();
        Converting converting = new Converting();
        File file = readAndWrite.writeFile("files/lab7_OEVM.ASM",converting.convert(readAndWrite.readFileAsString("files/lab7_OEVM.pas")));
        System.out.println("Файл создан. Путь к файлу:" + file.getAbsolutePath());
    }
}

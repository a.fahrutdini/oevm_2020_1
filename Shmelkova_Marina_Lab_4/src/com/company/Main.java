package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);//Ввод числа с клавиатуры
        System.out.println("Выберите необходимую операцию: \n 1. КНФ \n 2. ДНФ");

        int operation = scanner.nextInt();
        int sizeColumn = 5;
        int sizeStr = 16;
        int[][] array = {
                {1, 0, 0, 0, (int) (Math.random() * 2)},
                {0, 1, 0, 1, (int) (Math.random() * 2)},
                {0, 0, 0, 0, (int) (Math.random() * 2)},
                {0, 0, 0, 1, (int) (Math.random() * 2)},
                {0, 1, 1, 1, (int) (Math.random() * 2)},
                {0, 0, 0, 0, (int) (Math.random() * 2)},
                {1, 1, 1, 1, (int) (Math.random() * 2)},
                {0, 1, 0, 1, (int) (Math.random() * 2)},
                {1, 0, 1, 0, (int) (Math.random() * 2)},
                {1, 0, 1, 1, (int) (Math.random() * 2)},
                {1, 0, 1, 0, (int) (Math.random() * 2)},
                {1, 0, 0, 1, (int) (Math.random() * 2)},
                {1, 1, 1, 0, (int) (Math.random() * 2)},
                {1, 1, 0, 1, (int) (Math.random() * 2)},
                {0, 0, 0, 0, (int) (Math.random() * 2)},
                {1, 1, 0, 1, (int) (Math.random() * 2)},
        };

        if(operation == 1){
            HelperClass.function(array, sizeColumn, sizeStr);
            System.out.println(HelperClass.functionKNF(array, sizeColumn, sizeStr));
        }
        if(operation == 2){
            HelperClass.function(array, sizeColumn, sizeStr);
            System.out.println(HelperClass.functionDNF(array, sizeColumn, sizeStr));
        }
    }
}

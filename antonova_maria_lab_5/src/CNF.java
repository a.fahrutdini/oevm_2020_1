package com.company;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;

public class CNF {
    private int [][] _truthTable;
    private int _tLength;
    private int _tWidth;

    public CNF (TruthTable table) {
        _truthTable = table._truthTable;
        _tLength = table._tLength;
        _tWidth = table._tWidth;
    }

    public String [] formResult (){
        String strs [] = new String [_tLength];
        String res1;

        for (int i = 0; i < _tLength; i++) {
            res1 = "";
            if (_truthTable[i][_tWidth-1] == 0)
            {
                res1 += "(";
                for (int j = 0; j < _tWidth-1; j++)
                {
                    if(_truthTable[i][j] == 0) {
                        res1 += "-x";
                        res1 += j;
                    }
                    else {
                        res1 += "x";
                        res1 += j;
                    }
                    if (j < _tWidth-2)
                    res1 += "+";
                }
                res1 += ")";
                strs[i] = res1;
            }
        }
        return strs;
    }
}

public class KNFminimization implements IMinimization {
    private int[][] tableTruth;
    private StringBuilder result;

    public KNFminimization() {
        result = new StringBuilder();
    }

    @Override
    public void setTableTruth(int[][] tableTruth) {
        this.tableTruth = tableTruth;
    }

    @Override
    public void startMinimizing() {
        for (int i = 0; i < tableTruth.length; i++) {
            if(tableTruth[i][tableTruth[i].length-1]==0){
                if (result.length() == 0) {
                    result.append("( ");
                } else {
                    result.append("* ( ");
                }
                for (int j = 0; j < tableTruth[i].length - 1; j++) {
                    if (j != 0) {
                        result.append(" + ");
                    }
                    if (tableTruth[i][j] == 0) {
                        result.append("X").append(j+1);
                    }else{
                        result.append("-X").append(j+1);
                    }
                }
                if(result.length()!=0)
                    result.append(" )\n");
            }
        }
    }

    @Override
    public StringBuilder getResult() {
        return this.result;
    }
}

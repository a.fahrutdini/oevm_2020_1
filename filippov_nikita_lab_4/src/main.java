import java.util.InputMismatchException;
import java.util.Scanner;

public class main {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String choice;
        System.out.println("Введите логическую операцию");
        try {
            choice = in.next();
        } catch (InputMismatchException e) {
            System.out.println("Ошибка ввода");
            return;
        }

        IMinimization Minimization;
        if(LogicalOperations.getOperations(choice)!=null) {
            switch (LogicalOperations.getOperations(choice)) {
                case DNF: {
                    Minimization = new DNFminimization();
                    break;
                }
                case KNF: {
                    Minimization = new KNFminimization();
                    break;
                }
                default: return;
            }
        }else{
            return;
        }

        TruthTable truthTable = new TruthTable();
        Minimization.setTableTruth(truthTable.getTruthTable());
        Minimization.startMinimizing();
        System.out.printf(truthTable.toString());
        System.out.printf(Minimization.getResult().toString());
    }
}

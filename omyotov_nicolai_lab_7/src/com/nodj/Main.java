package com.nodj;

public class Main {

    public static void main(String[] args) {
        PascalParser pascalParser = new PascalParser("pascalProgram.pas");
        AssemblerMaker assemblerMaker = new AssemblerMaker(pascalParser);
    }
}

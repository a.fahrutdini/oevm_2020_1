format PE console
entry start
include 'win32a.inc'
section '.data' data readable writable
	keeperInputString db '%d', 0
	keeperResult db '%d', 0ah, 0
	x dd ?
	y dd ?
	res1 dd ?
	res2 dd ?
	res3 dd ?
	res4 dd ?
	str1 db 'input x: ', 0
	str3 db 'input y: ', 0
	str6 db 'x + y = ', 0
	str9 db 'x - y = ', 0
	str12 db 'x * y = ', 0
	str15 db 'x / y = ', 0
section '.code' code readable executable
	 start:
		 push str1
		 call [printf]
		 push x
		 push keeperInputString
		 call [scanf]

		 push str3
		 call [printf]
		 push y
		 push keeperInputString
		 call [scanf]

		 mov ecx, [x]
		 add ecx, [y]
		 mov [res1], ecx
		 push str6
		 call [printf]
		 push [res1]
		 push keeperResult
		 call [printf]

		 mov ecx, [x]
		 sub ecx, [y]
		 mov [res2], ecx
		 push str9
		 call [printf]
		 push [res2]
		 push keeperResult
		 call [printf]

		 mov ecx, [x]
		 imul ecx, [y]
		 mov [res3], ecx
		 push str12
		 call [printf]
		 push [res3]
		 push keeperResult
		 call [printf]

		 mov eax, [x]
		 mov ecx, [y]
		 div ecx
		 mov [res4], eax
		 push str15
		 call [printf]
		 push [res4]
		 push keeperResult
		 call [printf]

	 finish:

		 call [getch]

		 call [ExitProcess]

section '.idata' import data readable

	 library kernel, 'kernel32.dll',\
	 msvcrt, 'msvcrt.dll'

	 import kernel,\
	 ExitProcess, 'ExitProcess'

	 import msvcrt,\
	 printf, 'printf',\
	 scanf, 'scanf',\
	 getch, '_getch'
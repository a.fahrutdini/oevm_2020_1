###### ПИбд-21 Омётов Николай
# Лабораторная работа №7
### Изучение методов трансляции

###### Как запустить лабораторную работу
1. Открыть папку в IntelliJ IDEA как проект
2. Запустить программу

###### Использованные технологии
- flat assembler
- IntelliJ IDEA

###### Что делает программа?
Результатом работы транслятора является программа для ассемблера FASM, идентичная по функционалу исходной программе. Полученная программа должна компилироваться и выполняться без ошибок.
Исходная программа на языке программирования Pascal имеет вид:
```
var
x, y: integer;
res1, res2, res3, res4: integer;
begin
write(‘input x: ’); readln(x);
write(‘input y: ’); readln(y);
res1 := x + y; write(‘x + y = ’); writeln(res1);
res2 := x - y; write(‘x - y = ’); writeln(res2);
res3 := x * y; write(‘x * y = ’); writeln(res3);
res4 := x / y; write(‘x / y = ’); writeln(res4);
end.
```

##### Тесты

```
format PE console
entry start
include 'win32a.inc'
section '.data' data readable writable
	keeperInputString db '%d', 0
	keeperResult db '%d', 0ah, 0
	x dd ?
	y dd ?
	res1 dd ?
	res2 dd ?
	res3 dd ?
	res4 dd ?
	str1 db 'input x: ', 0
	str3 db 'input y: ', 0
	str6 db 'x + y = ', 0
	str9 db 'x - y = ', 0
	str12 db 'x * y = ', 0
	str15 db 'x / y = ', 0
section '.code' code readable executable
	 start:
		 push str1
		 call [printf]
		 push x
		 push keeperInputString
		 call [scanf]

		 push str3
		 call [printf]
		 push y
		 push keeperInputString
		 call [scanf]

		 mov ecx, [x]
		 add ecx, [y]
		 mov [res1], ecx
		 push str6
		 call [printf]
		 push [res1]
		 push keeperResult
		 call [printf]

		 mov ecx, [x]
		 sub ecx, [y]
		 mov [res2], ecx
		 push str9
		 call [printf]
		 push [res2]
		 push keeperResult
		 call [printf]

		 mov ecx, [x]
		 imul ecx, [y]
		 mov [res3], ecx
		 push str12
		 call [printf]
		 push [res3]
		 push keeperResult
		 call [printf]

		 mov eax, [x]
		 mov ecx, [y]
		 div ecx
		 mov [res4], eax
		 push str15
		 call [printf]
		 push [res4]
		 push keeperResult
		 call [printf]

	 finish:

		 call [getch]

		 call [ExitProcess]

section '.idata' import data readable

	 library kernel, 'kernel32.dll',\
	 msvcrt, 'msvcrt.dll'

	 import kernel,\
	 ExitProcess, 'ExitProcess'

	 import msvcrt,\
	 printf, 'printf',\
	 scanf, 'scanf',\
	 getch, '_getch'
```

***Видео с примером работы программы:***
https://yadi.sk/i/N0aonvNJsks5Lw
## Лабораторная работа №5

### Студент группы ПИбд-21

### Шовкань Ян

### Ссылка на видео: https://drive.google.com/file/d/1eY0R-3fUeelYdiNON-2D4gD0fWmtAtDY/view?usp=sharing

##### Техническое задание:
Разработать программный продукт для визуализации работы
булевых функций. В качестве входных данных могут
выступают результаты работы программы, 
разработанной в рамках лабораторный работы №4.

##### Пример работы программы: 
* ДНФ:
![title](https://sun9-29.userapi.com/xxZNwXYcEdhWHIx4FDFzyTjruj3J9Dh_gtA63A/j4NmYUg09vg.jpg)
* КНФ:
![title](https://sun9-13.userapi.com/u8UXWcAsdtiZYZJ93sTfRmUDiMNmfl0w8v4fmQ/oS5j7RHHfd0.jpg)
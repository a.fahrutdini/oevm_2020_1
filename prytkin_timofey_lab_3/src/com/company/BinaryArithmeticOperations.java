package com.company;

import java.util.Arrays;

public class BinaryArithmeticOperations {

    /**
     * Этот метод вернет сумму двух двоичных чисел
     * @param firstTerm Первое слогаемое
     * @param secondTerm Второе слогаемое
     */
    public static String sum (char[] firstTerm, char[] secondTerm) {
        StringBuilder result = new StringBuilder();
        char currentChar;
        int inTheMind = 0;//То что переносится в слкдующий разряд при сложении (в уме)

        //Идем в цикле с первых разрядов (i и j) пока разряды не станут последними (i >= 0 или  j >= 0) или пока есть то, что у уме
        for (int i = firstTerm.length - 1 , j = secondTerm.length - 1; i >= 0 || j >= 0 || inTheMind == 1; --i, --j) {
            int firstBinaryDigit, secondBinaryDigit;

            //Узнаем первое двоичное число под i-ым разрядом
            if (i < 0) { firstBinaryDigit = 0; }
            else if (firstTerm[i] == '0') { firstBinaryDigit = 0; }
            else { firstBinaryDigit = 1; }

            //Узнаем второе двоичное число под j-ым разрядом
            if (j < 0) { secondBinaryDigit = 0; }
            else if (secondTerm[j] == '0') { secondBinaryDigit = 0; }
            else { secondBinaryDigit = 1; }

            //Вычисляем сумму
            int currentSum = firstBinaryDigit + secondBinaryDigit + inTheMind;

            //В зависимости от суммы определяем то что у уме и то что записывем в результат
            if (currentSum == 0) { currentChar = '0'; inTheMind = 0; }
            else if (currentSum == 1) { currentChar = '1'; inTheMind = 0; }
            else if (currentSum == 2) { currentChar = '0'; inTheMind = 1; }
            else { currentChar = '1'; inTheMind = 1; }//currentSum == 3

            result.insert(0, currentChar);
        }
        return result.toString();
    }

    /**
     * Этот метод вернет разность двух двоичных чисел
     * @param minuend Уменьшаемое
     * @param subtrahend Вычитаемое
     */
    public static String subtract(char[] minuend, char[] subtrahend) {
        boolean minus = false;

        if (Converter.convertToDecimal(2, minuend) < Converter.convertToDecimal(2, subtrahend)) {
            minus = true;
            char[] temp = subtrahend;
            subtrahend = minuend;
            minuend = temp;

        }
        else if (Converter.convertToDecimal(2, minuend) == Converter.convertToDecimal(2, subtrahend)) {return "0";}

        StringBuilder result = new StringBuilder();

        int differenceInDischarges = minuend.length - subtrahend.length;//Разница в разрядах
        char[] reverseForSubtrahend = new char[minuend.length];

        //Находим обратный код для вычитаемого
        for (int i = 0; i < reverseForSubtrahend.length; ++i) {
            if (i < differenceInDischarges) {reverseForSubtrahend[i] = '1'; }
            else {
                if (subtrahend[i - differenceInDischarges] == '0') { reverseForSubtrahend[i] = '1'; }
                else { reverseForSubtrahend[i] = '0'; }
            }
        }

        // дополнительный код = обратный код + 1
        char[] additionalCode = sum(reverseForSubtrahend, new char[] {'1'}).toCharArray();

        //Складываем дополнительный код и уменьшаемое
        char[] additionalSum = sum(additionalCode, minuend).toCharArray();

        //Флаг на наличие незначащих нулей
        boolean insignificantZeros = true;

        //Убираем последний разряд и все незначащие нули
        for (int i = 1; i < additionalSum.length; ++i) {
            if (additionalSum[i] == '1') { insignificantZeros = false; }
            else { if (insignificantZeros) { continue; } }
            result.append(additionalSum[i]);
        }

        //Если изначально вычитаемое было больше уменьшаемого ставим перед числом минус
        if (minus) { result.insert(0, '-');}

        return result.toString();
    }

    /**
     * Этот метод вернет произведение двух двоичных чисел
     * @param firstMultiplier Первый множитель
     * @param secondMultiplier Второй множитель
     */
    public static String multiply (char[] firstMultiplier, char[] secondMultiplier) {
        char[] result = {};
        //Произведение M на N - это суммирование M N раз
        for (int i = 0; i < Converter.convertToDecimal(2, secondMultiplier); ++i) {
            result = (sum(result, firstMultiplier)).toCharArray();
        }
        return new String(result);
    }

    /**
     * Этот метод вернет частное двух двоичных чисел
     * @param dividend делимое
     * @param divider делитель
     */
    public static String divide (char[] dividend, char[] divider) {
        long result = 0;
        //Деление M нв N это то, сколько раз N поместится в M
        while (Converter.convertToDecimal(2, dividend) >= Converter.convertToDecimal(2, divider) && !Arrays.equals(dividend, new char[]{'0'})) {
            dividend = (subtract(dividend, divider)).toCharArray();
            result++;
        }
        return Converter.convertToBinary(result);
    }
}

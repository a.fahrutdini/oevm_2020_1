package com.company;

import java.util.Arrays;

public class BinaryOperations {
    /*
     * Метод произведения двух бинарных чисел
     */
    public static String multiplicationOfBinaryNumbers(char[] firstBinaryDigit, char[] secondBinaryDigit) {
        char[] resultOfMultiplicationOfBinaryNumbers = {};
        for (int i = 0; i < ConvertingToNumberSystem.convertingToDecimalSystem(2, secondBinaryDigit); i++) {
            resultOfMultiplicationOfBinaryNumbers = (sumOfBinaryNumbers(resultOfMultiplicationOfBinaryNumbers, firstBinaryDigit).toCharArray());
        }
        return new String(resultOfMultiplicationOfBinaryNumbers);
    }

    /*
    Метод деления двух бинарных чисел
    */
    public static String divideOfBinaryNumbers(char[] helpOfDividend, char[] helpOfDivider) {
        int resultOfDivide = 0;
        while (ConvertingToNumberSystem.convertingToDecimalSystem(2, helpOfDividend) >= ConvertingToNumberSystem.convertingToDecimalSystem(2, helpOfDivider) && !Arrays.equals(helpOfDividend, new char[]{'0'})) {
            helpOfDividend = (differenceBetweenBinaryNumbers(helpOfDividend, helpOfDivider).toCharArray());
            resultOfDivide++;
        }
        return ConvertingToNumberSystem.convertingToBinarySystem(resultOfDivide);
    }

    /*
     * Метод вычитания бинарных чисел
     * В аргументах объявляю символьный массив вычитаемего и уменьшаемого, ну и флаг на проверку того, что первое число меньше второго
     */
    public static String differenceBetweenBinaryNumbers(char[] minuend, char[] subtrahend) {
        boolean minus = false;
        if (ConvertingToNumberSystem.convertingToDecimalSystem(2, minuend) < ConvertingToNumberSystem.convertingToDecimalSystem(2, subtrahend)) {
            minus = true;
            char[] tmp = subtrahend;
            subtrahend = minuend;
            minuend = tmp;
        } else if (ConvertingToNumberSystem.convertingToDecimalSystem(2, minuend) == ConvertingToNumberSystem.convertingToDecimalSystem(2, subtrahend)) {
            return "0";
        }

        StringBuilder resultDifferenceBetweenBinaryNumbers = new StringBuilder();
        int differenceDischarges = minuend.length - subtrahend.length;
        char[] reverseSubtrahend = new char[minuend.length];

        for (int i = 0; i < reverseSubtrahend.length; i++) {
            if (i < differenceDischarges) {
                reverseSubtrahend[i] = '1';
            } else if (subtrahend[i - differenceDischarges] == '0') {
                reverseSubtrahend[i] = '1';
            } else {
                reverseSubtrahend[i] = '0';
            }
        }
        //дополнительный код, который содержит в себе обратный код + 1
        char[] auxiliaryCode = sumOfBinaryNumbers(reverseSubtrahend, new char[]{'1'}).toCharArray();
        //сложение дополнительного кода и уменьшаемого
        char[] auxiliarySum = sumOfBinaryNumbers(auxiliaryCode, minuend).toCharArray();
        //проверка наличия незначащих нулей
        boolean helperFindZeros = true;

        for (int i = 1; i < auxiliarySum.length; i++) {
            if (auxiliarySum[i] == '1') {
                helperFindZeros = false;
            } else if (helperFindZeros) {
                continue;
            }
            resultDifferenceBetweenBinaryNumbers.append(auxiliarySum[i]);
        }
        return resultDifferenceBetweenBinaryNumbers.toString();
    }

    /*
     * Метод для сложения двоичных чисел
     * Содержит в себе переменную discharge, которая сохраняет остаток при сложении и переносит его в следующий разряд
     */
    public static String sumOfBinaryNumbers(char[] firstDigit, char[] secondDigit) {
        StringBuilder sb = new StringBuilder();
        char currentDigit;
        int discharge = 0;

        for (int i = firstDigit.length - 1, j = secondDigit.length - 1; i >= 0 || j >= 0 || discharge == 1; --i, --j) {

            int firstBinaryDigit;
            int secondBinaryDigit;

            if (i < 0) {
                firstBinaryDigit = 0;
            } else if (firstDigit[i] == '0') {
                firstBinaryDigit = 0;
            } else {
                firstBinaryDigit = 1;
            }

            if (j < 0) {
                secondBinaryDigit = 0;
            } else if (secondDigit[j] == '0') {
                secondBinaryDigit = 0;
            } else {
                secondBinaryDigit = 1;
            }
            //текущая сумма содержит в себе первое и второе бинарное число, а так же остаток при сложении первого и второго бинарных чисел
            int currentSum = firstBinaryDigit + secondBinaryDigit + discharge;

            if (currentSum == 0) {
                currentDigit = '0';
                discharge = 0;
            } else if (currentSum == 1) {
                currentDigit = '1';
                discharge = 0;
            } else if (currentSum == 2) {
                currentDigit = '0';
                discharge = 1;
            } else {
                currentDigit = '1';
                discharge = 1;
            }
            sb.insert(0, currentDigit);
        }
        return sb.toString();
    }
}

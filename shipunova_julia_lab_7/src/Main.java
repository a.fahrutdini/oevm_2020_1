public class Main {

    public static void main(String[] args) {
        CommandStorage commandStorage = new CommandStorage();
        PascalReader pascalReader = new PascalReader("shipunova_julia_lab_7/files/code.pas");
        pascalReader.readPasFile(commandStorage);

        FASMWriter fasmWriter = new FASMWriter("shipunova_julia_lab_7/files/code.ASM");
        fasmWriter.writeFile(commandStorage);
    }
}

import java.util.Random;

public class TruthTable {
    Random random = new Random();
    int countRows = 16;
    int countColumns = 5;
    private int[][] truthTable = new int[countColumns][countRows];

    public int[][] getTruthTable() {
        return truthTable;
    }

    public void fillTable() {
        for (int i = 0; i < countRows; i++) {
            char[] number = transferToTheBinary(i);
            for (int j = 0; j < countColumns - 1; j++) {
                truthTable[j][i] = number[j] - 48;
            }
            truthTable[4][i] = random.nextInt(2);
        }
    }

    public char[] transferToTheBinary(int number) {
        int finish_system = 2;
        StringBuilder answer = new StringBuilder();
        while (number >= finish_system) {
            answer.append(number % finish_system);
            number /= finish_system;
        }
        answer.append(number);
        answer.reverse();
        while (answer.length() < 4) {
            answer.insert(0, '0');
        }
        return answer.toString().toCharArray();
    }

    public void printTable() {
        System.out.println(" X1 | X2 | X3 | X4 | F");
        for (int i = 0; i < countRows; i++) {
            for (int j = 0; j < countColumns - 1; j++) {
                System.out.print("  " + truthTable[j][i] + " |");
            }
            System.out.print(" " + truthTable[countColumns - 1][i] + "\n");
        }
    }
}

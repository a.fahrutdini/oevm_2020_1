public class NormalForm {
    private String[] variables = {"X1", "X2", "X3", "X4"};

    public void convertToDisjunctiveNormalForm(int[][] truthTable) {
        int countColumns = truthTable.length;
        int countRows = truthTable[0].length;
        boolean plus = false;

        for (int i = 0; i < countRows; ++i) {
            if (truthTable[countColumns - 1][i] == 1) {
                if (plus) {
                    System.out.print(" + ");
                }
                System.out.print("(");
                plus = true;
                for (int j = 0; j < countColumns - 1; ++j) {
                    if (truthTable[j][i] == 0) {
                        System.out.print("!");
                    }
                    System.out.print(variables[j]);
                    if (j < countColumns - 2) {
                        System.out.print(" * ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }

    public void convertToConjunctiveNormalForm(int[][] truthTable) {
        int countColumns = truthTable.length;
        int countRows = truthTable[0].length;
        boolean multiplication = false;
        for (int i = 0; i < countRows; ++i) {
            if (truthTable[countColumns - 1][i] == 0) {
                if (multiplication) {
                    System.out.print(" * ");
                }
                System.out.print("(");
                multiplication = true;
                for (int j = 0; j < countColumns - 1; ++j) {
                    if (truthTable[j][i] == 1) {
                        System.out.print("!");
                    }
                    System.out.print(variables[j]);
                    if (j < countColumns - 2) {
                        System.out.print(" + ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }
}

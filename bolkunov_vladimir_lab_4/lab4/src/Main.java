import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Исходные данные:");
        Minimization minimization = new Minimization();
        System.out.println(minimization.tableToString());
        System.out.println("Выберите форму:");
        System.out.println("1 - ДНФ");
        System.out.println("2 - КНФ");
        System.out.println("0 - Выход");
        int input;
        do {
            input = scanner.nextInt();
        }
        while (input < 0 || input > 2);
        if (input == 1)
            System.out.println(minimization.minimize(true));
        else if (input == 2)
            System.out.println(minimization.minimize(false));
    }
}

package com.company;

public class Conversion {

    static char[] SYMBSS =
                   {'0',
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7',
                    '8',
                    '9',
                    'A',
                    'B',
                    'C',
                    'D',
                    'E',
                    'F'};

    static int HELPASC = 55;


    public static int convertToTen(String startNumber, char[] arrayNumber, int convertedToTenNumber, int startSystem) {
        arrayNumber = startNumber.toCharArray();
        int numberLength = startNumber.length();
        for (int i = numberLength - 1; i >= 0; i--) {
            for (int j = 0; j < 16; j++) {
                if (arrayNumber[i] == SYMBSS[j]) {
                    convertedToTenNumber += j * Math.pow(startSystem, numberLength - i - 1);
                }
            }
        }
        return convertedToTenNumber;
    }


    public static String convert (int convertedTenNumber, int remainder, int finSystem, String symbol, String result) {
        result = "";
        symbol = "";
        remainder = 0;
        do {
            remainder = convertedTenNumber % finSystem;
            symbol = "";

            if (convertedTenNumber % finSystem < 10) {
                symbol = Integer.toString(remainder);
            } else {
                symbol += (char) (remainder + HELPASC);
            }
            result = symbol + result;
            convertedTenNumber /= finSystem;
        } while (convertedTenNumber > 0);
        return result;
    }
}

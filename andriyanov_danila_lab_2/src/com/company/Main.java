package com.company;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        System.out.print("Введите исходную систему счисления: ");
        Scanner start = new Scanner(System.in);
        int startSystem = start.nextInt();

        System.out.print("Введите конечную систему счисления: ");
        Scanner fin = new Scanner(System.in);
        int finSystem = fin.nextInt();

        System.out.print("Введите число для конвертации: ");
        Scanner number = new Scanner(System.in);
        String startNumber = number.nextLine();

        char[] arrayNumber = startNumber.toCharArray();
        String result = "";
        String symbol = "";
        int remainder = 0;
        int convertedToTenNumber = 0;

   convertedToTenNumber = Conversion.convertToTen(startNumber, arrayNumber, convertedToTenNumber, startSystem);
   result = Conversion.convert(convertedToTenNumber, remainder, finSystem, symbol, result);

        System.out.print("Число в конечной системе счисления: " + result);
    }
}

package com.company;
import java.util.*;

public class Main {
    static final int N = 16;
    static final int M = 5;
    public static void main(String[] args) {
        int[][] truthTable = new int [N][M];
        truthTable = HelperClass.outputAndFillTable(truthTable);

        System.out.println();
        System.out.println("Введите 0 для использования КНФ");
        System.out.println("Введите 1 для использования ДНФ");

        Scanner input = new Scanner(System.in);
        int operation = input.nextInt();

        if (operation == 0){
            HelperClass.conjunctiveNormalForm(truthTable);
        }
        else if (operation == 1){
            HelperClass.disjunctiveNormalForm(truthTable);
        }
        else {
            System.out.println("Ошибка");
        }
    }
}
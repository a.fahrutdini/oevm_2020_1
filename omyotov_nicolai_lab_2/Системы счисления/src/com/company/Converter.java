package com.company;

public class Converter {
    private int baseIn;
    private int baseOut;
    private String numIn;
    private String numOut;

    public int getBaseIn(){
        return baseIn;
    }

    public void setBaseIn(int baseIn){
        this.baseIn = baseIn;
    }

    public int getBaseOut(){
        return baseOut;
    }

    public void setBaseOut(int baseOut){
        this.baseOut = baseOut;
    }

    public String getNumIn(){
        return numIn;
    }

    public void setNumIn(String numIn){
        this.numIn = numIn;
    }

    public String getNumOut() {
        return numOut;
    }

    public void convert(){
        int num = 0;
        StringBuilder numOut = new StringBuilder();
        char[] numInCharArray = numIn.toCharArray();
        String nums = "0123456789ABCDEF";
        for (int i = 0; i < numIn.length(); i++){
            num += nums.indexOf(numInCharArray[i]) * Math.pow(baseIn, numIn.length()-i-1);
        }
        if(num == 0){
            this.numOut = "0";
        }
        else {
            while (num != 0) {
                numOut.append(nums.charAt(num % baseOut));
                num /= baseOut;
            }
            this.numOut = numOut.reverse().toString();
        }
    }
}

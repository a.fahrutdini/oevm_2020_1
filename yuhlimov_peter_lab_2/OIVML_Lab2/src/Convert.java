public class Convert {
	// Conversion to decimal system
	public static int toTen(String numb, int NowO) {
		try {
			int degree = 0;
			int r_numb = 0;
			int[] arr_numb = toArrNumbs(numb);
			if (arr_numb == null) {
				System.out.print("");
				return -1;
			}
			for (int i = arr_numb.length - 1; i >= 0; i--) {
				if (arr_numb[i] >= NowO) {
					System.out.print("Invalid data entered");
					return -1;
				}
				int a = (int) Math.pow(NowO, degree);
				int b = arr_numb[i];
				r_numb += a * b;
				degree++;
			}
			return r_numb;
		} catch (Exception ex) {
			System.out.print("Invalid data entered");
			return -1;
		}
	}

	// Conversion from decimal to the specified number system
	public static String toNextO(int numb, int NextO) {
		int safeNumb;
		int[] NextNumb = new int[20];
		int rang = 0;
		
		while (numb > 0) {
			safeNumb = numb / NextO;
			NextNumb[rang] = numb - safeNumb * NextO;
			rang++;
			numb = safeNumb;
		}
		String newNumb = "";
		
		for (int i = rang - 1; i >= 0; i--) {
			if (NextNumb[i] < 10) {
				newNumb += NextNumb[i];
			} else {
				switch (NextNumb[i]) {
				case 10:
					newNumb += "A";
					break;
				case 11:
					newNumb += "B";
					break;
				case 12:
					newNumb += "C";
					break;
				case 13:
					newNumb += "D";
					break;
				case 14:
					newNumb += "E";
					break;
				case 15:
					newNumb += "F";
					break;
				}
			}
		}
		return newNumb;
	}

	// Translating a string representing a number in any number system to an array
	public static int[] toArrNumbs(String numb) {
		int[] arr = new int[numb.length()];
		char[] item = numb.toCharArray();

		for (int i = 0; i < item.length; i++) {
			String safe_str = "" + item[i];
			if (isDigit(safe_str)) {
				arr[i] = Integer.parseInt(safe_str);
			} else {
				switch (safe_str) {
				case "A":
					arr[i] = 10;
					break;
				case "B":
					arr[i] = 11;
					break;
				case "C":
					arr[i] = 12;
					break;
				case "D":
					arr[i] = 13;
					break;
				case "E":
					arr[i] = 14;
					break;
				case "F":
					arr[i] = 15;
					break;
				default:
					return null;
				}
			}
		}
		return arr;
	}

	// Whether the string is a number or not
	private static boolean isDigit(String s) throws NumberFormatException {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}

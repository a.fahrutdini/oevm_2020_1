package com.company;

import java.awt.*;

public class HelperDraw {
    private static final int tableRows = 16;
    private static final int tableColumn = 5;
    private static int x = 10;
    private static int y = 10;
    private static int widthCells = 30;
    private static int heightCells = 30;

    private static void drawTableKNF(int[][] array, Graphics g) {
        for (int i = 0; i < tableRows; i++) {
            if (array[i][tableColumn - 1] == 0) {
                g.setColor(Color.BLUE);
            } else {
                g.setColor(Color.RED);
            }

            for (int j = 0; j < tableColumn; j++) {
                g.drawRect(x + widthCells * j, y + heightCells * i, widthCells, heightCells);
                g.drawString(array[i][j] + "", x + 10 + widthCells * j, y + 20 + heightCells * i);
            }
        }
    }

    private static void drawTableDNF(int[][] array, Graphics g) {
        for (int i = 0; i < tableRows; i++) {
            if (array[i][tableColumn - 1] == 1) {
                g.setColor(Color.BLUE);
            } else {
                g.setColor(Color.RED);
            }

            for (int j = 0; j < tableColumn; j++) {
                g.drawRect(x + widthCells * j, y + heightCells * i, widthCells, heightCells);
                g.drawString(array[i][j] + "", x + 10 + widthCells * j, y + 20 + heightCells * i);
            }
        }
    }

    public static void drawTable(int array[][], String operation, Graphics g) {
        for (int i = 0; i < tableColumn - 1; i++) {
            g.drawRect(x + widthCells * i, y, widthCells, heightCells);
            g.drawString("X" + (i + 1), x * 2 + widthCells * i, y * 3);
        }

        g.drawRect(x + widthCells * (tableColumn - 1), y, widthCells, heightCells);
        g.drawString("F", x + 10 + widthCells * (tableColumn - 1), y + 20);

        y += 30;

        for (int i = 0; i < tableRows; i++) {
            for (int j = 0; j < tableColumn; j++) {
                g.drawRect(x + widthCells * j, y + heightCells * i, widthCells, heightCells);
                g.drawString(array[i][j] + "", x + 10 + widthCells * j, y + 20 + heightCells * i);
            }
        }

        if (FormTable.knf) {
            drawTableKNF(array, g);
        } else if (FormTable.dnf) {
            drawTableDNF(array, g);
        }
        y -= 30;
    }
}

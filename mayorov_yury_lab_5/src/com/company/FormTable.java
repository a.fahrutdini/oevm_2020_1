package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormTable {
    private final JButton button_click_DNF = new JButton("DNF");
    private final JButton button_click_KNF = new JButton("KNF");
    private final JTextArea result = new JTextArea();

    private final int[][] arrayForVisualization = Helper.generateArray();
    private final JPanel spaceForTable = new TableVisualization(arrayForVisualization);

    public static boolean knf;
    public static boolean dnf;

    public FormTable() {
        initialization();
    }

    private void initialization() {
        JFrame _form = new JFrame();
        _form.setTitle("DNF or KNF | Mayorov PIbd-22");
        _form.setBounds(100, 100, 400, 570);
        _form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        _form.getContentPane().setLayout(null);
        _form.setVisible(true);

        spaceForTable.setBounds(0, 0, 180, 525);
        _form.getContentPane().add(spaceForTable);

        result.setBounds(190, 20, 160, 400);
        result.setEditable(false);
        _form.getContentPane().add(result);

        button_click_KNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                button_click_KNF.setEnabled(true);
                button_click_DNF.setEnabled(false);

                knf = true;

                result.setText((Helper.knf(arrayForVisualization)));
                spaceForTable.repaint();
            }
        });
        button_click_KNF.setBounds(190, 450, 160, 30);
        _form.getContentPane().add(button_click_KNF);

        button_click_DNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                button_click_DNF.setEnabled(true);
                button_click_KNF.setEnabled(false);

                dnf = true;

                result.setText((Helper.dnf(arrayForVisualization)));
                spaceForTable.repaint();
            }
        });
        button_click_DNF.setBounds(190, 485, 160, 30);
        _form.getContentPane().add(button_click_DNF);
    }

    public static void main(String[] args) {
        FormTable form = new FormTable();
    }
}

package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);//Ввод числа с клавиатуры
        System.out.println("Введите исходную систему счисления:");
        long notation1 = scanner.nextInt();

        System.out.println("Введите первое число:");
        String number1 = scanner.next(); // Ввод числа в строку

        System.out.println("Введите второе число:");
        String number2 = scanner.next(); // Ввод числа в строку
        int sign = 0; // Знак
        char[] numberChar1 = number1.toCharArray();//Представление числа в качестве массива символов
        char[] numberChar2 = number2.toCharArray();//Представление числа в качестве массива символов
        HelperClass.checkForBugs(notation1, numberChar1);
        HelperClass.checkForBugs(notation1, numberChar2);
        long firstConvertTo10 = HelperClass.convertTo10Base(numberChar1, notation1);
        long secondConvertTo10 = HelperClass.convertTo10Base(numberChar2, notation1);

        if(secondConvertTo10 > firstConvertTo10){ // Проверка на большее число
            long temp = firstConvertTo10;
            firstConvertTo10 = secondConvertTo10;
            secondConvertTo10 = temp;
            sign = 1;
        }

        char[] firstConvertTo2 = (HelperClass.convertToFinalBase(firstConvertTo10)).toCharArray();
        char[] secondConvertTo2 = (HelperClass.convertToFinalBase(secondConvertTo10)).toCharArray();

        System.out.println("Выбирите желаемую арифметическую операцию:" );
        System.out.println("1. + " );
        System.out.println("2. - " );
        System.out.println("3. * " );
        System.out.println("4. / " );
        int operation =  scanner.nextInt();
        if (operation == 1){
            System.out.println(ArithmeticOperations.fold(firstConvertTo2, secondConvertTo2));
        }
        else if(operation == 2){
            if(sign == 1){
                System.out.println('-' + ArithmeticOperations.subtract(firstConvertTo2, secondConvertTo2));
            }
            else {
                System.out.println(ArithmeticOperations.subtract(firstConvertTo2, secondConvertTo2));
            }
        }
        else if(operation == 3){
            System.out.println(ArithmeticOperations.multiply(firstConvertTo2, secondConvertTo2));
        }
        else if(operation == 4){
            System.out.println(ArithmeticOperations.divide(firstConvertTo2, secondConvertTo2));
        }
    }
}

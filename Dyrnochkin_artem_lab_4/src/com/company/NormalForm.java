package com.company;

public class NormalForm {
    private String[] variables = {"X1", "X2", "X3", "X4"};

    public void convertDisjunctiveForm(int[][] Table) {
        int countColumns = Table.length;
        int countRows = Table[0].length;
        boolean plus = false;

        for (int i = 0; i < countRows; ++i) {
            if (Table[countColumns - 1][i] == 1) {
                if (plus) {
                    System.out.print(" + ");
                }
                System.out.print("(");
                plus = true;
                for (int j = 0; j < countColumns - 1; ++j) {
                    if (Table[j][i] == 0) {
                        System.out.print("!");
                    }
                    System.out.print(variables[j]);
                    if (j < countColumns - 2) {
                        System.out.print(" * ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }

    public void convertConjunctiveForm(int[][] Table) {
        int countColumns = Table.length;
        int countRows = Table[0].length;
        boolean multiplications = false;
        for (int i = 0; i < countRows; ++i) {
            if (Table[countColumns - 1][i] == 0) {
                if (multiplications) {
                    System.out.print(" * ");
                }
                System.out.print("(");
                multiplications = true;
                for (int j = 0; j < countColumns - 1; ++j) {
                    if (Table[j][i] == 1) {
                        System.out.print("!");
                    }
                    System.out.print(variables[j]);
                    if (j < countColumns - 2) {
                        System.out.print(" + ");
                    } else {
                        System.out.print(")");
                    }
                }
            }
        }
    }


}

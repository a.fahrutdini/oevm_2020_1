package com.company;

import java.util.Random;

public class Table {

    Random rand = new Random();
    int countRows = 16;
    int countColumns = 5;
    private int[][] Table = new int[countColumns][countRows];

    public int[][] getTable() {
        return Table;
    }

    public void fillTable() {
        for (int i = 0; i < countRows; i++) {
            char[] num = transferBinary(i);
            for (int j = 0; j < countColumns - 1; j++) {
                Table[j][i] = num[j] - 48;
            }
            Table[4][i] = rand.nextInt(2);
        }
    }

    public char[] transferBinary(int number) {
        int finish_system = 2;
        StringBuilder StrBuilder = new StringBuilder();
        while (number >= finish_system) {
            StrBuilder.append(number % finish_system);
            number /= finish_system;
        }
        StrBuilder.append(number);
        StrBuilder.reverse();
        while (StrBuilder.length() < 4) {
            StrBuilder.insert(0, '0');
        }
        return StrBuilder.toString().toCharArray();
    }

    public void printTable() {
        System.out.println(" x1 | x2 | x3 | x4 | F");
        for (int i = 0; i < countRows; i++) {
            for (int j = 0; j < countColumns - 1; j++) {
                System.out.print("  " + Table[j][i] + " |");
            }
            System.out.print(" " + Table[countColumns - 1][i] + "\n");
        }
    }


}

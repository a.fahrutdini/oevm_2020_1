package com.company;

import java.util.Random;

public class Helper {

    public static int[][] generateArray() {
        Random random = new Random();
        int[][] array = {
                {0, 0, 0, 0, random.nextInt(2)},
                {0, 0, 0, 1, random.nextInt(2)},
                {0, 0, 1, 0, random.nextInt(2)},
                {0, 0, 1, 1, random.nextInt(2)},
                {0, 1, 0, 0, random.nextInt(2)},
                {0, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {0, 1, 1, 1, random.nextInt(2)},
                {1, 0, 0, 0, random.nextInt(2)},
                {1, 0, 0, 1, random.nextInt(2)},
                {1, 0, 1, 0, random.nextInt(2)},
                {1, 0, 1, 1, random.nextInt(2)},
                {1, 1, 0, 0, random.nextInt(2)},
                {1, 1, 0, 1, random.nextInt(2)},
                {1, 1, 1, 0, random.nextInt(2)},
                {1, 1, 1, 1, random.nextInt(2)},
        };
        return array;
    }

    public static void knf(int[][] randomArray) {
        String result = new String();
        for (int[] item : randomArray) {
            if (item[4] == 0) {
                if (result.isEmpty()) {
                    result += ((item[0] == 0) ? "(x1 + " : "(!x1 + ");
                } else {
                    result += ((item[0] == 0) ? " * (!x1 + " : " * (!x1 + ");
                }
                result += ((item[1] == 0) ? "x2 + " : " !x2 + ");
                result += ((item[2] == 0) ? "x3 + " : " !x3 + ");
                result += ((item[3] == 0) ? "x4)" : " !x4)");
            }
        }
        System.out.println(result);
    }

    public static void dnf(int[][] randomArray) {
        String result = new String();
        for (int[] item : randomArray) {
            if (item[4] == 1) {
                if (result.isEmpty()) {
                    result += ((item[0] == 0) ? "(!x1 * " : "(x1 * ");
                } else {
                    result += ((item[0] == 0) ? " + (!x1 * " : " + (x1 * ");
                }
                result += ((item[1] == 0) ? "!x2 * " : " x2 * ");
                result += ((item[2] == 0) ? "!x3 * " : " x3 * ");
                result += ((item[3] == 0) ? "!x4)" : " x4)");
            }
        }
        System.out.println(result);
    }
}
public class MathNumeralSystem {
    private static final int maxSymbols = 16;

    private static String parseToAdditionCode(String value) {
        value = removeParentheses(value);
        value = value.replace('0', '.');
        value = value.replace('1', '0');
        value = value.replace('.', '1');
        StringBuilder valueSb = new StringBuilder("1111111111111111");
        valueSb.insert(valueSb.length() - value.length(), value);
        valueSb.delete(maxSymbols, maxSymbols * 2);
        value = valueSb.toString();
        value = augment(value, "1");
        return value;
    }

    private static String deleteExtraZero(String value) {
        if (value.length() >= maxSymbols) {
            value = value.substring(value.length() - maxSymbols);
        }
        if (value.indexOf('1') != -1) {
            value = value.substring(value.indexOf('1'));
        } else {
            value = "0";
        }
        return value;
    }

    private static String parseToDirectCodeFromAddition(String value) {
        value = augment(value, "(-1)");
        value = value.replace('0', '.');
        value = value.replace('1', '0');
        value = value.replace('.', '1');
        value = value.substring(value.indexOf('1'));
        return value;
    }

    public static String substract(String firstValue, String secondValue) {
        if (secondValue.toCharArray()[0] == '(') {
            secondValue = removeParentheses(secondValue);
        } else {
            secondValue = "(-" + secondValue + ")";
        }
        return augment(firstValue, secondValue);
    }

    private static String removeParentheses(String value) {
        value = value.substring(2, value.length() - 1);
        return value;
    }

    public static String multiplicate(String firstValue, String secondValue) {
        boolean firstValueNegative = false;
        boolean secondValueNegative = false;

        if (firstValue.equals("0") || secondValue.equals("0")) {
            return "0";
        }

        if (firstValue.toCharArray()[0] == '(') {
            firstValue = removeParentheses(firstValue);
            firstValueNegative = true;
        }
        if (secondValue.toCharArray()[0] == '(') {
            secondValue = removeParentheses(secondValue);
            secondValueNegative = true;
        }

        char[] secondMultiplier = new StringBuilder(secondValue).reverse().toString().toCharArray();

        String result = "0";
        StringBuilder bitShift = new StringBuilder();
        for (char c : secondMultiplier) {
            if (c == '1') {
                result = augment(result, firstValue + bitShift.toString());
            }
            bitShift.append(0);
        }

        if (!(firstValueNegative && secondValueNegative) && (firstValueNegative || secondValueNegative)) {
            result = "-" + result;
        }
        return result;
    }

    public static String divide(String firstValue, String secondValue) {
        boolean firstValueNegative = false;
        boolean secondValueNegative = false;
        StringBuilder resultSb = new StringBuilder();
        StringBuilder remains = new StringBuilder();
        char[] dividend = firstValue.toCharArray();

        if (firstValue.toCharArray()[0] == '(') {
            firstValue = removeParentheses(firstValue);
            firstValueNegative = true;
        }
        if (secondValue.toCharArray()[0] == '(') {
            secondValue = removeParentheses(secondValue);
            secondValueNegative = true;
        }

        if (secondValue.equals("0")) {
            return "Делить на 0 нельзя.";
        }
        if (firstValue.equals("0") || substract(firstValue, secondValue).toCharArray()[0] == '-') {
            return "0";
        }

        for (int i = 0; i < firstValue.length(); i++) {
            remains.append(dividend[i]);
            if (substract(remains.toString(), secondValue).toCharArray()[0] == '-') {
                resultSb.append(0);
            } else {
                remains = new StringBuilder(substract(remains.toString(), secondValue));
                resultSb.append(1);
            }
        }

        String result = resultSb.toString();
        result = deleteExtraZero(result);
        if (!(firstValueNegative && secondValueNegative) && (firstValueNegative || secondValueNegative)) {
            result = "-" + result;
        }
        return result;
    }

    private static String sumBitByBit(String firstValue, String secondValue) {
        char[] firstValueCharArray = new StringBuilder(firstValue).reverse().toString().toCharArray();
        char[] secondValueCharArray = new StringBuilder(secondValue).reverse().toString().toCharArray();
        StringBuilder result = new StringBuilder();

        int smallestLength = Math.min(firstValueCharArray.length, secondValueCharArray.length);

        boolean discharge = false;

        for (int i = 0; i < smallestLength; i++) {
            if (firstValueCharArray[i] == '0' && secondValueCharArray[i] == '0') {
                if (discharge) {
                    result.append(1);
                    discharge = false;
                } else {
                    result.append(0);
                }
            } else if (firstValueCharArray[i] == '1' && secondValueCharArray[i] == '1') {
                if (discharge) {
                    result.append(1);
                } else {
                    result.append(0);
                    discharge = true;
                }
            } else if (firstValueCharArray[i] == '1' || secondValueCharArray[i] == '1') {
                if (discharge) {
                    result.append(0);
                } else {
                    result.append(1);
                }
            }
        }

        char[] maxArray;
        if (firstValueCharArray.length >= secondValueCharArray.length) {
            maxArray = firstValueCharArray;
        } else {
            maxArray = secondValueCharArray;
        }

        for (int i = smallestLength; i < maxArray.length; i++) {
            if (maxArray[i] == '0') {
                if (discharge) {
                    result.append(1);
                    discharge = false;
                } else {
                    result.append(0);
                }
            } else if (maxArray[i] == '1') {
                if (discharge) {
                    result.append(0);
                } else {
                    result.append(1);
                }
            }
        }
        if (discharge) {
            result.append(1);
        }
        return result.reverse().toString();
    }

    private static String convertToRequiredForm(String result, boolean firstValueNegative, boolean secondValueNegative) {
        if (firstValueNegative && secondValueNegative) {
            result = parseToDirectCodeFromAddition(result);
            result = "-" + deleteExtraZero(result);
        } else if (firstValueNegative || secondValueNegative) {
            if (result.length() == maxSymbols) {
                result = '-' + parseToDirectCodeFromAddition(result);
            } else {
                result = deleteExtraZero(result);
            }
        }
        return result;
    }

    public static String augment(String firstValue, String secondValue) {
        boolean firstValueNegative = false;
        boolean secondValueNegative = false;

        if (firstValue.indexOf('(') == 0) {
            firstValue = parseToAdditionCode(firstValue);
            firstValueNegative = true;
        }
        if (secondValue.indexOf('(') == 0) {
            secondValue = parseToAdditionCode(secondValue);
            secondValueNegative = true;
        }
        String result = sumBitByBit(firstValue, secondValue);
        result = convertToRequiredForm(result, firstValueNegative, secondValueNegative);
        return result;
    }
}

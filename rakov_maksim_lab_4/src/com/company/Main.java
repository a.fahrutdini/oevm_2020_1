import com.company.Helper;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner outcome = new Scanner(System.in);
        Helper H = new Helper();
        System.out.print("Таблица истинности \t\tF \n");
        int[][] array = {
                {0,0,0,0,random.nextInt(2)},
                {0,0,0,1,random.nextInt(2)},
                {0,0,1,0,random.nextInt(2)},
                {0,0,1,1,random.nextInt(2)},
                {0,1,0,0,random.nextInt(2)},
                {0,1,0,1,random.nextInt(2)},
                {0,1,1,0,random.nextInt(2)},
                {0,1,1,1,random.nextInt(2)},
                {1,0,0,0,random.nextInt(2)},
                {1,0,0,1,random.nextInt(2)},
                {1,0,1,0,random.nextInt(2)},
                {1,0,1,1,random.nextInt(2)},
                {1,1,0,0,random.nextInt(2)},
                {1,1,0,1,random.nextInt(2)},
                {1,1,1,0,random.nextInt(2)},
                {1,1,1,1,random.nextInt(2)},
        };
        H.printArray(array);
        System.out.print("ДНФ - ввести 1\t КНФ - ввести 0 \n");
        if (outcome.nextInt() == 1) {
            System.out.print(H.DNF(array));
        } else {
            System.out.print(H.KNF(array));
        }
    }
}

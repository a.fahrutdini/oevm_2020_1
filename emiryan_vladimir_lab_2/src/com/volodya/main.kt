package com.volodya

import java.util.Scanner

fun main() {
    val int = Scanner(System.`in`)
    var a: Int = int.nextInt()
    var b: Int = int.nextInt()
    var sign: Boolean = false
    var inputString: String? = readLine()

    if (inputString!!.get(0) == '-') {
        val regex = Regex("-")
        inputString = regex.replace(inputString, "")
        sign = true
    }

    var newDigit: Long = 0
    var j = 0

    if(a == 10) {
        newDigit = inputString.toLong()
    } else {
        newDigit = toDecimal(inputString, a)
    }

    if (b != 10) {
        var new: String = toX(newDigit, b)
        if (sign) {
            new = "-" + new
        }
        print(new)
    } else {
        if(sign){
            newDigit *= -1
        }
        print(newDigit)
    }
}
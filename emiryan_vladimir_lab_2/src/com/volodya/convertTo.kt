package com.volodya

fun toDecimal(digit: String, n: Int): Long {
    var res: Long = 0
    var j = digit.length - 1
    for(i in 0..digit.length - 1) {
        var tmpD: Int
        if(digit.get(j) in 'A'..'Z') {
            tmpD = digit.get(j) - 'A' + 10
        } else {
            tmpD = digit.get(j).toInt() - 0x30
        }
        res += tmpD * pow(n, i)
        j--
    }
    return res
}

fun toX(dig: Long, n: Int): String {
    var digit: Long = dig
    var res: String = ""
    do {
        var tmpD: Char = ((digit % n) + 0x30).toChar()
        if(10 <= (digit % n) && (digit % n) <= 36) {
            tmpD = (digit % n + 55).toChar()
        }
        res += tmpD
        digit /= n
    } while (digit != 0.toLong())
    res = res.reversed()
    return res
}

fun pow(a: Int, b: Int): Long {
    var res: Long = 1
    for (i in 1..b) {
        res *= a
    }
    return res
}
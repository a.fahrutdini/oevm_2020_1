package com.company;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Window {

    private JFrame frame;
    private JButton btnCreate = new JButton("Create");
    private JButton btnDNF = new JButton("DNF");
    private JButton btnKNF = new JButton("KNF");
    private Functions func = new Functions();
    private Panel MyPanel = new Panel(func);

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Window window = new Window();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Window() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 893, 603);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        MyPanel.setBounds(10, 11, 298, 542);
        frame.getContentPane().add(MyPanel);

        JTextArea outPutFuncArea = new JTextArea();
        outPutFuncArea.setBounds(359, 136, 470, 390);
        frame.getContentPane().add(outPutFuncArea);


        btnCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                func.randomFilling();
                btnDNF.setEnabled(true);
                btnKNF.setEnabled(true);
                outPutFuncArea.setText("");
                MyPanel.repaint();
            }
        });
        btnCreate.setBounds(326, 11, 120, 52);
        frame.getContentPane().add(btnCreate);


        btnDNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String str = new String(func.DNF());
                outPutFuncArea.append(str);
                MyPanel.repaint();
                btnDNF.setEnabled(false);
            }
        });
        btnDNF.setBounds(474, 11, 106, 52);
        frame.getContentPane().add(btnDNF);


        btnKNF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = new String(func.KNF());
                outPutFuncArea.append(str);
                MyPanel.repaint();
                btnKNF.setEnabled(false);
            }
        });
        btnKNF.setBounds(610, 11, 106, 52);
        frame.getContentPane().add(btnKNF);

    }

}
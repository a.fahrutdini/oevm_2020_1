package com.company;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {
    Functions func;

    public Panel(Functions func) {
        this.func = func;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        func.draw(g);
    }
}

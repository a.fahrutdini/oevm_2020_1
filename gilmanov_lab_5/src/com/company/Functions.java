package com.company;

import java.awt.*;
import java.util.Random;

public class Functions {

    private int[][] arrayForm = {{0, 0, 0, 0}, {0, 0, 0, 1}, {0, 0, 1, 0}, {0, 0, 1, 1}, {0, 1, 0, 0},
            {0, 1, 0, 1}, {0, 1, 1, 0}, {0, 1, 1, 1}, {1, 0, 0, 0}, {1, 0, 0, 1}, {1, 0, 1, 0},
            {1, 0, 1, 1}, {1, 1, 0, 0}, {1, 1, 0, 1}, {1, 1, 1, 0}, {1, 1, 1, 1},};

    private int[] arrayRandom = new int[arrayForm.length];

    public void randomFilling() {
        for (int i = 0; i < arrayRandom.length; i++) {
            Random random = new Random();
            arrayRandom[i] = random.nextInt(2);
        }
    }

    public StringBuilder DNF() {
        int count = 0;
        StringBuilder strDNF = new StringBuilder();

        strDNF.append("ДНФ");
        strDNF.append('\n');
        for (int i = 0; i < arrayForm.length; i++) {
            if (arrayRandom[i] == 1) {
                if (count > 0) {
                    strDNF.append(" + ");
                }
                strDNF.append("(");
                for (int j = 0; j < arrayForm[i].length; j++) {
                    if (arrayForm[i][j] == 0) {
                        strDNF.append("!");
                    }
                    strDNF.append("X" + (j + 1));
                    if (j < arrayForm[i].length - 1) {
                        strDNF.append(" * ");
                    }
                }
                strDNF.append(")");
                strDNF.append('\n');
                count++;
            }
        }
        strDNF.append('\n');
        return strDNF;
    }

    public StringBuilder KNF() {
        int count = 0;
        StringBuilder strKNF = new StringBuilder();
        strKNF.append("КНФ");
        strKNF.append('\n');

        for (int i = 0; i < arrayForm.length; i++) {
            if (arrayRandom[i] == 0) {
                if (count > 0) {
                    strKNF.append(" * ");
                }
                strKNF.append("(");
                for (int j = 0; j < arrayForm[i].length; j++) {
                    if (arrayForm[i][j] == 1) {
                        strKNF.append("!");
                    }
                    strKNF.append("X" + (j + 1));
                    if (j < arrayForm[i].length - 1) {
                        strKNF.append(" + ");
                    }
                }
                strKNF.append(")");
                strKNF.append('\n');
                count++;
            }
        }
        strKNF.append('\n');
        return strKNF;
    }

    private int sizeI = 16;
    private int sizeJ = 4;

    private int X = 10;
    private int Y = 10;

    private int width = 30;
    private int height = 30;

    public void draw(Graphics g) {
        for (int i = 0; i <= sizeJ - 1; i++) {
            g.drawRect(X + width * i, Y, width, height);
            g.drawString("X" + (i + 1), X * 2 + width * i, Y * 3);
        }

        g.drawRect(X + width * (sizeJ), Y, width, height);
        g.drawString("F", X + 10 + width * (sizeJ), Y + 20);

        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                g.drawRect(X + width * j, Y + height * i + 30, width, height);
                g.drawString(arrayForm[i][j] + "", X + 10 + width * j, Y + 20 + height * i + 30);
            }
            g.drawRect(X + width * 4, Y + height * i + 30, width, height);
            g.drawString(arrayRandom[i] + "", X + 10 + width * 4, Y + 20 + height * i + 30);
        }
    }
}


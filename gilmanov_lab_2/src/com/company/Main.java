package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Введите исходную СС");

        Scanner cs = new Scanner(System.in);
        int startSS = cs.nextInt();
        System.out.println("Введите конечную СС");
        int endSS = cs.nextInt();
        System.out.println("Введите интерпретируемое число");
        String num = cs.next();

        MethodsOfConverting trans = new MethodsOfConverting(num, startSS, endSS);

        trans.convertingFromStartSSinto10th();

        trans.convertingIntoFinalSS();
    }
}

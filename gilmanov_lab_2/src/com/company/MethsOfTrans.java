package com.company;

class MethodsOfConverting {
    private String inNum;
    private int startSS;
    private int endSS;
    private int num10th = 0;
    private int helper1 = 10;
    private int helper2 = 9;

    public MethodsOfConverting(String num, int startSS, int endSS) {
        inNum = num;
        this.startSS = startSS;
        this.endSS = endSS;
    }

    public void convertingFromStartSSinto10th() {
        char[] convInNum = inNum.toCharArray();
        int cnt = convInNum.length - 1;
        int tmp = 0;
        for (int i = 0; i < convInNum.length; i++) {
            if (convInNum[i] >= 'A' && convInNum[i] < 'A' - helper1 + startSS) {
                tmp = convInNum[i] - 'A' + helper1;
            } else {
                if (convInNum[i] >= '0' && convInNum[i] <= '9') {
                    tmp = Character.getNumericValue(convInNum[i]);
                } else {
                    num10th = -1;
                    return;
                }
            }
            num10th += tmp * Math.pow(startSS, cnt);
            cnt--;
        }
    }

    public void convertingIntoFinalSS() {
        StringBuilder endNum = new StringBuilder();

        while (num10th != 0) {
            if (num10th % endSS > helper2) {
                endNum.append((char) ((num10th % endSS) - helper1 + 'A'));
            }
            if (num10th % endSS <= helper2) {
                endNum.append(num10th % endSS);
            }
            num10th /= endSS;
        }

        endNum.reverse();
        System.out.println(endNum);
    }
}

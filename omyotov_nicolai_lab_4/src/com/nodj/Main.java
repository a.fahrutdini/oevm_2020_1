package com.nodj;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Converter converter = new Converter();
        Scanner sc = new Scanner(System.in);
        Helper helper = new Helper();
        helper.checkNum("Укажите тип функции (ДНФ - 1)(КНФ - 2)", sc, converter);
        System.out.println(converter.toStringMatrix());
        System.out.println(converter.convert());
    }
}

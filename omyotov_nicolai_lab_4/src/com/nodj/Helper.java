package com.nodj;

import java.util.Scanner;

public class Helper {
    void checkNum(String message, Scanner sc, Converter converter) {
        while (true) {
            System.out.println(message);
            try {
                int number = sc.nextInt();
                if (number < 1 || number > 2) {
                    System.out.println("Неверно! Число должно быть введено как 1 или 2");
                } else {
                    converter.setType(number);
                    break;
                }
            } catch (Exception e) {
                System.out.println("Вы ввели не число!");
                sc.next();
            }
        }
    }
}

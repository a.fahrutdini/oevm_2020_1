public class Helper {

    private static final String[] arrayX = {"X1", "X2", "X3", "X4"};

    private static StringBuilder result = new StringBuilder();

    public static void toDisjunctiveForm(int N, int M, int[][] matrix) {

        boolean firstSign = false; //булевая переменная для выставления скобок

        for(int i = 0; i < N; i++) {

            if (matrix[i][M] == 1) {    //если фукция = 1, то строим ДНФ

                if (!firstSign) {   //выставление открывающих скобок
                    result.append("(");
                } else {
                    result.append(" + (");
                }

                for(int j = 0; j < M; j++)
                {
                    if (j == 0) //на проверку первого элемента(перед ним не ставится *)
                    {
                        if (matrix[i][j] == 0)  //если перем-ная = 0, то выводится инверсия переменной
                            result.append("-").append(arrayX[j]);
                        else
                            result.append(arrayX[j]);

                    } else {

                        if (matrix[i][j] == 0)
                            result.append(" * -").append(arrayX[j]);
                        else
                            result.append(" * ").append(arrayX[j]);
                    }
                }

                result.append(")");
                firstSign = true;
            }
        }

        System.out.print(result.toString());
    }
    public static void toConjunctiveForm(int N, int M, int[][] matrix) {

        boolean firstSign = false; //булевая переменная для выставления скобок

        for (int i = 0; i < N; i++) {

            if (matrix[i][M] == 0) {    //если фукция = 0, то строим КНФ

                if (!firstSign) {   //выставление открывающих скобок
                    result.append("(");
                } else {
                    result.append(" * (");
                }

                for (int j = 0; j < M; j++) {
                    if (j == 0) //на проверку первого элемента(перед ним не ставится +)
                    {
                        if (matrix[i][j] == 1)  //если перем-ная = 1, то выводится инверсия переменной
                            result.append("-").append(arrayX[j]);
                        else
                            result.append(arrayX[j]);

                    } else {

                        if (matrix[i][j] == 1)
                            result.append(" + -").append(arrayX[j]);
                        else
                            result.append(" + ").append(arrayX[j]);
                    }
                }

                result.append( ")");
                firstSign = true;
            }
        }

        System.out.print(result.toString());
    }
}

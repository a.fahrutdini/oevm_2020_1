import java.util.Scanner;

public class Main {

    public static final int LENGTH = 4;

    public static final int ROWS = (int) Math.pow(2, LENGTH);

    public static int[][] matrix = new int[ROWS + 1][LENGTH + 1];

    private static void printMatrix() {

        int lastElement = LENGTH;
        int num, countRevers;

        for (int i = 0; i < ROWS; i++) {

           matrix[i][lastElement]=(int)(Math.random()*2);

            for (int j = LENGTH-1; j >= 0; j--) {
                countRevers = LENGTH - 1 - j;

                num = (i / (int) Math.pow(2, j)) % 2;
                matrix[i][countRevers] = num;

                System.out.print( num + " ");
            }

            System.out.print("= " + matrix[i][lastElement] + "\n");
        }
    }

    public static void main(String[] args) {
        printMatrix();

        System.out.printf("\nChoose the operation:\n     1 - DNF\n     2 - KNF\nEnter: ");
        Scanner in = new Scanner(System.in);
        int operation = in.nextInt();

        if (operation == 1) {
            Helper.toDisjunctiveForm(ROWS, LENGTH, matrix);
        } else{
            Helper.toConjunctiveForm(ROWS, LENGTH, matrix);
        }
    }
}

package com.company;

import java.util.ArrayList;
import java.util.regex.*;

public class Parse {

    Assembler assembler;

    private String stringOfFile;
    private String variables;
    private String code;
    private ArrayList<String> arrayVarsString = new ArrayList<>();
    private ArrayList<String> arrayCodeString = new ArrayList<>();
    private ArrayList<String> arrayVars = new ArrayList<>();

    public Parse(String str, Assembler assembler) {

        this.stringOfFile = str;
        this.assembler = assembler;

    }

    public void parseVarAndBody() {

        Pattern patternVariables = Pattern.compile("(?<=var)[\\s\\S]*(?=begin)");
        Matcher matcherVariables = patternVariables.matcher(stringOfFile);

        while (matcherVariables.find()) {
            variables = stringOfFile.substring(matcherVariables.start(), matcherVariables.end());
        }

        Pattern patternCode = Pattern.compile("(?<=begin)[\\s\\S]*(?=end.)");
        Matcher matcherCode = patternCode.matcher(stringOfFile);

        while (matcherCode.find()) {
            code = stringOfFile.substring(matcherCode.start(), matcherCode.end());
        }

        parseVarsStr();
        parseCodeStr();
    }

    public void parseVarsStr() {

        Pattern patternVariables = Pattern.compile("([a-zA-Z]([a-zA-Z0-9_]*[\\s]*,[\\s]*)*)([a-zA-Z][a-zA-Z0-9_]*)[\\s]*:[\\s]*integer[\\s]*;");
        Matcher matcherVariables = patternVariables.matcher(variables);

        while (matcherVariables.find()) {
            arrayVarsString.add(variables.substring(matcherVariables.start(), matcherVariables.end()));
        }

        parseVarsInt();
    }

    public void parseVarsInt() {

        Pattern patternVariables = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcherVariables;
        String var;

        for (String string : arrayVarsString) {

            matcherVariables = patternVariables.matcher(string);

            while (matcherVariables.find()) {
                var = string.substring(matcherVariables.start(), matcherVariables.end());
                if (!var.equals("integer")) { arrayVars.add(var); }
            }
        }

        assembler.addToVariables(arrayVars);
    }

    public void parseCodeStr() {

        Pattern patternCode = Pattern.compile("[a-zA-Z].*;");
        Matcher matcherCode = patternCode.matcher(code);

        while (matcherCode.find()) {
            arrayCodeString.add(code.substring(matcherCode.start(), matcherCode.end()));
        }

        createCode();
    }

    public void createCode() {

        Pattern patternWrite = Pattern.compile("write\\('(.*)'\\);");
        Pattern patternWriteLn = Pattern.compile("writeln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");

        Pattern patternReadLn = Pattern.compile("readln\\(([a-zA-Z][a-zA-Z0-9]*)\\);");
        Pattern patternOperation = Pattern.compile("([a-z][a-zA-Z0-9]*)[\\s]*[\\s]*:=[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*([+\\-*/])[\\s]*([a-z][a-zA-Z0-9]*)[\\s]*;");

        for (String string : arrayCodeString) {

            if(string.matches(patternWrite.toString())){
                Matcher matcherForCode = patternWrite.matcher(string);

                if (matcherForCode.find()) {
                    assembler.addToCodeWrite(matcherForCode.group(1));
                }
            }

            else if(string.matches(patternReadLn.toString())){
                Matcher matcherForCode = patternReadLn.matcher(string);

                if (matcherForCode.find()) {
                    assembler.addToCodeReadLn(matcherForCode.group(1));
                }
            }

            else if(string.matches(patternOperation.toString())){
                Matcher matcherForCode = patternOperation.matcher(string);

                if (matcherForCode.find()) {
                    assembler.addToCodeOperation(matcherForCode.group(1), matcherForCode.group(2), matcherForCode.group(3), matcherForCode.group(4));
                }
            }

            else if(string.matches(patternWriteLn.toString())){
                Matcher matcherForCode = patternWriteLn.matcher(string);

                if (matcherForCode.find()) {
                    assembler.addToCodeWriteLn(matcherForCode.group(1));
                }
            }
        }
    }
}

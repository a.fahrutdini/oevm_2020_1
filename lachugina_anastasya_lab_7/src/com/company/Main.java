package com.company;

public class Main {

    public static void main(String[] args) {

        Pascal pascal = new Pascal();

        Assembler assembler = new Assembler();

        Parse parse = new Parse(pascal.read(), assembler);
        parse.parseVarAndBody();

        assembler.write();
    }
}

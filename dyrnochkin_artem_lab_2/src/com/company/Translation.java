package com.company;

public class Translation {

    int eror = 0;
    int buffer = 0;
    int mistake = 48;
    char letter = 'A';
    char letter_end = 'F';

    public void transfer (int start, String count) {
        char[] array = count.toCharArray();

        for(int i = array.length - 1; i >= 0; i--){
            double pow = Math.pow(start, Math.abs(i - array.length + 1));
            if (Character.isDigit(array[i])){
                if ((int) array[i] - mistake > start) {
                    System.out.println("Данное значение не подходит к системе счисления");
                    eror++;
                    return;
                }
                buffer += ((int) array[i] - mistake) * pow;
            }
            else {
                if(start < 10){
                    System.out.println("Данное значение неверно ");
                    eror++;
                    return;
                }
                if (array[i] >= letter && array[i] <= letter_end) {
                    buffer += (10 + array[i] - letter) * pow;
                }
            }
        }
    }

    public void endTransfer (int finish, StringBuffer StrBuf) {
        while (buffer >= finish) {
            if(buffer % finish >= 10){
                switch (buffer % finish){
                    case 10:
                        StrBuf.append("A");
                        break;
                    case 11:
                        StrBuf.append("B");
                        break;
                    case 12:
                        StrBuf.append("C");
                        break;
                    case 13:
                        StrBuf.append("D");
                        break;
                    case 14:
                        StrBuf.append("E");
                        break;
                    case 15:
                        StrBuf.append("F");
                        break;
                }
            }
            else {
                StrBuf.append(buffer % finish);
            }
            buffer = buffer /  finish;
        }
        if(buffer >= 10) {
            switch (buffer) {
                case 10:
                    StrBuf.append("A");
                    break;
                case 11:
                    StrBuf.append("B");
                    break;
                case 12:
                    StrBuf.append("C");
                    break;
                case 13:
                    StrBuf.append("D");
                    break;
                case 14:
                    StrBuf.append("E");
                    break;
                case 15:
                    StrBuf.append("F");
                    break;
            }
        }
        else {
            StrBuf.append(buffer);
        }
        StrBuf.reverse();
        System.out.println(StrBuf);
    }
}

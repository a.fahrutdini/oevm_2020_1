package com.company;

import java.awt.*;
import java.util.Properties;

public class Drawing {
    Property prop = new Property();

    private final int M = prop.getPropertyInt("M");
    private final int N = prop.getPropertyInt("N");

    private int startX = 10;
    private int startY = 10;
    private int addX = 15;
    private int addY = 25;
    private int width = 35;
    private int height = 35;
    private static String[] variables= {"X1", "X2", "X3", "X4", "F"};

    public void draw(int truthTable[][], Graphics g) {

        for (int i = 0; i < M - 1; i++) {
            g.drawRect(startX + width * i, startY, width, height);
            g.drawString(variables[i], startX * 2 + width * i, startY * 3);
        }

        g.drawRect(startX + width * (M - 1), startY, width, height);
        g.drawString("F", startX + 10 + width * (M - 1), startY + 20);

        startY += height;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                g.drawRect(startX + width * j, startY + height * i, width, height);
                g.drawString(Integer.toString(truthTable[i][j]), startX + addX + width * j, startY + addY + height * i);
            }
        }

        if (Main._KNF) {
            drawKNF(truthTable, g);
        }

        if (Main._DNF) {
            drawDNF(truthTable, g);
        }

        startY -= height;
    }

    private void drawKNF(int truthTable[][], Graphics g) {
        for (int i = 0; i < N; ++i) {
            if (truthTable[i][M - 1] == 0) {
                g.setColor(new Color(Integer.parseInt(prop.getPropertyString("mainColor"))));
            } else {
                g.setColor(new Color(Integer.parseInt(prop.getPropertyString("dopColor"))));
            }

            for (int j = 0; j < M; j++) {
                g.drawRect(startX + width * j, startY + height * i, width, height);
                g.drawString(Integer.toString(truthTable[i][j]), startX + addX + width * j, startY + addY + height * i);
            }
        }
    }

    private void drawDNF(int truthTable[][], Graphics g) {
        for (int i = 0; i < N; ++i) {
            if (truthTable[i][M - 1] == 1) {
                g.setColor(new Color(Integer.parseInt(prop.getPropertyString("mainColor"))));
            } else {
                g.setColor(new Color(Integer.parseInt(prop.getPropertyString("dopColor"))));
            }

            for (int j = 0; j < M; j++) {
                g.drawRect(startX + width * j, startY + height * i, width, height);
                g.drawString(Integer.toString(truthTable[i][j]), startX + addX + width * j, startY + addY + height * i);
            }
        }
    }

    public String convertToDNF(int truthTable[][], int n, int m) {
        boolean printPlus = false;
        StringBuilder strBuilder = new StringBuilder();
        int cnt = 0;

        for (int i = 0; i < n; ++i) {
            if (truthTable[i][m - 1] == 1) {
                // если не первое слагаемое, то ставим плюс
                if (printPlus) {
                    strBuilder.append(" + ");
                }
                strBuilder.append("(");
                printPlus = true;

                for (int j = 0; j < m - 1; ++j) {
                    if (truthTable[i][j] == 0) {
                        strBuilder.append("!");
                    }
                    strBuilder.append(variables[j]);
                    if (j < m - 2) {
                        strBuilder.append(" * ");
                    } else {
                        strBuilder.append(")");
                    }
                }
                if (cnt % 2 == 1) {             // выводим по 2 элемента в строке
                    strBuilder.append("\n");    // и делаем отступ
                }
                cnt++;
            }
        }
        return strBuilder.toString();
    }
    public String convertToKNF(int truthTable[][], int n, int m) {
        boolean printStar = false;
        StringBuilder strBuilder = new StringBuilder();
        int cnt = 0;

        for (int i = 0; i < n; ++i) {
            if (truthTable[i][m - 1] == 0) {
                // если не первый множитель, то ставим *
                if (printStar) {
                    strBuilder.append(" * ");
                }
                strBuilder.append("(");
                printStar = true;

                for (int j = 0; j < m - 1; ++j) {
                    if (truthTable[i][j] == 1) {
                        strBuilder.append("!");
                    }
                    strBuilder.append(variables[j]);
                    if (j < m - 2) {
                        strBuilder.append(" + ");
                    } else {
                        strBuilder.append(")");
                    }
                }
                if (cnt % 2 == 1) {           // выводим по 2 элемента в строке
                    strBuilder.append("\n");  // и делаем отступ
                }
                cnt++;
            }
        }
        return strBuilder.toString();
    }
}

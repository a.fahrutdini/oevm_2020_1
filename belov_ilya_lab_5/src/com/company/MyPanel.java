package com.company;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
    private int truthTable[][];
    Drawing drawing = new Drawing();

    public MyPanel(int truthTable[][]) {
        this.truthTable = truthTable;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        drawing.draw(truthTable, g);
    }
}

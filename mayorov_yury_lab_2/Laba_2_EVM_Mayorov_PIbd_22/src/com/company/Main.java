package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter first number system: ");
        int firstNumberSystem = scanner.nextInt();
        System.out.print("Enter second number system: ");
        int secondNumberSystem = scanner.nextInt();
        System.out.print("Enter the digit to transfer: ");
        char[] digit = scanner.next().toCharArray();

        System.out.println("\n");
        System.out.println("Result of translation from " + firstNumberSystem + " in " + secondNumberSystem + " number system: " + convertingToOtherSystem(firstNumberSystem, secondNumberSystem, digit));
    }

    public static String convertingToOtherSystem(int firstNumberSystem, int secondNumberSystem, char[] digit) {
        /*
        firstHelperConvertToChar используется для перевода в int-аналогичный код char;

        secondHelperConvertToChar используется так же, как и firstHelperConvertToChar,
        но в случае, если на входе буква;
        */

        StringBuilder sb = new StringBuilder();

        int firstHelperConvertToChar = 48;
        int secondHelperConvertToChar = 55;

        int remains = convertingToDecimal(firstNumberSystem, digit);

        if (remains == 0) {
            sb.append("0");
            return sb.toString();
        }

        for (int i = 0; remains > 0; i++) {
            if (remains % secondNumberSystem <= 9) {
                sb.append((char) (remains % secondNumberSystem + firstHelperConvertToChar));
            } else {
                sb.append((char) (remains % secondNumberSystem + secondHelperConvertToChar));
            }
            remains = remains / secondNumberSystem;
        }
        return sb.reverse().toString();
    }

    public static int convertingToDecimal(int firstNumberSystem, char[] digit) {
        int result = 0;
        for (int i = 0; i < digit.length; i++) {
            int tmp;
            if (digit[i] >= '0' && digit[i] <= '9') {
                tmp = digit[i] - '0';
            } else {
                tmp = 10 + digit[i] - 'A';
            }
            result += tmp * Math.pow(firstNumberSystem, digit.length - i - 1);
        }
        return result;
    }
}
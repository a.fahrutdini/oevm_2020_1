import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();
        Scanner in = new Scanner(System.in);
        Methods M = new Methods();

        int sizeJ = 5;
        int sizeI = 16;

        int[][] array = {
                {0,0,0,0,random.nextInt(2)},
                {0,0,0,1,random.nextInt(2)},
                {0,0,1,0,random.nextInt(2)},
                {0,0,1,1,random.nextInt(2)},
                {0,1,0,0,random.nextInt(2)},
                {0,1,0,1,random.nextInt(2)},
                {0,1,1,0,random.nextInt(2)},
                {0,1,1,1,random.nextInt(2)},
                {1,0,0,0,random.nextInt(2)},
                {1,0,0,1,random.nextInt(2)},
                {1,0,1,0,random.nextInt(2)},
                {1,0,1,1,random.nextInt(2)},
                {1,1,0,0,random.nextInt(2)},
                {1,1,0,1,random.nextInt(2)},
                {1,1,1,0,random.nextInt(2)},
                {1,1,1,1,random.nextInt(2)},
        };
        M.printArray(array,sizeI,sizeJ);
        System.out.println("Для вывода ДНФ нажмите 1, для вывода КНФ нажмите 0");
        switch (in.nextInt()){
            case 1:
                System.out.print(M.DNF(array,sizeI,sizeJ));
                break;
            case 0:
                System.out.print(M.KNF(array,sizeI,sizeJ));
                break;
            default:
                System.out.print("Неизвестная команда");
                break;
        }
    }
}
package com.company;

public class Helper {
    public static long convertToDecimal (char[] number, int startSystem) {
        long number10 = 0;
        int finalIntTen = 10;
        int codeA = 65;
        for (int i = 0; i < number.length; ++i) {
            int сurDigit = 1;
            if (number[i] >= '0' && number[i] <= '9') {
                сurDigit = number[i] - '0';
            } else if (Character.isLetter(number[i])) {
                сurDigit = finalIntTen + number[i] - codeA;
            }

            number10 = (long)((double)number10 + (double)сurDigit * Math.pow((double)startSystem, (double)(number.length - i - 1)));
        }

        return number10;
    }

    public static String convertToBinary (long number10) {
        String result = "";
        long remainder = 1;
        int binaryNotation = 2;

        for (String сurChar = ""; number10 > 0; number10 /= (long)binaryNotation) {
            remainder = number10 % (long)binaryNotation;
            сurChar = "";
            if (number10 % (long)binaryNotation < 10) {
                сurChar = Long.toString(remainder);
            } else {
                сurChar = сurChar + (char)((int)('A' + remainder - 10));
            }

            result = сurChar + result;
        }

        return result;
    }
}

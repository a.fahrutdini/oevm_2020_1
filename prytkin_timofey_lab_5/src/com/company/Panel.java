package com.company;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class Panel extends JPanel {

    private TruthTable truthTable;

    public Panel(TruthTable truthTable){
        this.truthTable = truthTable;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        truthTable.draw(g);
    }
}

package com.company;

import java.awt.*;
import java.io.IOException;
import java.util.HashMap;

public class TruthTable {

    private int countOfRows;
    private int countOfColumns;
    private int heightAndWidthOfCells;
    private int distanceFromTop;
    private int distanceFromLeft;
    private int distanceFromBottomOfCell;
    private int distanceFromLeftOfCell;
    private int fontSize;

    private boolean isDNF = false;
    private boolean isKNF = false;
    private boolean isEndOfSteps = false;
    private int step;
    public static int[][] truthTable;

    public TruthTable() throws IOException {
        ConfigReader configReader = new ConfigReader();

        HashMap<String, Integer> config = configReader.getConfig();

        countOfRows = config.get("countOfRows");
        countOfColumns = config.get("countOfColumns");
        heightAndWidthOfCells = config.get("heightAndWidthOfCells");
        distanceFromTop = config.get("distanceFromTop");
        distanceFromLeft = config.get("distanceFromLeft");
        distanceFromBottomOfCell = config.get("distanceFromBottomOfCell");
        distanceFromLeftOfCell = config.get("distanceFromLeftOfCell");
        fontSize = config.get("fontSize");

        truthTable = new int[countOfRows][countOfColumns];

        fillTruthTable();
    }

    /**
     * Этот метод выберет ДНФ
     */
    public void chooseDNF() {
        isDNF = true;
        step = 1;
    }

    /**
     * Этот метод выберет КНФ
     */
    public void chooseKNF() {
        isKNF = true;
        step = 1;
    }

    /**
     * Этот метод сбросит все настройки касаемо шагов
     */
    public void reset(){
        step = 1;
        isEndOfSteps = false;
        isDNF = false;
        isKNF = false;
    }

    /**
     * Этот метод вернет статус закончены ли шаги к построению нормальной формы или нет
     */
    public boolean getEndOfStepStatus() { return isEndOfSteps; }

    /**
     * Этот метод вернет число в двоичной системе счисления дополненный спереди нулями до 4-разрядного
     */
    public void fillTruthTable() {
        for (int i = 0; i < countOfRows; ++i) {
            char[] iterativeString = Converter.convertToBinary(i).toCharArray();
            for (int j = 0; j < countOfColumns - 1; ++j) { truthTable[i][j] = iterativeString[j] - '0'; }
            truthTable[i][countOfColumns - 1] = (int) (Math.random() * 2);
        }
    }

    /**
     * Этот метод произведет отрисовку таблицы истинности и текущий шаг
     * @param g Полотно
     */
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        for (int i = 0; i < countOfRows; i++) {
            for (int j = 0; j < countOfColumns; j++) {
                g2.drawRect(distanceFromLeft + heightAndWidthOfCells * j, distanceFromTop + heightAndWidthOfCells * i, heightAndWidthOfCells, heightAndWidthOfCells);
                if (i == 0) {
                    g2.setFont(new Font("TimesRoman", Font.BOLD, fontSize));
                    if (j != countOfColumns - 1) {
                        g2.drawString("X" + (j + 1), distanceFromLeft + heightAndWidthOfCells * j + distanceFromLeftOfCell, distanceFromTop + heightAndWidthOfCells * (i + 1) - distanceFromBottomOfCell);
                    }
                    else {
                        g2.drawString("F", distanceFromLeft + heightAndWidthOfCells * j + distanceFromLeftOfCell, distanceFromTop + heightAndWidthOfCells * (i + 1) - distanceFromBottomOfCell);
                    }
                }
                else {
                    if (j != countOfColumns - 1) {
                        g2.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));
                    }
                    else {
                        g2.setFont(new Font("TimesRoman", Font.BOLD, fontSize));
                    }
                    g2.drawString(truthTable[i][j] + "", distanceFromLeft + heightAndWidthOfCells * j + distanceFromLeftOfCell, distanceFromTop + heightAndWidthOfCells * (i + 1) - distanceFromBottomOfCell);
                }
            }
        }

        if(isDNF || isKNF){
            g2.setStroke(new BasicStroke(5));
            NormalForm normalForm = new NormalForm(countOfRows, countOfColumns);

            if (isDNF) { g2.setColor(Color.BLUE); }
            else { g2.setColor(Color.RED); }

            if(step > 0){
                g2.drawRect(distanceFromLeft, distanceFromTop + heightAndWidthOfCells * step , heightAndWidthOfCells * countOfColumns, heightAndWidthOfCells);
                if (isDNF) { normalForm.drawDisjunctiveNormalForm(truthTable, step + 1, g); }
                else { normalForm.drawConjunctiveNormalForm(truthTable, step + 1, g); }
            }
            step++;
            if(step > countOfRows - 1) {
                isDNF = false;
                isKNF = false;
                isEndOfSteps = true;
            }
        }
    }
}

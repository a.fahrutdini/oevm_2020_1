package com.company;

public class HelperClass {
    static final char[] BASES = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    static final int OFFSET = 55;//'A' - 10
    public static char[] toUpper(String finNumberStr, char[] finNumberCh) {
        for (int i = 0; i < finNumberStr.length(); i++) {
            if (finNumberCh[i] >= 'a' && finNumberCh[i] <= 'z') {
                finNumberCh[i] -= ' ';
            }
        }
        return finNumberCh;
    }
    public static int convertTo10Base(String finNumberStr,char[] finNumberCh, int finNumberInt, int initialSystem) {
        for (int i = finNumberStr.length() - 1; i >= 0; i--) {
            for (int j = 0; j < 16; j++) {
                if (finNumberCh[i] == BASES[j]) {
                    finNumberInt += j * Math.pow(initialSystem, finNumberStr.length() - i - 1);
                }
            }
        }
        return finNumberInt;
    }

    public static String convert(int finNumberInt, int ost, int finSystem, String symbol, String result) {
        while (finNumberInt > 0) {
            ost = finNumberInt % finSystem;
            symbol = "";

            if (finNumberInt % finSystem < 10) {
                symbol = Integer.toString(ost);
            } else {
                symbol += (char) (ost + OFFSET);
            }
            result = symbol + result;
            finNumberInt /= finSystem;
        }
        return result;
    }
}

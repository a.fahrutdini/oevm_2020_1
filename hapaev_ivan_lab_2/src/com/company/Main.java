package com.company;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.print("Введите исходную систему счисления: ");
        Scanner initial = new Scanner(System.in);
        int initialSystem = initial.nextInt();

        System.out.print("Введите конечную систему счисления: ");
        Scanner fin = new Scanner(System.in);
        int finSystem = fin.nextInt();

        System.out.print("Введите число для конвертации: ");
        Scanner number = new Scanner(System.in);
        String finNumberStr = number.nextLine();

        char[] finNumberCh = finNumberStr.toCharArray();
        String result = "";
        String symbol = "";
        int ost = 0;
        int finNumberInt = 0;

        finNumberCh = HelperClass.toUpper(finNumberStr, finNumberCh);

        finNumberInt = HelperClass.convertTo10Base(finNumberStr, finNumberCh, finNumberInt, initialSystem);

        result = HelperClass.convert(finNumberInt, ost, finSystem, symbol, result);

        System.out.print("Число в конечной системе счисления: " + result);
    }
}